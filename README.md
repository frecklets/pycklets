[![PyPI status](https://img.shields.io/pypi/status/pycklets.svg)](https://pypi.python.org/pypi/pycklets/)
[![PyPI version](https://img.shields.io/pypi/v/pycklets.svg)](https://pypi.python.org/pypi/pycklets/)
[![PyPI pyversions](https://img.shields.io/pypi/pyversions/pycklets.svg)](https://pypi.python.org/pypi/pycklets/)
[![Pipeline status](https://gitlab.com/frkl/pycklets/badges/develop/pipeline.svg)](https://gitlab.com/frkl/pycklets/pipelines)
[![Code style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

# pycklets

*No description available (yet).*


## Description

Documentation still to be done.

# Development

Assuming you use [pyenv](https://github.com/pyenv/pyenv) and [pyenv-virtualenv](https://github.com/pyenv/pyenv-virtualenv) for development, here's how to setup a 'pycklets' development environment manually:

    pyenv install 3.7.3
    pyenv virtualenv 3.7.3 pycklets
    git clone https://gitlab.com/frecklets/pycklets
    cd <pycklets_dir>
    pyenv local pycklets
    pip install -e .[develop,testing,docs]
    pre-commit install


## Copyright & license

Please check the [LICENSE](/LICENSE) file in this repository (it's a short license!), also check out the [*freckles* license page](https://freckles.io/license) for more details.

[Parity Public License 6.0.0](https://licensezero.com/licenses/parity)

[Copyright (c) 2019 frkl OÜ](https://frkl.io)
