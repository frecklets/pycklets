# -*- coding: utf-8 -*-


#
# module path: pycklets.register_path_details.RegisterPathDetails
#


from pyckles import AutoPycklet


class RegisterPathDetails(AutoPycklet):
    """If the path in question can not be read by the user who executes this frecklet, the 'become' argument needs to be set to 'true'.

       Args:
         become: Whether to use elevated privileges to check the path.
         path: The path to check.
         register_var: The name of the variable to register.

    """

    FRECKLET_ID = "register-path-details"

    def __init__(self, become=None, path=None, register_var=None):

        super(RegisterPathDetails, self).__init__(
            var_names=["become", "path", "register_var"]
        )
        self._become = become
        self._path = path
        self._register_var = register_var

    @property
    def become(self):
        return self._become

    @become.setter
    def become(self, become):
        self._become = become

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path

    @property
    def register_var(self):
        return self._register_var

    @register_var.setter
    def register_var(self, register_var):
        self._register_var = register_var


frecklet_class = RegisterPathDetails
