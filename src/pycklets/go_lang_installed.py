# -*- coding: utf-8 -*-


#
# module path: pycklets.go_lang_installed.GoLangInstalled
#


from pyckles import AutoPycklet


class GoLangInstalled(AutoPycklet):
    """Installs the Go programming language.

       Args:
         go_version: The version of go.

    """

    FRECKLET_ID = "go-lang-installed"

    def __init__(self, go_version=None):

        super(GoLangInstalled, self).__init__(var_names=["go_version"])
        self._go_version = go_version

    @property
    def go_version(self):
        return self._go_version

    @go_version.setter
    def go_version(self, go_version):
        self._go_version = go_version


frecklet_class = GoLangInstalled
