# -*- coding: utf-8 -*-


#
# module path: pycklets.folder_stowed.FolderStowed
#


from pyckles import AutoPycklet


class FolderStowed(AutoPycklet):
    """Stow a folder.

     This uses the [stow](https://www.gnu.org/software/stow/) application to symbolically link a folder (recursively).

     If ``target_owner`` is set, the destination folder will be changed to be owned by this user (and created with root permissions if
     it doesn't exist yet), and stow will be executed as that user.

     This does not install 'stow', use the 'stow-installed' frecklet beforehand if necessary.

       Args:
         become: Whether to stow using root permissions.
         dirname: The name of the directory to stow.
         src: The (parent) source directory to stow from.
         target: The (parent) target directory to stow into.
         target_owner: The user to who owns the destination folder.

    """

    FRECKLET_ID = "folder-stowed"

    def __init__(
        self, become=None, dirname=None, src=None, target=None, target_owner=None
    ):

        super(FolderStowed, self).__init__(
            var_names=["become", "dirname", "src", "target", "target_owner"]
        )
        self._become = become
        self._dirname = dirname
        self._src = src
        self._target = target
        self._target_owner = target_owner

    @property
    def become(self):
        return self._become

    @become.setter
    def become(self, become):
        self._become = become

    @property
    def dirname(self):
        return self._dirname

    @dirname.setter
    def dirname(self, dirname):
        self._dirname = dirname

    @property
    def src(self):
        return self._src

    @src.setter
    def src(self, src):
        self._src = src

    @property
    def target(self):
        return self._target

    @target.setter
    def target(self, target):
        self._target = target

    @property
    def target_owner(self):
        return self._target_owner

    @target_owner.setter
    def target_owner(self, target_owner):
        self._target_owner = target_owner


frecklet_class = FolderStowed
