# -*- coding: utf-8 -*-


#
# module path: pycklets.keycloak_postgresql_helper_script_file.KeycloakPostgresqlHelperScriptFile
#


from pyckles import AutoPycklet


class KeycloakPostgresqlHelperScriptFile(AutoPycklet):
    """No documentation available.

       Args:
         group: The group of the file.
         keycloak_db_name: The name of the keycloak db.
         keycloak_db_password: The password for the keycloak db.
         keycloak_db_user: The user for the keycloak db.
         mode: The permissions of the file.
         owner: The owner of the file.
         path: The path to the file.
         postgresql_driver_jar: The location to the postgres driver jar.

    """

    FRECKLET_ID = "keycloak-postgresql-helper-script-file"

    def __init__(
        self,
        group=None,
        keycloak_db_name="keycloak",
        keycloak_db_password="keycloak",
        keycloak_db_user="keycloak",
        mode=None,
        owner=None,
        path=None,
        postgresql_driver_jar=None,
    ):

        super(KeycloakPostgresqlHelperScriptFile, self).__init__(
            var_names=[
                "group",
                "keycloak_db_name",
                "keycloak_db_password",
                "keycloak_db_user",
                "mode",
                "owner",
                "path",
                "postgresql_driver_jar",
            ]
        )
        self._group = group
        self._keycloak_db_name = keycloak_db_name
        self._keycloak_db_password = keycloak_db_password
        self._keycloak_db_user = keycloak_db_user
        self._mode = mode
        self._owner = owner
        self._path = path
        self._postgresql_driver_jar = postgresql_driver_jar

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def keycloak_db_name(self):
        return self._keycloak_db_name

    @keycloak_db_name.setter
    def keycloak_db_name(self, keycloak_db_name):
        self._keycloak_db_name = keycloak_db_name

    @property
    def keycloak_db_password(self):
        return self._keycloak_db_password

    @keycloak_db_password.setter
    def keycloak_db_password(self, keycloak_db_password):
        self._keycloak_db_password = keycloak_db_password

    @property
    def keycloak_db_user(self):
        return self._keycloak_db_user

    @keycloak_db_user.setter
    def keycloak_db_user(self, keycloak_db_user):
        self._keycloak_db_user = keycloak_db_user

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, mode):
        self._mode = mode

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path

    @property
    def postgresql_driver_jar(self):
        return self._postgresql_driver_jar

    @postgresql_driver_jar.setter
    def postgresql_driver_jar(self, postgresql_driver_jar):
        self._postgresql_driver_jar = postgresql_driver_jar


frecklet_class = KeycloakPostgresqlHelperScriptFile
