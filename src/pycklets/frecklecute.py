# -*- coding: utf-8 -*-


#
# module path: pycklets.frecklecute.Frecklecute
#


from pyckles import AutoPycklet


class Frecklecute(AutoPycklet):
    """Execute a frecklet within another frecklet.

     This is useful, for example, to do some basic installation
     tasks on a freshly installed machine, harden it, add an admin user. After this, 'normal' frecklet tasks can be run.

       Args:
         become_pass: The password to gain elevated privileges on the password.
         elevated: Whether this frecklet needs elevated permissions.
         frecklet: The name of the frecklet.
         login_pass: The login/ssh password of the user when connecting to the target.
         target: The target string for this run.
         vars: The parameters for this frecklet.

    """

    FRECKLET_ID = "frecklecute"

    def __init__(
        self,
        become_pass=None,
        elevated=None,
        frecklet=None,
        login_pass=None,
        target="localhost",
        vars=None,
    ):

        super(Frecklecute, self).__init__(
            var_names=[
                "become_pass",
                "elevated",
                "frecklet",
                "login_pass",
                "target",
                "vars",
            ]
        )
        self._become_pass = become_pass
        self._elevated = elevated
        self._frecklet = frecklet
        self._login_pass = login_pass
        self._target = target
        self._vars = vars

    @property
    def become_pass(self):
        return self._become_pass

    @become_pass.setter
    def become_pass(self, become_pass):
        self._become_pass = become_pass

    @property
    def elevated(self):
        return self._elevated

    @elevated.setter
    def elevated(self, elevated):
        self._elevated = elevated

    @property
    def frecklet(self):
        return self._frecklet

    @frecklet.setter
    def frecklet(self, frecklet):
        self._frecklet = frecklet

    @property
    def login_pass(self):
        return self._login_pass

    @login_pass.setter
    def login_pass(self, login_pass):
        self._login_pass = login_pass

    @property
    def target(self):
        return self._target

    @target.setter
    def target(self, target):
        self._target = target

    @property
    def vars(self):
        return self._vars

    @vars.setter
    def vars(self, vars):
        self._vars = vars


frecklet_class = Frecklecute
