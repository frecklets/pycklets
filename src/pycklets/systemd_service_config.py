# -*- coding: utf-8 -*-


#
# module path: pycklets.systemd_service_config.SystemdServiceConfig
#


from pyckles import AutoPycklet


class SystemdServiceConfig(AutoPycklet):
    """Configuration file for environment variables to configure a systemd service.

       Args:
         env_vars: n/a
         service_name: The name of the service.

    """

    FRECKLET_ID = "systemd-service-config"

    def __init__(self, env_vars=None, service_name=None):

        super(SystemdServiceConfig, self).__init__(
            var_names=["env_vars", "service_name"]
        )
        self._env_vars = env_vars
        self._service_name = service_name

    @property
    def env_vars(self):
        return self._env_vars

    @env_vars.setter
    def env_vars(self, env_vars):
        self._env_vars = env_vars

    @property
    def service_name(self):
        return self._service_name

    @service_name.setter
    def service_name(self, service_name):
        self._service_name = service_name


frecklet_class = SystemdServiceConfig
