# -*- coding: utf-8 -*-


#
# module path: pycklets.ec2_security_group_exists.Ec2SecurityGroupExists
#


from pyckles import AutoPycklet


class Ec2SecurityGroupExists(AutoPycklet):
    """Create an instance on Amazon EC2, if it doesn't exist yet.

       Args:
         aws_access_key: The AWS access key.
         aws_secret_key: The AWS secret key.
         description: Description of the security group.
         group_name: The name of the security group.
         region: The AWS region to use.
         register: The name of the variable to register the result of this frecklet.
         vpc_id: ID of the VPC to create the group in.

    """

    FRECKLET_ID = "ec2-security-group-exists"

    def __init__(
        self,
        aws_access_key=None,
        aws_secret_key=None,
        description=None,
        group_name=None,
        region=None,
        register=None,
        vpc_id=None,
    ):

        super(Ec2SecurityGroupExists, self).__init__(
            var_names=[
                "aws_access_key",
                "aws_secret_key",
                "description",
                "group_name",
                "region",
                "register",
                "vpc_id",
            ]
        )
        self._aws_access_key = aws_access_key
        self._aws_secret_key = aws_secret_key
        self._description = description
        self._group_name = group_name
        self._region = region
        self._register = register
        self._vpc_id = vpc_id

    @property
    def aws_access_key(self):
        return self._aws_access_key

    @aws_access_key.setter
    def aws_access_key(self, aws_access_key):
        self._aws_access_key = aws_access_key

    @property
    def aws_secret_key(self):
        return self._aws_secret_key

    @aws_secret_key.setter
    def aws_secret_key(self, aws_secret_key):
        self._aws_secret_key = aws_secret_key

    @property
    def description(self):
        return self._description

    @description.setter
    def description(self, description):
        self._description = description

    @property
    def group_name(self):
        return self._group_name

    @group_name.setter
    def group_name(self, group_name):
        self._group_name = group_name

    @property
    def region(self):
        return self._region

    @region.setter
    def region(self, region):
        self._region = region

    @property
    def register(self):
        return self._register

    @register.setter
    def register(self, register):
        self._register = register

    @property
    def vpc_id(self):
        return self._vpc_id

    @vpc_id.setter
    def vpc_id(self, vpc_id):
        self._vpc_id = vpc_id


frecklet_class = Ec2SecurityGroupExists
