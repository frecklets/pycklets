# -*- coding: utf-8 -*-


#
# module path: pycklets.user_is_member_of_group.UserIsMemberOfGroup
#


from pyckles import AutoPycklet


class UserIsMemberOfGroup(AutoPycklet):
    """Ensures a user is member of a group.

       Args:
         group: The name of the group.
         user: The name of the user.

    """

    FRECKLET_ID = "user-is-member-of-group"

    def __init__(self, group=None, user=None):

        super(UserIsMemberOfGroup, self).__init__(var_names=["group", "user"])
        self._group = group
        self._user = user

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user


frecklet_class = UserIsMemberOfGroup
