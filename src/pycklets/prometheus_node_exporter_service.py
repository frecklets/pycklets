# -*- coding: utf-8 -*-


#
# module path: pycklets.prometheus_node_exporter_service.PrometheusNodeExporterService
#


from pyckles import AutoPycklet


class PrometheusNodeExporterService(AutoPycklet):
    """Installs the Prometheus node exporter.

       Args:
         disabled_collectors: List of disabled collectors.
         enabled_collectors: List of additionally enabled collectors.
         textfile_dir: Directory used by the Textfile Collector.
         version: The version of the node exporter.
         web_listen_address: Address on which the node exporter will listen.

    """

    FRECKLET_ID = "prometheus-node-exporter-service"

    def __init__(
        self,
        disabled_collectors=None,
        enabled_collectors=None,
        textfile_dir=None,
        version=None,
        web_listen_address=None,
    ):

        super(PrometheusNodeExporterService, self).__init__(
            var_names=[
                "disabled_collectors",
                "enabled_collectors",
                "textfile_dir",
                "version",
                "web_listen_address",
            ]
        )
        self._disabled_collectors = disabled_collectors
        self._enabled_collectors = enabled_collectors
        self._textfile_dir = textfile_dir
        self._version = version
        self._web_listen_address = web_listen_address

    @property
    def disabled_collectors(self):
        return self._disabled_collectors

    @disabled_collectors.setter
    def disabled_collectors(self, disabled_collectors):
        self._disabled_collectors = disabled_collectors

    @property
    def enabled_collectors(self):
        return self._enabled_collectors

    @enabled_collectors.setter
    def enabled_collectors(self, enabled_collectors):
        self._enabled_collectors = enabled_collectors

    @property
    def textfile_dir(self):
        return self._textfile_dir

    @textfile_dir.setter
    def textfile_dir(self, textfile_dir):
        self._textfile_dir = textfile_dir

    @property
    def version(self):
        return self._version

    @version.setter
    def version(self, version):
        self._version = version

    @property
    def web_listen_address(self):
        return self._web_listen_address

    @web_listen_address.setter
    def web_listen_address(self, web_listen_address):
        self._web_listen_address = web_listen_address


frecklet_class = PrometheusNodeExporterService
