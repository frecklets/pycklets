# -*- coding: utf-8 -*-


#
# module path: pycklets.init_service_enabled.InitServiceEnabled
#


from pyckles import AutoPycklet


class InitServiceEnabled(AutoPycklet):
    """Make sure an init service is enabled.

       Args:
         name: The name of the service.

    """

    FRECKLET_ID = "init-service-enabled"

    def __init__(self, name=None):

        super(InitServiceEnabled, self).__init__(var_names=["name"])
        self._name = name

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name


frecklet_class = InitServiceEnabled
