# -*- coding: utf-8 -*-


#
# module path: pycklets.freckles_dev_sources_synced.FrecklesDevSourcesSynced
#


from pyckles import AutoPycklet


class FrecklesDevSourcesSynced(AutoPycklet):
    """This is mainly used as a helper frecklet for 'freckles-dev-project'.

       Args:
         project_base: The base folder for the project sources (needs to be absolute!).

    """

    FRECKLET_ID = "freckles-dev-sources-synced"

    def __init__(self, project_base=None):

        super(FrecklesDevSourcesSynced, self).__init__(var_names=["project_base"])
        self._project_base = project_base

    @property
    def project_base(self):
        return self._project_base

    @project_base.setter
    def project_base(self, project_base):
        self._project_base = project_base


frecklet_class = FrecklesDevSourcesSynced
