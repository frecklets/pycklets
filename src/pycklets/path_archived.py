# -*- coding: utf-8 -*-


#
# module path: pycklets.path_archived.PathArchived
#


from pyckles import AutoPycklet


class PathArchived(AutoPycklet):
    """Archives a file or folder.

       Args:
         become: Whether to use root permissions to archive the path.
         dest: The file name of the destination archive.
         exclude_path: Path or glob to exclude from the archive.
         format: The type of compression to use.
         group: The group of the archive file.
         mode: The permissions of the archive file.
         owner: The owner of the archive file.
         parent_dir_mode: The permissions of the archive parent directory.
         path: Remote absolute path or glob of the file or files to compress or archive.

    """

    FRECKLET_ID = "path-archived"

    def __init__(
        self,
        become=None,
        dest=None,
        exclude_path=None,
        format="gz",
        group=None,
        mode=None,
        owner=None,
        parent_dir_mode=None,
        path=None,
    ):

        super(PathArchived, self).__init__(
            var_names=[
                "become",
                "dest",
                "exclude_path",
                "format",
                "group",
                "mode",
                "owner",
                "parent_dir_mode",
                "path",
            ]
        )
        self._become = become
        self._dest = dest
        self._exclude_path = exclude_path
        self._format = format
        self._group = group
        self._mode = mode
        self._owner = owner
        self._parent_dir_mode = parent_dir_mode
        self._path = path

    @property
    def become(self):
        return self._become

    @become.setter
    def become(self, become):
        self._become = become

    @property
    def dest(self):
        return self._dest

    @dest.setter
    def dest(self, dest):
        self._dest = dest

    @property
    def exclude_path(self):
        return self._exclude_path

    @exclude_path.setter
    def exclude_path(self, exclude_path):
        self._exclude_path = exclude_path

    @property
    def format(self):
        return self._format

    @format.setter
    def format(self, format):
        self._format = format

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, mode):
        self._mode = mode

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    @property
    def parent_dir_mode(self):
        return self._parent_dir_mode

    @parent_dir_mode.setter
    def parent_dir_mode(self, parent_dir_mode):
        self._parent_dir_mode = parent_dir_mode

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path


frecklet_class = PathArchived
