# -*- coding: utf-8 -*-


#
# module path: pycklets.config_values_in_file.ConfigValuesInFile
#


from pyckles import AutoPycklet


class ConfigValuesInFile(AutoPycklet):
    """Adds one or several key/value pairs to a file.

     For every pair, this looks for a line in a file that starts with the value of the ``key`` and ``sep`` variables.
     If it finds it, it'll replace that line with the provided ``key``/``value`` pair, separated by ``sep``.

     If your ``sep`` value contains whitespaces or special characters, the reg-ex matching probably won't work reliably and you
     might not be able to use this.

     If it doesn't find a match, the key/value pair will be appended to the end of the file.

     Missing user/group/parent-dir/file will be created if necessary.

       Args:
         become: Whether to use elevated privileges.
         config: A dict of config key/value pairs.
         group: The group of the file.
         mode: The permissions of the file.
         owner: The owner of the file.
         path: The path to the file
         sep: The seperator token.

    """

    FRECKLET_ID = "config-values-in-file"

    def __init__(
        self,
        become=None,
        config=None,
        group=None,
        mode=None,
        owner=None,
        path=None,
        sep=" = ",
    ):

        super(ConfigValuesInFile, self).__init__(
            var_names=["become", "config", "group", "mode", "owner", "path", "sep"]
        )
        self._become = become
        self._config = config
        self._group = group
        self._mode = mode
        self._owner = owner
        self._path = path
        self._sep = sep

    @property
    def become(self):
        return self._become

    @become.setter
    def become(self, become):
        self._become = become

    @property
    def config(self):
        return self._config

    @config.setter
    def config(self, config):
        self._config = config

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, mode):
        self._mode = mode

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path

    @property
    def sep(self):
        return self._sep

    @sep.setter
    def sep(self, sep):
        self._sep = sep


frecklet_class = ConfigValuesInFile
