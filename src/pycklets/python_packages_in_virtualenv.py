# -*- coding: utf-8 -*-


#
# module path: pycklets.python_packages_in_virtualenv.PythonPackagesInVirtualenv
#


from pyckles import AutoPycklet


class PythonPackagesInVirtualenv(AutoPycklet):
    """Installs Python packages into a Virtualenv.

     Don't use this directly (in most cases), just use the 'python-virtualenv' frecklet instead, because this frecklet does not create that virtualenv, nor does it ensure that Python is available at all.

       Args:
         pip_extra_args: Extra arguments forwarded to 'pip'.
         python_packages: All necessary Python packages.
         python_type: How to install Python. Defaults to 'pyenv'.
         python_version: The version of python.
         system_dependencies: System packages the application depends on.
         update: Update packages if already installed.
         user: The user who owns/runs the virtualenv.
         venv_base_path: The path that holds the virtualenv directory.
         venv_name: The name of the virtualenv to set up.
         venv_python_exe: The (optional) path to an existing Python executable to be used for the venv.

    """

    FRECKLET_ID = "python-packages-in-virtualenv"

    def __init__(
        self,
        pip_extra_args=None,
        python_packages=None,
        python_type="pyenv",
        python_version="latest",
        system_dependencies=None,
        update=None,
        user=None,
        venv_base_path=None,
        venv_name=None,
        venv_python_exe=None,
    ):

        super(PythonPackagesInVirtualenv, self).__init__(
            var_names=[
                "pip_extra_args",
                "python_packages",
                "python_type",
                "python_version",
                "system_dependencies",
                "update",
                "user",
                "venv_base_path",
                "venv_name",
                "venv_python_exe",
            ]
        )
        self._pip_extra_args = pip_extra_args
        self._python_packages = python_packages
        self._python_type = python_type
        self._python_version = python_version
        self._system_dependencies = system_dependencies
        self._update = update
        self._user = user
        self._venv_base_path = venv_base_path
        self._venv_name = venv_name
        self._venv_python_exe = venv_python_exe

    @property
    def pip_extra_args(self):
        return self._pip_extra_args

    @pip_extra_args.setter
    def pip_extra_args(self, pip_extra_args):
        self._pip_extra_args = pip_extra_args

    @property
    def python_packages(self):
        return self._python_packages

    @python_packages.setter
    def python_packages(self, python_packages):
        self._python_packages = python_packages

    @property
    def python_type(self):
        return self._python_type

    @python_type.setter
    def python_type(self, python_type):
        self._python_type = python_type

    @property
    def python_version(self):
        return self._python_version

    @python_version.setter
    def python_version(self, python_version):
        self._python_version = python_version

    @property
    def system_dependencies(self):
        return self._system_dependencies

    @system_dependencies.setter
    def system_dependencies(self, system_dependencies):
        self._system_dependencies = system_dependencies

    @property
    def update(self):
        return self._update

    @update.setter
    def update(self, update):
        self._update = update

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user

    @property
    def venv_base_path(self):
        return self._venv_base_path

    @venv_base_path.setter
    def venv_base_path(self, venv_base_path):
        self._venv_base_path = venv_base_path

    @property
    def venv_name(self):
        return self._venv_name

    @venv_name.setter
    def venv_name(self, venv_name):
        self._venv_name = venv_name

    @property
    def venv_python_exe(self):
        return self._venv_python_exe

    @venv_python_exe.setter
    def venv_python_exe(self, venv_python_exe):
        self._venv_python_exe = venv_python_exe


frecklet_class = PythonPackagesInVirtualenv
