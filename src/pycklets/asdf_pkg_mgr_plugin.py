# -*- coding: utf-8 -*-


#
# module path: pycklets.asdf_pkg_mgr_plugin.AsdfPkgMgrPlugin
#


from pyckles import AutoPycklet


class AsdfPkgMgrPlugin(AutoPycklet):
    """Install a plugin for asdf.

       Args:
         plugin: The asdf plugin to use.
         user: The user to install the plugin for.
         version: The version of the language runtime to install.

    """

    FRECKLET_ID = "asdf-pkg-mgr-plugin"

    def __init__(self, plugin=None, user=None, version=None):

        super(AsdfPkgMgrPlugin, self).__init__(var_names=["plugin", "user", "version"])
        self._plugin = plugin
        self._user = user
        self._version = version

    @property
    def plugin(self):
        return self._plugin

    @plugin.setter
    def plugin(self, plugin):
        self._plugin = plugin

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user

    @property
    def version(self):
        return self._version

    @version.setter
    def version(self, version):
        self._version = version


frecklet_class = AsdfPkgMgrPlugin
