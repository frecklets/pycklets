# -*- coding: utf-8 -*-


#
# module path: pycklets.config_value_in_file.ConfigValueInFile
#


from pyckles import AutoPycklet


class ConfigValueInFile(AutoPycklet):
    """Adds a key/value pair to a file.

     This looks for a line in a file that starts with the value of the ``key`` and ``sep`` variables. If it finds it, it'll
     replace that line with the provided ``key``/``value`` pair, separated by ``sep``.

     If your ``sep`` value contains whitespaces or special characters, the reg-ex matching probably won't work reliably.
     In that case you can provide the matching regular expression manually with the ``match-regex`` parameter.

     If it doesn't find a match, the key/value pair will be appended to the end of the file.

     Missing user/group/parent-dir/file will be created if necessary.

       Args:
         become: Whether to use elevated privileges.
         group: The group of the file.
         key: The config key.
         match_regex: An optional matcher (see help).
         mode: The permissions of the file.
         owner: The owner of the file.
         path: The path to the file
         sep: The seperator token.
         value: The config value.

    """

    FRECKLET_ID = "config-value-in-file"

    def __init__(
        self,
        become=None,
        group=None,
        key=None,
        match_regex=None,
        mode=None,
        owner=None,
        path=None,
        sep="=",
        value=None,
    ):

        super(ConfigValueInFile, self).__init__(
            var_names=[
                "become",
                "group",
                "key",
                "match_regex",
                "mode",
                "owner",
                "path",
                "sep",
                "value",
            ]
        )
        self._become = become
        self._group = group
        self._key = key
        self._match_regex = match_regex
        self._mode = mode
        self._owner = owner
        self._path = path
        self._sep = sep
        self._value = value

    @property
    def become(self):
        return self._become

    @become.setter
    def become(self, become):
        self._become = become

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def key(self):
        return self._key

    @key.setter
    def key(self, key):
        self._key = key

    @property
    def match_regex(self):
        return self._match_regex

    @match_regex.setter
    def match_regex(self, match_regex):
        self._match_regex = match_regex

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, mode):
        self._mode = mode

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path

    @property
    def sep(self):
        return self._sep

    @sep.setter
    def sep(self, sep):
        self._sep = sep

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        self._value = value


frecklet_class = ConfigValueInFile
