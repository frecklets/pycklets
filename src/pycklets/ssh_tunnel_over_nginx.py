# -*- coding: utf-8 -*-


#
# module path: pycklets.ssh_tunnel_over_nginx.SshTunnelOverNginx
#


from pyckles import AutoPycklet


class SshTunnelOverNginx(AutoPycklet):
    """Set up a self-hosted service similar to ngrok.

     A Nginx web server (with valid Letsencrypt https certificate) proxies content from your local machine,
     after you established an ssh-tunnel. You can use your own domain name(s), all you need is a virtual
     server somewhere on the internet.

     To connect to the service, use:

         ssh -N -T -R <ssh_port>:localhost:<your_local_application_port> [server_user@]<server_domain>

     Then, you (or anyone else) can connect with a browser on 'https://server_domain'.

       Args:
         hostname: The domain name the webserver should listen on.
         server_admin: The server admin email (used for the Letsencrypt cert request).
         ssh_port: The port nginx should connect to (on the server).

    """

    FRECKLET_ID = "ssh-tunnel-over-nginx"

    def __init__(self, hostname="localhost", server_admin=None, ssh_port=3333):

        super(SshTunnelOverNginx, self).__init__(
            var_names=["hostname", "server_admin", "ssh_port"]
        )
        self._hostname = hostname
        self._server_admin = server_admin
        self._ssh_port = ssh_port

    @property
    def hostname(self):
        return self._hostname

    @hostname.setter
    def hostname(self, hostname):
        self._hostname = hostname

    @property
    def server_admin(self):
        return self._server_admin

    @server_admin.setter
    def server_admin(self, server_admin):
        self._server_admin = server_admin

    @property
    def ssh_port(self):
        return self._ssh_port

    @ssh_port.setter
    def ssh_port(self, ssh_port):
        self._ssh_port = ssh_port


frecklet_class = SshTunnelOverNginx
