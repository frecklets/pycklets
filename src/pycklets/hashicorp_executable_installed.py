# -*- coding: utf-8 -*-


#
# module path: pycklets.hashicorp_executable_installed.HashicorpExecutableInstalled
#


from pyckles import AutoPycklet


class HashicorpExecutableInstalled(AutoPycklet):
    """A utility frecklet to help install Hashicorp products.

     Hashicorp tools (Packer, Terraform, Consul, ...) are built and hosted in a similar fashion, which is why it's
     convenient to have a wrapper frecklet like this. The 'terraform-installed', 'consul-installed', 'packer-installed' frecklets (and possibly others in the future) all use this frecklet as their base.

       Args:
         dest: The (absolute) path to the parent folder of the downloaded executable file.
         exe_mode: The mode for the executable file(s) (default: '0755').
         group: The group of the executable.
         owner: The owner of the executable.
         product_name: The name of the product.
         version: The version of the product.

    """

    FRECKLET_ID = "hashicorp-executable-installed"

    def __init__(
        self,
        dest=None,
        exe_mode="0755",
        group=None,
        owner=None,
        product_name=None,
        version=None,
    ):

        super(HashicorpExecutableInstalled, self).__init__(
            var_names=["dest", "exe_mode", "group", "owner", "product_name", "version"]
        )
        self._dest = dest
        self._exe_mode = exe_mode
        self._group = group
        self._owner = owner
        self._product_name = product_name
        self._version = version

    @property
    def dest(self):
        return self._dest

    @dest.setter
    def dest(self, dest):
        self._dest = dest

    @property
    def exe_mode(self):
        return self._exe_mode

    @exe_mode.setter
    def exe_mode(self, exe_mode):
        self._exe_mode = exe_mode

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    @property
    def product_name(self):
        return self._product_name

    @product_name.setter
    def product_name(self, product_name):
        self._product_name = product_name

    @property
    def version(self):
        return self._version

    @version.setter
    def version(self, version):
        self._version = version


frecklet_class = HashicorpExecutableInstalled
