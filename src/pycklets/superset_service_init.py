# -*- coding: utf-8 -*-


#
# module path: pycklets.superset_service_init.SupersetServiceInit
#


from pyckles import AutoPycklet


class SupersetServiceInit(AutoPycklet):
    """Execute init tasks after a fresh superset installation.

       Args:
         admin_email: The email address of the admin user.
         admin_firstname: The first name of the admin user.
         admin_lastname: The last name of the admin user.
         admin_password: The db password.
         admin_username: The username of the admin user.
         superset_user: The superset user.
         superset_virtualenv: The path to the virtualenv where Superset is installed.

    """

    FRECKLET_ID = "superset-service-init"

    def __init__(
        self,
        admin_email=None,
        admin_firstname=None,
        admin_lastname=None,
        admin_password=None,
        admin_username="admin",
        superset_user="superset",
        superset_virtualenv="/var/lib/pyenv/versions/3.6.5/envs/superset",
    ):

        super(SupersetServiceInit, self).__init__(
            var_names=[
                "admin_email",
                "admin_firstname",
                "admin_lastname",
                "admin_password",
                "admin_username",
                "superset_user",
                "superset_virtualenv",
            ]
        )
        self._admin_email = admin_email
        self._admin_firstname = admin_firstname
        self._admin_lastname = admin_lastname
        self._admin_password = admin_password
        self._admin_username = admin_username
        self._superset_user = superset_user
        self._superset_virtualenv = superset_virtualenv

    @property
    def admin_email(self):
        return self._admin_email

    @admin_email.setter
    def admin_email(self, admin_email):
        self._admin_email = admin_email

    @property
    def admin_firstname(self):
        return self._admin_firstname

    @admin_firstname.setter
    def admin_firstname(self, admin_firstname):
        self._admin_firstname = admin_firstname

    @property
    def admin_lastname(self):
        return self._admin_lastname

    @admin_lastname.setter
    def admin_lastname(self, admin_lastname):
        self._admin_lastname = admin_lastname

    @property
    def admin_password(self):
        return self._admin_password

    @admin_password.setter
    def admin_password(self, admin_password):
        self._admin_password = admin_password

    @property
    def admin_username(self):
        return self._admin_username

    @admin_username.setter
    def admin_username(self, admin_username):
        self._admin_username = admin_username

    @property
    def superset_user(self):
        return self._superset_user

    @superset_user.setter
    def superset_user(self, superset_user):
        self._superset_user = superset_user

    @property
    def superset_virtualenv(self):
        return self._superset_virtualenv

    @superset_virtualenv.setter
    def superset_virtualenv(self, superset_virtualenv):
        self._superset_virtualenv = superset_virtualenv


frecklet_class = SupersetServiceInit
