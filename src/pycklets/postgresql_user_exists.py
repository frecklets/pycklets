# -*- coding: utf-8 -*-


#
# module path: pycklets.postgresql_user_exists.PostgresqlUserExists
#


from pyckles import AutoPycklet


class PostgresqlUserExists(AutoPycklet):
    """Ensures a database user with the provided name is present on a PostgreSQL server.

     The PostgreSQL service itself won't be installed, use the 'postgresql-service' frecklet before this if necessary.

       Args:
         db_user: The name of the database user ('role' in postgres).
         db_user_password: The (hashed) password for the database user ('role' in PostgreSQL).

    """

    FRECKLET_ID = "postgresql-user-exists"

    def __init__(self, db_user=None, db_user_password=None):

        super(PostgresqlUserExists, self).__init__(
            var_names=["db_user", "db_user_password"]
        )
        self._db_user = db_user
        self._db_user_password = db_user_password

    @property
    def db_user(self):
        return self._db_user

    @db_user.setter
    def db_user(self, db_user):
        self._db_user = db_user

    @property
    def db_user_password(self):
        return self._db_user_password

    @db_user_password.setter
    def db_user_password(self, db_user_password):
        self._db_user_password = db_user_password


frecklet_class = PostgresqlUserExists
