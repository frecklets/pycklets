# -*- coding: utf-8 -*-


#
# module path: pycklets.user_exists.UserExists
#


from pyckles import AutoPycklet


class UserExists(AutoPycklet):
    """Ensure a user exists on a system.

     If no ``password`` argument is provided, the created user won't be able do login via ssh via
     password auth, and they won't be able to do sudo if passwordless sudo is not enabled for the user.

     This task allows for providing the password in plain text. It will

     Optionally, you can specify UID, main group and GID of the user.
     If the ``group`` var is specified, a corresponding group will be created if it doesn't exist yet.

       Args:
         gid: The GID of the users main group (optional).
         group: The name of the users main group.
         name: The name of the user to create.
         password: The user password in plain text.
         shell: The users default shell.
         system_user: Whether the user to create (and potentially group) should be created as system user.
         uid: The uid of the user to create (optional).

    """

    FRECKLET_ID = "user-exists"

    def __init__(
        self,
        gid=None,
        group=None,
        name=None,
        password=None,
        shell="/bin/bash",
        system_user=None,
        uid=None,
    ):

        super(UserExists, self).__init__(
            var_names=[
                "gid",
                "group",
                "name",
                "password",
                "shell",
                "system_user",
                "uid",
            ]
        )
        self._gid = gid
        self._group = group
        self._name = name
        self._password = password
        self._shell = shell
        self._system_user = system_user
        self._uid = uid

    @property
    def gid(self):
        return self._gid

    @gid.setter
    def gid(self, gid):
        self._gid = gid

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def password(self):
        return self._password

    @password.setter
    def password(self, password):
        self._password = password

    @property
    def shell(self):
        return self._shell

    @shell.setter
    def shell(self, shell):
        self._shell = shell

    @property
    def system_user(self):
        return self._system_user

    @system_user.setter
    def system_user(self, system_user):
        self._system_user = system_user

    @property
    def uid(self):
        return self._uid

    @uid.setter
    def uid(self, uid):
        self._uid = uid


frecklet_class = UserExists
