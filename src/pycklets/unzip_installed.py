# -*- coding: utf-8 -*-


#
# module path: pycklets.unzip_installed.UnzipInstalled
#


from pyckles import AutoPycklet


class UnzipInstalled(AutoPycklet):
    """Install the 'unzip' package.

       Args:

    """

    FRECKLET_ID = "unzip-installed"

    def __init__(self,):

        super(UnzipInstalled, self).__init__(var_names=[])


frecklet_class = UnzipInstalled
