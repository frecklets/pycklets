# -*- coding: utf-8 -*-


#
# module path: pycklets.netdata_service.NetdataService
#


from pyckles import AutoPycklet


class NetdataService(AutoPycklet):
    """Install [netdata](https://my-netdata.io/) using the [mrlesmithjr.ansible-netdata](https://github.com/mrlesmithjr/ansible-netdata) Ansible role.

     After install, netdata should be available on port 19999.

       Args:
         api_key: Netdata api key.
         bind: The ip address and port to listen to.
         is_registry: Whether this instance should act as netdata registry.
         registry: The registry to use.
         stream: Whether netdata streawming should be configured.
         stream_master_node: Stream master node.

    """

    FRECKLET_ID = "netdata-service"

    def __init__(
        self,
        api_key=None,
        bind=["*"],
        is_registry=None,
        registry="https://registry.my-netdata.io",
        stream=None,
        stream_master_node=None,
    ):

        super(NetdataService, self).__init__(
            var_names=[
                "api_key",
                "bind",
                "is_registry",
                "registry",
                "stream",
                "stream_master_node",
            ]
        )
        self._api_key = api_key
        self._bind = bind
        self._is_registry = is_registry
        self._registry = registry
        self._stream = stream
        self._stream_master_node = stream_master_node

    @property
    def api_key(self):
        return self._api_key

    @api_key.setter
    def api_key(self, api_key):
        self._api_key = api_key

    @property
    def bind(self):
        return self._bind

    @bind.setter
    def bind(self, bind):
        self._bind = bind

    @property
    def is_registry(self):
        return self._is_registry

    @is_registry.setter
    def is_registry(self, is_registry):
        self._is_registry = is_registry

    @property
    def registry(self):
        return self._registry

    @registry.setter
    def registry(self, registry):
        self._registry = registry

    @property
    def stream(self):
        return self._stream

    @stream.setter
    def stream(self, stream):
        self._stream = stream

    @property
    def stream_master_node(self):
        return self._stream_master_node

    @stream_master_node.setter
    def stream_master_node(self, stream_master_node):
        self._stream_master_node = stream_master_node


frecklet_class = NetdataService
