# -*- coding: utf-8 -*-


#
# module path: pycklets.vault_config_file.VaultConfigFile
#


from pyckles import AutoPycklet


class VaultConfigFile(AutoPycklet):
    """Configuration for a Hashicorp Vault service.

       Args:
         disable_listener_tcp_tls: Specifies if TLS will be disabled.
         disable_mlock: Whether to disable 'mlock' for this Vault instance.
         group: The group of the file.
         listener_tcp_address: Specifies the address to bind to for listening.
         mode: The permissions of the file.
         owner: The owner of the file.
         path: The path to the file.
         ui: Whether to enable the Vault UI on this machine.

    """

    FRECKLET_ID = "vault-config-file"

    def __init__(
        self,
        disable_listener_tcp_tls=None,
        disable_mlock=None,
        group=None,
        listener_tcp_address="0.0.0.0:8200",
        mode=None,
        owner=None,
        path=None,
        ui=None,
    ):

        super(VaultConfigFile, self).__init__(
            var_names=[
                "disable_listener_tcp_tls",
                "disable_mlock",
                "group",
                "listener_tcp_address",
                "mode",
                "owner",
                "path",
                "ui",
            ]
        )
        self._disable_listener_tcp_tls = disable_listener_tcp_tls
        self._disable_mlock = disable_mlock
        self._group = group
        self._listener_tcp_address = listener_tcp_address
        self._mode = mode
        self._owner = owner
        self._path = path
        self._ui = ui

    @property
    def disable_listener_tcp_tls(self):
        return self._disable_listener_tcp_tls

    @disable_listener_tcp_tls.setter
    def disable_listener_tcp_tls(self, disable_listener_tcp_tls):
        self._disable_listener_tcp_tls = disable_listener_tcp_tls

    @property
    def disable_mlock(self):
        return self._disable_mlock

    @disable_mlock.setter
    def disable_mlock(self, disable_mlock):
        self._disable_mlock = disable_mlock

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def listener_tcp_address(self):
        return self._listener_tcp_address

    @listener_tcp_address.setter
    def listener_tcp_address(self, listener_tcp_address):
        self._listener_tcp_address = listener_tcp_address

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, mode):
        self._mode = mode

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path

    @property
    def ui(self):
        return self._ui

    @ui.setter
    def ui(self, ui):
        self._ui = ui


frecklet_class = VaultConfigFile
