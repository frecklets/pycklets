# -*- coding: utf-8 -*-


#
# module path: pycklets.nginx_reverse_proxy_vhost_config.NginxReverseProxyVhostConfig
#


from pyckles import AutoPycklet


class NginxReverseProxyVhostConfig(AutoPycklet):
    """TOOD: documentation, examples

       Args:
         access_log: The access log.
         error_log: The error log path and (optional) log level.
         path: The path to the file.
         proxy_options: A list of options to set in the proxy location block.
         proxy_url: The url to proxy.
         server_admin: The server admin email.
         server_names: The server names.
         use_https: Whether to use https.

    """

    FRECKLET_ID = "nginx-reverse-proxy-vhost-config"

    def __init__(
        self,
        access_log=None,
        error_log=None,
        path=None,
        proxy_options=None,
        proxy_url=None,
        server_admin=None,
        server_names=["localhost"],
        use_https=None,
    ):

        super(NginxReverseProxyVhostConfig, self).__init__(
            var_names=[
                "access_log",
                "error_log",
                "path",
                "proxy_options",
                "proxy_url",
                "server_admin",
                "server_names",
                "use_https",
            ]
        )
        self._access_log = access_log
        self._error_log = error_log
        self._path = path
        self._proxy_options = proxy_options
        self._proxy_url = proxy_url
        self._server_admin = server_admin
        self._server_names = server_names
        self._use_https = use_https

    @property
    def access_log(self):
        return self._access_log

    @access_log.setter
    def access_log(self, access_log):
        self._access_log = access_log

    @property
    def error_log(self):
        return self._error_log

    @error_log.setter
    def error_log(self, error_log):
        self._error_log = error_log

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path

    @property
    def proxy_options(self):
        return self._proxy_options

    @proxy_options.setter
    def proxy_options(self, proxy_options):
        self._proxy_options = proxy_options

    @property
    def proxy_url(self):
        return self._proxy_url

    @proxy_url.setter
    def proxy_url(self, proxy_url):
        self._proxy_url = proxy_url

    @property
    def server_admin(self):
        return self._server_admin

    @server_admin.setter
    def server_admin(self, server_admin):
        self._server_admin = server_admin

    @property
    def server_names(self):
        return self._server_names

    @server_names.setter
    def server_names(self, server_names):
        self._server_names = server_names

    @property
    def use_https(self):
        return self._use_https

    @use_https.setter
    def use_https(self, use_https):
        self._use_https = use_https


frecklet_class = NginxReverseProxyVhostConfig
