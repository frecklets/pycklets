# -*- coding: utf-8 -*-


#
# module path: pycklets.consul_service.ConsulService
#


from pyckles import AutoPycklet


class ConsulService(AutoPycklet):
    """Create a user named 'consul' (if necessary), download the Consul binary into '/usr/local/bin' and create a
     systemd service unit ('consul') and enable/start it if so specified.

     It is recommended to create the approriate Consul server/client configuration files beforehand (in '/etc/consul.d'), but this frecklet also allows for providing basic configuration options if necessary.

       Args:
         arch: The architecture of the host system.
         consul_config: The consul configuration.
         dest: The (absolute) path to the parent folder of the downloaded executable file.
         enabled: Whether to enable the service.
         platform: The platform of the host system.
         started: Whether to start the service.
         version: The version of consul to install.

    """

    FRECKLET_ID = "consul-service"

    def __init__(
        self,
        arch=None,
        consul_config=None,
        dest=None,
        enabled=None,
        platform=None,
        started=None,
        version="1.5.3",
    ):

        super(ConsulService, self).__init__(
            var_names=[
                "arch",
                "consul_config",
                "dest",
                "enabled",
                "platform",
                "started",
                "version",
            ]
        )
        self._arch = arch
        self._consul_config = consul_config
        self._dest = dest
        self._enabled = enabled
        self._platform = platform
        self._started = started
        self._version = version

    @property
    def arch(self):
        return self._arch

    @arch.setter
    def arch(self, arch):
        self._arch = arch

    @property
    def consul_config(self):
        return self._consul_config

    @consul_config.setter
    def consul_config(self, consul_config):
        self._consul_config = consul_config

    @property
    def dest(self):
        return self._dest

    @dest.setter
    def dest(self, dest):
        self._dest = dest

    @property
    def enabled(self):
        return self._enabled

    @enabled.setter
    def enabled(self, enabled):
        self._enabled = enabled

    @property
    def platform(self):
        return self._platform

    @platform.setter
    def platform(self, platform):
        self._platform = platform

    @property
    def started(self):
        return self._started

    @started.setter
    def started(self, started):
        self._started = started

    @property
    def version(self):
        return self._version

    @version.setter
    def version(self, version):
        self._version = version


frecklet_class = ConsulService
