# -*- coding: utf-8 -*-


#
# module path: pycklets.group_exists.GroupExists
#


from pyckles import AutoPycklet


class GroupExists(AutoPycklet):
    """Create a group on a system if it doesn't exist yet.

     If the ``group`` argument is not present or empty, this task will be skipped.

     If the group exists, and the optional ``gid`` argument is specified, it'll be changed if necessary.
     Optionally, the group can be marked as a 'system' group.

       Args:
         gid: The gid of the group.
         group: The name of the group.
         system_group: Whether the group should be created as 'system' group.

    """

    FRECKLET_ID = "group-exists"

    def __init__(self, gid=None, group=None, system_group=None):

        super(GroupExists, self).__init__(var_names=["gid", "group", "system_group"])
        self._gid = gid
        self._group = group
        self._system_group = system_group

    @property
    def gid(self):
        return self._gid

    @gid.setter
    def gid(self, gid):
        self._gid = gid

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def system_group(self):
        return self._system_group

    @system_group.setter
    def system_group(self, system_group):
        self._system_group = system_group


frecklet_class = GroupExists
