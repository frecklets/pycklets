# -*- coding: utf-8 -*-


#
# module path: pycklets.superset_service_configuration_file.SupersetServiceConfigurationFile
#


from pyckles import AutoPycklet


class SupersetServiceConfigurationFile(AutoPycklet):
    """No documentation available.

       Args:
         db_host_ip: The db host.
         db_name: The db name (or absolute path, in case of sqlite).
         db_password: The db user password.
         db_type: The type of db to use.
         db_user: The db user name.
         group: The group of the file.
         mode: The permissions of the file.
         owner: The owner of the file.
         path: The path to the file.

    """

    FRECKLET_ID = "superset-service-configuration-file"

    def __init__(
        self,
        db_host_ip="127.0.0.1",
        db_name=None,
        db_password=None,
        db_type=None,
        db_user=None,
        group=None,
        mode=None,
        owner=None,
        path=None,
    ):

        super(SupersetServiceConfigurationFile, self).__init__(
            var_names=[
                "db_host_ip",
                "db_name",
                "db_password",
                "db_type",
                "db_user",
                "group",
                "mode",
                "owner",
                "path",
            ]
        )
        self._db_host_ip = db_host_ip
        self._db_name = db_name
        self._db_password = db_password
        self._db_type = db_type
        self._db_user = db_user
        self._group = group
        self._mode = mode
        self._owner = owner
        self._path = path

    @property
    def db_host_ip(self):
        return self._db_host_ip

    @db_host_ip.setter
    def db_host_ip(self, db_host_ip):
        self._db_host_ip = db_host_ip

    @property
    def db_name(self):
        return self._db_name

    @db_name.setter
    def db_name(self, db_name):
        self._db_name = db_name

    @property
    def db_password(self):
        return self._db_password

    @db_password.setter
    def db_password(self, db_password):
        self._db_password = db_password

    @property
    def db_type(self):
        return self._db_type

    @db_type.setter
    def db_type(self, db_type):
        self._db_type = db_type

    @property
    def db_user(self):
        return self._db_user

    @db_user.setter
    def db_user(self, db_user):
        self._db_user = db_user

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, mode):
        self._mode = mode

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path


frecklet_class = SupersetServiceConfigurationFile
