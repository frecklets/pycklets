# -*- coding: utf-8 -*-


#
# module path: pycklets.gitlab_runner_registered_linux.GitlabRunnerRegisteredLinux
#


from pyckles import AutoPycklet


class GitlabRunnerRegisteredLinux(AutoPycklet):
    """Ensure a gitlab runner is registered for a group or project.

       Args:
         api_token: Your private token to interact with the GitLab API.
         arch: The architecture of the target system.
         description: The unique name of the runner.
         gitlab_url: The URL of the Gitlab server, with protocol (i.e. http or https).
         install_docker: Whether to also install Docker.
         install_gitlab_runner: Whether to also make sure a gitlab runner service is setup and running.
         locked: Whether the runner is locked or not.
         registration_token: The registration token to use.
         run_untagged: Run untagged jobs or not.
         tags: The tags that apply to the runner.

    """

    FRECKLET_ID = "gitlab-runner-registered-linux"

    def __init__(
        self,
        api_token=None,
        arch="amd64",
        description=None,
        gitlab_url="https://gitlab.com",
        install_docker=None,
        install_gitlab_runner=None,
        locked=None,
        registration_token=None,
        run_untagged=True,
        tags=None,
    ):

        super(GitlabRunnerRegisteredLinux, self).__init__(
            var_names=[
                "api_token",
                "arch",
                "description",
                "gitlab_url",
                "install_docker",
                "install_gitlab_runner",
                "locked",
                "registration_token",
                "run_untagged",
                "tags",
            ]
        )
        self._api_token = api_token
        self._arch = arch
        self._description = description
        self._gitlab_url = gitlab_url
        self._install_docker = install_docker
        self._install_gitlab_runner = install_gitlab_runner
        self._locked = locked
        self._registration_token = registration_token
        self._run_untagged = run_untagged
        self._tags = tags

    @property
    def api_token(self):
        return self._api_token

    @api_token.setter
    def api_token(self, api_token):
        self._api_token = api_token

    @property
    def arch(self):
        return self._arch

    @arch.setter
    def arch(self, arch):
        self._arch = arch

    @property
    def description(self):
        return self._description

    @description.setter
    def description(self, description):
        self._description = description

    @property
    def gitlab_url(self):
        return self._gitlab_url

    @gitlab_url.setter
    def gitlab_url(self, gitlab_url):
        self._gitlab_url = gitlab_url

    @property
    def install_docker(self):
        return self._install_docker

    @install_docker.setter
    def install_docker(self, install_docker):
        self._install_docker = install_docker

    @property
    def install_gitlab_runner(self):
        return self._install_gitlab_runner

    @install_gitlab_runner.setter
    def install_gitlab_runner(self, install_gitlab_runner):
        self._install_gitlab_runner = install_gitlab_runner

    @property
    def locked(self):
        return self._locked

    @locked.setter
    def locked(self, locked):
        self._locked = locked

    @property
    def registration_token(self):
        return self._registration_token

    @registration_token.setter
    def registration_token(self, registration_token):
        self._registration_token = registration_token

    @property
    def run_untagged(self):
        return self._run_untagged

    @run_untagged.setter
    def run_untagged(self, run_untagged):
        self._run_untagged = run_untagged

    @property
    def tags(self):
        return self._tags

    @tags.setter
    def tags(self, tags):
        self._tags = tags


frecklet_class = GitlabRunnerRegisteredLinux
