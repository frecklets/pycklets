# -*- coding: utf-8 -*-


#
# module path: pycklets.freckles_recommended_dependencies.FrecklesRecommendedDependencies
#


from pyckles import AutoPycklet


class FrecklesRecommendedDependencies(AutoPycklet):
    """Install recommended base dependencies to be able to make full use of freckles.

       Args:

    """

    FRECKLET_ID = "freckles-recommended-dependencies"

    def __init__(self,):

        super(FrecklesRecommendedDependencies, self).__init__(var_names=[])


frecklet_class = FrecklesRecommendedDependencies
