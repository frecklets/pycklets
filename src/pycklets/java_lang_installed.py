# -*- coding: utf-8 -*-


#
# module path: pycklets.java_lang_installed.JavaLangInstalled
#


from pyckles import AutoPycklet


class JavaLangInstalled(AutoPycklet):
    """Install OpenJDK on Linux machines.

     Currently it's not possible to select a version, this will be fixed later.

       Args:

    """

    FRECKLET_ID = "java-lang-installed"

    def __init__(self,):

        super(JavaLangInstalled, self).__init__(var_names=[])


frecklet_class = JavaLangInstalled
