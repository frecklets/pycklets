# -*- coding: utf-8 -*-


#
# module path: pycklets.systemd_service_unit.SystemdServiceUnit
#


from pyckles import AutoPycklet


class SystemdServiceUnit(AutoPycklet):
    """Add a systemd unit for a service.

     This does not create the user from the 'service_user' parameter, nor the group from 'service_group'.

       Args:
         enabled: Whether to enable the service.
         install_alias: A space-separated list of additional names this unit shall be installed under.
         install_also: Additional units to install/deinstall when this unit is installed/deinstalled.
         install_default_instance: In template unit files, this specifies for which instance the unit shall be enabled if the template is enabled without any explicitly set instance.
         install_required_by: This option may be used more than once, or a space-separated list of unit names may be given.
         install_wanted_by: This option may be used more than once, or a space-separated list of unit names may be given.
         instance_name: The instance name, in case this unit is enabled or started, and the 'is_template_unit' flag is set.
         is_template_unit: Whether this unit is a 'template unit' (can be parametrized and instance name ends with '@').
         name: The name of the service.
         service_ambient_capabilities: Controls which capabilities to include in the ambient capability set for the executed process.
         service_bus_name: Takes a D-Bus bus name that this service is reachable as. This option is mandatory for services where Type= is set to dbus.
         service_capability_bounding_set: Controls which capabilities to include in the capability bounding set for the executed process.
         service_config: A map of key/value-pairs to put into a '/etc/conf.d/<service>' file.
         service_environment: Sets environment variables for executed processes.
         service_environment_file: Similar to Environment= but reads the environment variables from a text file.
         service_exec_reload: Commands to execute to trigger a configuration reload in the service.
         service_exec_start: Commands with their arguments that are executed when this service is started.
         service_exec_start_post: Additional commands that are executed before or after the command in ExecStart=, respectively.
         service_exec_start_pre: Additional commands that are executed before or after the command in ExecStart=, respectively.
         service_exec_stop: Commands to execute to stop the service started via ExecStart=.
         service_exec_stop_post: Additional commands that are executed after the service is stopped.
         service_group: Set the UNIX user or group that the processes are executed as, respectively.
         service_guess_main_pid: Takes a boolean value that specifies whether systemd should try to guess the main PID of a service if it cannot be determined reliably.
         service_kill_mode: Specifies how processes of this unit shall be killed. One of control-group, process, mixed, none.
         service_kill_signal: Specifies which signal to use when killing a service.
         service_limit: Set soft and hard limits on various resources for executed processes.
         service_memory_deny_write_execute: If set, attempts to create memory mappings that are writable and executable at the same time, or to change existing memory mappings to become executable, or mapping shared memory segments as executable are prohibited
         service_no_new_privileges: Takes a boolean argument. If true, ensures that the service process and all its children can never gain new privileges through execve() (e.g. via setuid or setgid bits, or filesystem capabilities).
         service_permissions_start_only: Takes a boolean argument. If true, the permission-related execution options, as configured with User= and similar options (see systemd.exec(5) for more information), are only applied to the process started with ExecStart=, and not to the various other ExecStartPre=, ExecStartPost=, ExecReload=, ExecStop=, and ExecStopPost= commands. If false, the setting is applied to all configured commands the same way. Defaults to false.
         service_pid_file: Takes a path referring to the PID file of the service.
         service_private_devices: Takes a boolean argument. If true, sets up a new /dev mount for the executed processes and only adds API pseudo devices such as /dev/null, /dev/zero or /dev/random (as well as the pseudo TTY subsystem) to it, but no physical devices such as /dev/sda, system memory /dev/mem, system ports /dev/port and others.
         service_private_tmp: If true, sets up a new file system namespace for the executed processes and mounts private /tmp and /var/tmp directories inside it that is not shared by processes outside of the namespace.
         service_protect_home: Takes a boolean argument or the special values "read-only" or "tmpfs".
         service_protect_system: Takes a boolean argument or the special values "full" or "strict".
         service_remain_after_exit: Takes a boolean value that specifies whether the service shall be considered active even when all its processes exited. Defaults to no.
         service_restart: Configures whether the service shall be restarted when the service process exits, is killed, or a timeout is reached.
         service_restart_force_exit_status: a list of exit status definitions that, when returned by the main service process, will force automatic service restarts, regardless of the restart setting configured with Restart=.
         service_restart_sec: Configures the time to sleep before restarting a service (as configured with Restart=).
         service_runtime_directory: Create a runtime directory for a service.
         service_runtime_directory_mode: Sets the permission for the 'service_runtime_directory' values.
         service_secure_bits: Controls the secure bits set for the executed process.
         service_success_exit_status: Takes a list of exit status definitions that, when returned by the main service process, will be considered successful termination, in addition to the normal successful exit code 0 and the signals SIGHUP, SIGINT, SIGTERM, and SIGPIPE.
         service_system_call_architectures: Takes a space-separated list of architecture identifiers. Selects from which architectures system calls may be invoked on this system.
         service_tasks_max: Specify the maximum number of tasks that may be created in the unit.
         service_timeout_sec: A shorthand for configuring both TimeoutStartSec= and TimeoutStopSec= to the specified value.
         service_timeout_start_sec: Configures the time to wait for start-up.
         service_timeout_stop_sec: Configures the time to wait for each ExecStop= command and the time to wait for the service itself to stop.
         service_type: Configures the process start-up type for this service unit.
         service_user: Set the UNIX user or group that the processes are executed as, respectively.
         service_working_directory: Takes a directory path relative to the service's root directory specified by RootDirectory=, or the special value "~".
         started: Whether to start the service.
         unit_after: These two settings ('Before', 'After') expect a space-separated list of unit names. They configure ordering dependencies between units.
         unit_before: These two settings ('Before', 'After') expect a space-separated list of unit names. They configure ordering dependencies between units.
         unit_binds_to: Configures requirement dependencies, very similar in style to Requires=.
         unit_condition: Before starting a unit, verify that the specified condition is true.
         unit_conflicts: A space-separated list of unit names. Configures negative requirement dependencies.
         unit_description: A human readable name for the unit.
         unit_documentation: A space-separated list of URIs referencing documentation for this unit or its configuration.
         unit_part_of: Configures dependencies similar to Requires=, but limited to stopping and restarting of units.
         unit_requires: Configures requirement dependencies on other units.
         unit_requisite: Similar to Requires=. However, if the units listed here are not started already, they will not be started and the starting of this unit will fail immediately.
         unit_start_limit_burst: Configure unit start rate limiting.
         unit_start_limit_interval_sec: Configure unit start rate limiting.
         unit_wants: A weaker version of Requires=. Units listed in this option will be started if the configuring unit is.

    """

    FRECKLET_ID = "systemd-service-unit"

    def __init__(
        self,
        enabled=None,
        install_alias=None,
        install_also=None,
        install_default_instance=None,
        install_required_by=None,
        install_wanted_by=None,
        instance_name=None,
        is_template_unit=None,
        name=None,
        service_ambient_capabilities=None,
        service_bus_name=None,
        service_capability_bounding_set=None,
        service_config=None,
        service_environment=None,
        service_environment_file=None,
        service_exec_reload=None,
        service_exec_start=None,
        service_exec_start_post=None,
        service_exec_start_pre=None,
        service_exec_stop=None,
        service_exec_stop_post=None,
        service_group=None,
        service_guess_main_pid=None,
        service_kill_mode=None,
        service_kill_signal=None,
        service_limit=None,
        service_memory_deny_write_execute=None,
        service_no_new_privileges=None,
        service_permissions_start_only=None,
        service_pid_file=None,
        service_private_devices=None,
        service_private_tmp=None,
        service_protect_home=None,
        service_protect_system=None,
        service_remain_after_exit=None,
        service_restart=None,
        service_restart_force_exit_status=None,
        service_restart_sec=None,
        service_runtime_directory=None,
        service_runtime_directory_mode=None,
        service_secure_bits=None,
        service_success_exit_status=None,
        service_system_call_architectures=None,
        service_tasks_max=None,
        service_timeout_sec=None,
        service_timeout_start_sec=None,
        service_timeout_stop_sec=None,
        service_type="simple",
        service_user=None,
        service_working_directory=None,
        started=None,
        unit_after=None,
        unit_before=None,
        unit_binds_to=None,
        unit_condition=None,
        unit_conflicts=None,
        unit_description=None,
        unit_documentation=None,
        unit_part_of=None,
        unit_requires=None,
        unit_requisite=None,
        unit_start_limit_burst=None,
        unit_start_limit_interval_sec=None,
        unit_wants=None,
    ):

        super(SystemdServiceUnit, self).__init__(
            var_names=[
                "enabled",
                "install_alias",
                "install_also",
                "install_default_instance",
                "install_required_by",
                "install_wanted_by",
                "instance_name",
                "is_template_unit",
                "name",
                "service_ambient_capabilities",
                "service_bus_name",
                "service_capability_bounding_set",
                "service_config",
                "service_environment",
                "service_environment_file",
                "service_exec_reload",
                "service_exec_start",
                "service_exec_start_post",
                "service_exec_start_pre",
                "service_exec_stop",
                "service_exec_stop_post",
                "service_group",
                "service_guess_main_pid",
                "service_kill_mode",
                "service_kill_signal",
                "service_limit",
                "service_memory_deny_write_execute",
                "service_no_new_privileges",
                "service_permissions_start_only",
                "service_pid_file",
                "service_private_devices",
                "service_private_tmp",
                "service_protect_home",
                "service_protect_system",
                "service_remain_after_exit",
                "service_restart",
                "service_restart_force_exit_status",
                "service_restart_sec",
                "service_runtime_directory",
                "service_runtime_directory_mode",
                "service_secure_bits",
                "service_success_exit_status",
                "service_system_call_architectures",
                "service_tasks_max",
                "service_timeout_sec",
                "service_timeout_start_sec",
                "service_timeout_stop_sec",
                "service_type",
                "service_user",
                "service_working_directory",
                "started",
                "unit_after",
                "unit_before",
                "unit_binds_to",
                "unit_condition",
                "unit_conflicts",
                "unit_description",
                "unit_documentation",
                "unit_part_of",
                "unit_requires",
                "unit_requisite",
                "unit_start_limit_burst",
                "unit_start_limit_interval_sec",
                "unit_wants",
            ]
        )
        self._enabled = enabled
        self._install_alias = install_alias
        self._install_also = install_also
        self._install_default_instance = install_default_instance
        self._install_required_by = install_required_by
        self._install_wanted_by = install_wanted_by
        self._instance_name = instance_name
        self._is_template_unit = is_template_unit
        self._name = name
        self._service_ambient_capabilities = service_ambient_capabilities
        self._service_bus_name = service_bus_name
        self._service_capability_bounding_set = service_capability_bounding_set
        self._service_config = service_config
        self._service_environment = service_environment
        self._service_environment_file = service_environment_file
        self._service_exec_reload = service_exec_reload
        self._service_exec_start = service_exec_start
        self._service_exec_start_post = service_exec_start_post
        self._service_exec_start_pre = service_exec_start_pre
        self._service_exec_stop = service_exec_stop
        self._service_exec_stop_post = service_exec_stop_post
        self._service_group = service_group
        self._service_guess_main_pid = service_guess_main_pid
        self._service_kill_mode = service_kill_mode
        self._service_kill_signal = service_kill_signal
        self._service_limit = service_limit
        self._service_memory_deny_write_execute = service_memory_deny_write_execute
        self._service_no_new_privileges = service_no_new_privileges
        self._service_permissions_start_only = service_permissions_start_only
        self._service_pid_file = service_pid_file
        self._service_private_devices = service_private_devices
        self._service_private_tmp = service_private_tmp
        self._service_protect_home = service_protect_home
        self._service_protect_system = service_protect_system
        self._service_remain_after_exit = service_remain_after_exit
        self._service_restart = service_restart
        self._service_restart_force_exit_status = service_restart_force_exit_status
        self._service_restart_sec = service_restart_sec
        self._service_runtime_directory = service_runtime_directory
        self._service_runtime_directory_mode = service_runtime_directory_mode
        self._service_secure_bits = service_secure_bits
        self._service_success_exit_status = service_success_exit_status
        self._service_system_call_architectures = service_system_call_architectures
        self._service_tasks_max = service_tasks_max
        self._service_timeout_sec = service_timeout_sec
        self._service_timeout_start_sec = service_timeout_start_sec
        self._service_timeout_stop_sec = service_timeout_stop_sec
        self._service_type = service_type
        self._service_user = service_user
        self._service_working_directory = service_working_directory
        self._started = started
        self._unit_after = unit_after
        self._unit_before = unit_before
        self._unit_binds_to = unit_binds_to
        self._unit_condition = unit_condition
        self._unit_conflicts = unit_conflicts
        self._unit_description = unit_description
        self._unit_documentation = unit_documentation
        self._unit_part_of = unit_part_of
        self._unit_requires = unit_requires
        self._unit_requisite = unit_requisite
        self._unit_start_limit_burst = unit_start_limit_burst
        self._unit_start_limit_interval_sec = unit_start_limit_interval_sec
        self._unit_wants = unit_wants

    @property
    def enabled(self):
        return self._enabled

    @enabled.setter
    def enabled(self, enabled):
        self._enabled = enabled

    @property
    def install_alias(self):
        return self._install_alias

    @install_alias.setter
    def install_alias(self, install_alias):
        self._install_alias = install_alias

    @property
    def install_also(self):
        return self._install_also

    @install_also.setter
    def install_also(self, install_also):
        self._install_also = install_also

    @property
    def install_default_instance(self):
        return self._install_default_instance

    @install_default_instance.setter
    def install_default_instance(self, install_default_instance):
        self._install_default_instance = install_default_instance

    @property
    def install_required_by(self):
        return self._install_required_by

    @install_required_by.setter
    def install_required_by(self, install_required_by):
        self._install_required_by = install_required_by

    @property
    def install_wanted_by(self):
        return self._install_wanted_by

    @install_wanted_by.setter
    def install_wanted_by(self, install_wanted_by):
        self._install_wanted_by = install_wanted_by

    @property
    def instance_name(self):
        return self._instance_name

    @instance_name.setter
    def instance_name(self, instance_name):
        self._instance_name = instance_name

    @property
    def is_template_unit(self):
        return self._is_template_unit

    @is_template_unit.setter
    def is_template_unit(self, is_template_unit):
        self._is_template_unit = is_template_unit

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def service_ambient_capabilities(self):
        return self._service_ambient_capabilities

    @service_ambient_capabilities.setter
    def service_ambient_capabilities(self, service_ambient_capabilities):
        self._service_ambient_capabilities = service_ambient_capabilities

    @property
    def service_bus_name(self):
        return self._service_bus_name

    @service_bus_name.setter
    def service_bus_name(self, service_bus_name):
        self._service_bus_name = service_bus_name

    @property
    def service_capability_bounding_set(self):
        return self._service_capability_bounding_set

    @service_capability_bounding_set.setter
    def service_capability_bounding_set(self, service_capability_bounding_set):
        self._service_capability_bounding_set = service_capability_bounding_set

    @property
    def service_config(self):
        return self._service_config

    @service_config.setter
    def service_config(self, service_config):
        self._service_config = service_config

    @property
    def service_environment(self):
        return self._service_environment

    @service_environment.setter
    def service_environment(self, service_environment):
        self._service_environment = service_environment

    @property
    def service_environment_file(self):
        return self._service_environment_file

    @service_environment_file.setter
    def service_environment_file(self, service_environment_file):
        self._service_environment_file = service_environment_file

    @property
    def service_exec_reload(self):
        return self._service_exec_reload

    @service_exec_reload.setter
    def service_exec_reload(self, service_exec_reload):
        self._service_exec_reload = service_exec_reload

    @property
    def service_exec_start(self):
        return self._service_exec_start

    @service_exec_start.setter
    def service_exec_start(self, service_exec_start):
        self._service_exec_start = service_exec_start

    @property
    def service_exec_start_post(self):
        return self._service_exec_start_post

    @service_exec_start_post.setter
    def service_exec_start_post(self, service_exec_start_post):
        self._service_exec_start_post = service_exec_start_post

    @property
    def service_exec_start_pre(self):
        return self._service_exec_start_pre

    @service_exec_start_pre.setter
    def service_exec_start_pre(self, service_exec_start_pre):
        self._service_exec_start_pre = service_exec_start_pre

    @property
    def service_exec_stop(self):
        return self._service_exec_stop

    @service_exec_stop.setter
    def service_exec_stop(self, service_exec_stop):
        self._service_exec_stop = service_exec_stop

    @property
    def service_exec_stop_post(self):
        return self._service_exec_stop_post

    @service_exec_stop_post.setter
    def service_exec_stop_post(self, service_exec_stop_post):
        self._service_exec_stop_post = service_exec_stop_post

    @property
    def service_group(self):
        return self._service_group

    @service_group.setter
    def service_group(self, service_group):
        self._service_group = service_group

    @property
    def service_guess_main_pid(self):
        return self._service_guess_main_pid

    @service_guess_main_pid.setter
    def service_guess_main_pid(self, service_guess_main_pid):
        self._service_guess_main_pid = service_guess_main_pid

    @property
    def service_kill_mode(self):
        return self._service_kill_mode

    @service_kill_mode.setter
    def service_kill_mode(self, service_kill_mode):
        self._service_kill_mode = service_kill_mode

    @property
    def service_kill_signal(self):
        return self._service_kill_signal

    @service_kill_signal.setter
    def service_kill_signal(self, service_kill_signal):
        self._service_kill_signal = service_kill_signal

    @property
    def service_limit(self):
        return self._service_limit

    @service_limit.setter
    def service_limit(self, service_limit):
        self._service_limit = service_limit

    @property
    def service_memory_deny_write_execute(self):
        return self._service_memory_deny_write_execute

    @service_memory_deny_write_execute.setter
    def service_memory_deny_write_execute(self, service_memory_deny_write_execute):
        self._service_memory_deny_write_execute = service_memory_deny_write_execute

    @property
    def service_no_new_privileges(self):
        return self._service_no_new_privileges

    @service_no_new_privileges.setter
    def service_no_new_privileges(self, service_no_new_privileges):
        self._service_no_new_privileges = service_no_new_privileges

    @property
    def service_permissions_start_only(self):
        return self._service_permissions_start_only

    @service_permissions_start_only.setter
    def service_permissions_start_only(self, service_permissions_start_only):
        self._service_permissions_start_only = service_permissions_start_only

    @property
    def service_pid_file(self):
        return self._service_pid_file

    @service_pid_file.setter
    def service_pid_file(self, service_pid_file):
        self._service_pid_file = service_pid_file

    @property
    def service_private_devices(self):
        return self._service_private_devices

    @service_private_devices.setter
    def service_private_devices(self, service_private_devices):
        self._service_private_devices = service_private_devices

    @property
    def service_private_tmp(self):
        return self._service_private_tmp

    @service_private_tmp.setter
    def service_private_tmp(self, service_private_tmp):
        self._service_private_tmp = service_private_tmp

    @property
    def service_protect_home(self):
        return self._service_protect_home

    @service_protect_home.setter
    def service_protect_home(self, service_protect_home):
        self._service_protect_home = service_protect_home

    @property
    def service_protect_system(self):
        return self._service_protect_system

    @service_protect_system.setter
    def service_protect_system(self, service_protect_system):
        self._service_protect_system = service_protect_system

    @property
    def service_remain_after_exit(self):
        return self._service_remain_after_exit

    @service_remain_after_exit.setter
    def service_remain_after_exit(self, service_remain_after_exit):
        self._service_remain_after_exit = service_remain_after_exit

    @property
    def service_restart(self):
        return self._service_restart

    @service_restart.setter
    def service_restart(self, service_restart):
        self._service_restart = service_restart

    @property
    def service_restart_force_exit_status(self):
        return self._service_restart_force_exit_status

    @service_restart_force_exit_status.setter
    def service_restart_force_exit_status(self, service_restart_force_exit_status):
        self._service_restart_force_exit_status = service_restart_force_exit_status

    @property
    def service_restart_sec(self):
        return self._service_restart_sec

    @service_restart_sec.setter
    def service_restart_sec(self, service_restart_sec):
        self._service_restart_sec = service_restart_sec

    @property
    def service_runtime_directory(self):
        return self._service_runtime_directory

    @service_runtime_directory.setter
    def service_runtime_directory(self, service_runtime_directory):
        self._service_runtime_directory = service_runtime_directory

    @property
    def service_runtime_directory_mode(self):
        return self._service_runtime_directory_mode

    @service_runtime_directory_mode.setter
    def service_runtime_directory_mode(self, service_runtime_directory_mode):
        self._service_runtime_directory_mode = service_runtime_directory_mode

    @property
    def service_secure_bits(self):
        return self._service_secure_bits

    @service_secure_bits.setter
    def service_secure_bits(self, service_secure_bits):
        self._service_secure_bits = service_secure_bits

    @property
    def service_success_exit_status(self):
        return self._service_success_exit_status

    @service_success_exit_status.setter
    def service_success_exit_status(self, service_success_exit_status):
        self._service_success_exit_status = service_success_exit_status

    @property
    def service_system_call_architectures(self):
        return self._service_system_call_architectures

    @service_system_call_architectures.setter
    def service_system_call_architectures(self, service_system_call_architectures):
        self._service_system_call_architectures = service_system_call_architectures

    @property
    def service_tasks_max(self):
        return self._service_tasks_max

    @service_tasks_max.setter
    def service_tasks_max(self, service_tasks_max):
        self._service_tasks_max = service_tasks_max

    @property
    def service_timeout_sec(self):
        return self._service_timeout_sec

    @service_timeout_sec.setter
    def service_timeout_sec(self, service_timeout_sec):
        self._service_timeout_sec = service_timeout_sec

    @property
    def service_timeout_start_sec(self):
        return self._service_timeout_start_sec

    @service_timeout_start_sec.setter
    def service_timeout_start_sec(self, service_timeout_start_sec):
        self._service_timeout_start_sec = service_timeout_start_sec

    @property
    def service_timeout_stop_sec(self):
        return self._service_timeout_stop_sec

    @service_timeout_stop_sec.setter
    def service_timeout_stop_sec(self, service_timeout_stop_sec):
        self._service_timeout_stop_sec = service_timeout_stop_sec

    @property
    def service_type(self):
        return self._service_type

    @service_type.setter
    def service_type(self, service_type):
        self._service_type = service_type

    @property
    def service_user(self):
        return self._service_user

    @service_user.setter
    def service_user(self, service_user):
        self._service_user = service_user

    @property
    def service_working_directory(self):
        return self._service_working_directory

    @service_working_directory.setter
    def service_working_directory(self, service_working_directory):
        self._service_working_directory = service_working_directory

    @property
    def started(self):
        return self._started

    @started.setter
    def started(self, started):
        self._started = started

    @property
    def unit_after(self):
        return self._unit_after

    @unit_after.setter
    def unit_after(self, unit_after):
        self._unit_after = unit_after

    @property
    def unit_before(self):
        return self._unit_before

    @unit_before.setter
    def unit_before(self, unit_before):
        self._unit_before = unit_before

    @property
    def unit_binds_to(self):
        return self._unit_binds_to

    @unit_binds_to.setter
    def unit_binds_to(self, unit_binds_to):
        self._unit_binds_to = unit_binds_to

    @property
    def unit_condition(self):
        return self._unit_condition

    @unit_condition.setter
    def unit_condition(self, unit_condition):
        self._unit_condition = unit_condition

    @property
    def unit_conflicts(self):
        return self._unit_conflicts

    @unit_conflicts.setter
    def unit_conflicts(self, unit_conflicts):
        self._unit_conflicts = unit_conflicts

    @property
    def unit_description(self):
        return self._unit_description

    @unit_description.setter
    def unit_description(self, unit_description):
        self._unit_description = unit_description

    @property
    def unit_documentation(self):
        return self._unit_documentation

    @unit_documentation.setter
    def unit_documentation(self, unit_documentation):
        self._unit_documentation = unit_documentation

    @property
    def unit_part_of(self):
        return self._unit_part_of

    @unit_part_of.setter
    def unit_part_of(self, unit_part_of):
        self._unit_part_of = unit_part_of

    @property
    def unit_requires(self):
        return self._unit_requires

    @unit_requires.setter
    def unit_requires(self, unit_requires):
        self._unit_requires = unit_requires

    @property
    def unit_requisite(self):
        return self._unit_requisite

    @unit_requisite.setter
    def unit_requisite(self, unit_requisite):
        self._unit_requisite = unit_requisite

    @property
    def unit_start_limit_burst(self):
        return self._unit_start_limit_burst

    @unit_start_limit_burst.setter
    def unit_start_limit_burst(self, unit_start_limit_burst):
        self._unit_start_limit_burst = unit_start_limit_burst

    @property
    def unit_start_limit_interval_sec(self):
        return self._unit_start_limit_interval_sec

    @unit_start_limit_interval_sec.setter
    def unit_start_limit_interval_sec(self, unit_start_limit_interval_sec):
        self._unit_start_limit_interval_sec = unit_start_limit_interval_sec

    @property
    def unit_wants(self):
        return self._unit_wants

    @unit_wants.setter
    def unit_wants(self, unit_wants):
        self._unit_wants = unit_wants


frecklet_class = SystemdServiceUnit
