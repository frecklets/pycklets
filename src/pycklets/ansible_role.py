# -*- coding: utf-8 -*-


#
# module path: pycklets.ansible_role.AnsibleRole
#


from pyckles import AutoPycklet


class AnsibleRole(AutoPycklet):
    """This is a generic task to execute any of the Ansible roles that are hosted on [Ansible galaxy](https://galaxy.ansible.com).

     Currently only a few basic metadata keys are supported: ``include_type``, ``become`` &``become_user``. The other ones will be added
     when necessary.

     Be aware, if the role is not in the default or community resources repository, this will only work if 'allow_remote' or 'allow_remote_roles' is set to true in the context that is used.

       Args:
         become: Whether to become another user.
         become_user: The user to become.
         include_type: n/a
         name: The role name.
         role_vars: The parameters for the role.

    """

    FRECKLET_ID = "ansible-role"

    def __init__(
        self,
        become=None,
        become_user=None,
        include_type="include",
        name=None,
        role_vars=None,
    ):

        super(AnsibleRole, self).__init__(
            var_names=["become", "become_user", "include_type", "name", "role_vars"]
        )
        self._become = become
        self._become_user = become_user
        self._include_type = include_type
        self._name = name
        self._role_vars = role_vars

    @property
    def become(self):
        return self._become

    @become.setter
    def become(self, become):
        self._become = become

    @property
    def become_user(self):
        return self._become_user

    @become_user.setter
    def become_user(self, become_user):
        self._become_user = become_user

    @property
    def include_type(self):
        return self._include_type

    @include_type.setter
    def include_type(self, include_type):
        self._include_type = include_type

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def role_vars(self):
        return self._role_vars

    @role_vars.setter
    def role_vars(self, role_vars):
        self._role_vars = role_vars


frecklet_class = AnsibleRole
