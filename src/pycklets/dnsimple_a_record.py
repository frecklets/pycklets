# -*- coding: utf-8 -*-


#
# module path: pycklets.dnsimple_a_record.DnsimpleARecord
#


from pyckles import AutoPycklet


class DnsimpleARecord(AutoPycklet):
    """Add an 'A' record to a dnsimple domain.

       Args:
         account_email: The dnsimple account email.
         api_token: A dnsimple api token.
         domain: The (main) domain name.
         ip: The ip address to point to.
         record: The sub-domain name to create (without the main domain name).
         ttl: The time to life of the record.

    """

    FRECKLET_ID = "dnsimple-a-record"

    def __init__(
        self,
        account_email=None,
        api_token=None,
        domain=None,
        ip=None,
        record=None,
        ttl=None,
    ):

        super(DnsimpleARecord, self).__init__(
            var_names=["account_email", "api_token", "domain", "ip", "record", "ttl"]
        )
        self._account_email = account_email
        self._api_token = api_token
        self._domain = domain
        self._ip = ip
        self._record = record
        self._ttl = ttl

    @property
    def account_email(self):
        return self._account_email

    @account_email.setter
    def account_email(self, account_email):
        self._account_email = account_email

    @property
    def api_token(self):
        return self._api_token

    @api_token.setter
    def api_token(self, api_token):
        self._api_token = api_token

    @property
    def domain(self):
        return self._domain

    @domain.setter
    def domain(self, domain):
        self._domain = domain

    @property
    def ip(self):
        return self._ip

    @ip.setter
    def ip(self, ip):
        self._ip = ip

    @property
    def record(self):
        return self._record

    @record.setter
    def record(self, record):
        self._record = record

    @property
    def ttl(self):
        return self._ttl

    @ttl.setter
    def ttl(self, ttl):
        self._ttl = ttl


frecklet_class = DnsimpleARecord
