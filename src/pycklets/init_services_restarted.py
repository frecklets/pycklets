# -*- coding: utf-8 -*-


#
# module path: pycklets.init_services_restarted.InitServicesRestarted
#


from pyckles import AutoPycklet


class InitServicesRestarted(AutoPycklet):
    """Make sure a set of init services are restarted.

       Args:
         services: A list of services to restart.

    """

    FRECKLET_ID = "init-services-restarted"

    def __init__(self, services=None):

        super(InitServicesRestarted, self).__init__(var_names=["services"])
        self._services = services

    @property
    def services(self):
        return self._services

    @services.setter
    def services(self, services):
        self._services = services


frecklet_class = InitServicesRestarted
