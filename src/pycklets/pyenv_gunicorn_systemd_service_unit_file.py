# -*- coding: utf-8 -*-


#
# module path: pycklets.pyenv_gunicorn_systemd_service_unit_file.PyenvGunicornSystemdServiceUnitFile
#


from pyckles import AutoPycklet


class PyenvGunicornSystemdServiceUnitFile(AutoPycklet):
    """No documentation available.

       Args:
         app_module: The app entry point.
         enabled: Whether to enable the service.
         group: The group who owns/runs the virtualenv.
         gunicorn_config: The gunicorn config.
         port: The port the gunicorn should listen to.
         service_config: Environment variables to configure the app.
         started: Whether to start the service.
         user: The user who owns/runs the virtualenv.
         venv_name: The name of the Pyenv virtualenv to use.
         working_directory: The working directory of the service.

    """

    FRECKLET_ID = "pyenv-gunicorn-systemd-service-unit-file"

    def __init__(
        self,
        app_module=None,
        enabled=None,
        group=None,
        gunicorn_config=None,
        port=8088,
        service_config=None,
        started=None,
        user=None,
        venv_name=None,
        working_directory=None,
    ):

        super(PyenvGunicornSystemdServiceUnitFile, self).__init__(
            var_names=[
                "app_module",
                "enabled",
                "group",
                "gunicorn_config",
                "port",
                "service_config",
                "started",
                "user",
                "venv_name",
                "working_directory",
            ]
        )
        self._app_module = app_module
        self._enabled = enabled
        self._group = group
        self._gunicorn_config = gunicorn_config
        self._port = port
        self._service_config = service_config
        self._started = started
        self._user = user
        self._venv_name = venv_name
        self._working_directory = working_directory

    @property
    def app_module(self):
        return self._app_module

    @app_module.setter
    def app_module(self, app_module):
        self._app_module = app_module

    @property
    def enabled(self):
        return self._enabled

    @enabled.setter
    def enabled(self, enabled):
        self._enabled = enabled

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def gunicorn_config(self):
        return self._gunicorn_config

    @gunicorn_config.setter
    def gunicorn_config(self, gunicorn_config):
        self._gunicorn_config = gunicorn_config

    @property
    def port(self):
        return self._port

    @port.setter
    def port(self, port):
        self._port = port

    @property
    def service_config(self):
        return self._service_config

    @service_config.setter
    def service_config(self, service_config):
        self._service_config = service_config

    @property
    def started(self):
        return self._started

    @started.setter
    def started(self, started):
        self._started = started

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user

    @property
    def venv_name(self):
        return self._venv_name

    @venv_name.setter
    def venv_name(self, venv_name):
        self._venv_name = venv_name

    @property
    def working_directory(self):
        return self._working_directory

    @working_directory.setter
    def working_directory(self, working_directory):
        self._working_directory = working_directory


frecklet_class = PyenvGunicornSystemdServiceUnitFile
