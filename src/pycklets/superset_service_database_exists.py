# -*- coding: utf-8 -*-


#
# module path: pycklets.superset_service_database_exists.SupersetServiceDatabaseExists
#


from pyckles import AutoPycklet


class SupersetServiceDatabaseExists(AutoPycklet):
    """Create a Postgresql database for Superset.

       Args:
         db_host_ip: The db host ip address.
         db_name: the db name.
         db_password: The db password.
         db_user: The db username.

    """

    FRECKLET_ID = "superset-service-database-exists"

    def __init__(
        self,
        db_host_ip="127.0.0.1",
        db_name="superset",
        db_password=None,
        db_user="superset",
    ):

        super(SupersetServiceDatabaseExists, self).__init__(
            var_names=["db_host_ip", "db_name", "db_password", "db_user"]
        )
        self._db_host_ip = db_host_ip
        self._db_name = db_name
        self._db_password = db_password
        self._db_user = db_user

    @property
    def db_host_ip(self):
        return self._db_host_ip

    @db_host_ip.setter
    def db_host_ip(self, db_host_ip):
        self._db_host_ip = db_host_ip

    @property
    def db_name(self):
        return self._db_name

    @db_name.setter
    def db_name(self, db_name):
        self._db_name = db_name

    @property
    def db_password(self):
        return self._db_password

    @db_password.setter
    def db_password(self, db_password):
        self._db_password = db_password

    @property
    def db_user(self):
        return self._db_user

    @db_user.setter
    def db_user(self, db_user):
        self._db_user = db_user


frecklet_class = SupersetServiceDatabaseExists
