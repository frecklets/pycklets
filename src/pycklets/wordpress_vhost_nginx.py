# -*- coding: utf-8 -*-


#
# module path: pycklets.wordpress_vhost_nginx.WordpressVhostNginx
#


from pyckles import AutoPycklet


class WordpressVhostNginx(AutoPycklet):
    """Create Nginx wordpress virtual host config.

       Args:
         base_path: The wordpress project folders parent directory.
         disable_ipv6: Whether to disable ipv6 for this server block.
         host: The hostname of the server.
         server_admin: The email address to use in the vhost file and with letsencrypt, falls back to 'wp_admin_email.
         use_https: Request a lets-encrypt certificate and serve devpi via https (needs 'server_admin' or 'wp_admin_email' set).
         wp_title: The name of the wordpress instance.

    """

    FRECKLET_ID = "wordpress-vhost-nginx"

    def __init__(
        self,
        base_path="/var/www",
        disable_ipv6=None,
        host="localhost",
        server_admin=None,
        use_https=None,
        wp_title=None,
    ):

        super(WordpressVhostNginx, self).__init__(
            var_names=[
                "base_path",
                "disable_ipv6",
                "host",
                "server_admin",
                "use_https",
                "wp_title",
            ]
        )
        self._base_path = base_path
        self._disable_ipv6 = disable_ipv6
        self._host = host
        self._server_admin = server_admin
        self._use_https = use_https
        self._wp_title = wp_title

    @property
    def base_path(self):
        return self._base_path

    @base_path.setter
    def base_path(self, base_path):
        self._base_path = base_path

    @property
    def disable_ipv6(self):
        return self._disable_ipv6

    @disable_ipv6.setter
    def disable_ipv6(self, disable_ipv6):
        self._disable_ipv6 = disable_ipv6

    @property
    def host(self):
        return self._host

    @host.setter
    def host(self, host):
        self._host = host

    @property
    def server_admin(self):
        return self._server_admin

    @server_admin.setter
    def server_admin(self, server_admin):
        self._server_admin = server_admin

    @property
    def use_https(self):
        return self._use_https

    @use_https.setter
    def use_https(self, use_https):
        self._use_https = use_https

    @property
    def wp_title(self):
        return self._wp_title

    @wp_title.setter
    def wp_title(self, wp_title):
        self._wp_title = wp_title


frecklet_class = WordpressVhostNginx
