# -*- coding: utf-8 -*-


#
# module path: pycklets.homebrew_pkg_mgr.HomebrewPkgMgr
#


from pyckles import AutoPycklet


class HomebrewPkgMgr(AutoPycklet):
    """Install the [homebrew](https://brew.sh) package manager on Mac OS X.

     If not already available, the [OS X command-line tools package](https://developer.apple.com/library/archive/technotes/tn2339/_index.html) will also be installed.

       Args:

    """

    FRECKLET_ID = "homebrew-pkg-mgr"

    def __init__(self,):

        super(HomebrewPkgMgr, self).__init__(var_names=[])


frecklet_class = HomebrewPkgMgr
