# -*- coding: utf-8 -*-


#
# module path: pycklets.path_is_owned_by.PathIsOwnedBy
#


from pyckles import AutoPycklet


class PathIsOwnedBy(AutoPycklet):
    """Make sure a file/folder has a certain owner/group.

     This will recursively apply the owner/group change in case the path is a directory.
     If the path does not exist, an empty file will be created.

     Root/sudo permissions will be used to do the chown.

     If the owner/group does not exist on the machine, this will create them before changing the target ownership.

       Args:
         group: the group of the file/folder
         owner: the owner of the file/folder
         path: the path in question
         recursive: Whether to apply the changes recursively (if folder).
         system_user: Whether the user and group should be of system user/group type.

    """

    FRECKLET_ID = "path-is-owned-by"

    def __init__(
        self, group=None, owner=None, path=None, recursive=None, system_user=None
    ):

        super(PathIsOwnedBy, self).__init__(
            var_names=["group", "owner", "path", "recursive", "system_user"]
        )
        self._group = group
        self._owner = owner
        self._path = path
        self._recursive = recursive
        self._system_user = system_user

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path

    @property
    def recursive(self):
        return self._recursive

    @recursive.setter
    def recursive(self, recursive):
        self._recursive = recursive

    @property
    def system_user(self):
        return self._system_user

    @system_user.setter
    def system_user(self, system_user):
        self._system_user = system_user


frecklet_class = PathIsOwnedBy
