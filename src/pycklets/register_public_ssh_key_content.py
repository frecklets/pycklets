# -*- coding: utf-8 -*-


#
# module path: pycklets.register_public_ssh_key_content.RegisterPublicSshKeyContent
#


from pyckles import AutoPycklet


class RegisterPublicSshKeyContent(AutoPycklet):
    """Registers the content of a public ssh key to a variable.

     Either the 'path' or the 'key_content' argument must be provided

       Args:
         key_content: The content of the ssh key.
         path: The (absolute) path to the public ssh key.
         register_name: The name of the key to register the public key content under.
         user: The name of the user who owns the ssh key.

    """

    FRECKLET_ID = "register-public-ssh-key-content"

    def __init__(self, key_content=None, path=None, register_name=None, user=None):

        super(RegisterPublicSshKeyContent, self).__init__(
            var_names=["key_content", "path", "register_name", "user"]
        )
        self._key_content = key_content
        self._path = path
        self._register_name = register_name
        self._user = user

    @property
    def key_content(self):
        return self._key_content

    @key_content.setter
    def key_content(self, key_content):
        self._key_content = key_content

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path

    @property
    def register_name(self):
        return self._register_name

    @register_name.setter
    def register_name(self, register_name):
        self._register_name = register_name

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user


frecklet_class = RegisterPublicSshKeyContent
