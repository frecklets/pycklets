# -*- coding: utf-8 -*-


#
# module path: pycklets.conda_pkg_mgr.CondaPkgMgr
#


from pyckles import AutoPycklet


class CondaPkgMgr(AutoPycklet):
    """Install the conda package manager.

     No root permissions are required for this as long as the ``bzip2`` system package is available.

     If you specify a different 'user', it's currently not possible to add conda init code to that users
     '.profile'. You need to manually set 'no_add_to_path' to true to not get an error

       Args:
         init_files: Files to add a 'PATH=...' directive to.
         no_add_to_path: Don't add conda to PATH in init files.
         user: The user to install conda for.

    """

    FRECKLET_ID = "conda-pkg-mgr"

    def __init__(
        self,
        init_files=["{{ ansible_env.HOME }}/.profile"],
        no_add_to_path=None,
        user=None,
    ):

        super(CondaPkgMgr, self).__init__(
            var_names=["init_files", "no_add_to_path", "user"]
        )
        self._init_files = init_files
        self._no_add_to_path = no_add_to_path
        self._user = user

    @property
    def init_files(self):
        return self._init_files

    @init_files.setter
    def init_files(self, init_files):
        self._init_files = init_files

    @property
    def no_add_to_path(self):
        return self._no_add_to_path

    @no_add_to_path.setter
    def no_add_to_path(self, no_add_to_path):
        self._no_add_to_path = no_add_to_path

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user


frecklet_class = CondaPkgMgr
