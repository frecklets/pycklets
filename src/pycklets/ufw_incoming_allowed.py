# -*- coding: utf-8 -*-


#
# module path: pycklets.ufw_incoming_allowed.UfwIncomingAllowed
#


from pyckles import AutoPycklet


class UfwIncomingAllowed(AutoPycklet):
    """Configure ufw to allow incoming traffic that fits certain criteria.

     This frecklet will also install 'ufw' if it is not already present, but it won't enable it if it is not already.
     Make sure you have a rule to let you back in if you do that!

       Args:
         comment: A comment to the rule.
         from_ip: The source of the traffic to allow.
         from_port: The source port of the traffic to allow.
         interface: The name of the interface.
         protocol: The protocol.
         to_ip: Destination IP address.
         to_port: The destination port of the traffic to allow.

    """

    FRECKLET_ID = "ufw-incoming-allowed"

    def __init__(
        self,
        comment=None,
        from_ip="any",
        from_port=None,
        interface=None,
        protocol="any",
        to_ip="any",
        to_port=None,
    ):

        super(UfwIncomingAllowed, self).__init__(
            var_names=[
                "comment",
                "from_ip",
                "from_port",
                "interface",
                "protocol",
                "to_ip",
                "to_port",
            ]
        )
        self._comment = comment
        self._from_ip = from_ip
        self._from_port = from_port
        self._interface = interface
        self._protocol = protocol
        self._to_ip = to_ip
        self._to_port = to_port

    @property
    def comment(self):
        return self._comment

    @comment.setter
    def comment(self, comment):
        self._comment = comment

    @property
    def from_ip(self):
        return self._from_ip

    @from_ip.setter
    def from_ip(self, from_ip):
        self._from_ip = from_ip

    @property
    def from_port(self):
        return self._from_port

    @from_port.setter
    def from_port(self, from_port):
        self._from_port = from_port

    @property
    def interface(self):
        return self._interface

    @interface.setter
    def interface(self, interface):
        self._interface = interface

    @property
    def protocol(self):
        return self._protocol

    @protocol.setter
    def protocol(self, protocol):
        self._protocol = protocol

    @property
    def to_ip(self):
        return self._to_ip

    @to_ip.setter
    def to_ip(self, to_ip):
        self._to_ip = to_ip

    @property
    def to_port(self):
        return self._to_port

    @to_port.setter
    def to_port(self, to_port):
        self._to_port = to_port


frecklet_class = UfwIncomingAllowed
