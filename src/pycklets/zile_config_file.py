# -*- coding: utf-8 -*-


#
# module path: pycklets.zile_config_file.ZileConfigFile
#


from pyckles import AutoPycklet


class ZileConfigFile(AutoPycklet):
    """Configuration for the 'zile' text editor

       Args:
         auto_fill_mode: enable auto-filll-mode
         backup_directory: the directory for backup files, which must exist
         case_fold_search: 'true' means searches ignore case
         case_replace: 'true' means 'query-replace' shold preserve case in replacements
         fill_column: column beyond which automatic line-wrapping should happen
         group: The group of the file.
         highlight_nonselected_windows: if 'true', highlight region even in nonselected windows
         indent_tabs_mode: use tabs or whitespaces
         inhabit_splash_screen: whether to inhibit the startup screen
         key_bindings: Rebind keys.
         kill_whole_line: whether 'kill-line' with no arg starts at the beginning of line
         make_backup_files: 'true' means make a backup of a file the first time it is saved.
         mode: The permissions of the file.
         owner: The owner of the file.
         path: The path to the file.
         ring_bell: 'true' means ring the terminal bell on any error
         standard_indent: default number of columns for margin-changing functions to indent
         tab_always_indent: controls the operation of the TAB key
         tab_width: distance between tab stops

    """

    FRECKLET_ID = "zile-config-file"

    def __init__(
        self,
        auto_fill_mode=None,
        backup_directory="nil",
        case_fold_search=True,
        case_replace=True,
        fill_column=70,
        group=None,
        highlight_nonselected_windows=None,
        indent_tabs_mode=True,
        inhabit_splash_screen=None,
        key_bindings=None,
        kill_whole_line=None,
        make_backup_files=True,
        mode=None,
        owner=None,
        path=None,
        ring_bell=True,
        standard_indent=4,
        tab_always_indent=True,
        tab_width=8,
    ):

        super(ZileConfigFile, self).__init__(
            var_names=[
                "auto_fill_mode",
                "backup_directory",
                "case_fold_search",
                "case_replace",
                "fill_column",
                "group",
                "highlight_nonselected_windows",
                "indent_tabs_mode",
                "inhabit_splash_screen",
                "key_bindings",
                "kill_whole_line",
                "make_backup_files",
                "mode",
                "owner",
                "path",
                "ring_bell",
                "standard_indent",
                "tab_always_indent",
                "tab_width",
            ]
        )
        self._auto_fill_mode = auto_fill_mode
        self._backup_directory = backup_directory
        self._case_fold_search = case_fold_search
        self._case_replace = case_replace
        self._fill_column = fill_column
        self._group = group
        self._highlight_nonselected_windows = highlight_nonselected_windows
        self._indent_tabs_mode = indent_tabs_mode
        self._inhabit_splash_screen = inhabit_splash_screen
        self._key_bindings = key_bindings
        self._kill_whole_line = kill_whole_line
        self._make_backup_files = make_backup_files
        self._mode = mode
        self._owner = owner
        self._path = path
        self._ring_bell = ring_bell
        self._standard_indent = standard_indent
        self._tab_always_indent = tab_always_indent
        self._tab_width = tab_width

    @property
    def auto_fill_mode(self):
        return self._auto_fill_mode

    @auto_fill_mode.setter
    def auto_fill_mode(self, auto_fill_mode):
        self._auto_fill_mode = auto_fill_mode

    @property
    def backup_directory(self):
        return self._backup_directory

    @backup_directory.setter
    def backup_directory(self, backup_directory):
        self._backup_directory = backup_directory

    @property
    def case_fold_search(self):
        return self._case_fold_search

    @case_fold_search.setter
    def case_fold_search(self, case_fold_search):
        self._case_fold_search = case_fold_search

    @property
    def case_replace(self):
        return self._case_replace

    @case_replace.setter
    def case_replace(self, case_replace):
        self._case_replace = case_replace

    @property
    def fill_column(self):
        return self._fill_column

    @fill_column.setter
    def fill_column(self, fill_column):
        self._fill_column = fill_column

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def highlight_nonselected_windows(self):
        return self._highlight_nonselected_windows

    @highlight_nonselected_windows.setter
    def highlight_nonselected_windows(self, highlight_nonselected_windows):
        self._highlight_nonselected_windows = highlight_nonselected_windows

    @property
    def indent_tabs_mode(self):
        return self._indent_tabs_mode

    @indent_tabs_mode.setter
    def indent_tabs_mode(self, indent_tabs_mode):
        self._indent_tabs_mode = indent_tabs_mode

    @property
    def inhabit_splash_screen(self):
        return self._inhabit_splash_screen

    @inhabit_splash_screen.setter
    def inhabit_splash_screen(self, inhabit_splash_screen):
        self._inhabit_splash_screen = inhabit_splash_screen

    @property
    def key_bindings(self):
        return self._key_bindings

    @key_bindings.setter
    def key_bindings(self, key_bindings):
        self._key_bindings = key_bindings

    @property
    def kill_whole_line(self):
        return self._kill_whole_line

    @kill_whole_line.setter
    def kill_whole_line(self, kill_whole_line):
        self._kill_whole_line = kill_whole_line

    @property
    def make_backup_files(self):
        return self._make_backup_files

    @make_backup_files.setter
    def make_backup_files(self, make_backup_files):
        self._make_backup_files = make_backup_files

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, mode):
        self._mode = mode

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path

    @property
    def ring_bell(self):
        return self._ring_bell

    @ring_bell.setter
    def ring_bell(self, ring_bell):
        self._ring_bell = ring_bell

    @property
    def standard_indent(self):
        return self._standard_indent

    @standard_indent.setter
    def standard_indent(self, standard_indent):
        self._standard_indent = standard_indent

    @property
    def tab_always_indent(self):
        return self._tab_always_indent

    @tab_always_indent.setter
    def tab_always_indent(self, tab_always_indent):
        self._tab_always_indent = tab_always_indent

    @property
    def tab_width(self):
        return self._tab_width

    @tab_width.setter
    def tab_width(self, tab_width):
        self._tab_width = tab_width


frecklet_class = ZileConfigFile
