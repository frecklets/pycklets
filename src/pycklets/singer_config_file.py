# -*- coding: utf-8 -*-


#
# module path: pycklets.singer_config_file.SingerConfigFile
#


from pyckles import AutoPycklet


class SingerConfigFile(AutoPycklet):
    """Write a config file for a Singer tap.

       Args:
         config: The config dictioniary.
         path: The path to the config file.

    """

    FRECKLET_ID = "singer-config-file"

    def __init__(self, config=None, path=None):

        super(SingerConfigFile, self).__init__(var_names=["config", "path"])
        self._config = config
        self._path = path

    @property
    def config(self):
        return self._config

    @config.setter
    def config(self, config):
        self._config = config

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path


frecklet_class = SingerConfigFile
