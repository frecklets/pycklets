# -*- coding: utf-8 -*-


#
# module path: pycklets.container_image_from_frecklet_file.ContainerImageFromFreckletFile
#


from pyckles import AutoPycklet


class ContainerImageFromFreckletFile(AutoPycklet):
    """This is a convenience wrapper around the [``container-image-from-frecklet``](https://freckles.io/frecklets/default/virtualization/container-image-from-frecklet) *frecklet*, it sets a few command-line options so that the wrapped *frecklet* can use a frecklet *file* instead of a *frecklet* that needs to be in the default context.

       Args:
         freckles_extra_args: Extra base args for the freckles run.
         frecklet_path: The (absolute!) path to the frecklet file.
         frecklet_vars: The frecklet vars.
         image_name: The image name.
         image_type: The container image type.
         install_packer: Install Packer into '$HOME/.local/bin', if not already available.
         source_image: The name of the source/parent image.

    """

    FRECKLET_ID = "container-image-from-frecklet-file"

    def __init__(
        self,
        freckles_extra_args=None,
        frecklet_path=None,
        frecklet_vars=None,
        image_name=None,
        image_type="docker",
        install_packer=None,
        source_image=None,
    ):

        super(ContainerImageFromFreckletFile, self).__init__(
            var_names=[
                "freckles_extra_args",
                "frecklet_path",
                "frecklet_vars",
                "image_name",
                "image_type",
                "install_packer",
                "source_image",
            ]
        )
        self._freckles_extra_args = freckles_extra_args
        self._frecklet_path = frecklet_path
        self._frecklet_vars = frecklet_vars
        self._image_name = image_name
        self._image_type = image_type
        self._install_packer = install_packer
        self._source_image = source_image

    @property
    def freckles_extra_args(self):
        return self._freckles_extra_args

    @freckles_extra_args.setter
    def freckles_extra_args(self, freckles_extra_args):
        self._freckles_extra_args = freckles_extra_args

    @property
    def frecklet_path(self):
        return self._frecklet_path

    @frecklet_path.setter
    def frecklet_path(self, frecklet_path):
        self._frecklet_path = frecklet_path

    @property
    def frecklet_vars(self):
        return self._frecklet_vars

    @frecklet_vars.setter
    def frecklet_vars(self, frecklet_vars):
        self._frecklet_vars = frecklet_vars

    @property
    def image_name(self):
        return self._image_name

    @image_name.setter
    def image_name(self, image_name):
        self._image_name = image_name

    @property
    def image_type(self):
        return self._image_type

    @image_type.setter
    def image_type(self, image_type):
        self._image_type = image_type

    @property
    def install_packer(self):
        return self._install_packer

    @install_packer.setter
    def install_packer(self, install_packer):
        self._install_packer = install_packer

    @property
    def source_image(self):
        return self._source_image

    @source_image.setter
    def source_image(self, source_image):
        self._source_image = source_image


frecklet_class = ContainerImageFromFreckletFile
