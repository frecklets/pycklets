# -*- coding: utf-8 -*-


#
# module path: pycklets.nomad_server_config_file.NomadServerConfigFile
#


from pyckles import AutoPycklet


class NomadServerConfigFile(AutoPycklet):
    """Nomad server configuration.

       Args:
         bootstrap_expect: This flag provides the number of expected servers in the datacenter.
         group: The group of the file.
         mode: The permissions of the file.
         owner: The owner of the file.
         path: The path to the file.

    """

    FRECKLET_ID = "nomad-server-config-file"

    def __init__(
        self, bootstrap_expect=None, group=None, mode=None, owner=None, path=None
    ):

        super(NomadServerConfigFile, self).__init__(
            var_names=["bootstrap_expect", "group", "mode", "owner", "path"]
        )
        self._bootstrap_expect = bootstrap_expect
        self._group = group
        self._mode = mode
        self._owner = owner
        self._path = path

    @property
    def bootstrap_expect(self):
        return self._bootstrap_expect

    @bootstrap_expect.setter
    def bootstrap_expect(self, bootstrap_expect):
        self._bootstrap_expect = bootstrap_expect

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, mode):
        self._mode = mode

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path


frecklet_class = NomadServerConfigFile
