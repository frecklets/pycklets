# -*- coding: utf-8 -*-


#
# module path: pycklets.frecklet_file.FreckletFile
#


from pyckles import AutoPycklet


class FreckletFile(AutoPycklet):
    """A minimal (single task) frecklet.

       Args:
         frecklet_name: The name of the frecklet.
         frecklet_vars: The frecklet vars.
         group: The group of the file.
         mode: The permissions of the file.
         owner: The owner of the file.
         path: The path to the file.

    """

    FRECKLET_ID = "frecklet-file"

    def __init__(
        self,
        frecklet_name=None,
        frecklet_vars=None,
        group=None,
        mode=None,
        owner=None,
        path=None,
    ):

        super(FreckletFile, self).__init__(
            var_names=[
                "frecklet_name",
                "frecklet_vars",
                "group",
                "mode",
                "owner",
                "path",
            ]
        )
        self._frecklet_name = frecklet_name
        self._frecklet_vars = frecklet_vars
        self._group = group
        self._mode = mode
        self._owner = owner
        self._path = path

    @property
    def frecklet_name(self):
        return self._frecklet_name

    @frecklet_name.setter
    def frecklet_name(self, frecklet_name):
        self._frecklet_name = frecklet_name

    @property
    def frecklet_vars(self):
        return self._frecklet_vars

    @frecklet_vars.setter
    def frecklet_vars(self, frecklet_vars):
        self._frecklet_vars = frecklet_vars

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, mode):
        self._mode = mode

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path


frecklet_class = FreckletFile
