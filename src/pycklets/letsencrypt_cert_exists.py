# -*- coding: utf-8 -*-


#
# module path: pycklets.letsencrypt_cert_exists.LetsencryptCertExists
#


from pyckles import AutoPycklet


class LetsencryptCertExists(AutoPycklet):
    """Request and setup a lets-encrypt certificate for a hostname.

     This also creates a cron-job that monitors the certificate for
     expiration, and re-news it if necessary.

     If ``webserver_service_name`` is provided, that service is stopped before cert request, and restarted after.

       Args:
         document_root: The webroot path for the webserver (check underlying role for details).
         domain_names: The hostname(s).
         email: The email address to use with the letsencrypt service.
         letsencrypt_staging: Whether to use the letsencrypt staging server instead of production.
         renewal_command: The command to use for renewal in the cron job (check underlying role for details).
         webserver_service_name: The webserver service name, to be able to stop the service before cert request.

    """

    FRECKLET_ID = "letsencrypt-cert-exists"

    def __init__(
        self,
        document_root="/var/www",
        domain_names=None,
        email=None,
        letsencrypt_staging=None,
        renewal_command=None,
        webserver_service_name=None,
    ):

        super(LetsencryptCertExists, self).__init__(
            var_names=[
                "document_root",
                "domain_names",
                "email",
                "letsencrypt_staging",
                "renewal_command",
                "webserver_service_name",
            ]
        )
        self._document_root = document_root
        self._domain_names = domain_names
        self._email = email
        self._letsencrypt_staging = letsencrypt_staging
        self._renewal_command = renewal_command
        self._webserver_service_name = webserver_service_name

    @property
    def document_root(self):
        return self._document_root

    @document_root.setter
    def document_root(self, document_root):
        self._document_root = document_root

    @property
    def domain_names(self):
        return self._domain_names

    @domain_names.setter
    def domain_names(self, domain_names):
        self._domain_names = domain_names

    @property
    def email(self):
        return self._email

    @email.setter
    def email(self, email):
        self._email = email

    @property
    def letsencrypt_staging(self):
        return self._letsencrypt_staging

    @letsencrypt_staging.setter
    def letsencrypt_staging(self, letsencrypt_staging):
        self._letsencrypt_staging = letsencrypt_staging

    @property
    def renewal_command(self):
        return self._renewal_command

    @renewal_command.setter
    def renewal_command(self, renewal_command):
        self._renewal_command = renewal_command

    @property
    def webserver_service_name(self):
        return self._webserver_service_name

    @webserver_service_name.setter
    def webserver_service_name(self, webserver_service_name):
        self._webserver_service_name = webserver_service_name


frecklet_class = LetsencryptCertExists
