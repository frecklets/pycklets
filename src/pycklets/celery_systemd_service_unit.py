# -*- coding: utf-8 -*-


#
# module path: pycklets.celery_systemd_service_unit.CelerySystemdServiceUnit
#


from pyckles import AutoPycklet


class CelerySystemdServiceUnit(AutoPycklet):
    """Systemd service unit for Celery workers.

       Args:
         celery_bin: n/a
         celery_entrypoint: n/a
         env_files: Additional systemd environment configuration files.
         user: The user to run celery.

    """

    FRECKLET_ID = "celery-systemd-service-unit"

    def __init__(
        self, celery_bin=None, celery_entrypoint=None, env_files=None, user="celery"
    ):

        super(CelerySystemdServiceUnit, self).__init__(
            var_names=["celery_bin", "celery_entrypoint", "env_files", "user"]
        )
        self._celery_bin = celery_bin
        self._celery_entrypoint = celery_entrypoint
        self._env_files = env_files
        self._user = user

    @property
    def celery_bin(self):
        return self._celery_bin

    @celery_bin.setter
    def celery_bin(self, celery_bin):
        self._celery_bin = celery_bin

    @property
    def celery_entrypoint(self):
        return self._celery_entrypoint

    @celery_entrypoint.setter
    def celery_entrypoint(self, celery_entrypoint):
        self._celery_entrypoint = celery_entrypoint

    @property
    def env_files(self):
        return self._env_files

    @env_files.setter
    def env_files(self, env_files):
        self._env_files = env_files

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user


frecklet_class = CelerySystemdServiceUnit
