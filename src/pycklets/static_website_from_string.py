# -*- coding: utf-8 -*-


#
# module path: pycklets.static_website_from_string.StaticWebsiteFromString
#


from pyckles import AutoPycklet


class StaticWebsiteFromString(AutoPycklet):
    """Install and configure webserver to serve single webpage from string.

     This uses Nginx as a webserver.

       Args:
         content: The webpage content (in html format).
         default_server: Whether this server is the 'default' (catchall) server.
         hostname: The domain name the webserver should listen on.
         server_admin: The server admin email.
         use_https: Whether to use https (and request a letsencrypt certificate).

    """

    FRECKLET_ID = "static-website-from-string"

    def __init__(
        self,
        content=None,
        default_server=True,
        hostname=None,
        server_admin=None,
        use_https=None,
    ):

        super(StaticWebsiteFromString, self).__init__(
            var_names=[
                "content",
                "default_server",
                "hostname",
                "server_admin",
                "use_https",
            ]
        )
        self._content = content
        self._default_server = default_server
        self._hostname = hostname
        self._server_admin = server_admin
        self._use_https = use_https

    @property
    def content(self):
        return self._content

    @content.setter
    def content(self, content):
        self._content = content

    @property
    def default_server(self):
        return self._default_server

    @default_server.setter
    def default_server(self, default_server):
        self._default_server = default_server

    @property
    def hostname(self):
        return self._hostname

    @hostname.setter
    def hostname(self, hostname):
        self._hostname = hostname

    @property
    def server_admin(self):
        return self._server_admin

    @server_admin.setter
    def server_admin(self, server_admin):
        self._server_admin = server_admin

    @property
    def use_https(self):
        return self._use_https

    @use_https.setter
    def use_https(self, use_https):
        self._use_https = use_https


frecklet_class = StaticWebsiteFromString
