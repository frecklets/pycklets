# -*- coding: utf-8 -*-


#
# module path: pycklets.register_var.RegisterVar
#


from pyckles import AutoPycklet


class RegisterVar(AutoPycklet):
    """Registers the content of an (internal) Ansible variable.

       Args:
         register_name: n/a
         var_name: The Ansible variable name to debug

    """

    FRECKLET_ID = "register-var"

    def __init__(self, register_name=None, var_name=None):

        super(RegisterVar, self).__init__(var_names=["register_name", "var_name"])
        self._register_name = register_name
        self._var_name = var_name

    @property
    def register_name(self):
        return self._register_name

    @register_name.setter
    def register_name(self, register_name):
        self._register_name = register_name

    @property
    def var_name(self):
        return self._var_name

    @var_name.setter
    def var_name(self, var_name):
        self._var_name = var_name


frecklet_class = RegisterVar
