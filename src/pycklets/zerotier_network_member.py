# -*- coding: utf-8 -*-


#
# module path: pycklets.zerotier_network_member.ZerotierNetworkMember
#


from pyckles import AutoPycklet


class ZerotierNetworkMember(AutoPycklet):
    """Installs the [ZeroTier client](https://www.zerotier.com/download.shtml), adds a service init job (systemd, etc) so it will start on boot, and joins the network with the specified id.

     If the 'access_token' is provided, this auto-registers the new machine via the ZeroTier API and sets an (optionally provided) static ip address.

     If no 'network_id' is provided, only the zerotier client will be installed, nothing else.

       Args:
         access_token: The Zerotier access token.
         description: The description for this server.
         ips: The IP-addresses to assign.
         network_id: The Zerotier network id.
         short_hostname: Whether to register the short hostname without FQDN)

    """

    FRECKLET_ID = "zerotier-network-member"

    def __init__(
        self,
        access_token=None,
        description=None,
        ips=None,
        network_id=None,
        short_hostname=None,
    ):

        super(ZerotierNetworkMember, self).__init__(
            var_names=[
                "access_token",
                "description",
                "ips",
                "network_id",
                "short_hostname",
            ]
        )
        self._access_token = access_token
        self._description = description
        self._ips = ips
        self._network_id = network_id
        self._short_hostname = short_hostname

    @property
    def access_token(self):
        return self._access_token

    @access_token.setter
    def access_token(self, access_token):
        self._access_token = access_token

    @property
    def description(self):
        return self._description

    @description.setter
    def description(self, description):
        self._description = description

    @property
    def ips(self):
        return self._ips

    @ips.setter
    def ips(self, ips):
        self._ips = ips

    @property
    def network_id(self):
        return self._network_id

    @network_id.setter
    def network_id(self, network_id):
        self._network_id = network_id

    @property
    def short_hostname(self):
        return self._short_hostname

    @short_hostname.setter
    def short_hostname(self, short_hostname):
        self._short_hostname = short_hostname


frecklet_class = ZerotierNetworkMember
