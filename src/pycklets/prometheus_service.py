# -*- coding: utf-8 -*-


#
# module path: pycklets.prometheus_service.PrometheusService
#


from pyckles import AutoPycklet


class PrometheusService(AutoPycklet):
    """Installs the Prometheus monitoring service.

       Args:
         alert_relabel_configs: Alert relabeling rules.
         alert_rules: Full list of alerting rules.
         alertmanager_config: Configuration responsible for pointing where alertmanagers are.
         config_flags_extra: Additional configuration flags passed to prometheus binary at startup.
         external_labels: Additional labels.
         global_config: Prometheus global config.
         remote_read: Remote read.
         remote_write: Remote write.
         scrape_configs: Prometheus scrape jobs.
         storage_retention: Data retention period.
         targets: Targets which will be scraped.
         version: The version of Prometheus.
         web_external_url: External address on which prometheus is available.
         web_listen_address: Address on which prometheus will be listening.

    """

    FRECKLET_ID = "prometheus-service"

    def __init__(
        self,
        alert_relabel_configs=None,
        alert_rules=None,
        alertmanager_config=None,
        config_flags_extra=None,
        external_labels=None,
        global_config=None,
        remote_read=None,
        remote_write=None,
        scrape_configs=None,
        storage_retention=None,
        targets=None,
        version=None,
        web_external_url=None,
        web_listen_address=None,
    ):

        super(PrometheusService, self).__init__(
            var_names=[
                "alert_relabel_configs",
                "alert_rules",
                "alertmanager_config",
                "config_flags_extra",
                "external_labels",
                "global_config",
                "remote_read",
                "remote_write",
                "scrape_configs",
                "storage_retention",
                "targets",
                "version",
                "web_external_url",
                "web_listen_address",
            ]
        )
        self._alert_relabel_configs = alert_relabel_configs
        self._alert_rules = alert_rules
        self._alertmanager_config = alertmanager_config
        self._config_flags_extra = config_flags_extra
        self._external_labels = external_labels
        self._global_config = global_config
        self._remote_read = remote_read
        self._remote_write = remote_write
        self._scrape_configs = scrape_configs
        self._storage_retention = storage_retention
        self._targets = targets
        self._version = version
        self._web_external_url = web_external_url
        self._web_listen_address = web_listen_address

    @property
    def alert_relabel_configs(self):
        return self._alert_relabel_configs

    @alert_relabel_configs.setter
    def alert_relabel_configs(self, alert_relabel_configs):
        self._alert_relabel_configs = alert_relabel_configs

    @property
    def alert_rules(self):
        return self._alert_rules

    @alert_rules.setter
    def alert_rules(self, alert_rules):
        self._alert_rules = alert_rules

    @property
    def alertmanager_config(self):
        return self._alertmanager_config

    @alertmanager_config.setter
    def alertmanager_config(self, alertmanager_config):
        self._alertmanager_config = alertmanager_config

    @property
    def config_flags_extra(self):
        return self._config_flags_extra

    @config_flags_extra.setter
    def config_flags_extra(self, config_flags_extra):
        self._config_flags_extra = config_flags_extra

    @property
    def external_labels(self):
        return self._external_labels

    @external_labels.setter
    def external_labels(self, external_labels):
        self._external_labels = external_labels

    @property
    def global_config(self):
        return self._global_config

    @global_config.setter
    def global_config(self, global_config):
        self._global_config = global_config

    @property
    def remote_read(self):
        return self._remote_read

    @remote_read.setter
    def remote_read(self, remote_read):
        self._remote_read = remote_read

    @property
    def remote_write(self):
        return self._remote_write

    @remote_write.setter
    def remote_write(self, remote_write):
        self._remote_write = remote_write

    @property
    def scrape_configs(self):
        return self._scrape_configs

    @scrape_configs.setter
    def scrape_configs(self, scrape_configs):
        self._scrape_configs = scrape_configs

    @property
    def storage_retention(self):
        return self._storage_retention

    @storage_retention.setter
    def storage_retention(self, storage_retention):
        self._storage_retention = storage_retention

    @property
    def targets(self):
        return self._targets

    @targets.setter
    def targets(self, targets):
        self._targets = targets

    @property
    def version(self):
        return self._version

    @version.setter
    def version(self, version):
        self._version = version

    @property
    def web_external_url(self):
        return self._web_external_url

    @web_external_url.setter
    def web_external_url(self, web_external_url):
        self._web_external_url = web_external_url

    @property
    def web_listen_address(self):
        return self._web_listen_address

    @web_listen_address.setter
    def web_listen_address(self, web_listen_address):
        self._web_listen_address = web_listen_address


frecklet_class = PrometheusService
