# -*- coding: utf-8 -*-


#
# module path: pycklets.init_service_reloaded.InitServiceReloaded
#


from pyckles import AutoPycklet


class InitServiceReloaded(AutoPycklet):
    """Reload init service.

       Args:
         name: The name of the service.

    """

    FRECKLET_ID = "init-service-reloaded"

    def __init__(self, name=None):

        super(InitServiceReloaded, self).__init__(var_names=["name"])
        self._name = name

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name


frecklet_class = InitServiceReloaded
