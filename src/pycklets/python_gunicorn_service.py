# -*- coding: utf-8 -*-


#
# module path: pycklets.python_gunicorn_service.PythonGunicornService
#


from pyckles import AutoPycklet


class PythonGunicornService(AutoPycklet):
    """Setup a service executing an application from within a virtualenv.

     **NOTE**: Currently, this frecklet will only work if the user that is specified has their home directory under '/home/<username>'.

       Args:
         app_module: The app entry point.
         group: The name of the group to own the service (defaults to project name).
         pip_extra_args: Extra arguments forwarded to 'pip'.
         project_config: Environment variables to configure app.
         project_name: n/a
         python_packages: All necessary Python packages.
         python_version: The version of python.
         user: The name of the user to own the service (defaults to project name).

    """

    FRECKLET_ID = "python-gunicorn-service"

    def __init__(
        self,
        app_module=None,
        group="::value_from_project_name::",
        pip_extra_args=None,
        project_config=None,
        project_name=None,
        python_packages=None,
        python_version="latest",
        user="::value_from_project_name::",
    ):

        super(PythonGunicornService, self).__init__(
            var_names=[
                "app_module",
                "group",
                "pip_extra_args",
                "project_config",
                "project_name",
                "python_packages",
                "python_version",
                "user",
            ]
        )
        self._app_module = app_module
        self._group = group
        self._pip_extra_args = pip_extra_args
        self._project_config = project_config
        self._project_name = project_name
        self._python_packages = python_packages
        self._python_version = python_version
        self._user = user

    @property
    def app_module(self):
        return self._app_module

    @app_module.setter
    def app_module(self, app_module):
        self._app_module = app_module

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def pip_extra_args(self):
        return self._pip_extra_args

    @pip_extra_args.setter
    def pip_extra_args(self, pip_extra_args):
        self._pip_extra_args = pip_extra_args

    @property
    def project_config(self):
        return self._project_config

    @project_config.setter
    def project_config(self, project_config):
        self._project_config = project_config

    @property
    def project_name(self):
        return self._project_name

    @project_name.setter
    def project_name(self, project_name):
        self._project_name = project_name

    @property
    def python_packages(self):
        return self._python_packages

    @python_packages.setter
    def python_packages(self, python_packages):
        self._python_packages = python_packages

    @property
    def python_version(self):
        return self._python_version

    @python_version.setter
    def python_version(self, python_version):
        self._python_version = python_version

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user


frecklet_class = PythonGunicornService
