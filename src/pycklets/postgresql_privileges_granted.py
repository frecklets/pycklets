# -*- coding: utf-8 -*-


#
# module path: pycklets.postgresql_privileges_granted.PostgresqlPrivilegesGranted
#


from pyckles import AutoPycklet


class PostgresqlPrivilegesGranted(AutoPycklet):
    """Grant a privilege on a Postgresql database.

       Args:
         db_name: The name of the database to connect to and add the schema.
         object_type: The type of the object to grant permissions to.
         objects: A list of objects to grant permissions to.
         privileges: The list of permissions to grant (default: ['ALL']).
         roles: A list of roles that will be granted permissions.
         schema: Schema that contains the database objects specified via objects.
         session_role: Switch to session_role after connecting.

    """

    FRECKLET_ID = "postgresql-privileges-granted"

    def __init__(
        self,
        db_name=None,
        object_type="table",
        objects=None,
        privileges=["ALL"],
        roles=None,
        schema=None,
        session_role=None,
    ):

        super(PostgresqlPrivilegesGranted, self).__init__(
            var_names=[
                "db_name",
                "object_type",
                "objects",
                "privileges",
                "roles",
                "schema",
                "session_role",
            ]
        )
        self._db_name = db_name
        self._object_type = object_type
        self._objects = objects
        self._privileges = privileges
        self._roles = roles
        self._schema = schema
        self._session_role = session_role

    @property
    def db_name(self):
        return self._db_name

    @db_name.setter
    def db_name(self, db_name):
        self._db_name = db_name

    @property
    def object_type(self):
        return self._object_type

    @object_type.setter
    def object_type(self, object_type):
        self._object_type = object_type

    @property
    def objects(self):
        return self._objects

    @objects.setter
    def objects(self, objects):
        self._objects = objects

    @property
    def privileges(self):
        return self._privileges

    @privileges.setter
    def privileges(self, privileges):
        self._privileges = privileges

    @property
    def roles(self):
        return self._roles

    @roles.setter
    def roles(self, roles):
        self._roles = roles

    @property
    def schema(self):
        return self._schema

    @schema.setter
    def schema(self, schema):
        self._schema = schema

    @property
    def session_role(self):
        return self._session_role

    @session_role.setter
    def session_role(self, session_role):
        self._session_role = session_role


frecklet_class = PostgresqlPrivilegesGranted
