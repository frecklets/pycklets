# -*- coding: utf-8 -*-


#
# module path: pycklets.remote_apt_key_added.RemoteAptKeyAdded
#


from pyckles import AutoPycklet


class RemoteAptKeyAdded(AutoPycklet):
    """Ensures a repository signing key is added to the system.

       Args:
         url: The URL to retrieve key from.

    """

    FRECKLET_ID = "remote-apt-key-added"

    def __init__(self, url=None):

        super(RemoteAptKeyAdded, self).__init__(var_names=["url"])
        self._url = url

    @property
    def url(self):
        return self._url

    @url.setter
    def url(self, url):
        self._url = url


frecklet_class = RemoteAptKeyAdded
