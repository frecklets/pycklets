# -*- coding: utf-8 -*-


#
# module path: pycklets.wait_for_ssh.WaitForSsh
#


from pyckles import AutoPycklet


class WaitForSsh(AutoPycklet):
    """Wait for ssh service to be available on a host.

       Args:
         delay: Number of seconds to wait before starting to poll.
         extra_wait_time: Wait an extra amount of seconds before continuing.
         host: The name or IP address of the host.
         port: The port ssh listens on.
         timeout: Maximum number of seconds to wait for.

    """

    FRECKLET_ID = "wait-for-ssh"

    def __init__(
        self, delay=None, extra_wait_time=None, host=None, port=22, timeout=300
    ):

        super(WaitForSsh, self).__init__(
            var_names=["delay", "extra_wait_time", "host", "port", "timeout"]
        )
        self._delay = delay
        self._extra_wait_time = extra_wait_time
        self._host = host
        self._port = port
        self._timeout = timeout

    @property
    def delay(self):
        return self._delay

    @delay.setter
    def delay(self, delay):
        self._delay = delay

    @property
    def extra_wait_time(self):
        return self._extra_wait_time

    @extra_wait_time.setter
    def extra_wait_time(self, extra_wait_time):
        self._extra_wait_time = extra_wait_time

    @property
    def host(self):
        return self._host

    @host.setter
    def host(self, host):
        self._host = host

    @property
    def port(self):
        return self._port

    @port.setter
    def port(self, port):
        self._port = port

    @property
    def timeout(self):
        return self._timeout

    @timeout.setter
    def timeout(self, timeout):
        self._timeout = timeout


frecklet_class = WaitForSsh
