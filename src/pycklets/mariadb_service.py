# -*- coding: utf-8 -*-


#
# module path: pycklets.mariadb_service.MariadbService
#


from pyckles import AutoPycklet


class MariadbService(AutoPycklet):
    """This frecklet installs the MariaDB service using the [geerlingguy.mysql](https://github.com/geerlingguy/ansible-role-mysql)
     Ansible role.

     This frecklet does not do any configuration, and uses all the defaults of the underlying
     Ansible role. Use other tasks to do any configuration after executing this.

       Args:
         mysql_group_id: An (optional) custom mysql group id
         mysql_user_id: An (optional) custom mysql user id

    """

    FRECKLET_ID = "mariadb-service"

    def __init__(self, mysql_group_id=None, mysql_user_id=None):

        super(MariadbService, self).__init__(
            var_names=["mysql_group_id", "mysql_user_id"]
        )
        self._mysql_group_id = mysql_group_id
        self._mysql_user_id = mysql_user_id

    @property
    def mysql_group_id(self):
        return self._mysql_group_id

    @mysql_group_id.setter
    def mysql_group_id(self, mysql_group_id):
        self._mysql_group_id = mysql_group_id

    @property
    def mysql_user_id(self):
        return self._mysql_user_id

    @mysql_user_id.setter
    def mysql_user_id(self, mysql_user_id):
        self._mysql_user_id = mysql_user_id


frecklet_class = MariadbService
