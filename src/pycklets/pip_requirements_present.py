# -*- coding: utf-8 -*-


#
# module path: pycklets.pip_requirements_present.PipRequirementsPresent
#


from pyckles import AutoPycklet


class PipRequirementsPresent(AutoPycklet):
    """Install dependencies so 'pip' can be used by Ansible.

     This is a helper frecklet, and probably does not need  to be called directly by any user.
     This is a workaround for issue: https://github.com/ansible/ansible/issues/47361 , ideally this would not be necessary in every case.

       Args:

    """

    FRECKLET_ID = "pip-requirements-present"

    def __init__(self,):

        super(PipRequirementsPresent, self).__init__(var_names=[])


frecklet_class = PipRequirementsPresent
