# -*- coding: utf-8 -*-


#
# module path: pycklets.nextcloud_standalone_docker.NextcloudStandaloneDocker
#


from pyckles import AutoPycklet


class NextcloudStandaloneDocker(AutoPycklet):
    """TODO: add db support, etc

       Args:
         container_name: The name for the container image.
         ensure_docker_installed: Install Docker if not already present.
         persistent_data: The path to where the container should store its persistent data.
         port: The port where Nextcloud should be available.
         user: The local user who should own the nextcloud parent data folder (will also be added to the list of docker users).

    """

    FRECKLET_ID = "nextcloud-standalone-docker"

    def __init__(
        self,
        container_name="nextcloud",
        ensure_docker_installed=None,
        persistent_data=None,
        port=8080,
        user=None,
    ):

        super(NextcloudStandaloneDocker, self).__init__(
            var_names=[
                "container_name",
                "ensure_docker_installed",
                "persistent_data",
                "port",
                "user",
            ]
        )
        self._container_name = container_name
        self._ensure_docker_installed = ensure_docker_installed
        self._persistent_data = persistent_data
        self._port = port
        self._user = user

    @property
    def container_name(self):
        return self._container_name

    @container_name.setter
    def container_name(self, container_name):
        self._container_name = container_name

    @property
    def ensure_docker_installed(self):
        return self._ensure_docker_installed

    @ensure_docker_installed.setter
    def ensure_docker_installed(self, ensure_docker_installed):
        self._ensure_docker_installed = ensure_docker_installed

    @property
    def persistent_data(self):
        return self._persistent_data

    @persistent_data.setter
    def persistent_data(self, persistent_data):
        self._persistent_data = persistent_data

    @property
    def port(self):
        return self._port

    @port.setter
    def port(self, port):
        self._port = port

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user


frecklet_class = NextcloudStandaloneDocker
