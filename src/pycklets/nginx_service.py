# -*- coding: utf-8 -*-


#
# module path: pycklets.nginx_service.NginxService
#


from pyckles import AutoPycklet


class NginxService(AutoPycklet):
    """Installs the Nginx web server.

     This uses the [geerlingguy.nginx](https://github.com/geerlingguy/ansible-role-nginx)
     Ansible role to do the heavy lifting.

     It's recommended to use the 'webserver-service' frecklet instead of this if you want to provision a web server
     with things like https certificate, php installed, etc.

       Args:
         remove_default_vhost: Whether to remove the 'default' virtualhost configuration supplied by Nginx.
         user: the user nginx will run under

    """

    FRECKLET_ID = "nginx-service"

    def __init__(self, remove_default_vhost=True, user=None):

        super(NginxService, self).__init__(var_names=["remove_default_vhost", "user"])
        self._remove_default_vhost = remove_default_vhost
        self._user = user

    @property
    def remove_default_vhost(self):
        return self._remove_default_vhost

    @remove_default_vhost.setter
    def remove_default_vhost(self, remove_default_vhost):
        self._remove_default_vhost = remove_default_vhost

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user


frecklet_class = NginxService
