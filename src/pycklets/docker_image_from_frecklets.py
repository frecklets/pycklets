# -*- coding: utf-8 -*-


#
# module path: pycklets.docker_image_from_frecklets.DockerImageFromFrecklets
#


from pyckles import AutoPycklet


class DockerImageFromFrecklets(AutoPycklet):
    """Build a Docker image from a frecklet.

     This is just a proof-of-concept, to show that sometime like this is easily possible. It currently only works with Debian-based base images, as the pre-written Dockerfile template uses 'apt' to install curl. This would be easily changed once this is done properly.

       Args:
         base_image: The base image to use.
         force: Whether to force the build to be done, even if an image with that name exists already.
         frecklets: The frecklets to use in the Docker container.
         name: The name of the image.
         nocache: Whether to use cache when building an image.

    """

    FRECKLET_ID = "docker-image-from-frecklets"

    def __init__(
        self,
        base_image="ubuntu:18.04",
        force=None,
        frecklets=None,
        name=None,
        nocache=None,
    ):

        super(DockerImageFromFrecklets, self).__init__(
            var_names=["base_image", "force", "frecklets", "name", "nocache"]
        )
        self._base_image = base_image
        self._force = force
        self._frecklets = frecklets
        self._name = name
        self._nocache = nocache

    @property
    def base_image(self):
        return self._base_image

    @base_image.setter
    def base_image(self, base_image):
        self._base_image = base_image

    @property
    def force(self):
        return self._force

    @force.setter
    def force(self, force):
        self._force = force

    @property
    def frecklets(self):
        return self._frecklets

    @frecklets.setter
    def frecklets(self, frecklets):
        self._frecklets = frecklets

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def nocache(self):
        return self._nocache

    @nocache.setter
    def nocache(self, nocache):
        self._nocache = nocache


frecklet_class = DockerImageFromFrecklets
