# -*- coding: utf-8 -*-


#
# module path: pycklets.devpi_import_from_backup.DevpiImportFromBackup
#


from pyckles import AutoPycklet


class DevpiImportFromBackup(AutoPycklet):
    """Import will take a minute or two, as it also creates the mirror indexes.

       Args:
         backup_archive_file: The (local) backup destination file.

    """

    FRECKLET_ID = "devpi-import-from-backup"

    def __init__(self, backup_archive_file=None):

        super(DevpiImportFromBackup, self).__init__(var_names=["backup_archive_file"])
        self._backup_archive_file = backup_archive_file

    @property
    def backup_archive_file(self):
        return self._backup_archive_file

    @backup_archive_file.setter
    def backup_archive_file(self, backup_archive_file):
        self._backup_archive_file = backup_archive_file


frecklet_class = DevpiImportFromBackup
