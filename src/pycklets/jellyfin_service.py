# -*- coding: utf-8 -*-


#
# module path: pycklets.jellyfin_service.JellyfinService
#


from pyckles import AutoPycklet


class JellyfinService(AutoPycklet):
    """Install a Jellyfin streaming service.

       Args:
         fqdns: A list of fqdns of the host.

    """

    FRECKLET_ID = "jellyfin-service"

    def __init__(self, fqdns=None):

        super(JellyfinService, self).__init__(var_names=["fqdns"])
        self._fqdns = fqdns

    @property
    def fqdns(self):
        return self._fqdns

    @fqdns.setter
    def fqdns(self, fqdns):
        self._fqdns = fqdns


frecklet_class = JellyfinService
