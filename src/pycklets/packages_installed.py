# -*- coding: utf-8 -*-


#
# module path: pycklets.packages_installed.PackagesInstalled
#


from pyckles import AutoPycklet


class PackagesInstalled(AutoPycklet):
    """Install a list of packages, including (optionally) the package managers that are used to install them.

     More information and examples to come, for now please refer to the frecklet::pkg frecklet as well as [freckfrackery.install-pkgs Ansible role](https://gitlab.com/freckfrackery/freckfrackery.install-pkgs/blob/master/README.md) for more information.

       Args:
         become: Whether to use root permissions to install the packages.
         no_pkg_mgrs: Don't try to install necessary package managers.
         packages: The list of packages to install.

    """

    FRECKLET_ID = "packages-installed"

    def __init__(self, become=True, no_pkg_mgrs=None, packages=None):

        super(PackagesInstalled, self).__init__(
            var_names=["become", "no_pkg_mgrs", "packages"]
        )
        self._become = become
        self._no_pkg_mgrs = no_pkg_mgrs
        self._packages = packages

    @property
    def become(self):
        return self._become

    @become.setter
    def become(self, become):
        self._become = become

    @property
    def no_pkg_mgrs(self):
        return self._no_pkg_mgrs

    @no_pkg_mgrs.setter
    def no_pkg_mgrs(self, no_pkg_mgrs):
        self._no_pkg_mgrs = no_pkg_mgrs

    @property
    def packages(self):
        return self._packages

    @packages.setter
    def packages(self, packages):
        self._packages = packages


frecklet_class = PackagesInstalled
