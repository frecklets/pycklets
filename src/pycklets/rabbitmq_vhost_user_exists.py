# -*- coding: utf-8 -*-


#
# module path: pycklets.rabbitmq_vhost_user_exists.RabbitmqVhostUserExists
#


from pyckles import AutoPycklet


class RabbitmqVhostUserExists(AutoPycklet):
    """Create rabbitmq user with full privileges for a specific vhost.

       Args:
         password: The users password.
         user: The name of the user.
         vhost: The vhost name.

    """

    FRECKLET_ID = "rabbitmq-vhost-user-exists"

    def __init__(self, password=None, user=None, vhost=None):

        super(RabbitmqVhostUserExists, self).__init__(
            var_names=["password", "user", "vhost"]
        )
        self._password = password
        self._user = user
        self._vhost = vhost

    @property
    def password(self):
        return self._password

    @password.setter
    def password(self, password):
        self._password = password

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user

    @property
    def vhost(self):
        return self._vhost

    @vhost.setter
    def vhost(self, vhost):
        self._vhost = vhost


frecklet_class = RabbitmqVhostUserExists
