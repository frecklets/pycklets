# -*- coding: utf-8 -*-


#
# module path: pycklets.python_lang_installed.PythonLangInstalled
#


from pyckles import AutoPycklet


class PythonLangInstalled(AutoPycklet):
    """Install a Python environment for a user, using [pyenv](https://github.com/pyenv/pyenv) or system packages.

     If using 'pyenv', this will install all dependencies required to build Python, then it will download the specified version of Python,
     compile it, and use the 'pyenv global' command (which will write that information into the file $HOME/.pyenv/version)
     to make it the default version for the user.

     If Python type is 'pyenv', this will add a piece of code to ``$HOME/.bashrc`` to load pyenv when the user logs in.

       Args:
         build_opts: Build options to be forwarded (if supported by 'python_type').
         python_type: How to install Python.
         user: The user to install Python for.
         version: The version of Python to install.

    """

    FRECKLET_ID = "python-lang-installed"

    def __init__(
        self, build_opts=None, python_type="pyenv", user=None, version="latest"
    ):

        super(PythonLangInstalled, self).__init__(
            var_names=["build_opts", "python_type", "user", "version"]
        )
        self._build_opts = build_opts
        self._python_type = python_type
        self._user = user
        self._version = version

    @property
    def build_opts(self):
        return self._build_opts

    @build_opts.setter
    def build_opts(self, build_opts):
        self._build_opts = build_opts

    @property
    def python_type(self):
        return self._python_type

    @python_type.setter
    def python_type(self, python_type):
        self._python_type = python_type

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user

    @property
    def version(self):
        return self._version

    @version.setter
    def version(self, version):
        self._version = version


frecklet_class = PythonLangInstalled
