# -*- coding: utf-8 -*-


#
# module path: pycklets.docker_service.DockerService
#


from pyckles import AutoPycklet


class DockerService(AutoPycklet):
    """Installs Docker on a Linux machine.

     If the ``users`` variable is set, those users will be created if they don't exist yet. Do this seperately if you need to have more
     control about user creation (e.g. to provide password, ssh-pub-keys, etc.)

     Windows and Mac OS X are not supported just yet.

       Args:
         enabled: Whether to enable the docker service or not.
         started: Whether to start the docker service or not.
         users: A list of users who will be added to the 'docker' group.

    """

    FRECKLET_ID = "docker-service"

    def __init__(self, enabled=True, started=True, users=None):

        super(DockerService, self).__init__(var_names=["enabled", "started", "users"])
        self._enabled = enabled
        self._started = started
        self._users = users

    @property
    def enabled(self):
        return self._enabled

    @enabled.setter
    def enabled(self, enabled):
        self._enabled = enabled

    @property
    def started(self):
        return self._started

    @started.setter
    def started(self, started):
        self._started = started

    @property
    def users(self):
        return self._users

    @users.setter
    def users(self, users):
        self._users = users


frecklet_class = DockerService
