# -*- coding: utf-8 -*-


#
# module path: pycklets.systemd_template_units_configured.SystemdTemplateUnitsConfigured
#


from pyckles import AutoPycklet


class SystemdTemplateUnitsConfigured(AutoPycklet):
    """Configures one or multiple systemd template units.

       Args:
         enabled: Whether to enable the service or not.
         instance_names: The instance names.
         name: The name of the service.
         started: Whether to start the service or not.

    """

    FRECKLET_ID = "systemd-template-units-configured"

    def __init__(self, enabled=None, instance_names=None, name=None, started=None):

        super(SystemdTemplateUnitsConfigured, self).__init__(
            var_names=["enabled", "instance_names", "name", "started"]
        )
        self._enabled = enabled
        self._instance_names = instance_names
        self._name = name
        self._started = started

    @property
    def enabled(self):
        return self._enabled

    @enabled.setter
    def enabled(self, enabled):
        self._enabled = enabled

    @property
    def instance_names(self):
        return self._instance_names

    @instance_names.setter
    def instance_names(self, instance_names):
        self._instance_names = instance_names

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def started(self):
        return self._started

    @started.setter
    def started(self, started):
        self._started = started


frecklet_class = SystemdTemplateUnitsConfigured
