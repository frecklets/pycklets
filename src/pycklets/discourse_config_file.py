# -*- coding: utf-8 -*-


#
# module path: pycklets.discourse_config_file.DiscourseConfigFile
#


from pyckles import AutoPycklet


class DiscourseConfigFile(AutoPycklet):
    """No documentation available.

       Args:
         admin_emails: A list of email addresses that will be admins after install.
         discourse_plugins: A list of discourse plugin urls.
         discourse_smtp_address: The SMTP server address.
         discourse_smtp_enable_start_tls: Whether to use START_TLS when connecting to the SMTP server.
         discourse_smtp_password: The SMTP password.
         discourse_smtp_port: The SMTP port.
         discourse_smtp_user_name: The SMTP user name.
         discourse_version: The version of discourse.
         group: The group of the file.
         hostname: The discourse hostname.
         mode: The permissions of the file.
         owner: The owner of the file.
         path: The path to the file.
         set_from_email: A custom 'from' email address (e.g. noreply@example.com).
         use_https: Whether to use https with letsencrypt.

    """

    FRECKLET_ID = "discourse-config-file"

    def __init__(
        self,
        admin_emails=None,
        discourse_plugins=None,
        discourse_smtp_address=None,
        discourse_smtp_enable_start_tls=True,
        discourse_smtp_password=None,
        discourse_smtp_port=None,
        discourse_smtp_user_name=None,
        discourse_version="stable",
        group=None,
        hostname=None,
        mode=None,
        owner=None,
        path=None,
        set_from_email=None,
        use_https=True,
    ):

        super(DiscourseConfigFile, self).__init__(
            var_names=[
                "admin_emails",
                "discourse_plugins",
                "discourse_smtp_address",
                "discourse_smtp_enable_start_tls",
                "discourse_smtp_password",
                "discourse_smtp_port",
                "discourse_smtp_user_name",
                "discourse_version",
                "group",
                "hostname",
                "mode",
                "owner",
                "path",
                "set_from_email",
                "use_https",
            ]
        )
        self._admin_emails = admin_emails
        self._discourse_plugins = discourse_plugins
        self._discourse_smtp_address = discourse_smtp_address
        self._discourse_smtp_enable_start_tls = discourse_smtp_enable_start_tls
        self._discourse_smtp_password = discourse_smtp_password
        self._discourse_smtp_port = discourse_smtp_port
        self._discourse_smtp_user_name = discourse_smtp_user_name
        self._discourse_version = discourse_version
        self._group = group
        self._hostname = hostname
        self._mode = mode
        self._owner = owner
        self._path = path
        self._set_from_email = set_from_email
        self._use_https = use_https

    @property
    def admin_emails(self):
        return self._admin_emails

    @admin_emails.setter
    def admin_emails(self, admin_emails):
        self._admin_emails = admin_emails

    @property
    def discourse_plugins(self):
        return self._discourse_plugins

    @discourse_plugins.setter
    def discourse_plugins(self, discourse_plugins):
        self._discourse_plugins = discourse_plugins

    @property
    def discourse_smtp_address(self):
        return self._discourse_smtp_address

    @discourse_smtp_address.setter
    def discourse_smtp_address(self, discourse_smtp_address):
        self._discourse_smtp_address = discourse_smtp_address

    @property
    def discourse_smtp_enable_start_tls(self):
        return self._discourse_smtp_enable_start_tls

    @discourse_smtp_enable_start_tls.setter
    def discourse_smtp_enable_start_tls(self, discourse_smtp_enable_start_tls):
        self._discourse_smtp_enable_start_tls = discourse_smtp_enable_start_tls

    @property
    def discourse_smtp_password(self):
        return self._discourse_smtp_password

    @discourse_smtp_password.setter
    def discourse_smtp_password(self, discourse_smtp_password):
        self._discourse_smtp_password = discourse_smtp_password

    @property
    def discourse_smtp_port(self):
        return self._discourse_smtp_port

    @discourse_smtp_port.setter
    def discourse_smtp_port(self, discourse_smtp_port):
        self._discourse_smtp_port = discourse_smtp_port

    @property
    def discourse_smtp_user_name(self):
        return self._discourse_smtp_user_name

    @discourse_smtp_user_name.setter
    def discourse_smtp_user_name(self, discourse_smtp_user_name):
        self._discourse_smtp_user_name = discourse_smtp_user_name

    @property
    def discourse_version(self):
        return self._discourse_version

    @discourse_version.setter
    def discourse_version(self, discourse_version):
        self._discourse_version = discourse_version

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def hostname(self):
        return self._hostname

    @hostname.setter
    def hostname(self, hostname):
        self._hostname = hostname

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, mode):
        self._mode = mode

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path

    @property
    def set_from_email(self):
        return self._set_from_email

    @set_from_email.setter
    def set_from_email(self, set_from_email):
        self._set_from_email = set_from_email

    @property
    def use_https(self):
        return self._use_https

    @use_https.setter
    def use_https(self, use_https):
        self._use_https = use_https


frecklet_class = DiscourseConfigFile
