# -*- coding: utf-8 -*-


#
# module path: pycklets.gitlab_runner_service_linux.GitlabRunnerServiceLinux
#


from pyckles import AutoPycklet


class GitlabRunnerServiceLinux(AutoPycklet):
    """Ensures a gitlab-runner service exists on a machine

       Args:
         arch: The architecture of the target system.
         group: The name of the gitlab-runner service group.
         install_docker: Whether to also install Docker.
         user: The name of the gitlab-runner service user.

    """

    FRECKLET_ID = "gitlab-runner-service-linux"

    def __init__(
        self,
        arch="amd64",
        group="gitlab-runner",
        install_docker=None,
        user="gitlab-runner",
    ):

        super(GitlabRunnerServiceLinux, self).__init__(
            var_names=["arch", "group", "install_docker", "user"]
        )
        self._arch = arch
        self._group = group
        self._install_docker = install_docker
        self._user = user

    @property
    def arch(self):
        return self._arch

    @arch.setter
    def arch(self, arch):
        self._arch = arch

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def install_docker(self):
        return self._install_docker

    @install_docker.setter
    def install_docker(self, install_docker):
        self._install_docker = install_docker

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user


frecklet_class = GitlabRunnerServiceLinux
