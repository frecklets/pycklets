# -*- coding: utf-8 -*-


#
# module path: pycklets.pyenv_python_installed.PyenvPythonInstalled
#


from pyckles import AutoPycklet


class PyenvPythonInstalled(AutoPycklet):
    """**Note**: this is mostly deprecated now by the 'python-lang-installed' frecklet.

     Install a Python environment for a user, using [pyenv](https://github.com/pyenv/pyenv).

     This will install all dependencies required to build Python, then it will download the specified version of Python,
     compile it, and use the 'pyenv global' command (which will write that information into the file $HOME/.pyenv/version)
     to make it the default version for the user.

     It will also add a piece of code to ``$HOME/.bashrc`` to load pyenv when the user logs in. If you use this to install
     Python for a user different to the executing one, make sure to specify the 'path' argument (absolute path required,
     relative or using '~'/'$HOME' won't work).

       Args:
         path: The path to install pyenv into.
         set_global: Don't set 'global' version.
         user: The user to install pyenv for.
         version: The version of Python to install.

    """

    FRECKLET_ID = "pyenv-python-installed"

    def __init__(self, path=None, set_global=True, user=None, version="3.7.3"):

        super(PyenvPythonInstalled, self).__init__(
            var_names=["path", "set_global", "user", "version"]
        )
        self._path = path
        self._set_global = set_global
        self._user = user
        self._version = version

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path

    @property
    def set_global(self):
        return self._set_global

    @set_global.setter
    def set_global(self, set_global):
        self._set_global = set_global

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user

    @property
    def version(self):
        return self._version

    @version.setter
    def version(self, version):
        self._version = version


frecklet_class = PyenvPythonInstalled
