# -*- coding: utf-8 -*-


#
# module path: pycklets.users_are_members_of_group.UsersAreMembersOfGroup
#


from pyckles import AutoPycklet


class UsersAreMembersOfGroup(AutoPycklet):
    """Ensures a list of users are all members of a certain group.

       Args:
         group: The name of the group.
         users: A list of usernames.

    """

    FRECKLET_ID = "users-are-members-of-group"

    def __init__(self, group=None, users=None):

        super(UsersAreMembersOfGroup, self).__init__(var_names=["group", "users"])
        self._group = group
        self._users = users

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def users(self):
        return self._users

    @users.setter
    def users(self, users):
        self._users = users


frecklet_class = UsersAreMembersOfGroup
