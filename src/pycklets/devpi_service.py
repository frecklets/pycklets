# -*- coding: utf-8 -*-


#
# module path: pycklets.devpi_service.DevpiService
#


from pyckles import AutoPycklet


class DevpiService(AutoPycklet):
    """This frecklet installs devpi, adds a systed unit file. and runs devpi as a service.

     If a ``backup_archive_file`` is provided, it will be uploaded to the server, unarchived, and the backed-up
     state will be imported before the server will be started for the first time. Check the frecklet::devpi-create-backup
     *frecklet* for details about creating such a backup archive file. The import will take a minute or two, as it also
     (re-)creates the pypi mirror index.

     If the ``use_https`` variable is set, a certificate from LetsEncrypt will be requrested and installed, along
     with a cron job to automatically renew it before it expires. Nginx will be
     configured to redirect all http traffic to https.

       Args:
         admin_password: The initial admin password.
         backup_archive_file: A file to restore the service from.
         listen_address: The listen address of the devpi server.
         user: The user to run devpi as well as nginx.

    """

    FRECKLET_ID = "devpi-service"

    def __init__(
        self,
        admin_password=None,
        backup_archive_file=None,
        listen_address="localhost",
        user="devpi",
    ):

        super(DevpiService, self).__init__(
            var_names=[
                "admin_password",
                "backup_archive_file",
                "listen_address",
                "user",
            ]
        )
        self._admin_password = admin_password
        self._backup_archive_file = backup_archive_file
        self._listen_address = listen_address
        self._user = user

    @property
    def admin_password(self):
        return self._admin_password

    @admin_password.setter
    def admin_password(self, admin_password):
        self._admin_password = admin_password

    @property
    def backup_archive_file(self):
        return self._backup_archive_file

    @backup_archive_file.setter
    def backup_archive_file(self, backup_archive_file):
        self._backup_archive_file = backup_archive_file

    @property
    def listen_address(self):
        return self._listen_address

    @listen_address.setter
    def listen_address(self, listen_address):
        self._listen_address = listen_address

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user


frecklet_class = DevpiService
