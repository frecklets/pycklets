# -*- coding: utf-8 -*-


#
# module path: pycklets.passwordless_sudo_users.PasswordlessSudoUsers
#


from pyckles import AutoPycklet


class PasswordlessSudoUsers(AutoPycklet):
    """Ensure that one or more users can use passwordless sudo.

     This also makes sure that the sudo package is installed (if not already available), and creates the user(s) (if not already present).

       Args:
         users: n/a

    """

    FRECKLET_ID = "passwordless-sudo-users"

    def __init__(self, users=None):

        super(PasswordlessSudoUsers, self).__init__(var_names=["users"])
        self._users = users

    @property
    def users(self):
        return self._users

    @users.setter
    def users(self, users):
        self._users = users


frecklet_class = PasswordlessSudoUsers
