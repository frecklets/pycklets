# -*- coding: utf-8 -*-


#
# module path: pycklets.package_managers.PackageManagers
#


from pyckles import AutoPycklet


class PackageManagers(AutoPycklet):
    """This is mainly used in conjunction with the frecklet::pkg and frecklet::pkgs frecklets, so they
     can transparently install package managers that are used there.

     Supported package managers currently:

     - conda
     - git
     - homebrew
     - nix
     - pip
     - vagrant-plugin

     More information and examples to come, for now please refer to the [freckfrackery.install-pkg_mgrs Ansible role](https://gitlab.com/freckfrackery/freckfrackery.install-pkg_mgrs/blob/master/README.md) for more information.

       Args:
         package_managers: n/a

    """

    FRECKLET_ID = "package-managers"

    def __init__(self, package_managers=None):

        super(PackageManagers, self).__init__(var_names=["package_managers"])
        self._package_managers = package_managers

    @property
    def package_managers(self):
        return self._package_managers

    @package_managers.setter
    def package_managers(self, package_managers):
        self._package_managers = package_managers


frecklet_class = PackageManagers
