# -*- coding: utf-8 -*-


#
# module path: pycklets.apt_upgrade.AptUpgrade
#


from pyckles import AutoPycklet


class AptUpgrade(AutoPycklet):
    """Performs an apt upgrade.

       Args:
         cache_valid_time: Update the apt cache if its older than the cache_valid_time. This option is set in seconds.
         update_cache: Whether to upgrade the apt cache before upgrading.
         upgrade_type: The type of the upgrade.

    """

    FRECKLET_ID = "apt-upgrade"

    def __init__(
        self, cache_valid_time=None, update_cache=True, upgrade_type="default"
    ):

        super(AptUpgrade, self).__init__(
            var_names=["cache_valid_time", "update_cache", "upgrade_type"]
        )
        self._cache_valid_time = cache_valid_time
        self._update_cache = update_cache
        self._upgrade_type = upgrade_type

    @property
    def cache_valid_time(self):
        return self._cache_valid_time

    @cache_valid_time.setter
    def cache_valid_time(self, cache_valid_time):
        self._cache_valid_time = cache_valid_time

    @property
    def update_cache(self):
        return self._update_cache

    @update_cache.setter
    def update_cache(self, update_cache):
        self._update_cache = update_cache

    @property
    def upgrade_type(self):
        return self._upgrade_type

    @upgrade_type.setter
    def upgrade_type(self, upgrade_type):
        self._upgrade_type = upgrade_type


frecklet_class = AptUpgrade
