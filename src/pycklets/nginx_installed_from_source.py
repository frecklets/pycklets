# -*- coding: utf-8 -*-


#
# module path: pycklets.nginx_installed_from_source.NginxInstalledFromSource
#


from pyckles import AutoPycklet


class NginxInstalledFromSource(AutoPycklet):
    """Install Nginx from source (optionally incl. pagespeed module)

       Args:
         nginx_version: The version of Nginx
         pagespeed_version: The version of pagespeed.
         use_pagespeed: Whether to use the pagespeed module.
         worker_processes: The number of Nginx worker processes.

    """

    FRECKLET_ID = "nginx-installed-from-source"

    def __init__(
        self,
        nginx_version=None,
        pagespeed_version=None,
        use_pagespeed=None,
        worker_processes=2,
    ):

        super(NginxInstalledFromSource, self).__init__(
            var_names=[
                "nginx_version",
                "pagespeed_version",
                "use_pagespeed",
                "worker_processes",
            ]
        )
        self._nginx_version = nginx_version
        self._pagespeed_version = pagespeed_version
        self._use_pagespeed = use_pagespeed
        self._worker_processes = worker_processes

    @property
    def nginx_version(self):
        return self._nginx_version

    @nginx_version.setter
    def nginx_version(self, nginx_version):
        self._nginx_version = nginx_version

    @property
    def pagespeed_version(self):
        return self._pagespeed_version

    @pagespeed_version.setter
    def pagespeed_version(self, pagespeed_version):
        self._pagespeed_version = pagespeed_version

    @property
    def use_pagespeed(self):
        return self._use_pagespeed

    @use_pagespeed.setter
    def use_pagespeed(self, use_pagespeed):
        self._use_pagespeed = use_pagespeed

    @property
    def worker_processes(self):
        return self._worker_processes

    @worker_processes.setter
    def worker_processes(self, worker_processes):
        self._worker_processes = worker_processes


frecklet_class = NginxInstalledFromSource
