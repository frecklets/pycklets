# -*- coding: utf-8 -*-


#
# module path: pycklets.init_service_started.InitServiceStarted
#


from pyckles import AutoPycklet


class InitServiceStarted(AutoPycklet):
    """Make sure an init service is started.

       Args:
         name: The name of the service.

    """

    FRECKLET_ID = "init-service-started"

    def __init__(self, name=None):

        super(InitServiceStarted, self).__init__(var_names=["name"])
        self._name = name

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name


frecklet_class = InitServiceStarted
