# -*- coding: utf-8 -*-


#
# module path: pycklets.mailtrain_service.MailtrainService
#


from pyckles import AutoPycklet


class MailtrainService(AutoPycklet):
    """Install the mailtrain service.

     **NOTE**: This is work in progress, it'll probably not work.

       Args:
         version: The version of mailtrain.

    """

    FRECKLET_ID = "mailtrain-service"

    def __init__(self, version=None):

        super(MailtrainService, self).__init__(var_names=["version"])
        self._version = version

    @property
    def version(self):
        return self._version

    @version.setter
    def version(self, version):
        self._version = version


frecklet_class = MailtrainService
