# -*- coding: utf-8 -*-


#
# module path: pycklets.airflow_service.AirflowService
#


from pyckles import AutoPycklet


class AirflowService(AutoPycklet):
    """Install Postgresql service and create a database. Then install Airflow and configure it to use that database.

     TODO: reverse proxy, other options

       Args:
         airflow_executor: The executor class that airflow should use.

    """

    FRECKLET_ID = "airflow-service"

    def __init__(self, airflow_executor="LocalExecutor"):

        super(AirflowService, self).__init__(var_names=["airflow_executor"])
        self._airflow_executor = airflow_executor

    @property
    def airflow_executor(self):
        return self._airflow_executor

    @airflow_executor.setter
    def airflow_executor(self, airflow_executor):
        self._airflow_executor = airflow_executor


frecklet_class = AirflowService
