# -*- coding: utf-8 -*-


#
# module path: pycklets.apache_service.ApacheService
#


from pyckles import AutoPycklet


class ApacheService(AutoPycklet):
    """Installs the Apache web server.

     This uses the [geerlingguy.apache](https://github.com/geerlingguy/ansible-role-apache)
     Ansible role to do the heavy lifting.

     It's recommended to use the 'webserver-service' frecklet instead of this.

       Args:
         group: The group apache will run under.
         user: The user apache will run under.

    """

    FRECKLET_ID = "apache-service"

    def __init__(self, group=None, user=None):

        super(ApacheService, self).__init__(var_names=["group", "user"])
        self._group = group
        self._user = user

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user


frecklet_class = ApacheService
