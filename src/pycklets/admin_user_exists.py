# -*- coding: utf-8 -*-


#
# module path: pycklets.admin_user_exists.AdminUserExists
#


from pyckles import AutoPycklet


class AdminUserExists(AutoPycklet):
    """Create an admin user with an (optionally) provided password (hashed, for details see: [Ansible user module](https://docs.ansible.com/ansible/latest/modules/user_module.html)).

     If no ``admin_password`` argument is provided, the created user won't be able do login via ssh via
     password auth, and they won't be able to do sudo if passwordless sudo is not enabled for the user.

     Also lets you choose the default shell of that user, provide public ssh keys, and whether passwordless sudo should be enabled for the user.

       Args:
         admin_password: The user password.
         passwordless_sudo: Whether to enable passwordless sudo for this user.
         shell: Default shell of admin user.
         ssh_keys: A list of public ssh keys for this admin user.
         user_name: The username of the admin user.

    """

    FRECKLET_ID = "admin-user-exists"

    def __init__(
        self,
        admin_password=None,
        passwordless_sudo=None,
        shell="/bin/bash",
        ssh_keys=None,
        user_name=None,
    ):

        super(AdminUserExists, self).__init__(
            var_names=[
                "admin_password",
                "passwordless_sudo",
                "shell",
                "ssh_keys",
                "user_name",
            ]
        )
        self._admin_password = admin_password
        self._passwordless_sudo = passwordless_sudo
        self._shell = shell
        self._ssh_keys = ssh_keys
        self._user_name = user_name

    @property
    def admin_password(self):
        return self._admin_password

    @admin_password.setter
    def admin_password(self, admin_password):
        self._admin_password = admin_password

    @property
    def passwordless_sudo(self):
        return self._passwordless_sudo

    @passwordless_sudo.setter
    def passwordless_sudo(self, passwordless_sudo):
        self._passwordless_sudo = passwordless_sudo

    @property
    def shell(self):
        return self._shell

    @shell.setter
    def shell(self, shell):
        self._shell = shell

    @property
    def ssh_keys(self):
        return self._ssh_keys

    @ssh_keys.setter
    def ssh_keys(self, ssh_keys):
        self._ssh_keys = ssh_keys

    @property
    def user_name(self):
        return self._user_name

    @user_name.setter
    def user_name(self, user_name):
        self._user_name = user_name


frecklet_class = AdminUserExists
