# -*- coding: utf-8 -*-


#
# module path: pycklets.rabbitmq_service.RabbitmqService
#


from pyckles import AutoPycklet


class RabbitmqService(AutoPycklet):
    """Install RabbitMQ message broker service.

       Args:

    """

    FRECKLET_ID = "rabbitmq-service"

    def __init__(self,):

        super(RabbitmqService, self).__init__(var_names=[])


frecklet_class = RabbitmqService
