# -*- coding: utf-8 -*-


#
# module path: pycklets.pip_system_package_installed.PipSystemPackageInstalled
#


from pyckles import AutoPycklet


class PipSystemPackageInstalled(AutoPycklet):
    """Install a Python package system-wide, using root permissions.

       Args:
         package: The Python library to install or the url (bzr+,hg+,git+,svn+) of the remote packages.

    """

    FRECKLET_ID = "pip-system-package-installed"

    def __init__(self, package=None):

        super(PipSystemPackageInstalled, self).__init__(var_names=["package"])
        self._package = package

    @property
    def package(self):
        return self._package

    @package.setter
    def package(self, package):
        self._package = package


frecklet_class = PipSystemPackageInstalled
