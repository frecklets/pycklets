# -*- coding: utf-8 -*-


#
# module path: pycklets.rabbitmq_vhost_exists.RabbitmqVhostExists
#


from pyckles import AutoPycklet


class RabbitmqVhostExists(AutoPycklet):
    """Create RabbitMQ vhost.

       Args:
         name: The name of the vhost.

    """

    FRECKLET_ID = "rabbitmq-vhost-exists"

    def __init__(self, name=None):

        super(RabbitmqVhostExists, self).__init__(var_names=["name"])
        self._name = name

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name


frecklet_class = RabbitmqVhostExists
