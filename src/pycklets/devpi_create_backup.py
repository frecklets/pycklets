# -*- coding: utf-8 -*-


#
# module path: pycklets.devpi_create_backup.DevpiCreateBackup
#


from pyckles import AutoPycklet


class DevpiCreateBackup(AutoPycklet):
    """Note, this *frecklet* does not create the local destination parent folder for the fetched backup archive.

       Args:
         backup_archive_file: The (local) destination file/folder.

    """

    FRECKLET_ID = "devpi-create-backup"

    def __init__(self, backup_archive_file="~/Downloads/"):

        super(DevpiCreateBackup, self).__init__(var_names=["backup_archive_file"])
        self._backup_archive_file = backup_archive_file

    @property
    def backup_archive_file(self):
        return self._backup_archive_file

    @backup_archive_file.setter
    def backup_archive_file(self, backup_archive_file):
        self._backup_archive_file = backup_archive_file


frecklet_class = DevpiCreateBackup
