# -*- coding: utf-8 -*-


#
# module path: pycklets.nginx_server_block_file.NginxServerBlockFile
#


from pyckles import AutoPycklet


class NginxServerBlockFile(AutoPycklet):
    """Nginx server configuration

       Args:
         access_log: The access log.
         add_header: Adding header.
         basic_auth_message: The message that will be displayed to the user.
         basic_auth_user_file: The file to store htpasswd information.
         client_max_body_size: Client max body size.
         default_server: Whether this server is the 'default' (catchall) server.
         disable_ipv6: Whether to disable ipv6 for this server block.
         document_root: The document root.
         error_log: The error log path and (optional) log level.
         error_page: The error page.
         group: The group of the file.
         gzip: gzip configuration, if enabled
         gzip_enabled: Whether to enable gzip.
         index: The index files.
         listen_port: The port to listen to.
         listen_port_https: The port to listen for https.
         location_blocks: A list of location blocks.
         mode: The permissions of the file.
         owner: The owner of the file.
         path: The path to the file.
         proxy_read_timeout: Proxy read timeout.
         server_names: The server names.
         ssl_certificate_file: The ssl certificate file.
         ssl_certificate_key_file: The ssl certificate key file.
         ssl_trusted_certificate_file: The ssl certificate chain file.
         upstream: A list of upstreams.
         use_basic_auth: Whether to setup basic auth.
         use_https: Whether to use https.
         use_letsencrypt: Whether to use letsencrypt certificates.

    """

    FRECKLET_ID = "nginx-server-block-file"

    def __init__(
        self,
        access_log=None,
        add_header=None,
        basic_auth_message="Please authenticate.",
        basic_auth_user_file="/etc/htpasswd",
        client_max_body_size=None,
        default_server=None,
        disable_ipv6=None,
        document_root="/var/www/html",
        error_log=None,
        error_page=None,
        group=None,
        gzip=None,
        gzip_enabled=None,
        index=["index.html", "index.htm"],
        listen_port=80,
        listen_port_https=443,
        location_blocks=None,
        mode=None,
        owner=None,
        path=None,
        proxy_read_timeout=None,
        server_names=["localhost"],
        ssl_certificate_file=None,
        ssl_certificate_key_file=None,
        ssl_trusted_certificate_file=None,
        upstream=None,
        use_basic_auth=None,
        use_https=None,
        use_letsencrypt=True,
    ):

        super(NginxServerBlockFile, self).__init__(
            var_names=[
                "access_log",
                "add_header",
                "basic_auth_message",
                "basic_auth_user_file",
                "client_max_body_size",
                "default_server",
                "disable_ipv6",
                "document_root",
                "error_log",
                "error_page",
                "group",
                "gzip",
                "gzip_enabled",
                "index",
                "listen_port",
                "listen_port_https",
                "location_blocks",
                "mode",
                "owner",
                "path",
                "proxy_read_timeout",
                "server_names",
                "ssl_certificate_file",
                "ssl_certificate_key_file",
                "ssl_trusted_certificate_file",
                "upstream",
                "use_basic_auth",
                "use_https",
                "use_letsencrypt",
            ]
        )
        self._access_log = access_log
        self._add_header = add_header
        self._basic_auth_message = basic_auth_message
        self._basic_auth_user_file = basic_auth_user_file
        self._client_max_body_size = client_max_body_size
        self._default_server = default_server
        self._disable_ipv6 = disable_ipv6
        self._document_root = document_root
        self._error_log = error_log
        self._error_page = error_page
        self._group = group
        self._gzip = gzip
        self._gzip_enabled = gzip_enabled
        self._index = index
        self._listen_port = listen_port
        self._listen_port_https = listen_port_https
        self._location_blocks = location_blocks
        self._mode = mode
        self._owner = owner
        self._path = path
        self._proxy_read_timeout = proxy_read_timeout
        self._server_names = server_names
        self._ssl_certificate_file = ssl_certificate_file
        self._ssl_certificate_key_file = ssl_certificate_key_file
        self._ssl_trusted_certificate_file = ssl_trusted_certificate_file
        self._upstream = upstream
        self._use_basic_auth = use_basic_auth
        self._use_https = use_https
        self._use_letsencrypt = use_letsencrypt

    @property
    def access_log(self):
        return self._access_log

    @access_log.setter
    def access_log(self, access_log):
        self._access_log = access_log

    @property
    def add_header(self):
        return self._add_header

    @add_header.setter
    def add_header(self, add_header):
        self._add_header = add_header

    @property
    def basic_auth_message(self):
        return self._basic_auth_message

    @basic_auth_message.setter
    def basic_auth_message(self, basic_auth_message):
        self._basic_auth_message = basic_auth_message

    @property
    def basic_auth_user_file(self):
        return self._basic_auth_user_file

    @basic_auth_user_file.setter
    def basic_auth_user_file(self, basic_auth_user_file):
        self._basic_auth_user_file = basic_auth_user_file

    @property
    def client_max_body_size(self):
        return self._client_max_body_size

    @client_max_body_size.setter
    def client_max_body_size(self, client_max_body_size):
        self._client_max_body_size = client_max_body_size

    @property
    def default_server(self):
        return self._default_server

    @default_server.setter
    def default_server(self, default_server):
        self._default_server = default_server

    @property
    def disable_ipv6(self):
        return self._disable_ipv6

    @disable_ipv6.setter
    def disable_ipv6(self, disable_ipv6):
        self._disable_ipv6 = disable_ipv6

    @property
    def document_root(self):
        return self._document_root

    @document_root.setter
    def document_root(self, document_root):
        self._document_root = document_root

    @property
    def error_log(self):
        return self._error_log

    @error_log.setter
    def error_log(self, error_log):
        self._error_log = error_log

    @property
    def error_page(self):
        return self._error_page

    @error_page.setter
    def error_page(self, error_page):
        self._error_page = error_page

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def gzip(self):
        return self._gzip

    @gzip.setter
    def gzip(self, gzip):
        self._gzip = gzip

    @property
    def gzip_enabled(self):
        return self._gzip_enabled

    @gzip_enabled.setter
    def gzip_enabled(self, gzip_enabled):
        self._gzip_enabled = gzip_enabled

    @property
    def index(self):
        return self._index

    @index.setter
    def index(self, index):
        self._index = index

    @property
    def listen_port(self):
        return self._listen_port

    @listen_port.setter
    def listen_port(self, listen_port):
        self._listen_port = listen_port

    @property
    def listen_port_https(self):
        return self._listen_port_https

    @listen_port_https.setter
    def listen_port_https(self, listen_port_https):
        self._listen_port_https = listen_port_https

    @property
    def location_blocks(self):
        return self._location_blocks

    @location_blocks.setter
    def location_blocks(self, location_blocks):
        self._location_blocks = location_blocks

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, mode):
        self._mode = mode

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path

    @property
    def proxy_read_timeout(self):
        return self._proxy_read_timeout

    @proxy_read_timeout.setter
    def proxy_read_timeout(self, proxy_read_timeout):
        self._proxy_read_timeout = proxy_read_timeout

    @property
    def server_names(self):
        return self._server_names

    @server_names.setter
    def server_names(self, server_names):
        self._server_names = server_names

    @property
    def ssl_certificate_file(self):
        return self._ssl_certificate_file

    @ssl_certificate_file.setter
    def ssl_certificate_file(self, ssl_certificate_file):
        self._ssl_certificate_file = ssl_certificate_file

    @property
    def ssl_certificate_key_file(self):
        return self._ssl_certificate_key_file

    @ssl_certificate_key_file.setter
    def ssl_certificate_key_file(self, ssl_certificate_key_file):
        self._ssl_certificate_key_file = ssl_certificate_key_file

    @property
    def ssl_trusted_certificate_file(self):
        return self._ssl_trusted_certificate_file

    @ssl_trusted_certificate_file.setter
    def ssl_trusted_certificate_file(self, ssl_trusted_certificate_file):
        self._ssl_trusted_certificate_file = ssl_trusted_certificate_file

    @property
    def upstream(self):
        return self._upstream

    @upstream.setter
    def upstream(self, upstream):
        self._upstream = upstream

    @property
    def use_basic_auth(self):
        return self._use_basic_auth

    @use_basic_auth.setter
    def use_basic_auth(self, use_basic_auth):
        self._use_basic_auth = use_basic_auth

    @property
    def use_https(self):
        return self._use_https

    @use_https.setter
    def use_https(self, use_https):
        self._use_https = use_https

    @property
    def use_letsencrypt(self):
        return self._use_letsencrypt

    @use_letsencrypt.setter
    def use_letsencrypt(self, use_letsencrypt):
        self._use_letsencrypt = use_letsencrypt


frecklet_class = NginxServerBlockFile
