# -*- coding: utf-8 -*-


#
# module path: pycklets.file_fetched.FileFetched
#


from pyckles import AutoPycklet


class FileFetched(AutoPycklet):
    """This is still under development. For now, it won't create the local parent folder of the fetched file, and it'll
     always use the current (local) user as owner.

       Args:
         become: Whether to use elevated privileges to read the remote file. Avoid if possible.
         dest: The destination file on this host.
         flat: Whether to auto-rename the file after retrieval.
         src: The source file on the remote host.

    """

    FRECKLET_ID = "file-fetched"

    def __init__(self, become=None, dest="~/Downloads/", flat=True, src=None):

        super(FileFetched, self).__init__(var_names=["become", "dest", "flat", "src"])
        self._become = become
        self._dest = dest
        self._flat = flat
        self._src = src

    @property
    def become(self):
        return self._become

    @become.setter
    def become(self, become):
        self._become = become

    @property
    def dest(self):
        return self._dest

    @dest.setter
    def dest(self, dest):
        self._dest = dest

    @property
    def flat(self):
        return self._flat

    @flat.setter
    def flat(self, flat):
        self._flat = flat

    @property
    def src(self):
        return self._src

    @src.setter
    def src(self, src):
        self._src = src


frecklet_class = FileFetched
