# -*- coding: utf-8 -*-


#
# module path: pycklets.wordpress_folder_prepared.WordpressFolderPrepared
#


from pyckles import AutoPycklet


class WordpressFolderPrepared(AutoPycklet):
    """This uses the [oefenweb.wordpress Ansible role](https://github.com/Oefenweb/ansible-wordpress) to prepare a folder structure for
     multiple wordpress sites. For more information, check out the role
     documentation.

     First, it makes sure the [wp-cli](https://github.com/wp-cli/wp-cli) application is installed,
     then it downloads the 'Wordpress' application and puts it into the 'path' of each
     one of the 'wordpress_install' items. Then it configures (creates admin user, etc.) every one of those items
     (if not configured yet), and installs their themes and plugins.

     It also (optionally) sets up cron jobs and other options.

       Args:
         wordpress_installs: A dict describing all required wordpress installs.
         wp_cli_install_dir: The install directory for wp-cli.

    """

    FRECKLET_ID = "wordpress-folder-prepared"

    def __init__(self, wordpress_installs=None, wp_cli_install_dir=None):

        super(WordpressFolderPrepared, self).__init__(
            var_names=["wordpress_installs", "wp_cli_install_dir"]
        )
        self._wordpress_installs = wordpress_installs
        self._wp_cli_install_dir = wp_cli_install_dir

    @property
    def wordpress_installs(self):
        return self._wordpress_installs

    @wordpress_installs.setter
    def wordpress_installs(self, wordpress_installs):
        self._wordpress_installs = wordpress_installs

    @property
    def wp_cli_install_dir(self):
        return self._wp_cli_install_dir

    @wp_cli_install_dir.setter
    def wp_cli_install_dir(self, wp_cli_install_dir):
        self._wp_cli_install_dir = wp_cli_install_dir


frecklet_class = WordpressFolderPrepared
