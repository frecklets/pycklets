# -*- coding: utf-8 -*-


#
# module path: pycklets.folder_is_empty.FolderIsEmpty
#


from pyckles import AutoPycklet


class FolderIsEmpty(AutoPycklet):
    """Ensure a folder exists on the filesystem and is empty.

     If a file or folder already exists for that path, it will be deleted before the new folder will be created.

     If the ``owner`` and/or ``group`` variables is/are specified, create those in case they don't exist yet.

     If the ``owner`` variable is specified, this frecklet will use elevated permissions.

       Args:
         group: The group of the folder, will be created if necessary.
         mode: The permissions of the folder.
         owner: The owner of the folder, will be created if necessary.
         path: The path to the folder.
         system_user: Whether the user (and group) should be of system user/group type.

    """

    FRECKLET_ID = "folder-is-empty"

    def __init__(self, group=None, mode=None, owner=None, path=None, system_user=None):

        super(FolderIsEmpty, self).__init__(
            var_names=["group", "mode", "owner", "path", "system_user"]
        )
        self._group = group
        self._mode = mode
        self._owner = owner
        self._path = path
        self._system_user = system_user

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, mode):
        self._mode = mode

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path

    @property
    def system_user(self):
        return self._system_user

    @system_user.setter
    def system_user(self, system_user):
        self._system_user = system_user


frecklet_class = FolderIsEmpty
