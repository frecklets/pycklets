# -*- coding: utf-8 -*-


#
# module path: pycklets.sysctl_value.SysctlValue
#


from pyckles import AutoPycklet


class SysctlValue(AutoPycklet):
    """Set a sysctl value.

       Args:
         name: The name of the sysctl variable.
         set: Verify token value with the sysctl command and set with -w if necessary.
         value: The value.

    """

    FRECKLET_ID = "sysctl-value"

    def __init__(self, name=None, set=None, value=None):

        super(SysctlValue, self).__init__(var_names=["name", "set", "value"])
        self._name = name
        self._set = set
        self._value = value

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def set(self):
        return self._set

    @set.setter
    def set(self, set):
        self._set = set

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        self._value = value


frecklet_class = SysctlValue
