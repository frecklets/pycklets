# -*- coding: utf-8 -*-


#
# module path: pycklets.debug_secret_var.DebugSecretVar
#


from pyckles import AutoPycklet


class DebugSecretVar(AutoPycklet):
    """Displays the content of a secret variable.

     You most likely don't need this, this is really only for development.

       Args:
         var: the Ansible variable name to debug

    """

    FRECKLET_ID = "debug-secret-var"

    def __init__(self, var=None):

        super(DebugSecretVar, self).__init__(var_names=["var"])
        self._var = var

    @property
    def var(self):
        return self._var

    @var.setter
    def var(self, var):
        self._var = var


frecklet_class = DebugSecretVar
