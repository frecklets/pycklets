# -*- coding: utf-8 -*-


#
# module path: pycklets.ec2_instance_absent.Ec2InstanceAbsent
#


from pyckles import AutoPycklet


class Ec2InstanceAbsent(AutoPycklet):
    """Create an instance on Amazon EC2, if it doesn't exist yet.

       Args:
         aws_access_key: The AWS access key.
         aws_secret_key: The AWS secret key.
         instance_id: The instance id.
         region: The AWS region to use.

    """

    FRECKLET_ID = "ec2-instance-absent"

    def __init__(
        self, aws_access_key=None, aws_secret_key=None, instance_id=None, region=None
    ):

        super(Ec2InstanceAbsent, self).__init__(
            var_names=["aws_access_key", "aws_secret_key", "instance_id", "region"]
        )
        self._aws_access_key = aws_access_key
        self._aws_secret_key = aws_secret_key
        self._instance_id = instance_id
        self._region = region

    @property
    def aws_access_key(self):
        return self._aws_access_key

    @aws_access_key.setter
    def aws_access_key(self, aws_access_key):
        self._aws_access_key = aws_access_key

    @property
    def aws_secret_key(self):
        return self._aws_secret_key

    @aws_secret_key.setter
    def aws_secret_key(self, aws_secret_key):
        self._aws_secret_key = aws_secret_key

    @property
    def instance_id(self):
        return self._instance_id

    @instance_id.setter
    def instance_id(self, instance_id):
        self._instance_id = instance_id

    @property
    def region(self):
        return self._region

    @region.setter
    def region(self, region):
        self._region = region


frecklet_class = Ec2InstanceAbsent
