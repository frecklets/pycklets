# -*- coding: utf-8 -*-


#
# module path: pycklets.python_dev_project.PythonDevProject
#


from pyckles import AutoPycklet


class PythonDevProject(AutoPycklet):
    """(Optionally) clone a Python project git repo, install the right version of Python using pyenv, create a virtualenv for the
     project, then install the project and it's requirements into the virtualenv for development.

     Also create a .python-version in the project folder so the virtualenv is always activated when the folder is visited
     in the shell.

       Args:
         project_folder: The project folder.
         project_name: The name of the project.
         project_repo: The git repo url of the project (optional).
         python_version: The version of python.

    """

    FRECKLET_ID = "python-dev-project"

    def __init__(
        self,
        project_folder=None,
        project_name=None,
        project_repo=None,
        python_version="latest",
    ):

        super(PythonDevProject, self).__init__(
            var_names=[
                "project_folder",
                "project_name",
                "project_repo",
                "python_version",
            ]
        )
        self._project_folder = project_folder
        self._project_name = project_name
        self._project_repo = project_repo
        self._python_version = python_version

    @property
    def project_folder(self):
        return self._project_folder

    @project_folder.setter
    def project_folder(self, project_folder):
        self._project_folder = project_folder

    @property
    def project_name(self):
        return self._project_name

    @project_name.setter
    def project_name(self, project_name):
        self._project_name = project_name

    @property
    def project_repo(self):
        return self._project_repo

    @project_repo.setter
    def project_repo(self, project_repo):
        self._project_repo = project_repo

    @property
    def python_version(self):
        return self._python_version

    @python_version.setter
    def python_version(self, python_version):
        self._python_version = python_version


frecklet_class = PythonDevProject
