# -*- coding: utf-8 -*-


#
# module path: pycklets.execute_command.ExecuteCommand
#


from pyckles import AutoPycklet


class ExecuteCommand(AutoPycklet):
    """Execute a single command. The command won't be processed through the shell, so it won't reckognize environment
     variables like ``$HOME``, and can't be used to do shell operation like "<", ">", etc.

     Use the frecklet::execute-shell frecklet if that is what you need.

     Behind the scenes, this uses the ['command' Ansible module](https://docs.ansible.com/ansible/latest/modules/command_module.html),
     so check its documentation for more details.

       Args:
         become_user: The user to execute this command as.
         chdir: The working directory.
         command: The command to execute.
         ignore_error: Whether to ignore any potential errors.
         no_log: Whether to hide the log of this command (because for example the command contains sensitive information).
         register_name: Name of the register to store the result of this command.

    """

    FRECKLET_ID = "execute-command"

    def __init__(
        self,
        become_user=None,
        chdir=None,
        command=None,
        ignore_error=None,
        no_log=None,
        register_name=None,
    ):

        super(ExecuteCommand, self).__init__(
            var_names=[
                "become_user",
                "chdir",
                "command",
                "ignore_error",
                "no_log",
                "register_name",
            ]
        )
        self._become_user = become_user
        self._chdir = chdir
        self._command = command
        self._ignore_error = ignore_error
        self._no_log = no_log
        self._register_name = register_name

    @property
    def become_user(self):
        return self._become_user

    @become_user.setter
    def become_user(self, become_user):
        self._become_user = become_user

    @property
    def chdir(self):
        return self._chdir

    @chdir.setter
    def chdir(self, chdir):
        self._chdir = chdir

    @property
    def command(self):
        return self._command

    @command.setter
    def command(self, command):
        self._command = command

    @property
    def ignore_error(self):
        return self._ignore_error

    @ignore_error.setter
    def ignore_error(self, ignore_error):
        self._ignore_error = ignore_error

    @property
    def no_log(self):
        return self._no_log

    @no_log.setter
    def no_log(self, no_log):
        self._no_log = no_log

    @property
    def register_name(self):
        return self._register_name

    @register_name.setter
    def register_name(self, register_name):
        self._register_name = register_name


frecklet_class = ExecuteCommand
