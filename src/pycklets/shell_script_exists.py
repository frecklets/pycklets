# -*- coding: utf-8 -*-


#
# module path: pycklets.shell_script_exists.ShellScriptExists
#


from pyckles import AutoPycklet


class ShellScriptExists(AutoPycklet):
    """Creates an executable file with the provided script content.

       Args:
         group: The group of the file.
         mode: The permissions of the script.
         owner: The owner of the file.
         parent_dir_mode: The permissions of the parent directory.
         path: The path to the script.
         script_content: The script content.
         system_user: Whether the user and group should be of system user/group type.

    """

    FRECKLET_ID = "shell-script-exists"

    def __init__(
        self,
        group=None,
        mode="0755",
        owner=None,
        parent_dir_mode=None,
        path=None,
        script_content=None,
        system_user=None,
    ):

        super(ShellScriptExists, self).__init__(
            var_names=[
                "group",
                "mode",
                "owner",
                "parent_dir_mode",
                "path",
                "script_content",
                "system_user",
            ]
        )
        self._group = group
        self._mode = mode
        self._owner = owner
        self._parent_dir_mode = parent_dir_mode
        self._path = path
        self._script_content = script_content
        self._system_user = system_user

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, mode):
        self._mode = mode

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    @property
    def parent_dir_mode(self):
        return self._parent_dir_mode

    @parent_dir_mode.setter
    def parent_dir_mode(self, parent_dir_mode):
        self._parent_dir_mode = parent_dir_mode

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path

    @property
    def script_content(self):
        return self._script_content

    @script_content.setter
    def script_content(self, script_content):
        self._script_content = script_content

    @property
    def system_user(self):
        return self._system_user

    @system_user.setter
    def system_user(self, system_user):
        self._system_user = system_user


frecklet_class = ShellScriptExists
