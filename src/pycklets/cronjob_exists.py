# -*- coding: utf-8 -*-


#
# module path: pycklets.cronjob_exists.CronjobExists
#


from pyckles import AutoPycklet


class CronjobExists(AutoPycklet):
    """Ensures a cron job exists.

     This mostly just forwards arguments to the 'cron' Ansible module, please check it's documentation for more details.

       Args:
         day: Day of the month the job should run ( 1-31, *, */2, etc ).
         hour: Hour when the job should run ( 0-23, *, */2, etc ).
         job: The command to execute.
         minute: Minute when the job should run ( 0-59, *, */2, etc ).
         month: Month of the year the job should run ( 1-12, *, */2, etc ).
         name: Description of a crontab entry.
         special_time: Special time specification nickname.
         user: The specific user whose crontab should be modified. When unset, this parameter defaults to using root.
         weekday: Day of the week that the job should run ( 0-6 for Sunday-Saturday, *, etc ).

    """

    FRECKLET_ID = "cronjob-exists"

    def __init__(
        self,
        day=None,
        hour=None,
        job=None,
        minute=None,
        month=None,
        name=None,
        special_time=None,
        user="root",
        weekday=None,
    ):

        super(CronjobExists, self).__init__(
            var_names=[
                "day",
                "hour",
                "job",
                "minute",
                "month",
                "name",
                "special_time",
                "user",
                "weekday",
            ]
        )
        self._day = day
        self._hour = hour
        self._job = job
        self._minute = minute
        self._month = month
        self._name = name
        self._special_time = special_time
        self._user = user
        self._weekday = weekday

    @property
    def day(self):
        return self._day

    @day.setter
    def day(self, day):
        self._day = day

    @property
    def hour(self):
        return self._hour

    @hour.setter
    def hour(self, hour):
        self._hour = hour

    @property
    def job(self):
        return self._job

    @job.setter
    def job(self, job):
        self._job = job

    @property
    def minute(self):
        return self._minute

    @minute.setter
    def minute(self, minute):
        self._minute = minute

    @property
    def month(self):
        return self._month

    @month.setter
    def month(self, month):
        self._month = month

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def special_time(self):
        return self._special_time

    @special_time.setter
    def special_time(self, special_time):
        self._special_time = special_time

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user

    @property
    def weekday(self):
        return self._weekday

    @weekday.setter
    def weekday(self, weekday):
        self._weekday = weekday


frecklet_class = CronjobExists
