# -*- coding: utf-8 -*-


#
# module path: pycklets.osx_command_line_tools_installed.OsxCommandLineToolsInstalled
#


from pyckles import AutoPycklet


class OsxCommandLineToolsInstalled(AutoPycklet):
    """Install the Mac OS X command-line tools (without Xcode).

     The Command Line Tool package gives Mac terminal users many commonly used tools, utilities, and compilers, including
     make, GCC, clang, perl, svn, git, size, strip, strings, libtool, cpp, what, and many other useful commands that are
     usually found in default linux installations.

       Args:
         force: Force the install, even if the tools are already present.

    """

    FRECKLET_ID = "osx-command-line-tools-installed"

    def __init__(self, force=None):

        super(OsxCommandLineToolsInstalled, self).__init__(var_names=["force"])
        self._force = force

    @property
    def force(self):
        return self._force

    @force.setter
    def force(self, force):
        self._force = force


frecklet_class = OsxCommandLineToolsInstalled
