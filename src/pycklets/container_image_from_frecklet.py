# -*- coding: utf-8 -*-


#
# module path: pycklets.container_image_from_frecklet.ContainerImageFromFrecklet
#


from pyckles import AutoPycklet


class ContainerImageFromFrecklet(AutoPycklet):
    """If 'install_packer' is set to 'true' and Packer is not already present, install Packer into '$HOME/.local/bin'. Note that this might not work if you are on a non Linux/amd64 platform. In that case it's better to use the 'packer-installed' frecklet manually, before-hand and set this to 'false'.

     Create a Docker or LXD image from a base-image and a frecklet.

     Create a temporary working directory, then create two files to control the packer process:

       - packer-template.json: packer config file containing the call to the 'frecklecute' provisioner
       - source-frecklet.frecklet: the 'wrapper' frecklet to execute

     Then execute Packer via:

         packer build packer-template.json

     If no 'image_name' is provided, the name of the *frecklet* is used. If no 'source_image' is provided,
     a base Ubuntu 18.04 image is used ('ubuntu:18.04' for Docker, 'images:ubuntu/18.04' for LXD).

       Args:
         freckles_extra_args: Extra base args for the freckles run.
         frecklet_name: The name of the frecklet.
         frecklet_vars: The frecklet vars.
         image_name: The image name.
         image_type: The container image type.
         install_packer: Install Packer into '$HOME/.local/bin', if not already available.
         source_image: The name of the source/parent image.

    """

    FRECKLET_ID = "container-image-from-frecklet"

    def __init__(
        self,
        freckles_extra_args=None,
        frecklet_name=None,
        frecklet_vars=None,
        image_name=None,
        image_type="docker",
        install_packer=None,
        source_image=None,
    ):

        super(ContainerImageFromFrecklet, self).__init__(
            var_names=[
                "freckles_extra_args",
                "frecklet_name",
                "frecklet_vars",
                "image_name",
                "image_type",
                "install_packer",
                "source_image",
            ]
        )
        self._freckles_extra_args = freckles_extra_args
        self._frecklet_name = frecklet_name
        self._frecklet_vars = frecklet_vars
        self._image_name = image_name
        self._image_type = image_type
        self._install_packer = install_packer
        self._source_image = source_image

    @property
    def freckles_extra_args(self):
        return self._freckles_extra_args

    @freckles_extra_args.setter
    def freckles_extra_args(self, freckles_extra_args):
        self._freckles_extra_args = freckles_extra_args

    @property
    def frecklet_name(self):
        return self._frecklet_name

    @frecklet_name.setter
    def frecklet_name(self, frecklet_name):
        self._frecklet_name = frecklet_name

    @property
    def frecklet_vars(self):
        return self._frecklet_vars

    @frecklet_vars.setter
    def frecklet_vars(self, frecklet_vars):
        self._frecklet_vars = frecklet_vars

    @property
    def image_name(self):
        return self._image_name

    @image_name.setter
    def image_name(self, image_name):
        self._image_name = image_name

    @property
    def image_type(self):
        return self._image_type

    @image_type.setter
    def image_type(self, image_type):
        self._image_type = image_type

    @property
    def install_packer(self):
        return self._install_packer

    @install_packer.setter
    def install_packer(self, install_packer):
        self._install_packer = install_packer

    @property
    def source_image(self):
        return self._source_image

    @source_image.setter
    def source_image(self, source_image):
        self._source_image = source_image


frecklet_class = ContainerImageFromFrecklet
