# -*- coding: utf-8 -*-


#
# module path: pycklets.gitlab_runner_installed.GitlabRunnerInstalled
#


from pyckles import AutoPycklet


class GitlabRunnerInstalled(AutoPycklet):
    """Install a gitlab runner.

       Args:
         arch: The architecture of the target system.
         dest: The (absolute) path to the parent folder of the downloaded executable file.
         group: The group of the executable.
         owner: The owner of the executable.
         platform: The target platform.

    """

    FRECKLET_ID = "gitlab-runner-installed"

    def __init__(
        self, arch="amd64", dest=None, group=None, owner=None, platform="linux"
    ):

        super(GitlabRunnerInstalled, self).__init__(
            var_names=["arch", "dest", "group", "owner", "platform"]
        )
        self._arch = arch
        self._dest = dest
        self._group = group
        self._owner = owner
        self._platform = platform

    @property
    def arch(self):
        return self._arch

    @arch.setter
    def arch(self, arch):
        self._arch = arch

    @property
    def dest(self):
        return self._dest

    @dest.setter
    def dest(self, dest):
        self._dest = dest

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    @property
    def platform(self):
        return self._platform

    @platform.setter
    def platform(self, platform):
        self._platform = platform


frecklet_class = GitlabRunnerInstalled
