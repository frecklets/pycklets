# -*- coding: utf-8 -*-


#
# module path: pycklets.path_is_synced.PathIsSynced
#


from pyckles import AutoPycklet


class PathIsSynced(AutoPycklet):
    """Make sure a file or folder is synced between two locations.

     This does also create the remote owner/group if necessary.

     For this frecklet, the “local host” is the host the synchronize task originates on, and the “destination host” is the host synchronize is connecting to.

       Args:
         become: whether to use root permissions to do the download/chown of target
         delete_on_dest: Whether to delete the files on 'dest' that don't exist on the 'src'.
         dest: the host and path of the target
         group: the group of the dest folder/file
         mode: The permissions of the folder.
         owner: the owner of the dest folder/file
         src: the host and path of the source

    """

    FRECKLET_ID = "path-is-synced"

    def __init__(
        self,
        become=None,
        delete_on_dest=None,
        dest=None,
        group=None,
        mode=None,
        owner=None,
        src=None,
    ):

        super(PathIsSynced, self).__init__(
            var_names=[
                "become",
                "delete_on_dest",
                "dest",
                "group",
                "mode",
                "owner",
                "src",
            ]
        )
        self._become = become
        self._delete_on_dest = delete_on_dest
        self._dest = dest
        self._group = group
        self._mode = mode
        self._owner = owner
        self._src = src

    @property
    def become(self):
        return self._become

    @become.setter
    def become(self, become):
        self._become = become

    @property
    def delete_on_dest(self):
        return self._delete_on_dest

    @delete_on_dest.setter
    def delete_on_dest(self, delete_on_dest):
        self._delete_on_dest = delete_on_dest

    @property
    def dest(self):
        return self._dest

    @dest.setter
    def dest(self, dest):
        self._dest = dest

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, mode):
        self._mode = mode

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    @property
    def src(self):
        return self._src

    @src.setter
    def src(self, src):
        self._src = src


frecklet_class = PathIsSynced
