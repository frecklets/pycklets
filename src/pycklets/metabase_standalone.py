# -*- coding: utf-8 -*-


#
# module path: pycklets.metabase_standalone.MetabaseStandalone
#


from pyckles import AutoPycklet


class MetabaseStandalone(AutoPycklet):
    """Install a Postgresql database service on the host and create a database. Then install Metabase, and configure it to use said database.

     In case no 'metabase_db_password' is specified, freckles will generate a random one.

     TODO: reverse proxy

     NOTE: work in progess, will probably fail

       Args:
         metabase_db_password: The password for the db connection.
         metabase_host: The hostname to listen on.
         metabase_port: The port for metabase to listen on.

    """

    FRECKLET_ID = "metabase-standalone"

    def __init__(
        self,
        metabase_db_password="metabase_password",
        metabase_host="localhost",
        metabase_port=3000,
    ):

        super(MetabaseStandalone, self).__init__(
            var_names=["metabase_db_password", "metabase_host", "metabase_port"]
        )
        self._metabase_db_password = metabase_db_password
        self._metabase_host = metabase_host
        self._metabase_port = metabase_port

    @property
    def metabase_db_password(self):
        return self._metabase_db_password

    @metabase_db_password.setter
    def metabase_db_password(self, metabase_db_password):
        self._metabase_db_password = metabase_db_password

    @property
    def metabase_host(self):
        return self._metabase_host

    @metabase_host.setter
    def metabase_host(self, metabase_host):
        self._metabase_host = metabase_host

    @property
    def metabase_port(self):
        return self._metabase_port

    @metabase_port.setter
    def metabase_port(self, metabase_port):
        self._metabase_port = metabase_port


frecklet_class = MetabaseStandalone
