# -*- coding: utf-8 -*-


#
# module path: pycklets.gitlab_project_deploy_key_authorized.GitlabProjectDeployKeyAuthorized
#


from pyckles import AutoPycklet


class GitlabProjectDeployKeyAuthorized(AutoPycklet):
    """Add a deploy key to a GitLab project.

     This can be used to authorize your (or a host-) ssh public key to be able to pull from private repositories, or push a public or private repository. Useful for CI pipelines and such.

       Args:
         access_token: The Gitlab access token.
         can_push: Whether the key in question is allowed to push to the repo.
         deploy_key_title: The title of the deploy key on Gitlab.
         gitlab_project: The name of the Gitlab project.
         gitlab_user: The Gitlab user- or organization name.
         key_content: The content of the public ssh key.
         password: The password to unlock the key (only used if key doesn't exist already).
         path: The path to the private ssh key.
         user: The name of the ssh key owner.

    """

    FRECKLET_ID = "gitlab-project-deploy-key-authorized"

    def __init__(
        self,
        access_token=None,
        can_push=None,
        deploy_key_title="deploy",
        gitlab_project=None,
        gitlab_user=None,
        key_content=None,
        password=None,
        path="~/.ssh/id_",
        user=None,
    ):

        super(GitlabProjectDeployKeyAuthorized, self).__init__(
            var_names=[
                "access_token",
                "can_push",
                "deploy_key_title",
                "gitlab_project",
                "gitlab_user",
                "key_content",
                "password",
                "path",
                "user",
            ]
        )
        self._access_token = access_token
        self._can_push = can_push
        self._deploy_key_title = deploy_key_title
        self._gitlab_project = gitlab_project
        self._gitlab_user = gitlab_user
        self._key_content = key_content
        self._password = password
        self._path = path
        self._user = user

    @property
    def access_token(self):
        return self._access_token

    @access_token.setter
    def access_token(self, access_token):
        self._access_token = access_token

    @property
    def can_push(self):
        return self._can_push

    @can_push.setter
    def can_push(self, can_push):
        self._can_push = can_push

    @property
    def deploy_key_title(self):
        return self._deploy_key_title

    @deploy_key_title.setter
    def deploy_key_title(self, deploy_key_title):
        self._deploy_key_title = deploy_key_title

    @property
    def gitlab_project(self):
        return self._gitlab_project

    @gitlab_project.setter
    def gitlab_project(self, gitlab_project):
        self._gitlab_project = gitlab_project

    @property
    def gitlab_user(self):
        return self._gitlab_user

    @gitlab_user.setter
    def gitlab_user(self, gitlab_user):
        self._gitlab_user = gitlab_user

    @property
    def key_content(self):
        return self._key_content

    @key_content.setter
    def key_content(self, key_content):
        self._key_content = key_content

    @property
    def password(self):
        return self._password

    @password.setter
    def password(self, password):
        self._password = password

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user


frecklet_class = GitlabProjectDeployKeyAuthorized
