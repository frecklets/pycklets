# -*- coding: utf-8 -*-


#
# module path: pycklets.docker_image_from_folder.DockerImageFromFolder
#


from pyckles import AutoPycklet


class DockerImageFromFolder(AutoPycklet):
    """No documentation available.

       Args:
         base_path: The path where the Dockerfile and build context live.
         force: Whether to force the build to be done, even if an image with that name exists already.
         name: The name of the image.
         nocache: Whether to use cache when building an image.

    """

    FRECKLET_ID = "docker-image-from-folder"

    def __init__(self, base_path=None, force=None, name=None, nocache=None):

        super(DockerImageFromFolder, self).__init__(
            var_names=["base_path", "force", "name", "nocache"]
        )
        self._base_path = base_path
        self._force = force
        self._name = name
        self._nocache = nocache

    @property
    def base_path(self):
        return self._base_path

    @base_path.setter
    def base_path(self, base_path):
        self._base_path = base_path

    @property
    def force(self):
        return self._force

    @force.setter
    def force(self, force):
        self._force = force

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def nocache(self):
        return self._nocache

    @nocache.setter
    def nocache(self, nocache):
        self._nocache = nocache


frecklet_class = DockerImageFromFolder
