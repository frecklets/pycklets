# -*- coding: utf-8 -*-


#
# module path: pycklets.set_internal_var.SetInternalVar
#


from pyckles import AutoPycklet


class SetInternalVar(AutoPycklet):
    """Sets a nsbl-connector internal variable. This is mainly useful for debugging, but also has a few other uses
     if you are in a pinch. Usually it's best to not have to use this.

       Args:
         value: the value
         var: the var name

    """

    FRECKLET_ID = "set-internal-var"

    def __init__(self, value=None, var=None):

        super(SetInternalVar, self).__init__(var_names=["value", "var"])
        self._value = value
        self._var = var

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        self._value = value

    @property
    def var(self):
        return self._var

    @var.setter
    def var(self, var):
        self._var = var


frecklet_class = SetInternalVar
