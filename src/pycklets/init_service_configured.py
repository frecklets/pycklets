# -*- coding: utf-8 -*-


#
# module path: pycklets.init_service_configured.InitServiceConfigured
#


from pyckles import AutoPycklet


class InitServiceConfigured(AutoPycklet):
    """Configure an init service.

     You can use this to 'enable', 'disable', 'start' and 'stop' a service.

       Args:
         enabled: Whether to enable the service or not.
         instance_name: The instance name, in case the service is a systemd template unit.
         name: The name of the service.
         started: Whether to start the service or not.

    """

    FRECKLET_ID = "init-service-configured"

    def __init__(self, enabled=None, instance_name=None, name=None, started=None):

        super(InitServiceConfigured, self).__init__(
            var_names=["enabled", "instance_name", "name", "started"]
        )
        self._enabled = enabled
        self._instance_name = instance_name
        self._name = name
        self._started = started

    @property
    def enabled(self):
        return self._enabled

    @enabled.setter
    def enabled(self, enabled):
        self._enabled = enabled

    @property
    def instance_name(self):
        return self._instance_name

    @instance_name.setter
    def instance_name(self, instance_name):
        self._instance_name = instance_name

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def started(self):
        return self._started

    @started.setter
    def started(self, started):
        self._started = started


frecklet_class = InitServiceConfigured
