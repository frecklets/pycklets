# -*- coding: utf-8 -*-


#
# module path: pycklets.nginx_vhost_from_folder.NginxVhostFromFolder
#


from pyckles import AutoPycklet


class NginxVhostFromFolder(AutoPycklet):
    """Create a Nginx server block configuration file for hosting static html pages.

     Create a file '``/etc/nginx/sites-enabled/[hostname].[http(s)].conf``' and include configuration for a Nginx server block
     that uses the provided document root (default: '/var/www/html') to host a static web page.

     If 'use_https' is set to true, an automatic redirect from port ``http://...:80`` to ``https:/...:443`` is set up
     (certificates need to be created/put in place seperately). This uses the
     [``nginx-server-block-file``](https://freckles.io/frecklets/default/misc/nginx-server-block-file) *frecklet* to
     create the actual configuration file. For now, not all of the possible
     configuration options are forwarded, but those will be added as a need arises.

       Args:
         default_server: Whether this server is the 'default' (catchall) server.
         document_root: The document root.
         gzip_enabled: Whether to enable gzip.
         hostname: The domain name the webserver should listen on.
         index: The index file name(s).
         server_admin: The server admin email.
         server_names: The server names.
         use_https: Whether to use https (and request a letsencrypt certificate).

    """

    FRECKLET_ID = "nginx-vhost-from-folder"

    def __init__(
        self,
        default_server=None,
        document_root="/var/www/html",
        gzip_enabled=True,
        hostname="_",
        index=["index.html", "index.htm"],
        server_admin=None,
        server_names=["localhost"],
        use_https=None,
    ):

        super(NginxVhostFromFolder, self).__init__(
            var_names=[
                "default_server",
                "document_root",
                "gzip_enabled",
                "hostname",
                "index",
                "server_admin",
                "server_names",
                "use_https",
            ]
        )
        self._default_server = default_server
        self._document_root = document_root
        self._gzip_enabled = gzip_enabled
        self._hostname = hostname
        self._index = index
        self._server_admin = server_admin
        self._server_names = server_names
        self._use_https = use_https

    @property
    def default_server(self):
        return self._default_server

    @default_server.setter
    def default_server(self, default_server):
        self._default_server = default_server

    @property
    def document_root(self):
        return self._document_root

    @document_root.setter
    def document_root(self, document_root):
        self._document_root = document_root

    @property
    def gzip_enabled(self):
        return self._gzip_enabled

    @gzip_enabled.setter
    def gzip_enabled(self, gzip_enabled):
        self._gzip_enabled = gzip_enabled

    @property
    def hostname(self):
        return self._hostname

    @hostname.setter
    def hostname(self, hostname):
        self._hostname = hostname

    @property
    def index(self):
        return self._index

    @index.setter
    def index(self, index):
        self._index = index

    @property
    def server_admin(self):
        return self._server_admin

    @server_admin.setter
    def server_admin(self, server_admin):
        self._server_admin = server_admin

    @property
    def server_names(self):
        return self._server_names

    @server_names.setter
    def server_names(self, server_names):
        self._server_names = server_names

    @property
    def use_https(self):
        return self._use_https

    @use_https.setter
    def use_https(self, use_https):
        self._use_https = use_https


frecklet_class = NginxVhostFromFolder
