# -*- coding: utf-8 -*-


#
# module path: pycklets.hcloud_server_exists.HcloudServerExists
#


from pyckles import AutoPycklet


class HcloudServerExists(AutoPycklet):
    """Create a server with the specified name/id exists in your Hetzner hcloud account, if it doesn't exist yet.

     If a server exists, nothing is done, even if the 'image' and 'server_type' are different to the deployed server.

       Args:
         api_token: The hcloud API token.
         extra_wait_time: Extra wait time after ssh is ready.
         image: The OS image of the server.
         register_var: The name of the variable to register the result of this frecklet.
         server_name: The name of the cloud server to manage (for now, don't use '_', '-', or other special characters in the server name -- will be fixed later).
         server_type: The type of server to create.
         ssh_keys: List of SSH key names that correspond to the SSH keys configured for your Hetzner Cloud account access.
         wait_for_ssh: Whether to wait for ssh to become available

    """

    FRECKLET_ID = "hcloud-server-exists"

    def __init__(
        self,
        api_token=None,
        extra_wait_time=None,
        image="ubuntu-18.04",
        register_var="_register_hcloud_server",
        server_name=None,
        server_type="cx11",
        ssh_keys=None,
        wait_for_ssh=True,
    ):

        super(HcloudServerExists, self).__init__(
            var_names=[
                "api_token",
                "extra_wait_time",
                "image",
                "register_var",
                "server_name",
                "server_type",
                "ssh_keys",
                "wait_for_ssh",
            ]
        )
        self._api_token = api_token
        self._extra_wait_time = extra_wait_time
        self._image = image
        self._register_var = register_var
        self._server_name = server_name
        self._server_type = server_type
        self._ssh_keys = ssh_keys
        self._wait_for_ssh = wait_for_ssh

    @property
    def api_token(self):
        return self._api_token

    @api_token.setter
    def api_token(self, api_token):
        self._api_token = api_token

    @property
    def extra_wait_time(self):
        return self._extra_wait_time

    @extra_wait_time.setter
    def extra_wait_time(self, extra_wait_time):
        self._extra_wait_time = extra_wait_time

    @property
    def image(self):
        return self._image

    @image.setter
    def image(self, image):
        self._image = image

    @property
    def register_var(self):
        return self._register_var

    @register_var.setter
    def register_var(self, register_var):
        self._register_var = register_var

    @property
    def server_name(self):
        return self._server_name

    @server_name.setter
    def server_name(self, server_name):
        self._server_name = server_name

    @property
    def server_type(self):
        return self._server_type

    @server_type.setter
    def server_type(self, server_type):
        self._server_type = server_type

    @property
    def ssh_keys(self):
        return self._ssh_keys

    @ssh_keys.setter
    def ssh_keys(self, ssh_keys):
        self._ssh_keys = ssh_keys

    @property
    def wait_for_ssh(self):
        return self._wait_for_ssh

    @wait_for_ssh.setter
    def wait_for_ssh(self, wait_for_ssh):
        self._wait_for_ssh = wait_for_ssh


frecklet_class = HcloudServerExists
