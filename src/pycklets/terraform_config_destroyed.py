# -*- coding: utf-8 -*-


#
# module path: pycklets.terraform_config_destroyed.TerraformConfigDestroyed
#


from pyckles import AutoPycklet


class TerraformConfigDestroyed(AutoPycklet):
    """Destroy a terraform configuration using the Ansible 'terraform' module.

       Args:
         path: The path to the terraform configuration folder.

    """

    FRECKLET_ID = "terraform-config-destroyed"

    def __init__(self, path=None):

        super(TerraformConfigDestroyed, self).__init__(var_names=["path"])
        self._path = path

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path


frecklet_class = TerraformConfigDestroyed
