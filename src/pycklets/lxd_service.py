# -*- coding: utf-8 -*-


#
# module path: pycklets.lxd_service.LxdService
#


from pyckles import AutoPycklet


class LxdService(AutoPycklet):
    """Install/enable LXD on a target system.

       Args:
         bridge_addr: The ipv4 address for the bridge.
         bridge_dhcp_max: The ipv4 dhcp max value.
         bridge_dhcp_range: The ipv4 dhcp range (e.g. '10.10.10.2,10.10.10.200')
         bridge_ipv6_proxy: Whether to enable the ipv6 proxy on the bridge.
         bridge_name: If used, the name of the bridge.
         bridge_nat: Whether to enable nat for the ipv4 bridge.
         bridge_netmask: The ipv4 netmask.
         bridge_network: The ipv4 network (in CIDR, e.g. '10.10.10.10/24').
         create_bridge: Whether to use (and create if necessary) a bridge network (default: true)
         subid_range: Subid range.
         subid_start: Subid start.
         users: A list of users who will be added to the 'lxd' group.

    """

    FRECKLET_ID = "lxd-service"

    def __init__(
        self,
        bridge_addr=None,
        bridge_dhcp_max=None,
        bridge_dhcp_range=None,
        bridge_ipv6_proxy=None,
        bridge_name=None,
        bridge_nat=None,
        bridge_netmask=None,
        bridge_network=None,
        create_bridge=True,
        subid_range=None,
        subid_start=None,
        users=None,
    ):

        super(LxdService, self).__init__(
            var_names=[
                "bridge_addr",
                "bridge_dhcp_max",
                "bridge_dhcp_range",
                "bridge_ipv6_proxy",
                "bridge_name",
                "bridge_nat",
                "bridge_netmask",
                "bridge_network",
                "create_bridge",
                "subid_range",
                "subid_start",
                "users",
            ]
        )
        self._bridge_addr = bridge_addr
        self._bridge_dhcp_max = bridge_dhcp_max
        self._bridge_dhcp_range = bridge_dhcp_range
        self._bridge_ipv6_proxy = bridge_ipv6_proxy
        self._bridge_name = bridge_name
        self._bridge_nat = bridge_nat
        self._bridge_netmask = bridge_netmask
        self._bridge_network = bridge_network
        self._create_bridge = create_bridge
        self._subid_range = subid_range
        self._subid_start = subid_start
        self._users = users

    @property
    def bridge_addr(self):
        return self._bridge_addr

    @bridge_addr.setter
    def bridge_addr(self, bridge_addr):
        self._bridge_addr = bridge_addr

    @property
    def bridge_dhcp_max(self):
        return self._bridge_dhcp_max

    @bridge_dhcp_max.setter
    def bridge_dhcp_max(self, bridge_dhcp_max):
        self._bridge_dhcp_max = bridge_dhcp_max

    @property
    def bridge_dhcp_range(self):
        return self._bridge_dhcp_range

    @bridge_dhcp_range.setter
    def bridge_dhcp_range(self, bridge_dhcp_range):
        self._bridge_dhcp_range = bridge_dhcp_range

    @property
    def bridge_ipv6_proxy(self):
        return self._bridge_ipv6_proxy

    @bridge_ipv6_proxy.setter
    def bridge_ipv6_proxy(self, bridge_ipv6_proxy):
        self._bridge_ipv6_proxy = bridge_ipv6_proxy

    @property
    def bridge_name(self):
        return self._bridge_name

    @bridge_name.setter
    def bridge_name(self, bridge_name):
        self._bridge_name = bridge_name

    @property
    def bridge_nat(self):
        return self._bridge_nat

    @bridge_nat.setter
    def bridge_nat(self, bridge_nat):
        self._bridge_nat = bridge_nat

    @property
    def bridge_netmask(self):
        return self._bridge_netmask

    @bridge_netmask.setter
    def bridge_netmask(self, bridge_netmask):
        self._bridge_netmask = bridge_netmask

    @property
    def bridge_network(self):
        return self._bridge_network

    @bridge_network.setter
    def bridge_network(self, bridge_network):
        self._bridge_network = bridge_network

    @property
    def create_bridge(self):
        return self._create_bridge

    @create_bridge.setter
    def create_bridge(self, create_bridge):
        self._create_bridge = create_bridge

    @property
    def subid_range(self):
        return self._subid_range

    @subid_range.setter
    def subid_range(self, subid_range):
        self._subid_range = subid_range

    @property
    def subid_start(self):
        return self._subid_start

    @subid_start.setter
    def subid_start(self, subid_start):
        self._subid_start = subid_start

    @property
    def users(self):
        return self._users

    @users.setter
    def users(self, users):
        self._users = users


frecklet_class = LxdService
