# -*- coding: utf-8 -*-


#
# module path: pycklets.keycloak_postgresql_jdbc_driver_installed.KeycloakPostgresqlJdbcDriverInstalled
#


from pyckles import AutoPycklet


class KeycloakPostgresqlJdbcDriverInstalled(AutoPycklet):
    """Install the PostgreSQL-jdbc driver for Keycloak (if necessary).

       Args:
         keycloak_db_name: The keycloak database name.
         keycloak_db_password: The keycloak postgres database password.
         keycloak_db_user: The keycloak database user.
         keycloak_home: The home of the keycloak installation.
         pg_jdbc_version: The version of the driver.

    """

    FRECKLET_ID = "keycloak-postgresql-jdbc-driver-installed"

    def __init__(
        self,
        keycloak_db_name=None,
        keycloak_db_password=None,
        keycloak_db_user=None,
        keycloak_home="/opt/keycloak",
        pg_jdbc_version="42.2.5",
    ):

        super(KeycloakPostgresqlJdbcDriverInstalled, self).__init__(
            var_names=[
                "keycloak_db_name",
                "keycloak_db_password",
                "keycloak_db_user",
                "keycloak_home",
                "pg_jdbc_version",
            ]
        )
        self._keycloak_db_name = keycloak_db_name
        self._keycloak_db_password = keycloak_db_password
        self._keycloak_db_user = keycloak_db_user
        self._keycloak_home = keycloak_home
        self._pg_jdbc_version = pg_jdbc_version

    @property
    def keycloak_db_name(self):
        return self._keycloak_db_name

    @keycloak_db_name.setter
    def keycloak_db_name(self, keycloak_db_name):
        self._keycloak_db_name = keycloak_db_name

    @property
    def keycloak_db_password(self):
        return self._keycloak_db_password

    @keycloak_db_password.setter
    def keycloak_db_password(self, keycloak_db_password):
        self._keycloak_db_password = keycloak_db_password

    @property
    def keycloak_db_user(self):
        return self._keycloak_db_user

    @keycloak_db_user.setter
    def keycloak_db_user(self, keycloak_db_user):
        self._keycloak_db_user = keycloak_db_user

    @property
    def keycloak_home(self):
        return self._keycloak_home

    @keycloak_home.setter
    def keycloak_home(self, keycloak_home):
        self._keycloak_home = keycloak_home

    @property
    def pg_jdbc_version(self):
        return self._pg_jdbc_version

    @pg_jdbc_version.setter
    def pg_jdbc_version(self, pg_jdbc_version):
        self._pg_jdbc_version = pg_jdbc_version


frecklet_class = KeycloakPostgresqlJdbcDriverInstalled
