# -*- coding: utf-8 -*-


#
# module path: pycklets.ssh_keys_authorized.SshKeysAuthorized
#


from pyckles import AutoPycklet


class SshKeysAuthorized(AutoPycklet):
    """No documentation available.

       Args:
         keys: The list of public keys to add.
         user: The name of the user.

    """

    FRECKLET_ID = "ssh-keys-authorized"

    def __init__(self, keys=None, user=None):

        super(SshKeysAuthorized, self).__init__(var_names=["keys", "user"])
        self._keys = keys
        self._user = user

    @property
    def keys(self):
        return self._keys

    @keys.setter
    def keys(self, keys):
        self._keys = keys

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user


frecklet_class = SshKeysAuthorized
