# -*- coding: utf-8 -*-


#
# module path: pycklets.superset_standalone.SupersetStandalone
#


from pyckles import AutoPycklet


class SupersetStandalone(AutoPycklet):
    """Install a standalone Superset service.

     Install a Postgresql service, create a database on it. Then install Superset and configure it to use that database.

     In case no 'db_password' is specified, freckles will generate a random one.

     **NOTE**: this is work in progress, and will probably fail

       Args:
         admin_email: The email address of the admin user.
         admin_firstname: The first name of the admin user.
         admin_lastname: The last name of the admin user.
         admin_password: The service admin password.
         db_host_ip: The db host ip address.
         db_name: the db name.
         db_password: The db password.
         db_user: The db username.

    """

    FRECKLET_ID = "superset-standalone"

    def __init__(
        self,
        admin_email=None,
        admin_firstname=None,
        admin_lastname=None,
        admin_password=None,
        db_host_ip="127.0.0.1",
        db_name="superset",
        db_password="superset_db_password",
        db_user="superset",
    ):

        super(SupersetStandalone, self).__init__(
            var_names=[
                "admin_email",
                "admin_firstname",
                "admin_lastname",
                "admin_password",
                "db_host_ip",
                "db_name",
                "db_password",
                "db_user",
            ]
        )
        self._admin_email = admin_email
        self._admin_firstname = admin_firstname
        self._admin_lastname = admin_lastname
        self._admin_password = admin_password
        self._db_host_ip = db_host_ip
        self._db_name = db_name
        self._db_password = db_password
        self._db_user = db_user

    @property
    def admin_email(self):
        return self._admin_email

    @admin_email.setter
    def admin_email(self, admin_email):
        self._admin_email = admin_email

    @property
    def admin_firstname(self):
        return self._admin_firstname

    @admin_firstname.setter
    def admin_firstname(self, admin_firstname):
        self._admin_firstname = admin_firstname

    @property
    def admin_lastname(self):
        return self._admin_lastname

    @admin_lastname.setter
    def admin_lastname(self, admin_lastname):
        self._admin_lastname = admin_lastname

    @property
    def admin_password(self):
        return self._admin_password

    @admin_password.setter
    def admin_password(self, admin_password):
        self._admin_password = admin_password

    @property
    def db_host_ip(self):
        return self._db_host_ip

    @db_host_ip.setter
    def db_host_ip(self, db_host_ip):
        self._db_host_ip = db_host_ip

    @property
    def db_name(self):
        return self._db_name

    @db_name.setter
    def db_name(self, db_name):
        self._db_name = db_name

    @property
    def db_password(self):
        return self._db_password

    @db_password.setter
    def db_password(self, db_password):
        self._db_password = db_password

    @property
    def db_user(self):
        return self._db_user

    @db_user.setter
    def db_user(self, db_user):
        self._db_user = db_user


frecklet_class = SupersetStandalone
