# -*- coding: utf-8 -*-


#
# module path: pycklets.ssh_key_exists.SshKeyExists
#


from pyckles import AutoPycklet


class SshKeyExists(AutoPycklet):
    """Ensures an ssh key exists for a user.

     If the ssh key already exists, the password argument is ignored.

       Args:
         bits: The number of bits in the key to create.
         key_type: The type of key to create.
         password: The password to unlock the key (only used if key doesn't exist already).
         path: The (absolute!) path (without key_type token) to the (private) ssh key.
         user: The name of the ssh key owner.

    """

    FRECKLET_ID = "ssh-key-exists"

    def __init__(
        self, bits=4096, key_type="ed25519", password=None, path="~/.ssh/id_", user=None
    ):

        super(SshKeyExists, self).__init__(
            var_names=["bits", "key_type", "password", "path", "user"]
        )
        self._bits = bits
        self._key_type = key_type
        self._password = password
        self._path = path
        self._user = user

    @property
    def bits(self):
        return self._bits

    @bits.setter
    def bits(self, bits):
        self._bits = bits

    @property
    def key_type(self):
        return self._key_type

    @key_type.setter
    def key_type(self, key_type):
        self._key_type = key_type

    @property
    def password(self):
        return self._password

    @password.setter
    def password(self, password):
        self._password = password

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user


frecklet_class = SshKeyExists
