# -*- coding: utf-8 -*-


#
# module path: pycklets.singer_tap_installed.SingerTapInstalled
#


from pyckles import AutoPycklet


class SingerTapInstalled(AutoPycklet):
    """Add a Singer tap.

       Args:
         executable_name: The name of the tap executable (defaults to tap name).
         link_base_path: (Optional) path to collect links to Singer binaries.
         python_version: The Python version to use for the underlying virtualenv.
         tap_name: The name of the tap.
         tap_pip_string: The package name or git url for this tap.
         tap_system_dependencies: (Optional) system dependencies for the tap Python package to be installed.

    """

    FRECKLET_ID = "singer-tap-installed"

    def __init__(
        self,
        executable_name=None,
        link_base_path=None,
        python_version="latest",
        tap_name=None,
        tap_pip_string=None,
        tap_system_dependencies=None,
    ):

        super(SingerTapInstalled, self).__init__(
            var_names=[
                "executable_name",
                "link_base_path",
                "python_version",
                "tap_name",
                "tap_pip_string",
                "tap_system_dependencies",
            ]
        )
        self._executable_name = executable_name
        self._link_base_path = link_base_path
        self._python_version = python_version
        self._tap_name = tap_name
        self._tap_pip_string = tap_pip_string
        self._tap_system_dependencies = tap_system_dependencies

    @property
    def executable_name(self):
        return self._executable_name

    @executable_name.setter
    def executable_name(self, executable_name):
        self._executable_name = executable_name

    @property
    def link_base_path(self):
        return self._link_base_path

    @link_base_path.setter
    def link_base_path(self, link_base_path):
        self._link_base_path = link_base_path

    @property
    def python_version(self):
        return self._python_version

    @python_version.setter
    def python_version(self, python_version):
        self._python_version = python_version

    @property
    def tap_name(self):
        return self._tap_name

    @tap_name.setter
    def tap_name(self, tap_name):
        self._tap_name = tap_name

    @property
    def tap_pip_string(self):
        return self._tap_pip_string

    @tap_pip_string.setter
    def tap_pip_string(self, tap_pip_string):
        self._tap_pip_string = tap_pip_string

    @property
    def tap_system_dependencies(self):
        return self._tap_system_dependencies

    @tap_system_dependencies.setter
    def tap_system_dependencies(self, tap_system_dependencies):
        self._tap_system_dependencies = tap_system_dependencies


frecklet_class = SingerTapInstalled
