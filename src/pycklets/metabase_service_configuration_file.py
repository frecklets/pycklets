# -*- coding: utf-8 -*-


#
# module path: pycklets.metabase_service_configuration_file.MetabaseServiceConfigurationFile
#


from pyckles import AutoPycklet


class MetabaseServiceConfigurationFile(AutoPycklet):
    """No documentation available.

       Args:
         group: The group of the file.
         jetty_host: Host on which the inbuilt jetty server listens,
         jetty_port: Port on which the inbuilt jetty server listens,
         metabase_db_file: The path to the database file, in case of h2.
         metabase_db_host: The database host.
         metabase_db_name: The name of the database.
         metabase_db_password: The database password.
         metabase_db_port: The database port.
         metabase_db_type: Database type to connect to.
         metabase_db_user: The database user.
         metabase_emoji_in_logs: Setting it true will include emojis in logs, to disable set it to false.
         mode: The permissions of the file.
         owner: The owner of the file.
         password_complexity: Allowed password complexity (weak|normal|strong).
         password_length: Minimal password length.
         path: The path to the file.

    """

    FRECKLET_ID = "metabase-service-configuration-file"

    def __init__(
        self,
        group=None,
        jetty_host="localhost",
        jetty_port=3000,
        metabase_db_file=None,
        metabase_db_host="localhost",
        metabase_db_name="metabase",
        metabase_db_password=None,
        metabase_db_port=None,
        metabase_db_type="h2",
        metabase_db_user="metabase",
        metabase_emoji_in_logs=True,
        mode=None,
        owner=None,
        password_complexity="normal",
        password_length=8,
        path=None,
    ):

        super(MetabaseServiceConfigurationFile, self).__init__(
            var_names=[
                "group",
                "jetty_host",
                "jetty_port",
                "metabase_db_file",
                "metabase_db_host",
                "metabase_db_name",
                "metabase_db_password",
                "metabase_db_port",
                "metabase_db_type",
                "metabase_db_user",
                "metabase_emoji_in_logs",
                "mode",
                "owner",
                "password_complexity",
                "password_length",
                "path",
            ]
        )
        self._group = group
        self._jetty_host = jetty_host
        self._jetty_port = jetty_port
        self._metabase_db_file = metabase_db_file
        self._metabase_db_host = metabase_db_host
        self._metabase_db_name = metabase_db_name
        self._metabase_db_password = metabase_db_password
        self._metabase_db_port = metabase_db_port
        self._metabase_db_type = metabase_db_type
        self._metabase_db_user = metabase_db_user
        self._metabase_emoji_in_logs = metabase_emoji_in_logs
        self._mode = mode
        self._owner = owner
        self._password_complexity = password_complexity
        self._password_length = password_length
        self._path = path

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def jetty_host(self):
        return self._jetty_host

    @jetty_host.setter
    def jetty_host(self, jetty_host):
        self._jetty_host = jetty_host

    @property
    def jetty_port(self):
        return self._jetty_port

    @jetty_port.setter
    def jetty_port(self, jetty_port):
        self._jetty_port = jetty_port

    @property
    def metabase_db_file(self):
        return self._metabase_db_file

    @metabase_db_file.setter
    def metabase_db_file(self, metabase_db_file):
        self._metabase_db_file = metabase_db_file

    @property
    def metabase_db_host(self):
        return self._metabase_db_host

    @metabase_db_host.setter
    def metabase_db_host(self, metabase_db_host):
        self._metabase_db_host = metabase_db_host

    @property
    def metabase_db_name(self):
        return self._metabase_db_name

    @metabase_db_name.setter
    def metabase_db_name(self, metabase_db_name):
        self._metabase_db_name = metabase_db_name

    @property
    def metabase_db_password(self):
        return self._metabase_db_password

    @metabase_db_password.setter
    def metabase_db_password(self, metabase_db_password):
        self._metabase_db_password = metabase_db_password

    @property
    def metabase_db_port(self):
        return self._metabase_db_port

    @metabase_db_port.setter
    def metabase_db_port(self, metabase_db_port):
        self._metabase_db_port = metabase_db_port

    @property
    def metabase_db_type(self):
        return self._metabase_db_type

    @metabase_db_type.setter
    def metabase_db_type(self, metabase_db_type):
        self._metabase_db_type = metabase_db_type

    @property
    def metabase_db_user(self):
        return self._metabase_db_user

    @metabase_db_user.setter
    def metabase_db_user(self, metabase_db_user):
        self._metabase_db_user = metabase_db_user

    @property
    def metabase_emoji_in_logs(self):
        return self._metabase_emoji_in_logs

    @metabase_emoji_in_logs.setter
    def metabase_emoji_in_logs(self, metabase_emoji_in_logs):
        self._metabase_emoji_in_logs = metabase_emoji_in_logs

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, mode):
        self._mode = mode

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    @property
    def password_complexity(self):
        return self._password_complexity

    @password_complexity.setter
    def password_complexity(self, password_complexity):
        self._password_complexity = password_complexity

    @property
    def password_length(self):
        return self._password_length

    @password_length.setter
    def password_length(self, password_length):
        self._password_length = password_length

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path


frecklet_class = MetabaseServiceConfigurationFile
