# -*- coding: utf-8 -*-


#
# module path: pycklets.apt_repository_added.AptRepositoryAdded
#


from pyckles import AutoPycklet


class AptRepositoryAdded(AutoPycklet):
    """Ensures an apt repository is available and activated.

       Args:
         codename: Override the distribution codename to use for PPA repositories. Should usually only be set when working with a PPA on a non-Ubuntu target (e.g. Debian or Mint).
         components: List of components for this repo (e.g. ['main', 'non-free']
         is_src_repo: Whether the repository should be added into the sources list.
         repo_alias: When provided, the repo will be added in a separate file under '/etc/apt/sources.d', using the value of this argument as filename.
         repo_url: The url of the apt repository.
         update_cache: Run the equivalent of apt-get update when a change occurs. Cache updates are run after making changes.

    """

    FRECKLET_ID = "apt-repository-added"

    def __init__(
        self,
        codename=None,
        components=None,
        is_src_repo=None,
        repo_alias=None,
        repo_url=None,
        update_cache=True,
    ):

        super(AptRepositoryAdded, self).__init__(
            var_names=[
                "codename",
                "components",
                "is_src_repo",
                "repo_alias",
                "repo_url",
                "update_cache",
            ]
        )
        self._codename = codename
        self._components = components
        self._is_src_repo = is_src_repo
        self._repo_alias = repo_alias
        self._repo_url = repo_url
        self._update_cache = update_cache

    @property
    def codename(self):
        return self._codename

    @codename.setter
    def codename(self, codename):
        self._codename = codename

    @property
    def components(self):
        return self._components

    @components.setter
    def components(self, components):
        self._components = components

    @property
    def is_src_repo(self):
        return self._is_src_repo

    @is_src_repo.setter
    def is_src_repo(self, is_src_repo):
        self._is_src_repo = is_src_repo

    @property
    def repo_alias(self):
        return self._repo_alias

    @repo_alias.setter
    def repo_alias(self, repo_alias):
        self._repo_alias = repo_alias

    @property
    def repo_url(self):
        return self._repo_url

    @repo_url.setter
    def repo_url(self, repo_url):
        self._repo_url = repo_url

    @property
    def update_cache(self):
        return self._update_cache

    @update_cache.setter
    def update_cache(self, update_cache):
        self._update_cache = update_cache


frecklet_class = AptRepositoryAdded
