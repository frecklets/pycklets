# -*- coding: utf-8 -*-


#
# module path: pycklets.lxd_profile_present.LxdProfilePresent
#


from pyckles import AutoPycklet


class LxdProfilePresent(AutoPycklet):
    """Ensure a lxd profile exists.

       Args:
         config: The configuration for this profile.
         description: Description of the profile.
         devices: The devices for the profile.
         name: The name of the lxd profile.

    """

    FRECKLET_ID = "lxd-profile-present"

    def __init__(self, config=None, description=None, devices=None, name=None):

        super(LxdProfilePresent, self).__init__(
            var_names=["config", "description", "devices", "name"]
        )
        self._config = config
        self._description = description
        self._devices = devices
        self._name = name

    @property
    def config(self):
        return self._config

    @config.setter
    def config(self, config):
        self._config = config

    @property
    def description(self):
        return self._description

    @description.setter
    def description(self, description):
        self._description = description

    @property
    def devices(self):
        return self._devices

    @devices.setter
    def devices(self, devices):
        self._devices = devices

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name


frecklet_class = LxdProfilePresent
