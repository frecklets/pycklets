# -*- coding: utf-8 -*-


#
# module path: pycklets.wordpress_standalone.WordpressStandalone
#


from pyckles import AutoPycklet


class WordpressStandalone(AutoPycklet):
    """Sets up [Wordpress](https://wordpress.com) on a machine, including dependencies
     (MariaDB, Apache/Nginx, LetsEncrypt).

     Optionally this frecklet can also checkout a folder with theme/plugin sources and
     [intermingle](https://TODO) it with the wordpress project folder, to support
     good version control and configuration management practices.

     If no 'wp_db_password' is specified, freckles will generate a random one. That doesn't really matter (and is arguably
     more secure), since we don't need to know it in most cases. If we need to find it, we can check the Wordpress config file for it.

       Args:
         base_path: The wordpress project folders parent directory.
         disable_ipv6: Whether to disable ipv6 for this server block (only nginx supported for now).
         hostname: The hostname of the server.
         letsencrypt_staging: Whether to use the letsencrypt staging server.
         listen_ip: The ip to listen to (necessary if using the Apache webserver).
         project_source: The (optional) custom source code for this wordpress site.
         server_admin: The email address to use in the vhost file and with letsencrypt, falls back to 'wp_admin_email.
         use_https: Request a lets-encrypt certificate and serve devpi via https (needs 'server_admin' or 'wp_admin_email' set).
         vhost_name: The name of the vhost file.
         webserver: The webserver to use, currently supported: 'apache', nginx'.
         webserver_group: The group to run the webserver as (if applicable).
         webserver_use_pagespeed: Whether to install and use the pagespeed module.
         webserver_user: The user to run the webserver as.
         wp_admin_email: The email of the wordpress admin user.
         wp_admin_name: The name of the wordpress admin user.
         wp_admin_password: The password of the wordpress admin user.
         wp_content_parent: The path relative to 'wp_project_path' that contains the 'wp-content' folder.
         wp_db_dump_file: An (optional) database dump file.
         wp_db_host: The db host.
         wp_db_import: Whether to import a sql dump.
         wp_db_name: The name of the database to use.
         wp_db_password: The password for the database.
         wp_db_table_prefix: The wordpress db table prefix.
         wp_db_user: The db user.
         wp_group: The group who owns the project files.
         wp_plugins: A list of plugins to install.
         wp_project_path: The path to the wordpress project folder, needs to contain the folder 'wp-content'.
         wp_themes: A list of themes to install.
         wp_title: The name of the wordpress instance.
         wp_user: The user who owns the project files.
         wp_version: The version of Wordpress.

    """

    FRECKLET_ID = "wordpress-standalone"

    def __init__(
        self,
        base_path="/var/www",
        disable_ipv6=None,
        hostname=None,
        letsencrypt_staging=None,
        listen_ip="_default_",
        project_source=None,
        server_admin=None,
        use_https=None,
        vhost_name=None,
        webserver="nginx",
        webserver_group="www-data",
        webserver_use_pagespeed=None,
        webserver_user="www-data",
        wp_admin_email=None,
        wp_admin_name="admin",
        wp_admin_password=None,
        wp_content_parent=None,
        wp_db_dump_file=None,
        wp_db_host="localhost",
        wp_db_import=None,
        wp_db_name="wp_database",
        wp_db_password=None,
        wp_db_table_prefix="wp_",
        wp_db_user="wordpress",
        wp_group="www-data",
        wp_plugins=None,
        wp_project_path=None,
        wp_themes=None,
        wp_title=None,
        wp_user="www-data",
        wp_version=None,
    ):

        super(WordpressStandalone, self).__init__(
            var_names=[
                "base_path",
                "disable_ipv6",
                "hostname",
                "letsencrypt_staging",
                "listen_ip",
                "project_source",
                "server_admin",
                "use_https",
                "vhost_name",
                "webserver",
                "webserver_group",
                "webserver_use_pagespeed",
                "webserver_user",
                "wp_admin_email",
                "wp_admin_name",
                "wp_admin_password",
                "wp_content_parent",
                "wp_db_dump_file",
                "wp_db_host",
                "wp_db_import",
                "wp_db_name",
                "wp_db_password",
                "wp_db_table_prefix",
                "wp_db_user",
                "wp_group",
                "wp_plugins",
                "wp_project_path",
                "wp_themes",
                "wp_title",
                "wp_user",
                "wp_version",
            ]
        )
        self._base_path = base_path
        self._disable_ipv6 = disable_ipv6
        self._hostname = hostname
        self._letsencrypt_staging = letsencrypt_staging
        self._listen_ip = listen_ip
        self._project_source = project_source
        self._server_admin = server_admin
        self._use_https = use_https
        self._vhost_name = vhost_name
        self._webserver = webserver
        self._webserver_group = webserver_group
        self._webserver_use_pagespeed = webserver_use_pagespeed
        self._webserver_user = webserver_user
        self._wp_admin_email = wp_admin_email
        self._wp_admin_name = wp_admin_name
        self._wp_admin_password = wp_admin_password
        self._wp_content_parent = wp_content_parent
        self._wp_db_dump_file = wp_db_dump_file
        self._wp_db_host = wp_db_host
        self._wp_db_import = wp_db_import
        self._wp_db_name = wp_db_name
        self._wp_db_password = wp_db_password
        self._wp_db_table_prefix = wp_db_table_prefix
        self._wp_db_user = wp_db_user
        self._wp_group = wp_group
        self._wp_plugins = wp_plugins
        self._wp_project_path = wp_project_path
        self._wp_themes = wp_themes
        self._wp_title = wp_title
        self._wp_user = wp_user
        self._wp_version = wp_version

    @property
    def base_path(self):
        return self._base_path

    @base_path.setter
    def base_path(self, base_path):
        self._base_path = base_path

    @property
    def disable_ipv6(self):
        return self._disable_ipv6

    @disable_ipv6.setter
    def disable_ipv6(self, disable_ipv6):
        self._disable_ipv6 = disable_ipv6

    @property
    def hostname(self):
        return self._hostname

    @hostname.setter
    def hostname(self, hostname):
        self._hostname = hostname

    @property
    def letsencrypt_staging(self):
        return self._letsencrypt_staging

    @letsencrypt_staging.setter
    def letsencrypt_staging(self, letsencrypt_staging):
        self._letsencrypt_staging = letsencrypt_staging

    @property
    def listen_ip(self):
        return self._listen_ip

    @listen_ip.setter
    def listen_ip(self, listen_ip):
        self._listen_ip = listen_ip

    @property
    def project_source(self):
        return self._project_source

    @project_source.setter
    def project_source(self, project_source):
        self._project_source = project_source

    @property
    def server_admin(self):
        return self._server_admin

    @server_admin.setter
    def server_admin(self, server_admin):
        self._server_admin = server_admin

    @property
    def use_https(self):
        return self._use_https

    @use_https.setter
    def use_https(self, use_https):
        self._use_https = use_https

    @property
    def vhost_name(self):
        return self._vhost_name

    @vhost_name.setter
    def vhost_name(self, vhost_name):
        self._vhost_name = vhost_name

    @property
    def webserver(self):
        return self._webserver

    @webserver.setter
    def webserver(self, webserver):
        self._webserver = webserver

    @property
    def webserver_group(self):
        return self._webserver_group

    @webserver_group.setter
    def webserver_group(self, webserver_group):
        self._webserver_group = webserver_group

    @property
    def webserver_use_pagespeed(self):
        return self._webserver_use_pagespeed

    @webserver_use_pagespeed.setter
    def webserver_use_pagespeed(self, webserver_use_pagespeed):
        self._webserver_use_pagespeed = webserver_use_pagespeed

    @property
    def webserver_user(self):
        return self._webserver_user

    @webserver_user.setter
    def webserver_user(self, webserver_user):
        self._webserver_user = webserver_user

    @property
    def wp_admin_email(self):
        return self._wp_admin_email

    @wp_admin_email.setter
    def wp_admin_email(self, wp_admin_email):
        self._wp_admin_email = wp_admin_email

    @property
    def wp_admin_name(self):
        return self._wp_admin_name

    @wp_admin_name.setter
    def wp_admin_name(self, wp_admin_name):
        self._wp_admin_name = wp_admin_name

    @property
    def wp_admin_password(self):
        return self._wp_admin_password

    @wp_admin_password.setter
    def wp_admin_password(self, wp_admin_password):
        self._wp_admin_password = wp_admin_password

    @property
    def wp_content_parent(self):
        return self._wp_content_parent

    @wp_content_parent.setter
    def wp_content_parent(self, wp_content_parent):
        self._wp_content_parent = wp_content_parent

    @property
    def wp_db_dump_file(self):
        return self._wp_db_dump_file

    @wp_db_dump_file.setter
    def wp_db_dump_file(self, wp_db_dump_file):
        self._wp_db_dump_file = wp_db_dump_file

    @property
    def wp_db_host(self):
        return self._wp_db_host

    @wp_db_host.setter
    def wp_db_host(self, wp_db_host):
        self._wp_db_host = wp_db_host

    @property
    def wp_db_import(self):
        return self._wp_db_import

    @wp_db_import.setter
    def wp_db_import(self, wp_db_import):
        self._wp_db_import = wp_db_import

    @property
    def wp_db_name(self):
        return self._wp_db_name

    @wp_db_name.setter
    def wp_db_name(self, wp_db_name):
        self._wp_db_name = wp_db_name

    @property
    def wp_db_password(self):
        return self._wp_db_password

    @wp_db_password.setter
    def wp_db_password(self, wp_db_password):
        self._wp_db_password = wp_db_password

    @property
    def wp_db_table_prefix(self):
        return self._wp_db_table_prefix

    @wp_db_table_prefix.setter
    def wp_db_table_prefix(self, wp_db_table_prefix):
        self._wp_db_table_prefix = wp_db_table_prefix

    @property
    def wp_db_user(self):
        return self._wp_db_user

    @wp_db_user.setter
    def wp_db_user(self, wp_db_user):
        self._wp_db_user = wp_db_user

    @property
    def wp_group(self):
        return self._wp_group

    @wp_group.setter
    def wp_group(self, wp_group):
        self._wp_group = wp_group

    @property
    def wp_plugins(self):
        return self._wp_plugins

    @wp_plugins.setter
    def wp_plugins(self, wp_plugins):
        self._wp_plugins = wp_plugins

    @property
    def wp_project_path(self):
        return self._wp_project_path

    @wp_project_path.setter
    def wp_project_path(self, wp_project_path):
        self._wp_project_path = wp_project_path

    @property
    def wp_themes(self):
        return self._wp_themes

    @wp_themes.setter
    def wp_themes(self, wp_themes):
        self._wp_themes = wp_themes

    @property
    def wp_title(self):
        return self._wp_title

    @wp_title.setter
    def wp_title(self, wp_title):
        self._wp_title = wp_title

    @property
    def wp_user(self):
        return self._wp_user

    @wp_user.setter
    def wp_user(self, wp_user):
        self._wp_user = wp_user

    @property
    def wp_version(self):
        return self._wp_version

    @wp_version.setter
    def wp_version(self, wp_version):
        self._wp_version = wp_version


frecklet_class = WordpressStandalone
