# -*- coding: utf-8 -*-
# flake8: noqa

import os

from .admin_user_exists import AdminUserExists
from .airflow_service import AirflowService
from .ansible_module import AnsibleModule
from .ansible_role import AnsibleRole
from .ansible_tasklist import AnsibleTasklist
from .apache_service import ApacheService
from .apache_vhost_file import ApacheVhostFile
from .apache_vhost_from_folder import ApacheVhostFromFolder
from .apt_repository_added import AptRepositoryAdded
from .apt_upgrade import AptUpgrade
from .archive_extracted import ArchiveExtracted
from .arp_installed import ArpInstalled
from .asdf_pkg_mgr import AsdfPkgMgr
from .asdf_pkg_mgr_plugin import AsdfPkgMgrPlugin
from .basic_hardening import BasicHardening
from .celery_systemd_service_unit import CelerySystemdServiceUnit
from .command_output_to_file import CommandOutputToFile
from .conda_pkg_mgr import CondaPkgMgr
from .config_value_in_file import ConfigValueInFile
from .config_values_in_file import ConfigValuesInFile
from .consul_config_file import ConsulConfigFile
from .consul_installed import ConsulInstalled
from .consul_server_config_file import ConsulServerConfigFile
from .consul_service import ConsulService
from .container_image_from_frecklet import ContainerImageFromFrecklet
from .container_image_from_frecklet_file import ContainerImageFromFreckletFile
from .cronjob_exists import CronjobExists
from .curl_installed import CurlInstalled
from .deb_file_installed import DebFileInstalled
from .debug_home_dir import DebugHomeDir
from .debug_msg import DebugMsg
from .debug_secret_var import DebugSecretVar
from .debug_var import DebugVar
from .debug_vars import DebugVars
from .devpi_create_backup import DevpiCreateBackup
from .devpi_import_from_backup import DevpiImportFromBackup
from .devpi_installed import DevpiInstalled
from .devpi_nginx_vhost_config import DevpiNginxVhostConfig
from .devpi_service import DevpiService
from .devpi_standalone import DevpiStandalone
from .discourse_config_file import DiscourseConfigFile
from .discourse_standalone import DiscourseStandalone
from .dnsimple_a_record import DnsimpleARecord
from .docker_container_running import DockerContainerRunning
from .docker_frecklet_packer_template_file import DockerFreckletPackerTemplateFile
from .docker_image_from_folder import DockerImageFromFolder
from .docker_image_from_frecklets import DockerImageFromFrecklets
from .docker_service import DockerService
from .dotfiles import Dotfiles
from .ec2_instance_absent import Ec2InstanceAbsent
from .ec2_instance_exists import Ec2InstanceExists
from .ec2_security_group_exists import Ec2SecurityGroupExists
from .ec2_ssh_key_authorized import Ec2SshKeyAuthorized
from .executable_downloaded import ExecutableDownloaded
from .execute_ad_hoc_script import ExecuteAdHocScript
from .execute_command import ExecuteCommand
from .execute_shell import ExecuteShell
from .file_downloaded import FileDownloaded
from .file_fetched import FileFetched
from .file_is_present import FileIsPresent
from .file_with_content import FileWithContent
from .folder_exists import FolderExists
from .folder_is_empty import FolderIsEmpty
from .folder_stowed import FolderStowed
from .folders_intermingled import FoldersIntermingled
from .frecklecute import Frecklecute
from .freckles_dev_project import FrecklesDevProject
from .freckles_dev_sources_synced import FrecklesDevSourcesSynced
from .freckles_installed import FrecklesInstalled
from .freckles_recommended_dependencies import FrecklesRecommendedDependencies
from .frecklet_file import FreckletFile
from .generate_consul_encryption_key import GenerateConsulEncryptionKey
from .git_installed import GitInstalled
from .git_repo_synced import GitRepoSynced
from .gitlab_com_public_key_trusted import GitlabComPublicKeyTrusted
from .gitlab_project_deploy_key_authorized import GitlabProjectDeployKeyAuthorized
from .gitlab_runner_installed import GitlabRunnerInstalled
from .gitlab_runner_registered_linux import GitlabRunnerRegisteredLinux
from .gitlab_runner_service_linux import GitlabRunnerServiceLinux
from .go_lang_installed import GoLangInstalled
from .grafana_service import GrafanaService
from .group_exists import GroupExists
from .hashicorp_executable_installed import HashicorpExecutableInstalled
from .hcloud_server_exists import HcloudServerExists
from .hcloud_ssh_key_authorized import HcloudSshKeyAuthorized
from .homebrew_pkg_mgr import HomebrewPkgMgr
from .hostname import Hostname
from .init_service_configured import InitServiceConfigured
from .init_service_disabled import InitServiceDisabled
from .init_service_enabled import InitServiceEnabled
from .init_service_reloaded import InitServiceReloaded
from .init_service_restarted import InitServiceRestarted
from .init_service_started import InitServiceStarted
from .init_service_stopped import InitServiceStopped
from .init_services_restarted import InitServicesRestarted
from .initial_system_setup import InitialSystemSetup
from .ipv4_address_assigned import Ipv4AddressAssigned
from .java_lang_installed import JavaLangInstalled
from .jellyfin_service import JellyfinService
from .keycloak_add_user import KeycloakAddUser
from .keycloak_postgresql_helper_script_file import KeycloakPostgresqlHelperScriptFile
from .keycloak_postgresql_jdbc_driver_installed import (
    KeycloakPostgresqlJdbcDriverInstalled,
)
from .keycloak_service_config_file import KeycloakServiceConfigFile
from .keycloak_service_launcher_file import KeycloakServiceLauncherFile
from .keycloak_standalone import KeycloakStandalone
from .keycloak_standalone_config_file import KeycloakStandaloneConfigFile
from .letsencrypt_cert_exists import LetsencryptCertExists
from .link_exists import LinkExists
from .locales_generated import LocalesGenerated
from .lxd_container_absent import LxdContainerAbsent
from .lxd_container_running import LxdContainerRunning
from .lxd_frecklet_packer_template_file import LxdFreckletPackerTemplateFile
from .lxd_image_from_frecklet import LxdImageFromFrecklet
from .lxd_image_from_frecklet_file import LxdImageFromFreckletFile
from .lxd_profile_present import LxdProfilePresent
from .lxd_service import LxdService
from .mailtrain_service import MailtrainService
from .mariadb_database_exists import MariadbDatabaseExists
from .mariadb_service import MariadbService
from .matomo_standalone import MatomoStandalone
from .meltano_installed import MeltanoInstalled
from .meltano_project_standalone import MeltanoProjectStandalone
from .metabase_service import MetabaseService
from .metabase_service_configuration_file import MetabaseServiceConfigurationFile
from .metabase_standalone import MetabaseStandalone
from .netdata_service import NetdataService
from .nextcloud_standalone_docker import NextcloudStandaloneDocker
from .nginx_installed_from_source import NginxInstalledFromSource
from .nginx_reverse_proxy_vhost_config import NginxReverseProxyVhostConfig
from .nginx_server_block_file import NginxServerBlockFile
from .nginx_service import NginxService
from .nginx_vhost_from_folder import NginxVhostFromFolder
from .nix_pkg_mgr import NixPkgMgr
from .nmap_installed import NmapInstalled
from .nomad_client_config_file import NomadClientConfigFile
from .nomad_config_file import NomadConfigFile
from .nomad_installed import NomadInstalled
from .nomad_server_config_file import NomadServerConfigFile
from .nomad_service import NomadService
from .osx_command_line_tools_installed import OsxCommandLineToolsInstalled
from .package_installed import PackageInstalled
from .package_managers import PackageManagers
from .packages_installed import PackagesInstalled
from .packer_installed import PackerInstalled
from .parent_folder_exists import ParentFolderExists
from .passwordless_sudo_users import PasswordlessSudoUsers
from .path_archived import PathArchived
from .path_attributes import PathAttributes
from .path_has_mode import PathHasMode
from .path_is_absent import PathIsAbsent
from .path_is_owned_by import PathIsOwnedBy
from .path_is_synced import PathIsSynced
from .php_lang_installed import PhpLangInstalled
from .pip_requirements_present import PipRequirementsPresent
from .pip_system_package_installed import PipSystemPackageInstalled
from .pip_system_packages_installed import PipSystemPackagesInstalled
from .postgresql_database_exists import PostgresqlDatabaseExists
from .postgresql_privileges_granted import PostgresqlPrivilegesGranted
from .postgresql_schema_exists import PostgresqlSchemaExists
from .postgresql_service import PostgresqlService
from .postgresql_user_exists import PostgresqlUserExists
from .prometheus_mysqld_exporter_service import PrometheusMysqldExporterService
from .prometheus_node_exporter_service import PrometheusNodeExporterService
from .prometheus_service import PrometheusService
from .pyenv_gunicorn_systemd_service_unit_file import (
    PyenvGunicornSystemdServiceUnitFile,
)
from .pyenv_python_installed import PyenvPythonInstalled
from .python_dev_project import PythonDevProject
from .python_gunicorn_service import PythonGunicornService
from .python_gunicorn_systemd_service_unit_file import (
    PythonGunicornSystemdServiceUnitFile,
)
from .python_lang_installed import PythonLangInstalled
from .python_packages_in_virtualenv import PythonPackagesInVirtualenv
from .python_project_installed_virtualenv import PythonProjectInstalledVirtualenv
from .python_virtualenv import PythonVirtualenv
from .python_virtualenv_execute_shell import PythonVirtualenvExecuteShell
from .python_virtualenv_execute_shell_commands import (
    PythonVirtualenvExecuteShellCommands,
)
from .python_virtualenv_exists import PythonVirtualenvExists
from .rabbitmq_service import RabbitmqService
from .rabbitmq_vhost_exists import RabbitmqVhostExists
from .rabbitmq_vhost_user_exists import RabbitmqVhostUserExists
from .redis_service import RedisService
from .register_home_dir import RegisterHomeDir
from .register_path_details import RegisterPathDetails
from .register_public_ssh_key_content import RegisterPublicSshKeyContent
from .register_var import RegisterVar
from .remote_apt_key_added import RemoteAptKeyAdded
from .set_internal_var import SetInternalVar
from .shell_output_to_file import ShellOutputToFile
from .shell_script_exists import ShellScriptExists
from .singer_config_file import SingerConfigFile
from .singer_pipeline import SingerPipeline
from .singer_tap_installed import SingerTapInstalled
from .singer_target_installed import SingerTargetInstalled
from .sleep import Sleep
from .ssh_key_added_to_service import SshKeyAddedToService
from .ssh_key_authorized import SshKeyAuthorized
from .ssh_key_exists import SshKeyExists
from .ssh_key_is_absent import SshKeyIsAbsent
from .ssh_key_no_password_exists import SshKeyNoPasswordExists
from .ssh_keys_authorized import SshKeysAuthorized
from .ssh_tunnel_over_nginx import SshTunnelOverNginx
from .sshpass_installed import SshpassInstalled
from .static_website_from_folder import StaticWebsiteFromFolder
from .static_website_from_string import StaticWebsiteFromString
from .stow_installed import StowInstalled
from .superset_service import SupersetService
from .superset_service_configuration_file import SupersetServiceConfigurationFile
from .superset_service_database_exists import SupersetServiceDatabaseExists
from .superset_service_init import SupersetServiceInit
from .superset_standalone import SupersetStandalone
from .syncthing_installed import SyncthingInstalled
from .syncthing_service import SyncthingService
from .syncthing_upgrade_script import SyncthingUpgradeScript
from .sysctl_value import SysctlValue
from .systemd_service_config import SystemdServiceConfig
from .systemd_service_config_file import SystemdServiceConfigFile
from .systemd_service_unit import SystemdServiceUnit
from .systemd_service_unit_file import SystemdServiceUnitFile
from .systemd_services_started import SystemdServicesStarted
from .systemd_services_stopped import SystemdServicesStopped
from .systemd_template_units_configured import SystemdTemplateUnitsConfigured
from .terraform_config_applied import TerraformConfigApplied
from .terraform_config_destroyed import TerraformConfigDestroyed
from .terraform_installed import TerraformInstalled
from .terragrunt_installed import TerragruntInstalled
from .ufw_incoming_allowed import UfwIncomingAllowed
from .ufw_installed import UfwInstalled
from .unzip_installed import UnzipInstalled
from .user_exists import UserExists
from .user_is_member_of_group import UserIsMemberOfGroup
from .users_are_members_of_group import UsersAreMembersOfGroup
from .users_exist import UsersExist
from .vagrant_installed import VagrantInstalled
from .vault_config_file import VaultConfigFile
from .vault_installed import VaultInstalled
from .vault_service import VaultService
from .virtualbox_installed import VirtualboxInstalled
from .wait_for_ssh import WaitForSsh
from .webserver_service import WebserverService
from .wordpress_folder_prepared import WordpressFolderPrepared
from .wordpress_standalone import WordpressStandalone
from .wordpress_vhost_apache import WordpressVhostApache
from .wordpress_vhost_nginx import WordpressVhostNginx
from .zerotier_network_member import ZerotierNetworkMember
from .zerotier_service import ZerotierService
from .zile_config_file import ZileConfigFile
from .zulip_standalone import ZulipStandalone


def get_class(frecklet_name):
    frecklet_name = frecklet_name.replace("-", "_")

    import importlib

    module = importlib.import_module(
        "{}.{}".format("pycklets", frecklet_name), package=None
    )

    if not hasattr(module, "frecklet_class"):
        return None
    return getattr(module, "frecklet_class")


def create_obj(_frecklet_name, _strict=False, _exception_on_not_found=True, **vars):
    cl = get_class(frecklet_name=_frecklet_name)
    if cl is None:
        if _exception_on_not_found:
            raise Exception("No frecklet found with name: {}".format(_frecklet_name))
        else:
            return None

    if _strict:
        return cl(**vars)
    else:
        obj = cl()
        for k, v in vars.items():
            if hasattr(obj, k):
                setattr(obj, k, v)
        return obj


def get_resource_details():

    path = os.path.abspath(os.path.dirname(__file__))
    resources_dir = os.path.join(path, "resources")

    resources = {}
    for f in os.listdir(resources_dir):
        full = os.path.join(resources_dir, f)
        if os.path.isdir(full):
            resources.setdefault(f, []).append(full)

    result = {
        "module_base": path,
        "resources_dir": resources_dir,
        "resources": resources,
    }

    return result
