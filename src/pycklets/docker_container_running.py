# -*- coding: utf-8 -*-


#
# module path: pycklets.docker_container_running.DockerContainerRunning
#


from pyckles import AutoPycklet


class DockerContainerRunning(AutoPycklet):
    """Installs docker (if necessary), then downloads and executes a Docker image.

     TODO: Add other important 'docker run' flags. Maybe not all of them, have a 'docker-container-running-export' (or so) frecklet for that.
     Probably also have a different frecklet for interactive docker containers that can take command-line arguments. Not sure yet.

       Args:
         ensure_docker_installed: Install Docker if not already present.
         image: The image to use.
         name: A name to identify this container with.
         ports: A list of ports to expose on the host.
         users: A list of users who will be added to the 'docker' group.
         volumes: A list of volumes to mount.

    """

    FRECKLET_ID = "docker-container-running"

    def __init__(
        self,
        ensure_docker_installed=None,
        image=None,
        name=None,
        ports=None,
        users=None,
        volumes=None,
    ):

        super(DockerContainerRunning, self).__init__(
            var_names=[
                "ensure_docker_installed",
                "image",
                "name",
                "ports",
                "users",
                "volumes",
            ]
        )
        self._ensure_docker_installed = ensure_docker_installed
        self._image = image
        self._name = name
        self._ports = ports
        self._users = users
        self._volumes = volumes

    @property
    def ensure_docker_installed(self):
        return self._ensure_docker_installed

    @ensure_docker_installed.setter
    def ensure_docker_installed(self, ensure_docker_installed):
        self._ensure_docker_installed = ensure_docker_installed

    @property
    def image(self):
        return self._image

    @image.setter
    def image(self, image):
        self._image = image

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def ports(self):
        return self._ports

    @ports.setter
    def ports(self, ports):
        self._ports = ports

    @property
    def users(self):
        return self._users

    @users.setter
    def users(self, users):
        self._users = users

    @property
    def volumes(self):
        return self._volumes

    @volumes.setter
    def volumes(self, volumes):
        self._volumes = volumes


frecklet_class = DockerContainerRunning
