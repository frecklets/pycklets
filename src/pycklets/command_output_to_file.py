# -*- coding: utf-8 -*-


#
# module path: pycklets.command_output_to_file.CommandOutputToFile
#


from pyckles import AutoPycklet


class CommandOutputToFile(AutoPycklet):
    """Execute a command and write the output to a file.

     By default, this executes a command on the local (controller) host, and writes the file to the remote one. This can
     be mixed-and-matched in any possible combination though.

     This does not (yet) create either the user to execute the command as (``execute_as``) nor the owner of the result file
     (``owner``, ``group``). Make sure to create those manually if necessary. It also does not create the parent directory
     of the target file.

       Args:
         chdir: Change into this directory before running the shell command.
         command: The command to execute.
         execute_as: The user to execute the command as.
         group: The group of the target file.
         mode: The mode of the target file.
         owner: The owner of the target file.
         path: The path to the output file.
         remote_execute: Whether to execute the command on the remote host.
         stdin: Set the stdin of the command directly to the specified value.

    """

    FRECKLET_ID = "command-output-to-file"

    def __init__(
        self,
        chdir=None,
        command=None,
        execute_as=None,
        group=None,
        mode=None,
        owner=None,
        path=None,
        remote_execute=None,
        stdin=None,
    ):

        super(CommandOutputToFile, self).__init__(
            var_names=[
                "chdir",
                "command",
                "execute_as",
                "group",
                "mode",
                "owner",
                "path",
                "remote_execute",
                "stdin",
            ]
        )
        self._chdir = chdir
        self._command = command
        self._execute_as = execute_as
        self._group = group
        self._mode = mode
        self._owner = owner
        self._path = path
        self._remote_execute = remote_execute
        self._stdin = stdin

    @property
    def chdir(self):
        return self._chdir

    @chdir.setter
    def chdir(self, chdir):
        self._chdir = chdir

    @property
    def command(self):
        return self._command

    @command.setter
    def command(self, command):
        self._command = command

    @property
    def execute_as(self):
        return self._execute_as

    @execute_as.setter
    def execute_as(self, execute_as):
        self._execute_as = execute_as

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, mode):
        self._mode = mode

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path

    @property
    def remote_execute(self):
        return self._remote_execute

    @remote_execute.setter
    def remote_execute(self, remote_execute):
        self._remote_execute = remote_execute

    @property
    def stdin(self):
        return self._stdin

    @stdin.setter
    def stdin(self, stdin):
        self._stdin = stdin


frecklet_class = CommandOutputToFile
