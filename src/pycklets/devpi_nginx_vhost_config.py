# -*- coding: utf-8 -*-


#
# module path: pycklets.devpi_nginx_vhost_config.DevpiNginxVhostConfig
#


from pyckles import AutoPycklet


class DevpiNginxVhostConfig(AutoPycklet):
    """Creates a vhost for devpi on Nginx.

       Args:
         devpi_host: The host where devpi is running.
         devpi_port: The port devpi is listening to.
         devpi_proto: The devpi server protocol.
         devpi_root: The devpi server directory.
         hostname: The domain-name for the Nginx web-server.
         owner: The owner of the file.
         path: The path to the config file.
         use_https: Whether https is used.

    """

    FRECKLET_ID = "devpi-nginx-vhost-config"

    def __init__(
        self,
        devpi_host="localhost",
        devpi_port=3141,
        devpi_proto="http",
        devpi_root="/home/devpi/.devpi/server",
        hostname="localhost",
        owner="devpi",
        path="/etc/nginx/sites-available/vhost_devpi.conf",
        use_https=None,
    ):

        super(DevpiNginxVhostConfig, self).__init__(
            var_names=[
                "devpi_host",
                "devpi_port",
                "devpi_proto",
                "devpi_root",
                "hostname",
                "owner",
                "path",
                "use_https",
            ]
        )
        self._devpi_host = devpi_host
        self._devpi_port = devpi_port
        self._devpi_proto = devpi_proto
        self._devpi_root = devpi_root
        self._hostname = hostname
        self._owner = owner
        self._path = path
        self._use_https = use_https

    @property
    def devpi_host(self):
        return self._devpi_host

    @devpi_host.setter
    def devpi_host(self, devpi_host):
        self._devpi_host = devpi_host

    @property
    def devpi_port(self):
        return self._devpi_port

    @devpi_port.setter
    def devpi_port(self, devpi_port):
        self._devpi_port = devpi_port

    @property
    def devpi_proto(self):
        return self._devpi_proto

    @devpi_proto.setter
    def devpi_proto(self, devpi_proto):
        self._devpi_proto = devpi_proto

    @property
    def devpi_root(self):
        return self._devpi_root

    @devpi_root.setter
    def devpi_root(self, devpi_root):
        self._devpi_root = devpi_root

    @property
    def hostname(self):
        return self._hostname

    @hostname.setter
    def hostname(self, hostname):
        self._hostname = hostname

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path

    @property
    def use_https(self):
        return self._use_https

    @use_https.setter
    def use_https(self, use_https):
        self._use_https = use_https


frecklet_class = DevpiNginxVhostConfig
