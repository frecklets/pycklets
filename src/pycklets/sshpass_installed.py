# -*- coding: utf-8 -*-


#
# module path: pycklets.sshpass_installed.SshpassInstalled
#


from pyckles import AutoPycklet


class SshpassInstalled(AutoPycklet):
    """Install the '[sshpass](https://www.cyberciti.biz/faq/noninteractive-shell-script-ssh-password-provider/)' utility.

     'sshpass' is a small application that helps using ssh within scripts. There are some security implications to using it,
     so make sure to check out the webpage.

     If this is used on CentOS, the 'epel-release' repository package will also be installed and enabled.

       Args:
         pkg_mgr: the package manager to use

    """

    FRECKLET_ID = "sshpass-installed"

    def __init__(self, pkg_mgr="auto"):

        super(SshpassInstalled, self).__init__(var_names=["pkg_mgr"])
        self._pkg_mgr = pkg_mgr

    @property
    def pkg_mgr(self):
        return self._pkg_mgr

    @pkg_mgr.setter
    def pkg_mgr(self, pkg_mgr):
        self._pkg_mgr = pkg_mgr


frecklet_class = SshpassInstalled
