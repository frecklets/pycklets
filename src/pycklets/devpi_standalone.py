# -*- coding: utf-8 -*-


#
# module path: pycklets.devpi_standalone.DevpiStandalone
#


from pyckles import AutoPycklet


class DevpiStandalone(AutoPycklet):
    """This frecklet installs a complete nginx-proxied devpi server.

     If a ``backup_archive_file`` is provided, it will be uploaded to the server, unarchived, and the backed-up
     state will be imported before the server will be started for the first time. Check the frecklet::devpi-create-backup
     *frecklet* for details about creating such a backup archive file. The import will take a minute or two, as it also
     (re-)creates the pypi mirror index.

     If the ``use_https`` variable is set, a certificate from LetsEncrypt will be requrested and installed, along
     with a cron job to automatically renew it before it expires. Nginx will be
     configured to redirect all http traffic to https.

       Args:
         admin_password: The initial admin password.
         backup_archive_file: A file to restore the service from.
         email: The email address to use with letsencrypt.
         hostname: The hostname of the server.
         letsencrypt_staging: Whether to use the letsencrypt staging server.
         use_https: Request a lets-encrypt certificate and serve devpi via https (needs email-address).
         user: The user to run devpi as well as nginx.

    """

    FRECKLET_ID = "devpi-standalone"

    def __init__(
        self,
        admin_password=None,
        backup_archive_file=None,
        email=None,
        hostname="localhost",
        letsencrypt_staging=None,
        use_https=None,
        user="devpi",
    ):

        super(DevpiStandalone, self).__init__(
            var_names=[
                "admin_password",
                "backup_archive_file",
                "email",
                "hostname",
                "letsencrypt_staging",
                "use_https",
                "user",
            ]
        )
        self._admin_password = admin_password
        self._backup_archive_file = backup_archive_file
        self._email = email
        self._hostname = hostname
        self._letsencrypt_staging = letsencrypt_staging
        self._use_https = use_https
        self._user = user

    @property
    def admin_password(self):
        return self._admin_password

    @admin_password.setter
    def admin_password(self, admin_password):
        self._admin_password = admin_password

    @property
    def backup_archive_file(self):
        return self._backup_archive_file

    @backup_archive_file.setter
    def backup_archive_file(self, backup_archive_file):
        self._backup_archive_file = backup_archive_file

    @property
    def email(self):
        return self._email

    @email.setter
    def email(self, email):
        self._email = email

    @property
    def hostname(self):
        return self._hostname

    @hostname.setter
    def hostname(self, hostname):
        self._hostname = hostname

    @property
    def letsencrypt_staging(self):
        return self._letsencrypt_staging

    @letsencrypt_staging.setter
    def letsencrypt_staging(self, letsencrypt_staging):
        self._letsencrypt_staging = letsencrypt_staging

    @property
    def use_https(self):
        return self._use_https

    @use_https.setter
    def use_https(self, use_https):
        self._use_https = use_https

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user


frecklet_class = DevpiStandalone
