# -*- coding: utf-8 -*-


#
# module path: pycklets.keycloak_standalone_config_file.KeycloakStandaloneConfigFile
#


from pyckles import AutoPycklet


class KeycloakStandaloneConfigFile(AutoPycklet):
    """No documentation available.

       Args:
         group: The group of the file.
         keycloak_bind_ip: The ip address this server is listening on.
         keycloak_bind_ip_management: The ip address management interface listens on.
         mode: The permissions of the file.
         owner: The owner of the file.
         path: The path to the file.
         proxy_address_forwarding: Whether to setup proxy forwarding

    """

    FRECKLET_ID = "keycloak-standalone-config-file"

    def __init__(
        self,
        group=None,
        keycloak_bind_ip="127.0.0.1",
        keycloak_bind_ip_management=None,
        mode=None,
        owner=None,
        path=None,
        proxy_address_forwarding=None,
    ):

        super(KeycloakStandaloneConfigFile, self).__init__(
            var_names=[
                "group",
                "keycloak_bind_ip",
                "keycloak_bind_ip_management",
                "mode",
                "owner",
                "path",
                "proxy_address_forwarding",
            ]
        )
        self._group = group
        self._keycloak_bind_ip = keycloak_bind_ip
        self._keycloak_bind_ip_management = keycloak_bind_ip_management
        self._mode = mode
        self._owner = owner
        self._path = path
        self._proxy_address_forwarding = proxy_address_forwarding

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def keycloak_bind_ip(self):
        return self._keycloak_bind_ip

    @keycloak_bind_ip.setter
    def keycloak_bind_ip(self, keycloak_bind_ip):
        self._keycloak_bind_ip = keycloak_bind_ip

    @property
    def keycloak_bind_ip_management(self):
        return self._keycloak_bind_ip_management

    @keycloak_bind_ip_management.setter
    def keycloak_bind_ip_management(self, keycloak_bind_ip_management):
        self._keycloak_bind_ip_management = keycloak_bind_ip_management

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, mode):
        self._mode = mode

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path

    @property
    def proxy_address_forwarding(self):
        return self._proxy_address_forwarding

    @proxy_address_forwarding.setter
    def proxy_address_forwarding(self, proxy_address_forwarding):
        self._proxy_address_forwarding = proxy_address_forwarding


frecklet_class = KeycloakStandaloneConfigFile
