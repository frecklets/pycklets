# -*- coding: utf-8 -*-


#
# module path: pycklets.folders_intermingled.FoldersIntermingled
#


from pyckles import AutoPycklet


class FoldersIntermingled(AutoPycklet):
    """TO BE DONE -- This requires a bit of time to explain...

       Args:
         become: Whether to use elevated privileges to intermingle.
         checkout_dest: The (intermediate) checkout destination on the target host.
         checkout_fail_if_exists: Whether to fail if the destination folder exists.
         checkout_folder_group: The group of the checkout folder.
         checkout_folder_owner: The owner of the checkout folder.
         checkout_force: Whether to force-overwrite files on the checkout phase.
         checkout_src: The checkout source (local, remote, archive, ...).
         checkout_stage: Whether to perform the checkout phase.
         checkout_type: The checkout type.
         fail_if_target_exists: Whether to fail if the target folder exists.
         intermingle_config: The intermingle config.
         intermingle_folder_group: The group of the checkout folder.
         intermingle_folder_owner: The owner of the checkout folder.
         intermingle_src: The source folder on the target.
         intermingle_stage: Whether to perform the intermingle phase.
         intermingle_target: The target folder that gets intermingled.
         intermingle_type: The intermingle type.

    """

    FRECKLET_ID = "folders-intermingled"

    def __init__(
        self,
        become=None,
        checkout_dest=None,
        checkout_fail_if_exists=None,
        checkout_folder_group=None,
        checkout_folder_owner=None,
        checkout_force=None,
        checkout_src=None,
        checkout_stage=None,
        checkout_type="auto",
        fail_if_target_exists=None,
        intermingle_config=None,
        intermingle_folder_group=None,
        intermingle_folder_owner=None,
        intermingle_src=None,
        intermingle_stage=None,
        intermingle_target=None,
        intermingle_type="copy",
    ):

        super(FoldersIntermingled, self).__init__(
            var_names=[
                "become",
                "checkout_dest",
                "checkout_fail_if_exists",
                "checkout_folder_group",
                "checkout_folder_owner",
                "checkout_force",
                "checkout_src",
                "checkout_stage",
                "checkout_type",
                "fail_if_target_exists",
                "intermingle_config",
                "intermingle_folder_group",
                "intermingle_folder_owner",
                "intermingle_src",
                "intermingle_stage",
                "intermingle_target",
                "intermingle_type",
            ]
        )
        self._become = become
        self._checkout_dest = checkout_dest
        self._checkout_fail_if_exists = checkout_fail_if_exists
        self._checkout_folder_group = checkout_folder_group
        self._checkout_folder_owner = checkout_folder_owner
        self._checkout_force = checkout_force
        self._checkout_src = checkout_src
        self._checkout_stage = checkout_stage
        self._checkout_type = checkout_type
        self._fail_if_target_exists = fail_if_target_exists
        self._intermingle_config = intermingle_config
        self._intermingle_folder_group = intermingle_folder_group
        self._intermingle_folder_owner = intermingle_folder_owner
        self._intermingle_src = intermingle_src
        self._intermingle_stage = intermingle_stage
        self._intermingle_target = intermingle_target
        self._intermingle_type = intermingle_type

    @property
    def become(self):
        return self._become

    @become.setter
    def become(self, become):
        self._become = become

    @property
    def checkout_dest(self):
        return self._checkout_dest

    @checkout_dest.setter
    def checkout_dest(self, checkout_dest):
        self._checkout_dest = checkout_dest

    @property
    def checkout_fail_if_exists(self):
        return self._checkout_fail_if_exists

    @checkout_fail_if_exists.setter
    def checkout_fail_if_exists(self, checkout_fail_if_exists):
        self._checkout_fail_if_exists = checkout_fail_if_exists

    @property
    def checkout_folder_group(self):
        return self._checkout_folder_group

    @checkout_folder_group.setter
    def checkout_folder_group(self, checkout_folder_group):
        self._checkout_folder_group = checkout_folder_group

    @property
    def checkout_folder_owner(self):
        return self._checkout_folder_owner

    @checkout_folder_owner.setter
    def checkout_folder_owner(self, checkout_folder_owner):
        self._checkout_folder_owner = checkout_folder_owner

    @property
    def checkout_force(self):
        return self._checkout_force

    @checkout_force.setter
    def checkout_force(self, checkout_force):
        self._checkout_force = checkout_force

    @property
    def checkout_src(self):
        return self._checkout_src

    @checkout_src.setter
    def checkout_src(self, checkout_src):
        self._checkout_src = checkout_src

    @property
    def checkout_stage(self):
        return self._checkout_stage

    @checkout_stage.setter
    def checkout_stage(self, checkout_stage):
        self._checkout_stage = checkout_stage

    @property
    def checkout_type(self):
        return self._checkout_type

    @checkout_type.setter
    def checkout_type(self, checkout_type):
        self._checkout_type = checkout_type

    @property
    def fail_if_target_exists(self):
        return self._fail_if_target_exists

    @fail_if_target_exists.setter
    def fail_if_target_exists(self, fail_if_target_exists):
        self._fail_if_target_exists = fail_if_target_exists

    @property
    def intermingle_config(self):
        return self._intermingle_config

    @intermingle_config.setter
    def intermingle_config(self, intermingle_config):
        self._intermingle_config = intermingle_config

    @property
    def intermingle_folder_group(self):
        return self._intermingle_folder_group

    @intermingle_folder_group.setter
    def intermingle_folder_group(self, intermingle_folder_group):
        self._intermingle_folder_group = intermingle_folder_group

    @property
    def intermingle_folder_owner(self):
        return self._intermingle_folder_owner

    @intermingle_folder_owner.setter
    def intermingle_folder_owner(self, intermingle_folder_owner):
        self._intermingle_folder_owner = intermingle_folder_owner

    @property
    def intermingle_src(self):
        return self._intermingle_src

    @intermingle_src.setter
    def intermingle_src(self, intermingle_src):
        self._intermingle_src = intermingle_src

    @property
    def intermingle_stage(self):
        return self._intermingle_stage

    @intermingle_stage.setter
    def intermingle_stage(self, intermingle_stage):
        self._intermingle_stage = intermingle_stage

    @property
    def intermingle_target(self):
        return self._intermingle_target

    @intermingle_target.setter
    def intermingle_target(self, intermingle_target):
        self._intermingle_target = intermingle_target

    @property
    def intermingle_type(self):
        return self._intermingle_type

    @intermingle_type.setter
    def intermingle_type(self, intermingle_type):
        self._intermingle_type = intermingle_type


frecklet_class = FoldersIntermingled
