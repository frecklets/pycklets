# -*- coding: utf-8 -*-


#
# module path: pycklets.grafana_service.GrafanaService
#


from pyckles import AutoPycklet


class GrafanaService(AutoPycklet):
    """Installs the grafana service.

       Args:
         admin_password: The password of the default Grafana admin.
         admin_user: The name of the admin user.
         alerting_config: alerting config
         analytics_config: analytics config
         auth_config: auth config
         dashboard_dir: Path to a local directory containing dashboards files.
         dashboards: List of dashboards which should be imported.
         data_source_proxy_whitelist: White list of allowed ips/domains to use in data sources.
         database_config: database config
         datasources: List of datasources which should be configured.
         disable_gravatar: Whether to disable gravatar for user profile images.
         email_on_sign_up: Send welcome email after signing up.
         environment: Optional Environment parameters for Grafana installation.
         image_storage_config: image_storage config
         instance_name: The grafana instance name.
         ldap_config: ldap config
         listen_address: The address the service listens on.
         listen_port: The port the service listens on.
         listen_url: The full url to access this grafana instance.
         login_remember_days: The number of days the keep me logged in / remember me cookie lasts.
         metrics_config: metrics config
         secret_key: Secret key.
         server_config: server config
         session_config: session config
         smtp_config: smtp config
         snapshots_config: snapshots config
         tracing_config: tracing config
         users_config: users config
         version: The grafana version.

    """

    FRECKLET_ID = "grafana-service"

    def __init__(
        self,
        admin_password=None,
        admin_user="admin",
        alerting_config=None,
        analytics_config=None,
        auth_config=None,
        dashboard_dir=None,
        dashboards=None,
        data_source_proxy_whitelist=None,
        database_config=None,
        datasources=None,
        disable_gravatar=None,
        email_on_sign_up=None,
        environment=None,
        image_storage_config=None,
        instance_name=None,
        ldap_config=None,
        listen_address=None,
        listen_port=None,
        listen_url=None,
        login_remember_days=None,
        metrics_config=None,
        secret_key=None,
        server_config=None,
        session_config=None,
        smtp_config=None,
        snapshots_config=None,
        tracing_config=None,
        users_config=None,
        version=None,
    ):

        super(GrafanaService, self).__init__(
            var_names=[
                "admin_password",
                "admin_user",
                "alerting_config",
                "analytics_config",
                "auth_config",
                "dashboard_dir",
                "dashboards",
                "data_source_proxy_whitelist",
                "database_config",
                "datasources",
                "disable_gravatar",
                "email_on_sign_up",
                "environment",
                "image_storage_config",
                "instance_name",
                "ldap_config",
                "listen_address",
                "listen_port",
                "listen_url",
                "login_remember_days",
                "metrics_config",
                "secret_key",
                "server_config",
                "session_config",
                "smtp_config",
                "snapshots_config",
                "tracing_config",
                "users_config",
                "version",
            ]
        )
        self._admin_password = admin_password
        self._admin_user = admin_user
        self._alerting_config = alerting_config
        self._analytics_config = analytics_config
        self._auth_config = auth_config
        self._dashboard_dir = dashboard_dir
        self._dashboards = dashboards
        self._data_source_proxy_whitelist = data_source_proxy_whitelist
        self._database_config = database_config
        self._datasources = datasources
        self._disable_gravatar = disable_gravatar
        self._email_on_sign_up = email_on_sign_up
        self._environment = environment
        self._image_storage_config = image_storage_config
        self._instance_name = instance_name
        self._ldap_config = ldap_config
        self._listen_address = listen_address
        self._listen_port = listen_port
        self._listen_url = listen_url
        self._login_remember_days = login_remember_days
        self._metrics_config = metrics_config
        self._secret_key = secret_key
        self._server_config = server_config
        self._session_config = session_config
        self._smtp_config = smtp_config
        self._snapshots_config = snapshots_config
        self._tracing_config = tracing_config
        self._users_config = users_config
        self._version = version

    @property
    def admin_password(self):
        return self._admin_password

    @admin_password.setter
    def admin_password(self, admin_password):
        self._admin_password = admin_password

    @property
    def admin_user(self):
        return self._admin_user

    @admin_user.setter
    def admin_user(self, admin_user):
        self._admin_user = admin_user

    @property
    def alerting_config(self):
        return self._alerting_config

    @alerting_config.setter
    def alerting_config(self, alerting_config):
        self._alerting_config = alerting_config

    @property
    def analytics_config(self):
        return self._analytics_config

    @analytics_config.setter
    def analytics_config(self, analytics_config):
        self._analytics_config = analytics_config

    @property
    def auth_config(self):
        return self._auth_config

    @auth_config.setter
    def auth_config(self, auth_config):
        self._auth_config = auth_config

    @property
    def dashboard_dir(self):
        return self._dashboard_dir

    @dashboard_dir.setter
    def dashboard_dir(self, dashboard_dir):
        self._dashboard_dir = dashboard_dir

    @property
    def dashboards(self):
        return self._dashboards

    @dashboards.setter
    def dashboards(self, dashboards):
        self._dashboards = dashboards

    @property
    def data_source_proxy_whitelist(self):
        return self._data_source_proxy_whitelist

    @data_source_proxy_whitelist.setter
    def data_source_proxy_whitelist(self, data_source_proxy_whitelist):
        self._data_source_proxy_whitelist = data_source_proxy_whitelist

    @property
    def database_config(self):
        return self._database_config

    @database_config.setter
    def database_config(self, database_config):
        self._database_config = database_config

    @property
    def datasources(self):
        return self._datasources

    @datasources.setter
    def datasources(self, datasources):
        self._datasources = datasources

    @property
    def disable_gravatar(self):
        return self._disable_gravatar

    @disable_gravatar.setter
    def disable_gravatar(self, disable_gravatar):
        self._disable_gravatar = disable_gravatar

    @property
    def email_on_sign_up(self):
        return self._email_on_sign_up

    @email_on_sign_up.setter
    def email_on_sign_up(self, email_on_sign_up):
        self._email_on_sign_up = email_on_sign_up

    @property
    def environment(self):
        return self._environment

    @environment.setter
    def environment(self, environment):
        self._environment = environment

    @property
    def image_storage_config(self):
        return self._image_storage_config

    @image_storage_config.setter
    def image_storage_config(self, image_storage_config):
        self._image_storage_config = image_storage_config

    @property
    def instance_name(self):
        return self._instance_name

    @instance_name.setter
    def instance_name(self, instance_name):
        self._instance_name = instance_name

    @property
    def ldap_config(self):
        return self._ldap_config

    @ldap_config.setter
    def ldap_config(self, ldap_config):
        self._ldap_config = ldap_config

    @property
    def listen_address(self):
        return self._listen_address

    @listen_address.setter
    def listen_address(self, listen_address):
        self._listen_address = listen_address

    @property
    def listen_port(self):
        return self._listen_port

    @listen_port.setter
    def listen_port(self, listen_port):
        self._listen_port = listen_port

    @property
    def listen_url(self):
        return self._listen_url

    @listen_url.setter
    def listen_url(self, listen_url):
        self._listen_url = listen_url

    @property
    def login_remember_days(self):
        return self._login_remember_days

    @login_remember_days.setter
    def login_remember_days(self, login_remember_days):
        self._login_remember_days = login_remember_days

    @property
    def metrics_config(self):
        return self._metrics_config

    @metrics_config.setter
    def metrics_config(self, metrics_config):
        self._metrics_config = metrics_config

    @property
    def secret_key(self):
        return self._secret_key

    @secret_key.setter
    def secret_key(self, secret_key):
        self._secret_key = secret_key

    @property
    def server_config(self):
        return self._server_config

    @server_config.setter
    def server_config(self, server_config):
        self._server_config = server_config

    @property
    def session_config(self):
        return self._session_config

    @session_config.setter
    def session_config(self, session_config):
        self._session_config = session_config

    @property
    def smtp_config(self):
        return self._smtp_config

    @smtp_config.setter
    def smtp_config(self, smtp_config):
        self._smtp_config = smtp_config

    @property
    def snapshots_config(self):
        return self._snapshots_config

    @snapshots_config.setter
    def snapshots_config(self, snapshots_config):
        self._snapshots_config = snapshots_config

    @property
    def tracing_config(self):
        return self._tracing_config

    @tracing_config.setter
    def tracing_config(self, tracing_config):
        self._tracing_config = tracing_config

    @property
    def users_config(self):
        return self._users_config

    @users_config.setter
    def users_config(self, users_config):
        self._users_config = users_config

    @property
    def version(self):
        return self._version

    @version.setter
    def version(self, version):
        self._version = version


frecklet_class = GrafanaService
