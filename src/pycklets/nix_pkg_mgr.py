# -*- coding: utf-8 -*-


#
# module path: pycklets.nix_pkg_mgr.NixPkgMgr
#


from pyckles import AutoPycklet


class NixPkgMgr(AutoPycklet):
    """Install the nix package manager for the user running this frecklet.

     This follows the instructions from the [nix homepage](https://nixos.org/nix/download.html).

     NOTE: currently, this does not seem to work

     TODO: allow installation for other users, and multi-user setups.

       Args:
         disable_signature_verify: don't verify nix installer signature

    """

    FRECKLET_ID = "nix-pkg-mgr"

    def __init__(self, disable_signature_verify=None):

        super(NixPkgMgr, self).__init__(var_names=["disable_signature_verify"])
        self._disable_signature_verify = disable_signature_verify

    @property
    def disable_signature_verify(self):
        return self._disable_signature_verify

    @disable_signature_verify.setter
    def disable_signature_verify(self, disable_signature_verify):
        self._disable_signature_verify = disable_signature_verify


frecklet_class = NixPkgMgr
