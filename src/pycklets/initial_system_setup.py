# -*- coding: utf-8 -*-


#
# module path: pycklets.initial_system_setup.InitialSystemSetup
#


from pyckles import AutoPycklet


class InitialSystemSetup(AutoPycklet):
    """This frecklet can be used to harden a freshly installed server. It sets up an admin user account with password-less sudo enabled, disables password-auth and root login for ssh, and also optionally enables automatic update and mosh as an ssh alternative.

     If no ``admin_password`` argument is provided, the created user won't be able do login via ssh via
     password auth, and they won't be able to do sudo if passwordless sudo is not enabled for the user.

       Args:
         admin_password: The admin password.
         admin_pub_keys: A list of public ssh keys for the admin user.
         admin_user: The name of the admin user.
         auto_updates_enabled: Whether to enable automatic updates.
         extra_packages: A list of extra system packages to install.
         fail2ban_enabled: Whether to install and enable fail2ban.
         mosh_enabled: Whether to install and configure mosh.
         passwordless_sudo: Whether to enable passwordless sudo for admin user.
         ssh_password_auth: Whether to enable ssh password auth.
         ssh_root_access: Whether to enable ssh root access.
         ufw_enabled: Whether to install and enable the ufw firewall.
         ufw_open_tcp_ports: A list of tcp ports to open (if ufw enabled).
         ufw_open_udp_ports: A list of udp ports to open (if ufw enabled).

    """

    FRECKLET_ID = "initial-system-setup"

    def __init__(
        self,
        admin_password=None,
        admin_pub_keys=None,
        admin_user="admin",
        auto_updates_enabled=None,
        extra_packages=None,
        fail2ban_enabled=None,
        mosh_enabled=None,
        passwordless_sudo=True,
        ssh_password_auth=None,
        ssh_root_access=None,
        ufw_enabled=None,
        ufw_open_tcp_ports=None,
        ufw_open_udp_ports=None,
    ):

        super(InitialSystemSetup, self).__init__(
            var_names=[
                "admin_password",
                "admin_pub_keys",
                "admin_user",
                "auto_updates_enabled",
                "extra_packages",
                "fail2ban_enabled",
                "mosh_enabled",
                "passwordless_sudo",
                "ssh_password_auth",
                "ssh_root_access",
                "ufw_enabled",
                "ufw_open_tcp_ports",
                "ufw_open_udp_ports",
            ]
        )
        self._admin_password = admin_password
        self._admin_pub_keys = admin_pub_keys
        self._admin_user = admin_user
        self._auto_updates_enabled = auto_updates_enabled
        self._extra_packages = extra_packages
        self._fail2ban_enabled = fail2ban_enabled
        self._mosh_enabled = mosh_enabled
        self._passwordless_sudo = passwordless_sudo
        self._ssh_password_auth = ssh_password_auth
        self._ssh_root_access = ssh_root_access
        self._ufw_enabled = ufw_enabled
        self._ufw_open_tcp_ports = ufw_open_tcp_ports
        self._ufw_open_udp_ports = ufw_open_udp_ports

    @property
    def admin_password(self):
        return self._admin_password

    @admin_password.setter
    def admin_password(self, admin_password):
        self._admin_password = admin_password

    @property
    def admin_pub_keys(self):
        return self._admin_pub_keys

    @admin_pub_keys.setter
    def admin_pub_keys(self, admin_pub_keys):
        self._admin_pub_keys = admin_pub_keys

    @property
    def admin_user(self):
        return self._admin_user

    @admin_user.setter
    def admin_user(self, admin_user):
        self._admin_user = admin_user

    @property
    def auto_updates_enabled(self):
        return self._auto_updates_enabled

    @auto_updates_enabled.setter
    def auto_updates_enabled(self, auto_updates_enabled):
        self._auto_updates_enabled = auto_updates_enabled

    @property
    def extra_packages(self):
        return self._extra_packages

    @extra_packages.setter
    def extra_packages(self, extra_packages):
        self._extra_packages = extra_packages

    @property
    def fail2ban_enabled(self):
        return self._fail2ban_enabled

    @fail2ban_enabled.setter
    def fail2ban_enabled(self, fail2ban_enabled):
        self._fail2ban_enabled = fail2ban_enabled

    @property
    def mosh_enabled(self):
        return self._mosh_enabled

    @mosh_enabled.setter
    def mosh_enabled(self, mosh_enabled):
        self._mosh_enabled = mosh_enabled

    @property
    def passwordless_sudo(self):
        return self._passwordless_sudo

    @passwordless_sudo.setter
    def passwordless_sudo(self, passwordless_sudo):
        self._passwordless_sudo = passwordless_sudo

    @property
    def ssh_password_auth(self):
        return self._ssh_password_auth

    @ssh_password_auth.setter
    def ssh_password_auth(self, ssh_password_auth):
        self._ssh_password_auth = ssh_password_auth

    @property
    def ssh_root_access(self):
        return self._ssh_root_access

    @ssh_root_access.setter
    def ssh_root_access(self, ssh_root_access):
        self._ssh_root_access = ssh_root_access

    @property
    def ufw_enabled(self):
        return self._ufw_enabled

    @ufw_enabled.setter
    def ufw_enabled(self, ufw_enabled):
        self._ufw_enabled = ufw_enabled

    @property
    def ufw_open_tcp_ports(self):
        return self._ufw_open_tcp_ports

    @ufw_open_tcp_ports.setter
    def ufw_open_tcp_ports(self, ufw_open_tcp_ports):
        self._ufw_open_tcp_ports = ufw_open_tcp_ports

    @property
    def ufw_open_udp_ports(self):
        return self._ufw_open_udp_ports

    @ufw_open_udp_ports.setter
    def ufw_open_udp_ports(self, ufw_open_udp_ports):
        self._ufw_open_udp_ports = ufw_open_udp_ports


frecklet_class = InitialSystemSetup
