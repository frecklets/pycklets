# -*- coding: utf-8 -*-


#
# module path: pycklets.postgresql_database_exists.PostgresqlDatabaseExists
#


from pyckles import AutoPycklet


class PostgresqlDatabaseExists(AutoPycklet):
    """Ensures a database with the provided name is present on a PostgreSQL server.

     The PostgreSQL service itself won't be installed, use the 'postgresql-service' frecklet before this if necessary.

     If ``db_user`` is provided, its value will be used as the owner of the database.

       Args:
         db_encoding: The encoding of the db (default: 'UTF-8').
         db_lc_collate: The collation order to use in the database.
         db_lc_ctype: Character classification (LC_CTYPE) to use in the database.
         db_name: The name of the database to use from the dump.
         db_template: The template used to create the database.
         db_user: The name of the database user ('role' in postgres).
         db_user_password: The (hashed) password for the database user ('role' in PostgreSQL).

    """

    FRECKLET_ID = "postgresql-database-exists"

    def __init__(
        self,
        db_encoding="UTF-8",
        db_lc_collate=None,
        db_lc_ctype=None,
        db_name=None,
        db_template=None,
        db_user=None,
        db_user_password=None,
    ):

        super(PostgresqlDatabaseExists, self).__init__(
            var_names=[
                "db_encoding",
                "db_lc_collate",
                "db_lc_ctype",
                "db_name",
                "db_template",
                "db_user",
                "db_user_password",
            ]
        )
        self._db_encoding = db_encoding
        self._db_lc_collate = db_lc_collate
        self._db_lc_ctype = db_lc_ctype
        self._db_name = db_name
        self._db_template = db_template
        self._db_user = db_user
        self._db_user_password = db_user_password

    @property
    def db_encoding(self):
        return self._db_encoding

    @db_encoding.setter
    def db_encoding(self, db_encoding):
        self._db_encoding = db_encoding

    @property
    def db_lc_collate(self):
        return self._db_lc_collate

    @db_lc_collate.setter
    def db_lc_collate(self, db_lc_collate):
        self._db_lc_collate = db_lc_collate

    @property
    def db_lc_ctype(self):
        return self._db_lc_ctype

    @db_lc_ctype.setter
    def db_lc_ctype(self, db_lc_ctype):
        self._db_lc_ctype = db_lc_ctype

    @property
    def db_name(self):
        return self._db_name

    @db_name.setter
    def db_name(self, db_name):
        self._db_name = db_name

    @property
    def db_template(self):
        return self._db_template

    @db_template.setter
    def db_template(self, db_template):
        self._db_template = db_template

    @property
    def db_user(self):
        return self._db_user

    @db_user.setter
    def db_user(self, db_user):
        self._db_user = db_user

    @property
    def db_user_password(self):
        return self._db_user_password

    @db_user_password.setter
    def db_user_password(self, db_user_password):
        self._db_user_password = db_user_password


frecklet_class = PostgresqlDatabaseExists
