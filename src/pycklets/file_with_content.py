# -*- coding: utf-8 -*-


#
# module path: pycklets.file_with_content.FileWithContent
#


from pyckles import AutoPycklet


class FileWithContent(AutoPycklet):
    """Ensure a file exists and its content is the one specified as input. Create the necessary user and parent directories if necessary.

     If a user is created, it won't have any password set, so you might have to do that in a different (ideally earlier - to have more control) step.

     If intermediate parent directories have to be created, they will inherit the owner/group information of the file to be created. If any of those
     parent directories already exist, they won't be touched at all.

     If a ``owner`` value is provided, this will use 'root' permissions to (potentially) create the parent folder, and the file.

       Args:
         content: The file content.
         group: The group of the file.
         mode: The permissions of the file.
         owner: The owner of the file.
         parent_dir_mode: The permissions of the parent directory.
         path: The path to the file.

    """

    FRECKLET_ID = "file-with-content"

    def __init__(
        self,
        content=None,
        group=None,
        mode=None,
        owner=None,
        parent_dir_mode=None,
        path=None,
    ):

        super(FileWithContent, self).__init__(
            var_names=["content", "group", "mode", "owner", "parent_dir_mode", "path"]
        )
        self._content = content
        self._group = group
        self._mode = mode
        self._owner = owner
        self._parent_dir_mode = parent_dir_mode
        self._path = path

    @property
    def content(self):
        return self._content

    @content.setter
    def content(self, content):
        self._content = content

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, mode):
        self._mode = mode

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    @property
    def parent_dir_mode(self):
        return self._parent_dir_mode

    @parent_dir_mode.setter
    def parent_dir_mode(self, parent_dir_mode):
        self._parent_dir_mode = parent_dir_mode

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path


frecklet_class = FileWithContent
