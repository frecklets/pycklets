# -*- coding: utf-8 -*-


#
# module path: pycklets.syncthing_service.SyncthingService
#


from pyckles import AutoPycklet


class SyncthingService(AutoPycklet):
    """Download and install the Syncthing binary for the specified architecture/platform, then create a systemd
     service unit and enable the Syncthing service for one or several users (which will be created if they don't
     exist yet).

       Args:
         add_upgrade_cronjob: Whether to add a cron job to check for upgrades and upgrade/restart syncthing if there is one (default: false).
         arch: The architecture of the host system.
         platform: The platform of the host system.
         users: List of users to enable/start the Syncthing service for on system boot.
         version: The version of the product.

    """

    FRECKLET_ID = "syncthing-service"

    def __init__(
        self,
        add_upgrade_cronjob=None,
        arch="amd64",
        platform="linux",
        users=None,
        version="1.2.1",
    ):

        super(SyncthingService, self).__init__(
            var_names=["add_upgrade_cronjob", "arch", "platform", "users", "version"]
        )
        self._add_upgrade_cronjob = add_upgrade_cronjob
        self._arch = arch
        self._platform = platform
        self._users = users
        self._version = version

    @property
    def add_upgrade_cronjob(self):
        return self._add_upgrade_cronjob

    @add_upgrade_cronjob.setter
    def add_upgrade_cronjob(self, add_upgrade_cronjob):
        self._add_upgrade_cronjob = add_upgrade_cronjob

    @property
    def arch(self):
        return self._arch

    @arch.setter
    def arch(self, arch):
        self._arch = arch

    @property
    def platform(self):
        return self._platform

    @platform.setter
    def platform(self, platform):
        self._platform = platform

    @property
    def users(self):
        return self._users

    @users.setter
    def users(self, users):
        self._users = users

    @property
    def version(self):
        return self._version

    @version.setter
    def version(self, version):
        self._version = version


frecklet_class = SyncthingService
