# -*- coding: utf-8 -*-


#
# module path: pycklets.debug_home_dir.DebugHomeDir
#


from pyckles import AutoPycklet


class DebugHomeDir(AutoPycklet):
    """The user has to exist already on the target.

       Args:
         user: The name of the user.

    """

    FRECKLET_ID = "debug-home-dir"

    def __init__(self, user=None):

        super(DebugHomeDir, self).__init__(var_names=["user"])
        self._user = user

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user


frecklet_class = DebugHomeDir
