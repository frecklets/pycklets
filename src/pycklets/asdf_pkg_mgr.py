# -*- coding: utf-8 -*-


#
# module path: pycklets.asdf_pkg_mgr.AsdfPkgMgr
#


from pyckles import AutoPycklet


class AsdfPkgMgr(AutoPycklet):
    """Installs the [asdf](https://github.com/asdf-vm/asdf) versions/package manager.

     Optionally, you can also install plugins at the same time.

     This installs a global profile file into /etc/profile.d/asdf.sh. After installation of asdf you either need to logout and login again to have ``asdf`` available, or execute either ``source /etc/profile.d/asdf.sh`` or ``source ~/.asdf/asdf.sh``.

     Once *asdf* is available, you can install plugins by executing ``asdf pulgin-add [plugin-name]``, a list of available plugins can be found [here](https://github.com/asdf-vm/asdf-plugins).

     Having a plugin installed doesn't mean the language/pkg itself is installed, for that you need to pick a version. To list all available versions for a plugin, issue: ``asdf list-all [plugin]``, for example: ``asdf list-all ruby``. Then install via ``asdf install [package] [version]``, e.g. ``asdf install ruby 2.5.3``.
     Then you need to set that version to be global (or local, if that better fits your need): ``asdf global ruby 2.5.3``.

       Args:
         plugins: A list of plugins and details about them.
         user: The name of the user (optional).

    """

    FRECKLET_ID = "asdf-pkg-mgr"

    def __init__(self, plugins=None, user="{{ ansible_env.USER }}"):

        super(AsdfPkgMgr, self).__init__(var_names=["plugins", "user"])
        self._plugins = plugins
        self._user = user

    @property
    def plugins(self):
        return self._plugins

    @plugins.setter
    def plugins(self, plugins):
        self._plugins = plugins

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user


frecklet_class = AsdfPkgMgr
