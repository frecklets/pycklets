# -*- coding: utf-8 -*-


#
# module path: pycklets.nomad_client_config_file.NomadClientConfigFile
#


from pyckles import AutoPycklet


class NomadClientConfigFile(AutoPycklet):
    """Nomad server configuration.

       Args:
         group: The group of the file.
         mode: The permissions of the file.
         owner: The owner of the file.
         path: The path to the file.

    """

    FRECKLET_ID = "nomad-client-config-file"

    def __init__(self, group=None, mode=None, owner=None, path=None):

        super(NomadClientConfigFile, self).__init__(
            var_names=["group", "mode", "owner", "path"]
        )
        self._group = group
        self._mode = mode
        self._owner = owner
        self._path = path

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, mode):
        self._mode = mode

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path


frecklet_class = NomadClientConfigFile
