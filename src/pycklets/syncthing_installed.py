# -*- coding: utf-8 -*-


#
# module path: pycklets.syncthing_installed.SyncthingInstalled
#


from pyckles import AutoPycklet


class SyncthingInstalled(AutoPycklet):
    """Download the 'syncthing' binary for the selected architecture/platform and install it.

       Args:
         arch: The architecture of the host system.
         dest: The (absolute) path to the parent folder of the downloaded executable file.
         force: Force overwrite executable if it already exists.
         group: The group for the syncthing executable.
         owner: The user who runs syncthing.
         platform: The platform of the host system.
         version: The version of the product.

    """

    FRECKLET_ID = "syncthing-installed"

    def __init__(
        self,
        arch="amd64",
        dest=None,
        force=None,
        group=None,
        owner=None,
        platform="linux",
        version="1.2.1",
    ):

        super(SyncthingInstalled, self).__init__(
            var_names=["arch", "dest", "force", "group", "owner", "platform", "version"]
        )
        self._arch = arch
        self._dest = dest
        self._force = force
        self._group = group
        self._owner = owner
        self._platform = platform
        self._version = version

    @property
    def arch(self):
        return self._arch

    @arch.setter
    def arch(self, arch):
        self._arch = arch

    @property
    def dest(self):
        return self._dest

    @dest.setter
    def dest(self, dest):
        self._dest = dest

    @property
    def force(self):
        return self._force

    @force.setter
    def force(self, force):
        self._force = force

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    @property
    def platform(self):
        return self._platform

    @platform.setter
    def platform(self, platform):
        self._platform = platform

    @property
    def version(self):
        return self._version

    @version.setter
    def version(self, version):
        self._version = version


frecklet_class = SyncthingInstalled
