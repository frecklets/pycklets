# -*- coding: utf-8 -*-


#
# module path: pycklets.ansible_tasklist.AnsibleTasklist
#


from pyckles import AutoPycklet


class AnsibleTasklist(AutoPycklet):
    """This is a generic task to execute an Ansible tasklist

     Currently only a few basic metadata keys are supported: ``include_type``, ``become`` &``become_user``. The other ones will be added
     when necessary.

     Be aware, if the tasklist is not in the default or community resources repository, this will only work if 'allow_remote' or 'allow_remote_tasklists' is set to true in the context that is used.

       Args:
         become: Whether to become another user.
         become_user: The user to become.
         include_type: n/a
         name: The role name.
         tasklist_vars: The parameters for the tasklist.

    """

    FRECKLET_ID = "ansible-tasklist"

    def __init__(
        self,
        become=None,
        become_user=None,
        include_type="include",
        name=None,
        tasklist_vars=None,
    ):

        super(AnsibleTasklist, self).__init__(
            var_names=["become", "become_user", "include_type", "name", "tasklist_vars"]
        )
        self._become = become
        self._become_user = become_user
        self._include_type = include_type
        self._name = name
        self._tasklist_vars = tasklist_vars

    @property
    def become(self):
        return self._become

    @become.setter
    def become(self, become):
        self._become = become

    @property
    def become_user(self):
        return self._become_user

    @become_user.setter
    def become_user(self, become_user):
        self._become_user = become_user

    @property
    def include_type(self):
        return self._include_type

    @include_type.setter
    def include_type(self, include_type):
        self._include_type = include_type

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def tasklist_vars(self):
        return self._tasklist_vars

    @tasklist_vars.setter
    def tasklist_vars(self, tasklist_vars):
        self._tasklist_vars = tasklist_vars


frecklet_class = AnsibleTasklist
