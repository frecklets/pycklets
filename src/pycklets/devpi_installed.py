# -*- coding: utf-8 -*-


#
# module path: pycklets.devpi_installed.DevpiInstalled
#


from pyckles import AutoPycklet


class DevpiInstalled(AutoPycklet):
    """Install a devpi Python package repository.

     If a password is provided but one is already set on the service, this will fail and the error will be ignored.

       Args:
         admin_password: The initial admin password.
         devpi_server_base: The folder for the devpi server folder.
         devpi_virtualenv: The folder for the devpi virtualenv.
         host: The domain/ip to listen on, defaults to 'localhost'.
         port: The port to listen on.
         user: The user to run the devpi service.

    """

    FRECKLET_ID = "devpi-installed"

    def __init__(
        self,
        admin_password=None,
        devpi_server_base=None,
        devpi_virtualenv=None,
        host="localhost",
        port=3141,
        user="{{ ansible_env.USER }}",
    ):

        super(DevpiInstalled, self).__init__(
            var_names=[
                "admin_password",
                "devpi_server_base",
                "devpi_virtualenv",
                "host",
                "port",
                "user",
            ]
        )
        self._admin_password = admin_password
        self._devpi_server_base = devpi_server_base
        self._devpi_virtualenv = devpi_virtualenv
        self._host = host
        self._port = port
        self._user = user

    @property
    def admin_password(self):
        return self._admin_password

    @admin_password.setter
    def admin_password(self, admin_password):
        self._admin_password = admin_password

    @property
    def devpi_server_base(self):
        return self._devpi_server_base

    @devpi_server_base.setter
    def devpi_server_base(self, devpi_server_base):
        self._devpi_server_base = devpi_server_base

    @property
    def devpi_virtualenv(self):
        return self._devpi_virtualenv

    @devpi_virtualenv.setter
    def devpi_virtualenv(self, devpi_virtualenv):
        self._devpi_virtualenv = devpi_virtualenv

    @property
    def host(self):
        return self._host

    @host.setter
    def host(self, host):
        self._host = host

    @property
    def port(self):
        return self._port

    @port.setter
    def port(self, port):
        self._port = port

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user


frecklet_class = DevpiInstalled
