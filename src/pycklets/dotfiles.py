# -*- coding: utf-8 -*-


#
# module path: pycklets.dotfiles.Dotfiles
#


from pyckles import AutoPycklet


class Dotfiles(AutoPycklet):
    """Symbolically link dotfile folders into place and install associated applications.

     This frecklet is mainly meant to be used with ``freckelize`` to provide a 'one-command' way
     of initializing a new workstation setup, including application install and configuration.

     It can be used without ``freckelize``, but this usage is neither documented nor supported.

     **Note**: this is currently unmaintained, it will be re-written at some stage

       Args:
         __folder_files__: A list of files contained in the dotfile folder.
         extra_vars: n/a
         fail_on_install_error: Whether to stop if an install step fails (this will still continue until the current list is finished though).
         files: n/a
         folder_overlay: Overlay dictionary to specify more details install instructions for folder-based package install.
         packages: List of additional packages to install.
         path: The path to the folder to process.
         skip_extra_packages: Skip the install of extra packages specified in a .freckle file.
         skip_folder_packages: Skip the install of packages calculated from folder names.
         unstow: Unstow dotfiles folders instead of stowing them, this also prevents the installation of packages.
         vars: n/a

    """

    FRECKLET_ID = "dotfiles"

    def __init__(
        self,
        __folder_files__=None,
        extra_vars=None,
        fail_on_install_error=None,
        files=None,
        folder_overlay=None,
        packages=None,
        path=None,
        skip_extra_packages=None,
        skip_folder_packages=None,
        unstow=None,
        vars=None,
    ):

        super(Dotfiles, self).__init__(
            var_names=[
                "__folder_files__",
                "extra_vars",
                "fail_on_install_error",
                "files",
                "folder_overlay",
                "packages",
                "path",
                "skip_extra_packages",
                "skip_folder_packages",
                "unstow",
                "vars",
            ]
        )
        self.___folder_files__ = __folder_files__
        self._extra_vars = extra_vars
        self._fail_on_install_error = fail_on_install_error
        self._files = files
        self._folder_overlay = folder_overlay
        self._packages = packages
        self._path = path
        self._skip_extra_packages = skip_extra_packages
        self._skip_folder_packages = skip_folder_packages
        self._unstow = unstow
        self._vars = vars

    @property
    def __folder_files__(self):
        return self.___folder_files__

    @__folder_files__.setter
    def __folder_files__(self, __folder_files__):
        self.___folder_files__ = __folder_files__

    @property
    def extra_vars(self):
        return self._extra_vars

    @extra_vars.setter
    def extra_vars(self, extra_vars):
        self._extra_vars = extra_vars

    @property
    def fail_on_install_error(self):
        return self._fail_on_install_error

    @fail_on_install_error.setter
    def fail_on_install_error(self, fail_on_install_error):
        self._fail_on_install_error = fail_on_install_error

    @property
    def files(self):
        return self._files

    @files.setter
    def files(self, files):
        self._files = files

    @property
    def folder_overlay(self):
        return self._folder_overlay

    @folder_overlay.setter
    def folder_overlay(self, folder_overlay):
        self._folder_overlay = folder_overlay

    @property
    def packages(self):
        return self._packages

    @packages.setter
    def packages(self, packages):
        self._packages = packages

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path

    @property
    def skip_extra_packages(self):
        return self._skip_extra_packages

    @skip_extra_packages.setter
    def skip_extra_packages(self, skip_extra_packages):
        self._skip_extra_packages = skip_extra_packages

    @property
    def skip_folder_packages(self):
        return self._skip_folder_packages

    @skip_folder_packages.setter
    def skip_folder_packages(self, skip_folder_packages):
        self._skip_folder_packages = skip_folder_packages

    @property
    def unstow(self):
        return self._unstow

    @unstow.setter
    def unstow(self, unstow):
        self._unstow = unstow

    @property
    def vars(self):
        return self._vars

    @vars.setter
    def vars(self, vars):
        self._vars = vars


frecklet_class = Dotfiles
