# -*- coding: utf-8 -*-


#
# module path: pycklets.stow_installed.StowInstalled
#


from pyckles import AutoPycklet


class StowInstalled(AutoPycklet):
    """Install the '[stow](https://www.gnu.org/software/stow/)' package.

     ``stow`` is a package that can recursively create symlinks to a source folder, in a smart way. This can be useful
     -- for example -- if you want to version control a folder that has files strewn across different locations in the
     folder hierarchy. So, instead of creating a very involved ``.gitignore`` file, you would create a folder with only
     the files you are interested in, version-control it, and  then'stow' it into it's target location.

       Args:
         pkg_mgr: the package manager to use

    """

    FRECKLET_ID = "stow-installed"

    def __init__(self, pkg_mgr="auto"):

        super(StowInstalled, self).__init__(var_names=["pkg_mgr"])
        self._pkg_mgr = pkg_mgr

    @property
    def pkg_mgr(self):
        return self._pkg_mgr

    @pkg_mgr.setter
    def pkg_mgr(self, pkg_mgr):
        self._pkg_mgr = pkg_mgr


frecklet_class = StowInstalled
