# -*- coding: utf-8 -*-


#
# module path: pycklets.lxd_container_running.LxdContainerRunning
#


from pyckles import AutoPycklet


class LxdContainerRunning(AutoPycklet):
    """Makes sure a specific lxd image is running on this machine.

     Can (optionally) install lxd on the target machine before launching the container.

       Args:
         ensure_lxd_installed: Install LXD if not already present.
         image_name: The image name or alias.
         image_server: The server that hosts the image.
         image_server_protocol: The protocol the specified image server uses.
         name: A name to identify this container with.
         profiles: The lxd profile to use for this container.
         register_addresses: The key to register the container network details under.
         users: A list of users who will be added to the 'lxd' group.

    """

    FRECKLET_ID = "lxd-container-running"

    def __init__(
        self,
        ensure_lxd_installed=None,
        image_name=None,
        image_server=None,
        image_server_protocol="simplestreams",
        name=None,
        profiles=["default"],
        register_addresses=None,
        users=None,
    ):

        super(LxdContainerRunning, self).__init__(
            var_names=[
                "ensure_lxd_installed",
                "image_name",
                "image_server",
                "image_server_protocol",
                "name",
                "profiles",
                "register_addresses",
                "users",
            ]
        )
        self._ensure_lxd_installed = ensure_lxd_installed
        self._image_name = image_name
        self._image_server = image_server
        self._image_server_protocol = image_server_protocol
        self._name = name
        self._profiles = profiles
        self._register_addresses = register_addresses
        self._users = users

    @property
    def ensure_lxd_installed(self):
        return self._ensure_lxd_installed

    @ensure_lxd_installed.setter
    def ensure_lxd_installed(self, ensure_lxd_installed):
        self._ensure_lxd_installed = ensure_lxd_installed

    @property
    def image_name(self):
        return self._image_name

    @image_name.setter
    def image_name(self, image_name):
        self._image_name = image_name

    @property
    def image_server(self):
        return self._image_server

    @image_server.setter
    def image_server(self, image_server):
        self._image_server = image_server

    @property
    def image_server_protocol(self):
        return self._image_server_protocol

    @image_server_protocol.setter
    def image_server_protocol(self, image_server_protocol):
        self._image_server_protocol = image_server_protocol

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def profiles(self):
        return self._profiles

    @profiles.setter
    def profiles(self, profiles):
        self._profiles = profiles

    @property
    def register_addresses(self):
        return self._register_addresses

    @register_addresses.setter
    def register_addresses(self, register_addresses):
        self._register_addresses = register_addresses

    @property
    def users(self):
        return self._users

    @users.setter
    def users(self, users):
        self._users = users


frecklet_class = LxdContainerRunning
