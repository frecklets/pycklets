# -*- coding: utf-8 -*-


#
# module path: pycklets.singer_target_installed.SingerTargetInstalled
#


from pyckles import AutoPycklet


class SingerTargetInstalled(AutoPycklet):
    """Add a Singer target.

       Args:
         executable_name: The name of the tap executable (defaults to target name).
         link_base_path: (Optional) path to collect links to Singer binaries.
         python_version: The Python version to use for the underlying virtualenv.
         target_name: The name of the target.
         target_pip_string: The package name or git url for this target.
         target_system_dependencies: (Optional) system dependencies for the target Python package to be installed.

    """

    FRECKLET_ID = "singer-target-installed"

    def __init__(
        self,
        executable_name=None,
        link_base_path=None,
        python_version="latest",
        target_name=None,
        target_pip_string=None,
        target_system_dependencies=None,
    ):

        super(SingerTargetInstalled, self).__init__(
            var_names=[
                "executable_name",
                "link_base_path",
                "python_version",
                "target_name",
                "target_pip_string",
                "target_system_dependencies",
            ]
        )
        self._executable_name = executable_name
        self._link_base_path = link_base_path
        self._python_version = python_version
        self._target_name = target_name
        self._target_pip_string = target_pip_string
        self._target_system_dependencies = target_system_dependencies

    @property
    def executable_name(self):
        return self._executable_name

    @executable_name.setter
    def executable_name(self, executable_name):
        self._executable_name = executable_name

    @property
    def link_base_path(self):
        return self._link_base_path

    @link_base_path.setter
    def link_base_path(self, link_base_path):
        self._link_base_path = link_base_path

    @property
    def python_version(self):
        return self._python_version

    @python_version.setter
    def python_version(self, python_version):
        self._python_version = python_version

    @property
    def target_name(self):
        return self._target_name

    @target_name.setter
    def target_name(self, target_name):
        self._target_name = target_name

    @property
    def target_pip_string(self):
        return self._target_pip_string

    @target_pip_string.setter
    def target_pip_string(self, target_pip_string):
        self._target_pip_string = target_pip_string

    @property
    def target_system_dependencies(self):
        return self._target_system_dependencies

    @target_system_dependencies.setter
    def target_system_dependencies(self, target_system_dependencies):
        self._target_system_dependencies = target_system_dependencies


frecklet_class = SingerTargetInstalled
