# -*- coding: utf-8 -*-


#
# module path: pycklets.debug_msg.DebugMsg
#


from pyckles import AutoPycklet


class DebugMsg(AutoPycklet):
    """Display a debug message.

       Args:
         msg: The message

    """

    FRECKLET_ID = "debug-msg"

    def __init__(self, msg=None):

        super(DebugMsg, self).__init__(var_names=["msg"])
        self._msg = msg

    @property
    def msg(self):
        return self._msg

    @msg.setter
    def msg(self, msg):
        self._msg = msg


frecklet_class = DebugMsg
