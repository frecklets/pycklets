# -*- coding: utf-8 -*-


#
# module path: pycklets.terraform_config_applied.TerraformConfigApplied
#


from pyckles import AutoPycklet


class TerraformConfigApplied(AutoPycklet):
    """Apply a terraform configuration using the Ansible 'terraform' module.

       Args:
         binary_path: The path of a terraform binary to use, relative to the 'path' unless you supply an absolute path.
         force_init: To avoid duplicating infra, if a state file can't be found this will force a `terraform init` (defaults to 'false')
         path: The path to the terraform configuration folder.
         state_file: The path to an existing Terraform state file to use when building plan.
         targets: A list of specific resources to target in this plan/application. The resources selected here will also auto-include any dependencies.
         variables: A group of key-values to override template variables or those in variables files

    """

    FRECKLET_ID = "terraform-config-applied"

    def __init__(
        self,
        binary_path=None,
        force_init=None,
        path=None,
        state_file=None,
        targets=None,
        variables=None,
    ):

        super(TerraformConfigApplied, self).__init__(
            var_names=[
                "binary_path",
                "force_init",
                "path",
                "state_file",
                "targets",
                "variables",
            ]
        )
        self._binary_path = binary_path
        self._force_init = force_init
        self._path = path
        self._state_file = state_file
        self._targets = targets
        self._variables = variables

    @property
    def binary_path(self):
        return self._binary_path

    @binary_path.setter
    def binary_path(self, binary_path):
        self._binary_path = binary_path

    @property
    def force_init(self):
        return self._force_init

    @force_init.setter
    def force_init(self, force_init):
        self._force_init = force_init

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path

    @property
    def state_file(self):
        return self._state_file

    @state_file.setter
    def state_file(self, state_file):
        self._state_file = state_file

    @property
    def targets(self):
        return self._targets

    @targets.setter
    def targets(self, targets):
        self._targets = targets

    @property
    def variables(self):
        return self._variables

    @variables.setter
    def variables(self, variables):
        self._variables = variables


frecklet_class = TerraformConfigApplied
