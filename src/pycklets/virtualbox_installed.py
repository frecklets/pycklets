# -*- coding: utf-8 -*-


#
# module path: pycklets.virtualbox_installed.VirtualboxInstalled
#


from pyckles import AutoPycklet


class VirtualboxInstalled(AutoPycklet):
    """Installs the Virtualbox VM manager package.

       Args:

    """

    FRECKLET_ID = "virtualbox-installed"

    def __init__(self,):

        super(VirtualboxInstalled, self).__init__(var_names=[])


frecklet_class = VirtualboxInstalled
