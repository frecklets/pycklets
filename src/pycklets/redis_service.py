# -*- coding: utf-8 -*-


#
# module path: pycklets.redis_service.RedisService
#


from pyckles import AutoPycklet


class RedisService(AutoPycklet):
    """Install a Redis service.

       Args:
         bind: The address to listen on (set to '0.0.0.0' to listen on all interfaces).
         databases: The number of Redis databases (default: 16).
         db_compression: Compress string objects in database.
         dbdir: Database path.
         logfile: The log file path.
         loglevel: Log level (default: notice).
         maxmemory: Limit memory usage to the specified amount of bytes. Leave at 0 for unlimited.
         port: The port to listen on.
         save: Snapshotting configuration; setting values in this list will save the database to disk if the given number of seconds (e.g. 900) and the given number of write operations (e.g. 1) have occurred. Example for list item: '300 10'.
         timeout: Close a connection after a client is idle N seconds. Set to 0 to disable timeout.
         unix_socket: If set, Redis will also listen on a local Unix socket.

    """

    FRECKLET_ID = "redis-service"

    def __init__(
        self,
        bind="127.0.0.1",
        databases=None,
        db_compression=True,
        dbdir=None,
        logfile=None,
        loglevel=None,
        maxmemory=None,
        port=6379,
        save=None,
        timeout=None,
        unix_socket=None,
    ):

        super(RedisService, self).__init__(
            var_names=[
                "bind",
                "databases",
                "db_compression",
                "dbdir",
                "logfile",
                "loglevel",
                "maxmemory",
                "port",
                "save",
                "timeout",
                "unix_socket",
            ]
        )
        self._bind = bind
        self._databases = databases
        self._db_compression = db_compression
        self._dbdir = dbdir
        self._logfile = logfile
        self._loglevel = loglevel
        self._maxmemory = maxmemory
        self._port = port
        self._save = save
        self._timeout = timeout
        self._unix_socket = unix_socket

    @property
    def bind(self):
        return self._bind

    @bind.setter
    def bind(self, bind):
        self._bind = bind

    @property
    def databases(self):
        return self._databases

    @databases.setter
    def databases(self, databases):
        self._databases = databases

    @property
    def db_compression(self):
        return self._db_compression

    @db_compression.setter
    def db_compression(self, db_compression):
        self._db_compression = db_compression

    @property
    def dbdir(self):
        return self._dbdir

    @dbdir.setter
    def dbdir(self, dbdir):
        self._dbdir = dbdir

    @property
    def logfile(self):
        return self._logfile

    @logfile.setter
    def logfile(self, logfile):
        self._logfile = logfile

    @property
    def loglevel(self):
        return self._loglevel

    @loglevel.setter
    def loglevel(self, loglevel):
        self._loglevel = loglevel

    @property
    def maxmemory(self):
        return self._maxmemory

    @maxmemory.setter
    def maxmemory(self, maxmemory):
        self._maxmemory = maxmemory

    @property
    def port(self):
        return self._port

    @port.setter
    def port(self, port):
        self._port = port

    @property
    def save(self):
        return self._save

    @save.setter
    def save(self, save):
        self._save = save

    @property
    def timeout(self):
        return self._timeout

    @timeout.setter
    def timeout(self, timeout):
        self._timeout = timeout

    @property
    def unix_socket(self):
        return self._unix_socket

    @unix_socket.setter
    def unix_socket(self, unix_socket):
        self._unix_socket = unix_socket


frecklet_class = RedisService
