# -*- coding: utf-8 -*-


#
# module path: pycklets.init_service_disabled.InitServiceDisabled
#


from pyckles import AutoPycklet


class InitServiceDisabled(AutoPycklet):
    """Make sure an init service is disabled.

       Args:
         name: The name of the service.

    """

    FRECKLET_ID = "init-service-disabled"

    def __init__(self, name=None):

        super(InitServiceDisabled, self).__init__(var_names=["name"])
        self._name = name

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name


frecklet_class = InitServiceDisabled
