# -*- coding: utf-8 -*-


#
# module path: pycklets.python_project_installed_virtualenv.PythonProjectInstalledVirtualenv
#


from pyckles import AutoPycklet


class PythonProjectInstalledVirtualenv(AutoPycklet):
    """Install a local folder that contains a Python project into a virtualenv

       Args:
         editable: Whether to install in 'editable mode' (using '-e' flag).
         ignore_error: Whether to ignore any potential errors.
         src_path: The local path.
         user: The user to run the command as.
         virtualenv_path: The (absolute) virtualenv (base) path.

    """

    FRECKLET_ID = "python-project-installed-virtualenv"

    def __init__(
        self,
        editable=None,
        ignore_error=None,
        src_path=None,
        user=None,
        virtualenv_path=None,
    ):

        super(PythonProjectInstalledVirtualenv, self).__init__(
            var_names=[
                "editable",
                "ignore_error",
                "src_path",
                "user",
                "virtualenv_path",
            ]
        )
        self._editable = editable
        self._ignore_error = ignore_error
        self._src_path = src_path
        self._user = user
        self._virtualenv_path = virtualenv_path

    @property
    def editable(self):
        return self._editable

    @editable.setter
    def editable(self, editable):
        self._editable = editable

    @property
    def ignore_error(self):
        return self._ignore_error

    @ignore_error.setter
    def ignore_error(self, ignore_error):
        self._ignore_error = ignore_error

    @property
    def src_path(self):
        return self._src_path

    @src_path.setter
    def src_path(self, src_path):
        self._src_path = src_path

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user

    @property
    def virtualenv_path(self):
        return self._virtualenv_path

    @virtualenv_path.setter
    def virtualenv_path(self, virtualenv_path):
        self._virtualenv_path = virtualenv_path


frecklet_class = PythonProjectInstalledVirtualenv
