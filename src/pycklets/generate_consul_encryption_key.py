# -*- coding: utf-8 -*-


#
# module path: pycklets.generate_consul_encryption_key.GenerateConsulEncryptionKey
#


from pyckles import AutoPycklet


class GenerateConsulEncryptionKey(AutoPycklet):
    """Convenience frecklet to generate a consul encryption key.

     This is a bit silly, as it installs consul if not available. In most cases, just use 'consul keygen' somewhere 'consul' is installed and be done with it.

       Args:

    """

    FRECKLET_ID = "generate-consul-encryption-key"

    def __init__(self,):

        super(GenerateConsulEncryptionKey, self).__init__(var_names=[])


frecklet_class = GenerateConsulEncryptionKey
