# -*- coding: utf-8 -*-


#
# module path: pycklets.init_service_restarted.InitServiceRestarted
#


from pyckles import AutoPycklet


class InitServiceRestarted(AutoPycklet):
    """Make sure an init service is restarted.

       Args:
         name: The name of the service.

    """

    FRECKLET_ID = "init-service-restarted"

    def __init__(self, name=None):

        super(InitServiceRestarted, self).__init__(var_names=["name"])
        self._name = name

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name


frecklet_class = InitServiceRestarted
