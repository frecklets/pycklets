# -*- coding: utf-8 -*-


#
# module path: pycklets.nmap_installed.NmapInstalled
#


from pyckles import AutoPycklet


class NmapInstalled(AutoPycklet):
    """Install the '[nmap](https://nmap.org) network scanning utility.

       Args:
         pkg_mgr: the package manager to use

    """

    FRECKLET_ID = "nmap-installed"

    def __init__(self, pkg_mgr="auto"):

        super(NmapInstalled, self).__init__(var_names=["pkg_mgr"])
        self._pkg_mgr = pkg_mgr

    @property
    def pkg_mgr(self):
        return self._pkg_mgr

    @pkg_mgr.setter
    def pkg_mgr(self, pkg_mgr):
        self._pkg_mgr = pkg_mgr


frecklet_class = NmapInstalled
