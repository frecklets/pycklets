# -*- coding: utf-8 -*-


#
# module path: pycklets.keycloak_add_user.KeycloakAddUser
#


from pyckles import AutoPycklet


class KeycloakAddUser(AutoPycklet):
    """Adds a keycloak (admin) user to a Keycloak instance.

       Args:
         keycloak_home: The path to the keycloak app.
         password: The users password.
         realm: The realm.
         username: The username.

    """

    FRECKLET_ID = "keycloak-add-user"

    def __init__(
        self,
        keycloak_home="/opt/keycloak",
        password=None,
        realm="master",
        username=None,
    ):

        super(KeycloakAddUser, self).__init__(
            var_names=["keycloak_home", "password", "realm", "username"]
        )
        self._keycloak_home = keycloak_home
        self._password = password
        self._realm = realm
        self._username = username

    @property
    def keycloak_home(self):
        return self._keycloak_home

    @keycloak_home.setter
    def keycloak_home(self, keycloak_home):
        self._keycloak_home = keycloak_home

    @property
    def password(self):
        return self._password

    @password.setter
    def password(self, password):
        self._password = password

    @property
    def realm(self):
        return self._realm

    @realm.setter
    def realm(self, realm):
        self._realm = realm

    @property
    def username(self):
        return self._username

    @username.setter
    def username(self, username):
        self._username = username


frecklet_class = KeycloakAddUser
