# -*- coding: utf-8 -*-


#
# module path: pycklets.apache_vhost_file.ApacheVhostFile
#


from pyckles import AutoPycklet


class ApacheVhostFile(AutoPycklet):
    """Apache vhost configuration

       Args:
         custom_log: The custom log.
         document_root: The document root.
         error_log: The error log file for this vhost.
         folder_directives: A list of "Directory", "DirectoryMatch", "Files", "FilesMatch", "Location", "LocationMatch" directives.
         group: The group of the file.
         listen_ip: The ip to listen to.
         listen_port: The port to listen to.
         listen_port_https: The port to listen for https.
         mode: The permissions of the file.
         owner: The owner of the file.
         path: The path to the file.
         server_admin: The server admin email.
         server_aliases: A list of server aliases.
         server_name: The server name.
         ssl_ca_certificate_file: The ssl ca certificate file.
         ssl_certificate_chain_file: The ssl certificate chain file.
         ssl_certificate_file: The ssl certificate file.
         use_https: Whether to use https.
         use_letsencrypt: Whether to use letsencrypt certificates.

    """

    FRECKLET_ID = "apache-vhost-file"

    def __init__(
        self,
        custom_log="${APACHE_LOG_DIR}/access.log combined",
        document_root="/var/www/html",
        error_log="${APACHE_LOG_DIR}/error.log",
        folder_directives=None,
        group=None,
        listen_ip="_default_",
        listen_port=80,
        listen_port_https=443,
        mode=None,
        owner=None,
        path=None,
        server_admin=None,
        server_aliases=None,
        server_name="localhost",
        ssl_ca_certificate_file=None,
        ssl_certificate_chain_file=None,
        ssl_certificate_file=None,
        use_https=None,
        use_letsencrypt=True,
    ):

        super(ApacheVhostFile, self).__init__(
            var_names=[
                "custom_log",
                "document_root",
                "error_log",
                "folder_directives",
                "group",
                "listen_ip",
                "listen_port",
                "listen_port_https",
                "mode",
                "owner",
                "path",
                "server_admin",
                "server_aliases",
                "server_name",
                "ssl_ca_certificate_file",
                "ssl_certificate_chain_file",
                "ssl_certificate_file",
                "use_https",
                "use_letsencrypt",
            ]
        )
        self._custom_log = custom_log
        self._document_root = document_root
        self._error_log = error_log
        self._folder_directives = folder_directives
        self._group = group
        self._listen_ip = listen_ip
        self._listen_port = listen_port
        self._listen_port_https = listen_port_https
        self._mode = mode
        self._owner = owner
        self._path = path
        self._server_admin = server_admin
        self._server_aliases = server_aliases
        self._server_name = server_name
        self._ssl_ca_certificate_file = ssl_ca_certificate_file
        self._ssl_certificate_chain_file = ssl_certificate_chain_file
        self._ssl_certificate_file = ssl_certificate_file
        self._use_https = use_https
        self._use_letsencrypt = use_letsencrypt

    @property
    def custom_log(self):
        return self._custom_log

    @custom_log.setter
    def custom_log(self, custom_log):
        self._custom_log = custom_log

    @property
    def document_root(self):
        return self._document_root

    @document_root.setter
    def document_root(self, document_root):
        self._document_root = document_root

    @property
    def error_log(self):
        return self._error_log

    @error_log.setter
    def error_log(self, error_log):
        self._error_log = error_log

    @property
    def folder_directives(self):
        return self._folder_directives

    @folder_directives.setter
    def folder_directives(self, folder_directives):
        self._folder_directives = folder_directives

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def listen_ip(self):
        return self._listen_ip

    @listen_ip.setter
    def listen_ip(self, listen_ip):
        self._listen_ip = listen_ip

    @property
    def listen_port(self):
        return self._listen_port

    @listen_port.setter
    def listen_port(self, listen_port):
        self._listen_port = listen_port

    @property
    def listen_port_https(self):
        return self._listen_port_https

    @listen_port_https.setter
    def listen_port_https(self, listen_port_https):
        self._listen_port_https = listen_port_https

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, mode):
        self._mode = mode

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path

    @property
    def server_admin(self):
        return self._server_admin

    @server_admin.setter
    def server_admin(self, server_admin):
        self._server_admin = server_admin

    @property
    def server_aliases(self):
        return self._server_aliases

    @server_aliases.setter
    def server_aliases(self, server_aliases):
        self._server_aliases = server_aliases

    @property
    def server_name(self):
        return self._server_name

    @server_name.setter
    def server_name(self, server_name):
        self._server_name = server_name

    @property
    def ssl_ca_certificate_file(self):
        return self._ssl_ca_certificate_file

    @ssl_ca_certificate_file.setter
    def ssl_ca_certificate_file(self, ssl_ca_certificate_file):
        self._ssl_ca_certificate_file = ssl_ca_certificate_file

    @property
    def ssl_certificate_chain_file(self):
        return self._ssl_certificate_chain_file

    @ssl_certificate_chain_file.setter
    def ssl_certificate_chain_file(self, ssl_certificate_chain_file):
        self._ssl_certificate_chain_file = ssl_certificate_chain_file

    @property
    def ssl_certificate_file(self):
        return self._ssl_certificate_file

    @ssl_certificate_file.setter
    def ssl_certificate_file(self, ssl_certificate_file):
        self._ssl_certificate_file = ssl_certificate_file

    @property
    def use_https(self):
        return self._use_https

    @use_https.setter
    def use_https(self, use_https):
        self._use_https = use_https

    @property
    def use_letsencrypt(self):
        return self._use_letsencrypt

    @use_letsencrypt.setter
    def use_letsencrypt(self, use_letsencrypt):
        self._use_letsencrypt = use_letsencrypt


frecklet_class = ApacheVhostFile
