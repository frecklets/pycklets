# -*- coding: utf-8 -*-


#
# module path: pycklets.vault_installed.VaultInstalled
#


from pyckles import AutoPycklet


class VaultInstalled(AutoPycklet):
    """Installs the Hashicorp [Vault](https://www.vaultproject.io/) binary for the architecture/platform of your choice.

     If no owner is specified, the binary will be installed for the user who runs the frecklet, into '$HOME/.local/bin'.
     If you select 'root' as owner, the binary will be installed into '/usr/local/bin'. If you specify a 'owner' and 'group' and they don't exist yet, they will be created.

       Args:
         arch: The architecture of the host system.
         dest: The (absolute) path to the parent folder of the downloaded executable file.
         group: The group of the executable.
         owner: The owner of the executable.
         platform: The platform of the host system.
         version: The version of Vault to install.

    """

    FRECKLET_ID = "vault-installed"

    def __init__(
        self,
        arch=None,
        dest=None,
        group=None,
        owner=None,
        platform=None,
        version="1.2.1",
    ):

        super(VaultInstalled, self).__init__(
            var_names=["arch", "dest", "group", "owner", "platform", "version"]
        )
        self._arch = arch
        self._dest = dest
        self._group = group
        self._owner = owner
        self._platform = platform
        self._version = version

    @property
    def arch(self):
        return self._arch

    @arch.setter
    def arch(self, arch):
        self._arch = arch

    @property
    def dest(self):
        return self._dest

    @dest.setter
    def dest(self, dest):
        self._dest = dest

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    @property
    def platform(self):
        return self._platform

    @platform.setter
    def platform(self, platform):
        self._platform = platform

    @property
    def version(self):
        return self._version

    @version.setter
    def version(self, version):
        self._version = version


frecklet_class = VaultInstalled
