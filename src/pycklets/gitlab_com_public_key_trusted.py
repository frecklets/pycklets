# -*- coding: utf-8 -*-


#
# module path: pycklets.gitlab_com_public_key_trusted.GitlabComPublicKeyTrusted
#


from pyckles import AutoPycklet


class GitlabComPublicKeyTrusted(AutoPycklet):
    """Add gitlab.com public host key to 'known_hosts'.

       Args:
         user: The user to add the key for.

    """

    FRECKLET_ID = "gitlab-com-public-key-trusted"

    def __init__(self, user=None):

        super(GitlabComPublicKeyTrusted, self).__init__(var_names=["user"])
        self._user = user

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user


frecklet_class = GitlabComPublicKeyTrusted
