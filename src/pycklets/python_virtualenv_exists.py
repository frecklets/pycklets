# -*- coding: utf-8 -*-


#
# module path: pycklets.python_virtualenv_exists.PythonVirtualenvExists
#


from pyckles import AutoPycklet


class PythonVirtualenvExists(AutoPycklet):
    """Installs Python using the specified 'python_type', then uses that to create a virtualenv with the
     selected name and Python version. In most cases you won't use this frecklet, but the 'python-virtualenv' one, as
     that lets you also install python packages into the virtualenv.

     If you used 'pyenv' as 'python_type', this will add a piece of code to ``$HOME/.bashrc`` to load pyenv when the user logs in.

       Args:
         gid: The gid of the group.
         group: The group who owns/runs the virtualenv.
         python_base_path: The base path to install Python into (if using 'pyenv' or 'conda').
         python_build_opts: Build options to be forwarded (if supported by 'install_type').
         python_type: How to install Python. Defaults to 'pyenv'.
         python_version: The version of python.
         system_user: Whether the user and group should be a system user/group.
         uid: The uid of the user to create (optional).
         user: The user who owns/runs the virtualenv.
         venv_base_path: The path that holds the virtualenv directory.
         venv_name: The name of the virtualenv to set up.
         venv_python_exe: The (optional) path to an existing Python executable to be used for the venv.

    """

    FRECKLET_ID = "python-virtualenv-exists"

    def __init__(
        self,
        gid=None,
        group=None,
        python_base_path=None,
        python_build_opts=None,
        python_type="pyenv",
        python_version="latest",
        system_user=None,
        uid=None,
        user=None,
        venv_base_path=None,
        venv_name=None,
        venv_python_exe=None,
    ):

        super(PythonVirtualenvExists, self).__init__(
            var_names=[
                "gid",
                "group",
                "python_base_path",
                "python_build_opts",
                "python_type",
                "python_version",
                "system_user",
                "uid",
                "user",
                "venv_base_path",
                "venv_name",
                "venv_python_exe",
            ]
        )
        self._gid = gid
        self._group = group
        self._python_base_path = python_base_path
        self._python_build_opts = python_build_opts
        self._python_type = python_type
        self._python_version = python_version
        self._system_user = system_user
        self._uid = uid
        self._user = user
        self._venv_base_path = venv_base_path
        self._venv_name = venv_name
        self._venv_python_exe = venv_python_exe

    @property
    def gid(self):
        return self._gid

    @gid.setter
    def gid(self, gid):
        self._gid = gid

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def python_base_path(self):
        return self._python_base_path

    @python_base_path.setter
    def python_base_path(self, python_base_path):
        self._python_base_path = python_base_path

    @property
    def python_build_opts(self):
        return self._python_build_opts

    @python_build_opts.setter
    def python_build_opts(self, python_build_opts):
        self._python_build_opts = python_build_opts

    @property
    def python_type(self):
        return self._python_type

    @python_type.setter
    def python_type(self, python_type):
        self._python_type = python_type

    @property
    def python_version(self):
        return self._python_version

    @python_version.setter
    def python_version(self, python_version):
        self._python_version = python_version

    @property
    def system_user(self):
        return self._system_user

    @system_user.setter
    def system_user(self, system_user):
        self._system_user = system_user

    @property
    def uid(self):
        return self._uid

    @uid.setter
    def uid(self, uid):
        self._uid = uid

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user

    @property
    def venv_base_path(self):
        return self._venv_base_path

    @venv_base_path.setter
    def venv_base_path(self, venv_base_path):
        self._venv_base_path = venv_base_path

    @property
    def venv_name(self):
        return self._venv_name

    @venv_name.setter
    def venv_name(self, venv_name):
        self._venv_name = venv_name

    @property
    def venv_python_exe(self):
        return self._venv_python_exe

    @venv_python_exe.setter
    def venv_python_exe(self, venv_python_exe):
        self._venv_python_exe = venv_python_exe


frecklet_class = PythonVirtualenvExists
