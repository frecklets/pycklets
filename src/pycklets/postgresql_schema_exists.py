# -*- coding: utf-8 -*-


#
# module path: pycklets.postgresql_schema_exists.PostgresqlSchemaExists
#


from pyckles import AutoPycklet


class PostgresqlSchemaExists(AutoPycklet):
    """Ensures a schema exists for an (existing) postgresql database.

       Args:
         db_name: The name of the database to connect to and add the schema.
         db_schema_name: Name of the schema to add.
         db_user: The name of the role to set as owner of the schema.

    """

    FRECKLET_ID = "postgresql-schema-exists"

    def __init__(self, db_name=None, db_schema_name=None, db_user=None):

        super(PostgresqlSchemaExists, self).__init__(
            var_names=["db_name", "db_schema_name", "db_user"]
        )
        self._db_name = db_name
        self._db_schema_name = db_schema_name
        self._db_user = db_user

    @property
    def db_name(self):
        return self._db_name

    @db_name.setter
    def db_name(self, db_name):
        self._db_name = db_name

    @property
    def db_schema_name(self):
        return self._db_schema_name

    @db_schema_name.setter
    def db_schema_name(self, db_schema_name):
        self._db_schema_name = db_schema_name

    @property
    def db_user(self):
        return self._db_user

    @db_user.setter
    def db_user(self, db_user):
        self._db_user = db_user


frecklet_class = PostgresqlSchemaExists
