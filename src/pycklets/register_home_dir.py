# -*- coding: utf-8 -*-


#
# module path: pycklets.register_home_dir.RegisterHomeDir
#


from pyckles import AutoPycklet


class RegisterHomeDir(AutoPycklet):
    """The user has to exist already on the target.

       Args:
         register_name: n/a
         user: The name of the user.

    """

    FRECKLET_ID = "register-home-dir"

    def __init__(self, register_name=None, user=None):

        super(RegisterHomeDir, self).__init__(var_names=["register_name", "user"])
        self._register_name = register_name
        self._user = user

    @property
    def register_name(self):
        return self._register_name

    @register_name.setter
    def register_name(self, register_name):
        self._register_name = register_name

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user


frecklet_class = RegisterHomeDir
