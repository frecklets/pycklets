# -*- coding: utf-8 -*-


#
# module path: pycklets.ufw_installed.UfwInstalled
#


from pyckles import AutoPycklet


class UfwInstalled(AutoPycklet):
    """Install the ufw firewall.

       Args:

    """

    FRECKLET_ID = "ufw-installed"

    def __init__(self,):

        super(UfwInstalled, self).__init__(var_names=[])


frecklet_class = UfwInstalled
