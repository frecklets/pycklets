# -*- coding: utf-8 -*-


#
# module path: pycklets.file_downloaded.FileDownloaded
#


from pyckles import AutoPycklet


class FileDownloaded(AutoPycklet):
    """Download a file, create intermediate destination directories and a user/group if necessary.

     If no 'dest' option is provided, the file will be downloaded into '~/Downloads'.

     This uses the [Ansible get_url module](https://docs.ansible.com/ansible/latest/modules/get_url_module.html), check
     it's help for more details.

       Args:
         dest: The destination file (or directory).
         force: Whether to force download/overwrite the target.
         group: The group of the target file.
         mode: The mode the file should have, in octal (e.g. 0755).
         owner: The owner of the target file.
         url: The url to download.

    """

    FRECKLET_ID = "file-downloaded"

    def __init__(
        self,
        dest="~/Downloads/",
        force=None,
        group=None,
        mode=None,
        owner=None,
        url=None,
    ):

        super(FileDownloaded, self).__init__(
            var_names=["dest", "force", "group", "mode", "owner", "url"]
        )
        self._dest = dest
        self._force = force
        self._group = group
        self._mode = mode
        self._owner = owner
        self._url = url

    @property
    def dest(self):
        return self._dest

    @dest.setter
    def dest(self, dest):
        self._dest = dest

    @property
    def force(self):
        return self._force

    @force.setter
    def force(self, force):
        self._force = force

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, mode):
        self._mode = mode

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    @property
    def url(self):
        return self._url

    @url.setter
    def url(self, url):
        self._url = url


frecklet_class = FileDownloaded
