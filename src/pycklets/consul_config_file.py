# -*- coding: utf-8 -*-


#
# module path: pycklets.consul_config_file.ConsulConfigFile
#


from pyckles import AutoPycklet


class ConsulConfigFile(AutoPycklet):
    """Consul daemon config file.

       Args:
         client_addr: The address to which Consul will bind client interfaces, including the HTTP and DNS servers.
         data_dir: This flag provides a data directory for the agent to store state.
         datacenter: This flag controls the datacenter in which the agent is running.
         encrypt: Specifies the secret key to use for encryption of Consul network traffic.
         group: The group of the file.
         mode: The permissions of the file.
         owner: The owner of the file.
         path: The path to the file.
         retry_join: Similar to 'join' but allows retrying a join if the first attempt fails.

    """

    FRECKLET_ID = "consul-config-file"

    def __init__(
        self,
        client_addr=None,
        data_dir=None,
        datacenter="dc1",
        encrypt=None,
        group=None,
        mode=None,
        owner=None,
        path=None,
        retry_join=None,
    ):

        super(ConsulConfigFile, self).__init__(
            var_names=[
                "client_addr",
                "data_dir",
                "datacenter",
                "encrypt",
                "group",
                "mode",
                "owner",
                "path",
                "retry_join",
            ]
        )
        self._client_addr = client_addr
        self._data_dir = data_dir
        self._datacenter = datacenter
        self._encrypt = encrypt
        self._group = group
        self._mode = mode
        self._owner = owner
        self._path = path
        self._retry_join = retry_join

    @property
    def client_addr(self):
        return self._client_addr

    @client_addr.setter
    def client_addr(self, client_addr):
        self._client_addr = client_addr

    @property
    def data_dir(self):
        return self._data_dir

    @data_dir.setter
    def data_dir(self, data_dir):
        self._data_dir = data_dir

    @property
    def datacenter(self):
        return self._datacenter

    @datacenter.setter
    def datacenter(self, datacenter):
        self._datacenter = datacenter

    @property
    def encrypt(self):
        return self._encrypt

    @encrypt.setter
    def encrypt(self, encrypt):
        self._encrypt = encrypt

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, mode):
        self._mode = mode

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path

    @property
    def retry_join(self):
        return self._retry_join

    @retry_join.setter
    def retry_join(self, retry_join):
        self._retry_join = retry_join


frecklet_class = ConsulConfigFile
