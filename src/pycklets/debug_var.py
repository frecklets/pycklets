# -*- coding: utf-8 -*-


#
# module path: pycklets.debug_var.DebugVar
#


from pyckles import AutoPycklet


class DebugVar(AutoPycklet):
    """Displays the content of an (internal) Ansible variable.

       Args:
         var: the Ansible variable name to debug

    """

    FRECKLET_ID = "debug-var"

    def __init__(self, var=None):

        super(DebugVar, self).__init__(var_names=["var"])
        self._var = var

    @property
    def var(self):
        return self._var

    @var.setter
    def var(self, var):
        self._var = var


frecklet_class = DebugVar
