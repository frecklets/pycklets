# -*- coding: utf-8 -*-


#
# module path: pycklets.pip_system_packages_installed.PipSystemPackagesInstalled
#


from pyckles import AutoPycklet


class PipSystemPackagesInstalled(AutoPycklet):
    """Install a list Python packages system-wide, using root permissions.

       Args:
         packages: A list of a Python libraries to install or the urls (bzr+,hg+,git+,svn+) of the remote packages.

    """

    FRECKLET_ID = "pip-system-packages-installed"

    def __init__(self, packages=None):

        super(PipSystemPackagesInstalled, self).__init__(var_names=["packages"])
        self._packages = packages

    @property
    def packages(self):
        return self._packages

    @packages.setter
    def packages(self, packages):
        self._packages = packages


frecklet_class = PipSystemPackagesInstalled
