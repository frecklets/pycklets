# -*- coding: utf-8 -*-


#
# module path: pycklets.meltano_project_standalone.MeltanoProjectStandalone
#


from pyckles import AutoPycklet


class MeltanoProjectStandalone(AutoPycklet):
    """Install a Meltano project from git.

       Args:
         group: The group to install meltano for.
         path: The path to install the Meltano project into.
         project_name: The name of the project.
         repo: The git repo url of the project.
         user: The user to install meltano for.
         version: The git repo version/branch.

    """

    FRECKLET_ID = "meltano-project-standalone"

    def __init__(
        self,
        group="meltano",
        path="/var/lib/meltano",
        project_name="meltano",
        repo=None,
        user="meltano",
        version="master",
    ):

        super(MeltanoProjectStandalone, self).__init__(
            var_names=["group", "path", "project_name", "repo", "user", "version"]
        )
        self._group = group
        self._path = path
        self._project_name = project_name
        self._repo = repo
        self._user = user
        self._version = version

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path

    @property
    def project_name(self):
        return self._project_name

    @project_name.setter
    def project_name(self, project_name):
        self._project_name = project_name

    @property
    def repo(self):
        return self._repo

    @repo.setter
    def repo(self, repo):
        self._repo = repo

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user

    @property
    def version(self):
        return self._version

    @version.setter
    def version(self, version):
        self._version = version


frecklet_class = MeltanoProjectStandalone
