# -*- coding: utf-8 -*-


#
# module path: pycklets.ipv4_address_assigned.Ipv4AddressAssigned
#


from pyckles import AutoPycklet


class Ipv4AddressAssigned(AutoPycklet):
    """Add an IP address to an interface.

     This is often relevant for cloud services that offer so-called 'floating' IP addresses. Those are a useful, because you can point a dns record to them, and then easily switch the underlaying virtual machine. Otherwise you'd have to change the dns record, which would mean you'd have to wait for that change to propagate through all the dns layers. That usually takes a few hours, or even a day in some cases.

     This frecklet does not check whether the interface was already added at a different location than the value of ``iface_file_name``.
     Also, this only adds a very basic interface, in the form of:

     ```
     auto <interface>
     iface <interface> inet static
       address <ip>
       netmask 32
     ```

     Currently this task also restarts the 'networking' service, independent of whether the interface was changed or not.

       Args:
         iface: The name of the network interface.
         iface_file_name: The name of the interface file under /etc/network/interfaces.d.
         ip: The ip to assign.

    """

    FRECKLET_ID = "ipv4-address-assigned"

    def __init__(self, iface="eth0:1", iface_file_name="60-floating-ip.cfg", ip=None):

        super(Ipv4AddressAssigned, self).__init__(
            var_names=["iface", "iface_file_name", "ip"]
        )
        self._iface = iface
        self._iface_file_name = iface_file_name
        self._ip = ip

    @property
    def iface(self):
        return self._iface

    @iface.setter
    def iface(self, iface):
        self._iface = iface

    @property
    def iface_file_name(self):
        return self._iface_file_name

    @iface_file_name.setter
    def iface_file_name(self, iface_file_name):
        self._iface_file_name = iface_file_name

    @property
    def ip(self):
        return self._ip

    @ip.setter
    def ip(self, ip):
        self._ip = ip


frecklet_class = Ipv4AddressAssigned
