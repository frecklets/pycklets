# -*- coding: utf-8 -*-


#
# module path: pycklets.static_website_from_folder.StaticWebsiteFromFolder
#


from pyckles import AutoPycklet


class StaticWebsiteFromFolder(AutoPycklet):
    """This is under development, for now only 'nginx' is supported.

       Args:
         default_server: Whether this server is the 'default' (catchall) server.
         gzip_enabled: Whether to enable gzip (only supported for nginx for now).
         hostname: The domain name the webserver should listen on.
         index: The index file name(s).
         letsencrypt_staging: Whether to use the letsencrypt staging server (for developing -- production only allows a few requests per day).
         path: The document root.
         server_admin: The server admin email.
         skip_cert_request: Skip letsencrypt certificate request.
         use_https: Whether to use https (and request a letsencrypt certificate).
         webserver: The webserver type.
         webserver_user: The webserver user.

    """

    FRECKLET_ID = "static-website-from-folder"

    def __init__(
        self,
        default_server=True,
        gzip_enabled=True,
        hostname=None,
        index=["index.html", "index.htm"],
        letsencrypt_staging=None,
        path="/var/www/html",
        server_admin=None,
        skip_cert_request=None,
        use_https=None,
        webserver="nginx",
        webserver_user=None,
    ):

        super(StaticWebsiteFromFolder, self).__init__(
            var_names=[
                "default_server",
                "gzip_enabled",
                "hostname",
                "index",
                "letsencrypt_staging",
                "path",
                "server_admin",
                "skip_cert_request",
                "use_https",
                "webserver",
                "webserver_user",
            ]
        )
        self._default_server = default_server
        self._gzip_enabled = gzip_enabled
        self._hostname = hostname
        self._index = index
        self._letsencrypt_staging = letsencrypt_staging
        self._path = path
        self._server_admin = server_admin
        self._skip_cert_request = skip_cert_request
        self._use_https = use_https
        self._webserver = webserver
        self._webserver_user = webserver_user

    @property
    def default_server(self):
        return self._default_server

    @default_server.setter
    def default_server(self, default_server):
        self._default_server = default_server

    @property
    def gzip_enabled(self):
        return self._gzip_enabled

    @gzip_enabled.setter
    def gzip_enabled(self, gzip_enabled):
        self._gzip_enabled = gzip_enabled

    @property
    def hostname(self):
        return self._hostname

    @hostname.setter
    def hostname(self, hostname):
        self._hostname = hostname

    @property
    def index(self):
        return self._index

    @index.setter
    def index(self, index):
        self._index = index

    @property
    def letsencrypt_staging(self):
        return self._letsencrypt_staging

    @letsencrypt_staging.setter
    def letsencrypt_staging(self, letsencrypt_staging):
        self._letsencrypt_staging = letsencrypt_staging

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path

    @property
    def server_admin(self):
        return self._server_admin

    @server_admin.setter
    def server_admin(self, server_admin):
        self._server_admin = server_admin

    @property
    def skip_cert_request(self):
        return self._skip_cert_request

    @skip_cert_request.setter
    def skip_cert_request(self, skip_cert_request):
        self._skip_cert_request = skip_cert_request

    @property
    def use_https(self):
        return self._use_https

    @use_https.setter
    def use_https(self, use_https):
        self._use_https = use_https

    @property
    def webserver(self):
        return self._webserver

    @webserver.setter
    def webserver(self, webserver):
        self._webserver = webserver

    @property
    def webserver_user(self):
        return self._webserver_user

    @webserver_user.setter
    def webserver_user(self, webserver_user):
        self._webserver_user = webserver_user


frecklet_class = StaticWebsiteFromFolder
