# -*- coding: utf-8 -*-


#
# module path: pycklets.zerotier_service.ZerotierService
#


from pyckles import AutoPycklet


class ZerotierService(AutoPycklet):
    """Installs the [ZeroTier client](https://www.zerotier.com/download.shtml), and a service init job (systemd, etc).

     This does not join a network or anything else. Check out the 'zerotier-network-member' for this.

       Args:
         enabled: Whether to enable the zerotier-one service or not.
         started: Whether to start the zerotier-one service or not.

    """

    FRECKLET_ID = "zerotier-service"

    def __init__(self, enabled=None, started=None):

        super(ZerotierService, self).__init__(var_names=["enabled", "started"])
        self._enabled = enabled
        self._started = started

    @property
    def enabled(self):
        return self._enabled

    @enabled.setter
    def enabled(self, enabled):
        self._enabled = enabled

    @property
    def started(self):
        return self._started

    @started.setter
    def started(self, started):
        self._started = started


frecklet_class = ZerotierService
