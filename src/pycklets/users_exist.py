# -*- coding: utf-8 -*-


#
# module path: pycklets.users_exist.UsersExist
#


from pyckles import AutoPycklet


class UsersExist(AutoPycklet):
    """Make sure a list of users exist on a system.

     This is a fairly basic frecklet, use the 'user-exist' one if you need to specify details about users.

       Args:
         names: A list of usernames to make sure exist.
         system_user: Whether the users to create should be created as system user.

    """

    FRECKLET_ID = "users-exist"

    def __init__(self, names=None, system_user=None):

        super(UsersExist, self).__init__(var_names=["names", "system_user"])
        self._names = names
        self._system_user = system_user

    @property
    def names(self):
        return self._names

    @names.setter
    def names(self, names):
        self._names = names

    @property
    def system_user(self):
        return self._system_user

    @system_user.setter
    def system_user(self, system_user):
        self._system_user = system_user


frecklet_class = UsersExist
