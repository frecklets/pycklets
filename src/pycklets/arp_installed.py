# -*- coding: utf-8 -*-


#
# module path: pycklets.arp_installed.ArpInstalled
#


from pyckles import AutoPycklet


class ArpInstalled(AutoPycklet):
    """Install the arp network scanning utility.

       Args:
         pkg_mgr: the package manager to use

    """

    FRECKLET_ID = "arp-installed"

    def __init__(self, pkg_mgr="auto"):

        super(ArpInstalled, self).__init__(var_names=["pkg_mgr"])
        self._pkg_mgr = pkg_mgr

    @property
    def pkg_mgr(self):
        return self._pkg_mgr

    @pkg_mgr.setter
    def pkg_mgr(self, pkg_mgr):
        self._pkg_mgr = pkg_mgr


frecklet_class = ArpInstalled
