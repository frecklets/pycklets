# -*- coding: utf-8 -*-


#
# module path: pycklets.singer_pipeline.SingerPipeline
#


from pyckles import AutoPycklet


class SingerPipeline(AutoPycklet):
    """Create a directory with everything necessary for a single Singer pipe.

       Args:
         path: The directory to use for the pipe.
         tap_config: The tap config.
         tap_name: The name of the tap.
         tap_pip_string: The package name or git url for this tap.
         tap_system_dependencies: (Optional) system dependencies for the tap Python package to be installed.
         target_config: The target config.
         target_name: The name of the target.
         target_pip_string: The package name or git url for this target.
         target_system_dependencies: (Optional) system dependencies for the target Python package to be installed.

    """

    FRECKLET_ID = "singer-pipeline"

    def __init__(
        self,
        path=None,
        tap_config=None,
        tap_name=None,
        tap_pip_string=None,
        tap_system_dependencies=None,
        target_config=None,
        target_name=None,
        target_pip_string=None,
        target_system_dependencies=None,
    ):

        super(SingerPipeline, self).__init__(
            var_names=[
                "path",
                "tap_config",
                "tap_name",
                "tap_pip_string",
                "tap_system_dependencies",
                "target_config",
                "target_name",
                "target_pip_string",
                "target_system_dependencies",
            ]
        )
        self._path = path
        self._tap_config = tap_config
        self._tap_name = tap_name
        self._tap_pip_string = tap_pip_string
        self._tap_system_dependencies = tap_system_dependencies
        self._target_config = target_config
        self._target_name = target_name
        self._target_pip_string = target_pip_string
        self._target_system_dependencies = target_system_dependencies

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path

    @property
    def tap_config(self):
        return self._tap_config

    @tap_config.setter
    def tap_config(self, tap_config):
        self._tap_config = tap_config

    @property
    def tap_name(self):
        return self._tap_name

    @tap_name.setter
    def tap_name(self, tap_name):
        self._tap_name = tap_name

    @property
    def tap_pip_string(self):
        return self._tap_pip_string

    @tap_pip_string.setter
    def tap_pip_string(self, tap_pip_string):
        self._tap_pip_string = tap_pip_string

    @property
    def tap_system_dependencies(self):
        return self._tap_system_dependencies

    @tap_system_dependencies.setter
    def tap_system_dependencies(self, tap_system_dependencies):
        self._tap_system_dependencies = tap_system_dependencies

    @property
    def target_config(self):
        return self._target_config

    @target_config.setter
    def target_config(self, target_config):
        self._target_config = target_config

    @property
    def target_name(self):
        return self._target_name

    @target_name.setter
    def target_name(self, target_name):
        self._target_name = target_name

    @property
    def target_pip_string(self):
        return self._target_pip_string

    @target_pip_string.setter
    def target_pip_string(self, target_pip_string):
        self._target_pip_string = target_pip_string

    @property
    def target_system_dependencies(self):
        return self._target_system_dependencies

    @target_system_dependencies.setter
    def target_system_dependencies(self, target_system_dependencies):
        self._target_system_dependencies = target_system_dependencies


frecklet_class = SingerPipeline
