# -*- coding: utf-8 -*-


#
# module path: pycklets.vault_service.VaultService
#


from pyckles import AutoPycklet


class VaultService(AutoPycklet):
    """Create a user named 'vault' (if necessary), download the Vault binary into '/usr/local/bin' and create a
     systemd service unit ('vault') and enable/start it if so specified.

     If 'vault_config' is set, the content of the variable will be stored into '/etc/vault.d/vault.hcl'.

       Args:
         arch: The architecture of the host system.
         dest: The (absolute) path to the parent folder of the downloaded executable file.
         enabled: Whether to enable the service.
         platform: The platform of the host system.
         started: Whether to start the service.
         vault_config: The vault configuration.
         version: The version of Vault to install.

    """

    FRECKLET_ID = "vault-service"

    def __init__(
        self,
        arch=None,
        dest=None,
        enabled=None,
        platform=None,
        started=None,
        vault_config=None,
        version="1.2.1",
    ):

        super(VaultService, self).__init__(
            var_names=[
                "arch",
                "dest",
                "enabled",
                "platform",
                "started",
                "vault_config",
                "version",
            ]
        )
        self._arch = arch
        self._dest = dest
        self._enabled = enabled
        self._platform = platform
        self._started = started
        self._vault_config = vault_config
        self._version = version

    @property
    def arch(self):
        return self._arch

    @arch.setter
    def arch(self, arch):
        self._arch = arch

    @property
    def dest(self):
        return self._dest

    @dest.setter
    def dest(self, dest):
        self._dest = dest

    @property
    def enabled(self):
        return self._enabled

    @enabled.setter
    def enabled(self, enabled):
        self._enabled = enabled

    @property
    def platform(self):
        return self._platform

    @platform.setter
    def platform(self, platform):
        self._platform = platform

    @property
    def started(self):
        return self._started

    @started.setter
    def started(self, started):
        self._started = started

    @property
    def vault_config(self):
        return self._vault_config

    @vault_config.setter
    def vault_config(self, vault_config):
        self._vault_config = vault_config

    @property
    def version(self):
        return self._version

    @version.setter
    def version(self, version):
        self._version = version


frecklet_class = VaultService
