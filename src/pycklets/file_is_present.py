# -*- coding: utf-8 -*-


#
# module path: pycklets.file_is_present.FileIsPresent
#


from pyckles import AutoPycklet


class FileIsPresent(AutoPycklet):
    """Ensure a file exists, create the necessary user (owner), as well as parent directories if necessary.

     If a user needs to be created, it won't have any password set, so you might have to do that in a different (ideally earlier - to have more control) step.

     If intermediate parent directories have to be created, they will inherit the owner/group information of the file to be created. If any of those
     parent directories already exist, they won't be touched at all.

     If the ``owner`` variable is specified, this frecklet will use elevated permissions.

       Args:
         group: The group of the file.
         mode: The permissions of the file.
         owner: The owner of the file.
         parent_dir_mode: The permissions of the parent directory.
         path: The path to the file.

    """

    FRECKLET_ID = "file-is-present"

    def __init__(
        self, group=None, mode=None, owner=None, parent_dir_mode=None, path=None
    ):

        super(FileIsPresent, self).__init__(
            var_names=["group", "mode", "owner", "parent_dir_mode", "path"]
        )
        self._group = group
        self._mode = mode
        self._owner = owner
        self._parent_dir_mode = parent_dir_mode
        self._path = path

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, mode):
        self._mode = mode

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    @property
    def parent_dir_mode(self):
        return self._parent_dir_mode

    @parent_dir_mode.setter
    def parent_dir_mode(self, parent_dir_mode):
        self._parent_dir_mode = parent_dir_mode

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path


frecklet_class = FileIsPresent
