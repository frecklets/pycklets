# -*- coding: utf-8 -*-


#
# module path: pycklets.ssh_key_authorized.SshKeyAuthorized
#


from pyckles import AutoPycklet


class SshKeyAuthorized(AutoPycklet):
    """No documentation available.

       Args:
         key: The public key to add.
         user: The name of the user.

    """

    FRECKLET_ID = "ssh-key-authorized"

    def __init__(self, key=None, user=None):

        super(SshKeyAuthorized, self).__init__(var_names=["key", "user"])
        self._key = key
        self._user = user

    @property
    def key(self):
        return self._key

    @key.setter
    def key(self, key):
        self._key = key

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user


frecklet_class = SshKeyAuthorized
