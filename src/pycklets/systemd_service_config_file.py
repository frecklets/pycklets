# -*- coding: utf-8 -*-


#
# module path: pycklets.systemd_service_config_file.SystemdServiceConfigFile
#


from pyckles import AutoPycklet


class SystemdServiceConfigFile(AutoPycklet):
    """Environment variables for a systemd unit.

       Args:
         env_vars: n/a
         group: The group of the file.
         mode: The permissions of the file.
         owner: The owner of the file.
         path: The path to the file.

    """

    FRECKLET_ID = "systemd-service-config-file"

    def __init__(self, env_vars=None, group=None, mode=None, owner=None, path=None):

        super(SystemdServiceConfigFile, self).__init__(
            var_names=["env_vars", "group", "mode", "owner", "path"]
        )
        self._env_vars = env_vars
        self._group = group
        self._mode = mode
        self._owner = owner
        self._path = path

    @property
    def env_vars(self):
        return self._env_vars

    @env_vars.setter
    def env_vars(self, env_vars):
        self._env_vars = env_vars

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, mode):
        self._mode = mode

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path


frecklet_class = SystemdServiceConfigFile
