# -*- coding: utf-8 -*-


#
# module path: pycklets.executable_downloaded.ExecutableDownloaded
#


from pyckles import AutoPycklet


class ExecutableDownloaded(AutoPycklet):
    """Download a binary, (optionally) extract it, make it executable.

     This frecklet tries to be as generically useful as possible for the situation where there is a single-file binary.

     It comes with a few defaults, which are dependent on the user that was chosen as the owner and the value for 'dest' that was specified:

       - if no user:
         - if localhost target: user that executes the frecklet is used
         - if non-localhost target: login user for target is used

       - no dest:
         - if user == 'root': dest = '/usr/local/bin'
         - if user is not 'root': dest = "~/.local/bin"

     Since a lot of 'single-binary'-applications come as a zipped (or otherwise archived) package, this frecklet also comes with an
     'extract' flag. If set, *freckles* will try to extract the downloaded artifact into a temporary folder, and then move the content(s)
     of the archive to the target 'dest'-ination (using the 'exe_name' value as a pattern matcher -- see below).

     Also, depending on whether 'exe_name' is provided and 'extract' is true or false, the behaviour of this *frecklet* slightly changes:

       - if no 'exe_name':
         - if 'extract' is false: full path to executable is "{{ dest }}/{{ basename of url }}"
         - if 'extract' is true: all content of the archive will be moved into "{{ dest }}"
       - if 'exe_name':
         - if 'extract' is false: full path to executable is "{{ dest }}/{{ exe_name }}"
         - if 'extract' is true: '
           - if not 'extract_subfolder': 'exe_name' will be treated as glob, and all files matching that glob will be moved into "{{ dest }}"
           - if 'extract_subfolder', 'exe_name' will be treated as glob, and all files in 'extract_subfolder' matching that glob will be moved into "{{ dest }}"

       Args:
         dest: The (absolute) path to the parent folder of the downloaded executable file.
         exe_mode: The mode for the executable file(s) (default: '0755').
         exe_name: A (matcher) glob to determine which files to copy to destination (in case of 'extract'), or the target file name (defaults to either '*' or 'url | basename'.
         extract: Whether the downloaded file is an archive and needs to be extracted.
         extract_subfolder: Base part where to start looking for the exe_name patter.
         force: Force overwrite executable if it already exists.
         group: The group of the executable.
         owner: The owner of the executable.
         url: The url to download (can be templated).

    """

    FRECKLET_ID = "executable-downloaded"

    def __init__(
        self,
        dest=None,
        exe_mode="0755",
        exe_name=None,
        extract=None,
        extract_subfolder=None,
        force=None,
        group=None,
        owner=None,
        url=None,
    ):

        super(ExecutableDownloaded, self).__init__(
            var_names=[
                "dest",
                "exe_mode",
                "exe_name",
                "extract",
                "extract_subfolder",
                "force",
                "group",
                "owner",
                "url",
            ]
        )
        self._dest = dest
        self._exe_mode = exe_mode
        self._exe_name = exe_name
        self._extract = extract
        self._extract_subfolder = extract_subfolder
        self._force = force
        self._group = group
        self._owner = owner
        self._url = url

    @property
    def dest(self):
        return self._dest

    @dest.setter
    def dest(self, dest):
        self._dest = dest

    @property
    def exe_mode(self):
        return self._exe_mode

    @exe_mode.setter
    def exe_mode(self, exe_mode):
        self._exe_mode = exe_mode

    @property
    def exe_name(self):
        return self._exe_name

    @exe_name.setter
    def exe_name(self, exe_name):
        self._exe_name = exe_name

    @property
    def extract(self):
        return self._extract

    @extract.setter
    def extract(self, extract):
        self._extract = extract

    @property
    def extract_subfolder(self):
        return self._extract_subfolder

    @extract_subfolder.setter
    def extract_subfolder(self, extract_subfolder):
        self._extract_subfolder = extract_subfolder

    @property
    def force(self):
        return self._force

    @force.setter
    def force(self, force):
        self._force = force

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    @property
    def url(self):
        return self._url

    @url.setter
    def url(self, url):
        self._url = url


frecklet_class = ExecutableDownloaded
