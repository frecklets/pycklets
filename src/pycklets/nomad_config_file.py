# -*- coding: utf-8 -*-


#
# module path: pycklets.nomad_config_file.NomadConfigFile
#


from pyckles import AutoPycklet


class NomadConfigFile(AutoPycklet):
    """Consul daemon config file.

       Args:
         data_dir: This flag provides a data directory for the agent to store state.
         datacenter: This flag controls the datacenter in which the agent is running.
         group: The group of the file.
         mode: The permissions of the file.
         owner: The owner of the file.
         path: The path to the file.

    """

    FRECKLET_ID = "nomad-config-file"

    def __init__(
        self,
        data_dir=None,
        datacenter="dc1",
        group=None,
        mode=None,
        owner=None,
        path=None,
    ):

        super(NomadConfigFile, self).__init__(
            var_names=["data_dir", "datacenter", "group", "mode", "owner", "path"]
        )
        self._data_dir = data_dir
        self._datacenter = datacenter
        self._group = group
        self._mode = mode
        self._owner = owner
        self._path = path

    @property
    def data_dir(self):
        return self._data_dir

    @data_dir.setter
    def data_dir(self, data_dir):
        self._data_dir = data_dir

    @property
    def datacenter(self):
        return self._datacenter

    @datacenter.setter
    def datacenter(self, datacenter):
        self._datacenter = datacenter

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, mode):
        self._mode = mode

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path


frecklet_class = NomadConfigFile
