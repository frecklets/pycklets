# -*- coding: utf-8 -*-


#
# module path: pycklets.link_exists.LinkExists
#


from pyckles import AutoPycklet


class LinkExists(AutoPycklet):
    """Ensure a filesystem link from to a file/folder exists.

     If the ``owner`` and/or ``group`` variables is/are specified, those will be created if they don't exist yet.

       Args:
         dest: The path to the target of the link.
         group: The group of the link, will be created if necessary.
         hard: Whether to create a hard link instead of a symlink.
         mode: The permissions of the link.
         owner: The owner of the link, will be created if necessary.
         parent_dir_mode: The permissions of the parent directory.
         src: The path to the file to link to.

    """

    FRECKLET_ID = "link-exists"

    def __init__(
        self,
        dest=None,
        group=None,
        hard=None,
        mode=None,
        owner=None,
        parent_dir_mode=None,
        src=None,
    ):

        super(LinkExists, self).__init__(
            var_names=[
                "dest",
                "group",
                "hard",
                "mode",
                "owner",
                "parent_dir_mode",
                "src",
            ]
        )
        self._dest = dest
        self._group = group
        self._hard = hard
        self._mode = mode
        self._owner = owner
        self._parent_dir_mode = parent_dir_mode
        self._src = src

    @property
    def dest(self):
        return self._dest

    @dest.setter
    def dest(self, dest):
        self._dest = dest

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def hard(self):
        return self._hard

    @hard.setter
    def hard(self, hard):
        self._hard = hard

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, mode):
        self._mode = mode

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    @property
    def parent_dir_mode(self):
        return self._parent_dir_mode

    @parent_dir_mode.setter
    def parent_dir_mode(self, parent_dir_mode):
        self._parent_dir_mode = parent_dir_mode

    @property
    def src(self):
        return self._src

    @src.setter
    def src(self, src):
        self._src = src


frecklet_class = LinkExists
