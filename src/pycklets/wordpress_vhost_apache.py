# -*- coding: utf-8 -*-


#
# module path: pycklets.wordpress_vhost_apache.WordpressVhostApache
#


from pyckles import AutoPycklet


class WordpressVhostApache(AutoPycklet):
    """Create Apache wordpress virtual host config.

       Args:
         base_path: The wordpress project folders parent directory.
         host: The hostname of the server.
         listen_ip: The ip to listen to (necessary if using the Apache webserver.
         server_admin: The email address to use in the vhost file and with letsencrypt, falls back to 'wp_admin_email.
         use_https: Request a lets-encrypt certificate and serve devpi via https (needs 'server_admin' or 'wp_admin_email' set).
         wp_title: The name of the wordpress instance.

    """

    FRECKLET_ID = "wordpress-vhost-apache"

    def __init__(
        self,
        base_path="/var/www",
        host="localhost",
        listen_ip="_default_",
        server_admin=None,
        use_https=None,
        wp_title=None,
    ):

        super(WordpressVhostApache, self).__init__(
            var_names=[
                "base_path",
                "host",
                "listen_ip",
                "server_admin",
                "use_https",
                "wp_title",
            ]
        )
        self._base_path = base_path
        self._host = host
        self._listen_ip = listen_ip
        self._server_admin = server_admin
        self._use_https = use_https
        self._wp_title = wp_title

    @property
    def base_path(self):
        return self._base_path

    @base_path.setter
    def base_path(self, base_path):
        self._base_path = base_path

    @property
    def host(self):
        return self._host

    @host.setter
    def host(self, host):
        self._host = host

    @property
    def listen_ip(self):
        return self._listen_ip

    @listen_ip.setter
    def listen_ip(self, listen_ip):
        self._listen_ip = listen_ip

    @property
    def server_admin(self):
        return self._server_admin

    @server_admin.setter
    def server_admin(self, server_admin):
        self._server_admin = server_admin

    @property
    def use_https(self):
        return self._use_https

    @use_https.setter
    def use_https(self, use_https):
        self._use_https = use_https

    @property
    def wp_title(self):
        return self._wp_title

    @wp_title.setter
    def wp_title(self, wp_title):
        self._wp_title = wp_title


frecklet_class = WordpressVhostApache
