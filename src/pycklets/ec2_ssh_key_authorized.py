# -*- coding: utf-8 -*-


#
# module path: pycklets.ec2_ssh_key_authorized.Ec2SshKeyAuthorized
#


from pyckles import AutoPycklet


class Ec2SshKeyAuthorized(AutoPycklet):
    """Add ssh key for use with AWS EC2.

       Args:
         aws_access_key: The AWS access key.
         aws_secret_key: The AWS secret key.
         key_content: The content of the public ssh key.
         name: The alias of the ssh key on AWS.
         password: The password to unlock the key (only used if key doesn't exist already).
         path: The path to the private ssh key.
         user: The name of the ssh key owner.

    """

    FRECKLET_ID = "ec2-ssh-key-authorized"

    def __init__(
        self,
        aws_access_key=None,
        aws_secret_key=None,
        key_content=None,
        name=None,
        password=None,
        path="~/.ssh/id_",
        user=None,
    ):

        super(Ec2SshKeyAuthorized, self).__init__(
            var_names=[
                "aws_access_key",
                "aws_secret_key",
                "key_content",
                "name",
                "password",
                "path",
                "user",
            ]
        )
        self._aws_access_key = aws_access_key
        self._aws_secret_key = aws_secret_key
        self._key_content = key_content
        self._name = name
        self._password = password
        self._path = path
        self._user = user

    @property
    def aws_access_key(self):
        return self._aws_access_key

    @aws_access_key.setter
    def aws_access_key(self, aws_access_key):
        self._aws_access_key = aws_access_key

    @property
    def aws_secret_key(self):
        return self._aws_secret_key

    @aws_secret_key.setter
    def aws_secret_key(self, aws_secret_key):
        self._aws_secret_key = aws_secret_key

    @property
    def key_content(self):
        return self._key_content

    @key_content.setter
    def key_content(self, key_content):
        self._key_content = key_content

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def password(self):
        return self._password

    @password.setter
    def password(self, password):
        self._password = password

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user


frecklet_class = Ec2SshKeyAuthorized
