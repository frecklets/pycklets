# -*- coding: utf-8 -*-


#
# module path: pycklets.path_is_absent.PathIsAbsent
#


from pyckles import AutoPycklet


class PathIsAbsent(AutoPycklet):
    """Ensure a file or folder is absent.
     If the path is a folder, it will be deleted recursively.

       Args:
         become: Whether to use elevated privileges when deleting a file/tree.
         path: The path.

    """

    FRECKLET_ID = "path-is-absent"

    def __init__(self, become=None, path=None):

        super(PathIsAbsent, self).__init__(var_names=["become", "path"])
        self._become = become
        self._path = path

    @property
    def become(self):
        return self._become

    @become.setter
    def become(self, become):
        self._become = become

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path


frecklet_class = PathIsAbsent
