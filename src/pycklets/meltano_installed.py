# -*- coding: utf-8 -*-


#
# module path: pycklets.meltano_installed.MeltanoInstalled
#


from pyckles import AutoPycklet


class MeltanoInstalled(AutoPycklet):
    """Install a Meltano service.

       Args:
         user: The user to install Meltano for.

    """

    FRECKLET_ID = "meltano-installed"

    def __init__(self, user="meltano"):

        super(MeltanoInstalled, self).__init__(var_names=["user"])
        self._user = user

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user


frecklet_class = MeltanoInstalled
