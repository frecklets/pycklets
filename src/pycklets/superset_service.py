# -*- coding: utf-8 -*-


#
# module path: pycklets.superset_service.SupersetService
#


from pyckles import AutoPycklet


class SupersetService(AutoPycklet):
    """Install a Superset business intelligence service.

     This only installs the application, and a systemd service unit. For a full setup, check out the 'superset-standalone' frecklet.

       Args:
         admin_email: The email address of the admin user.
         admin_firstname: The first name of the admin user.
         admin_lastname: The last name of the admin user.
         admin_password: The service admin password.
         admin_username: The username of the admin user.
         db_host_ip: The db host ip address.
         db_name: the db name.
         db_password: The db password.
         db_type: The type of db to use.
         db_user: The db username.
         python_version: The version of Python to use for the Superset virtualenv.
         superset_group: The superset group.
         superset_user: The superset user.
         superset_version: The version of superset.

    """

    FRECKLET_ID = "superset-service"

    def __init__(
        self,
        admin_email=None,
        admin_firstname=None,
        admin_lastname=None,
        admin_password=None,
        admin_username="admin",
        db_host_ip="127.0.0.1",
        db_name="superset",
        db_password=None,
        db_type="sqlite",
        db_user="superset",
        python_version="3.7.2",
        superset_group="superset",
        superset_user="superset",
        superset_version="0.28.1",
    ):

        super(SupersetService, self).__init__(
            var_names=[
                "admin_email",
                "admin_firstname",
                "admin_lastname",
                "admin_password",
                "admin_username",
                "db_host_ip",
                "db_name",
                "db_password",
                "db_type",
                "db_user",
                "python_version",
                "superset_group",
                "superset_user",
                "superset_version",
            ]
        )
        self._admin_email = admin_email
        self._admin_firstname = admin_firstname
        self._admin_lastname = admin_lastname
        self._admin_password = admin_password
        self._admin_username = admin_username
        self._db_host_ip = db_host_ip
        self._db_name = db_name
        self._db_password = db_password
        self._db_type = db_type
        self._db_user = db_user
        self._python_version = python_version
        self._superset_group = superset_group
        self._superset_user = superset_user
        self._superset_version = superset_version

    @property
    def admin_email(self):
        return self._admin_email

    @admin_email.setter
    def admin_email(self, admin_email):
        self._admin_email = admin_email

    @property
    def admin_firstname(self):
        return self._admin_firstname

    @admin_firstname.setter
    def admin_firstname(self, admin_firstname):
        self._admin_firstname = admin_firstname

    @property
    def admin_lastname(self):
        return self._admin_lastname

    @admin_lastname.setter
    def admin_lastname(self, admin_lastname):
        self._admin_lastname = admin_lastname

    @property
    def admin_password(self):
        return self._admin_password

    @admin_password.setter
    def admin_password(self, admin_password):
        self._admin_password = admin_password

    @property
    def admin_username(self):
        return self._admin_username

    @admin_username.setter
    def admin_username(self, admin_username):
        self._admin_username = admin_username

    @property
    def db_host_ip(self):
        return self._db_host_ip

    @db_host_ip.setter
    def db_host_ip(self, db_host_ip):
        self._db_host_ip = db_host_ip

    @property
    def db_name(self):
        return self._db_name

    @db_name.setter
    def db_name(self, db_name):
        self._db_name = db_name

    @property
    def db_password(self):
        return self._db_password

    @db_password.setter
    def db_password(self, db_password):
        self._db_password = db_password

    @property
    def db_type(self):
        return self._db_type

    @db_type.setter
    def db_type(self, db_type):
        self._db_type = db_type

    @property
    def db_user(self):
        return self._db_user

    @db_user.setter
    def db_user(self, db_user):
        self._db_user = db_user

    @property
    def python_version(self):
        return self._python_version

    @python_version.setter
    def python_version(self, python_version):
        self._python_version = python_version

    @property
    def superset_group(self):
        return self._superset_group

    @superset_group.setter
    def superset_group(self, superset_group):
        self._superset_group = superset_group

    @property
    def superset_user(self):
        return self._superset_user

    @superset_user.setter
    def superset_user(self, superset_user):
        self._superset_user = superset_user

    @property
    def superset_version(self):
        return self._superset_version

    @superset_version.setter
    def superset_version(self, superset_version):
        self._superset_version = superset_version


frecklet_class = SupersetService
