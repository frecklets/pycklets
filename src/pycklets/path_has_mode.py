# -*- coding: utf-8 -*-


#
# module path: pycklets.path_has_mode.PathHasMode
#


from pyckles import AutoPycklet


class PathHasMode(AutoPycklet):
    """Make sure a file/folder has a certain owner/group.

     This will recursively apply the mode change in case the path is a directory.
     If the path does not exist, nothing will be done.

     Root/sudo permissions will be used to do the chmod.

       Args:
         mode: The mode to apply.
         path: the path
         recursive: Whether to apply the changes recursively (if folder).

    """

    FRECKLET_ID = "path-has-mode"

    def __init__(self, mode=None, path=None, recursive=None):

        super(PathHasMode, self).__init__(var_names=["mode", "path", "recursive"])
        self._mode = mode
        self._path = path
        self._recursive = recursive

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, mode):
        self._mode = mode

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path

    @property
    def recursive(self):
        return self._recursive

    @recursive.setter
    def recursive(self, recursive):
        self._recursive = recursive


frecklet_class = PathHasMode
