# -*- coding: utf-8 -*-


#
# module path: pycklets.git_repo_synced.GitRepoSynced
#


from pyckles import AutoPycklet


class GitRepoSynced(AutoPycklet):
    """Clones or pulls a git repository.

     Create the group/owner if not available on the system. This does not put the owner into the group if both are specified,
     so do that before-hand if you need that much control.

     This does not install 'git' if it isn't already available and will fail if that's the case.

       Args:
         dest: The destination path.
         ensure_git: Ensure git is installed.
         group: The group of the target folder.
         owner: The owner of the target folder.
         repo: The source repository.
         version: The version (tag, branch, hash, ..) to use.

    """

    FRECKLET_ID = "git-repo-synced"

    def __init__(
        self,
        dest=None,
        ensure_git=None,
        group=None,
        owner=None,
        repo=None,
        version="master",
    ):

        super(GitRepoSynced, self).__init__(
            var_names=["dest", "ensure_git", "group", "owner", "repo", "version"]
        )
        self._dest = dest
        self._ensure_git = ensure_git
        self._group = group
        self._owner = owner
        self._repo = repo
        self._version = version

    @property
    def dest(self):
        return self._dest

    @dest.setter
    def dest(self, dest):
        self._dest = dest

    @property
    def ensure_git(self):
        return self._ensure_git

    @ensure_git.setter
    def ensure_git(self, ensure_git):
        self._ensure_git = ensure_git

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    @property
    def repo(self):
        return self._repo

    @repo.setter
    def repo(self, repo):
        self._repo = repo

    @property
    def version(self):
        return self._version

    @version.setter
    def version(self, version):
        self._version = version


frecklet_class = GitRepoSynced
