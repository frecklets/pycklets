# -*- coding: utf-8 -*-


#
# module path: pycklets.terragrunt_installed.TerragruntInstalled
#


from pyckles import AutoPycklet


class TerragruntInstalled(AutoPycklet):
    """Installs [Terragrunt](https://github.com/gruntwork-io/terragrunt).

       Args:
         arch: The architecture of the host system.
         dest: The destination folder.
         group: The group of the executable.
         install_terraform: Whether to also install terraform.
         owner: The owner of the executable.
         platform: The platform of the host system.
         version: The version of the product.

    """

    FRECKLET_ID = "terragrunt-installed"

    def __init__(
        self,
        arch="amd64",
        dest="/usr/local/bin",
        group="root",
        install_terraform=None,
        owner="root",
        platform="linux",
        version="0.19.8",
    ):

        super(TerragruntInstalled, self).__init__(
            var_names=[
                "arch",
                "dest",
                "group",
                "install_terraform",
                "owner",
                "platform",
                "version",
            ]
        )
        self._arch = arch
        self._dest = dest
        self._group = group
        self._install_terraform = install_terraform
        self._owner = owner
        self._platform = platform
        self._version = version

    @property
    def arch(self):
        return self._arch

    @arch.setter
    def arch(self, arch):
        self._arch = arch

    @property
    def dest(self):
        return self._dest

    @dest.setter
    def dest(self, dest):
        self._dest = dest

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def install_terraform(self):
        return self._install_terraform

    @install_terraform.setter
    def install_terraform(self, install_terraform):
        self._install_terraform = install_terraform

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    @property
    def platform(self):
        return self._platform

    @platform.setter
    def platform(self, platform):
        self._platform = platform

    @property
    def version(self):
        return self._version

    @version.setter
    def version(self, version):
        self._version = version


frecklet_class = TerragruntInstalled
