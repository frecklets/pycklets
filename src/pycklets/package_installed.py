# -*- coding: utf-8 -*-


#
# module path: pycklets.package_installed.PackageInstalled
#


from pyckles import AutoPycklet


class PackageInstalled(AutoPycklet):
    """Install a single package, including the package manager that that was chosen, if necessary.

     More information and examples to come, for now please refer to the [freckfrackery.install-pkgs Ansible role](https://gitlab.com/freckfrackery/freckfrackery.install-pkgs/blob/master/README.md) for more information.

       Args:
         become: Whether to use root permissions to install the package.
         no_pkg_mgr: Don't try to install a necessary package manager.
         package: The package name.

    """

    FRECKLET_ID = "package-installed"

    def __init__(self, become=True, no_pkg_mgr=None, package=None):

        super(PackageInstalled, self).__init__(
            var_names=["become", "no_pkg_mgr", "package"]
        )
        self._become = become
        self._no_pkg_mgr = no_pkg_mgr
        self._package = package

    @property
    def become(self):
        return self._become

    @become.setter
    def become(self, become):
        self._become = become

    @property
    def no_pkg_mgr(self):
        return self._no_pkg_mgr

    @no_pkg_mgr.setter
    def no_pkg_mgr(self, no_pkg_mgr):
        self._no_pkg_mgr = no_pkg_mgr

    @property
    def package(self):
        return self._package

    @package.setter
    def package(self, package):
        self._package = package


frecklet_class = PackageInstalled
