# -*- coding: utf-8 -*-


#
# module path: pycklets.postgresql_service.PostgresqlService
#


from pyckles import AutoPycklet


class PostgresqlService(AutoPycklet):
    """This frecklet installs the PostgreSQL service using the [ANXS.postgresql](https://gitlab.com/frkl-downstream/ANXS.postgresql)
     Ansible role.

     This frecklet does not do any configuration, and uses all the defaults of the underlying
     Ansible role. Use other tasks to do any configuration after executing this.

       Args:
         listen_addresses: The network addresses this instance listens on. Default: localhost.
         max_connections: Max. allowed connections. Default: 100
         pg_hba: A list of hosts to allow connections from.
         port: The port this postgresql service listens on. Default: 5432.
         postgresql_group_id: An (optional) custom PostgreSQL group gid.
         postgresql_user_id: An (optional) custom PostgreSQL user uid.
         postgresql_version: The version of postgresql.
         superuser_reserved_connections: Superuser reserved connections. Default: 3

    """

    FRECKLET_ID = "postgresql-service"

    def __init__(
        self,
        listen_addresses=None,
        max_connections=None,
        pg_hba=None,
        port=None,
        postgresql_group_id=None,
        postgresql_user_id=None,
        postgresql_version=None,
        superuser_reserved_connections=None,
    ):

        super(PostgresqlService, self).__init__(
            var_names=[
                "listen_addresses",
                "max_connections",
                "pg_hba",
                "port",
                "postgresql_group_id",
                "postgresql_user_id",
                "postgresql_version",
                "superuser_reserved_connections",
            ]
        )
        self._listen_addresses = listen_addresses
        self._max_connections = max_connections
        self._pg_hba = pg_hba
        self._port = port
        self._postgresql_group_id = postgresql_group_id
        self._postgresql_user_id = postgresql_user_id
        self._postgresql_version = postgresql_version
        self._superuser_reserved_connections = superuser_reserved_connections

    @property
    def listen_addresses(self):
        return self._listen_addresses

    @listen_addresses.setter
    def listen_addresses(self, listen_addresses):
        self._listen_addresses = listen_addresses

    @property
    def max_connections(self):
        return self._max_connections

    @max_connections.setter
    def max_connections(self, max_connections):
        self._max_connections = max_connections

    @property
    def pg_hba(self):
        return self._pg_hba

    @pg_hba.setter
    def pg_hba(self, pg_hba):
        self._pg_hba = pg_hba

    @property
    def port(self):
        return self._port

    @port.setter
    def port(self, port):
        self._port = port

    @property
    def postgresql_group_id(self):
        return self._postgresql_group_id

    @postgresql_group_id.setter
    def postgresql_group_id(self, postgresql_group_id):
        self._postgresql_group_id = postgresql_group_id

    @property
    def postgresql_user_id(self):
        return self._postgresql_user_id

    @postgresql_user_id.setter
    def postgresql_user_id(self, postgresql_user_id):
        self._postgresql_user_id = postgresql_user_id

    @property
    def postgresql_version(self):
        return self._postgresql_version

    @postgresql_version.setter
    def postgresql_version(self, postgresql_version):
        self._postgresql_version = postgresql_version

    @property
    def superuser_reserved_connections(self):
        return self._superuser_reserved_connections

    @superuser_reserved_connections.setter
    def superuser_reserved_connections(self, superuser_reserved_connections):
        self._superuser_reserved_connections = superuser_reserved_connections


frecklet_class = PostgresqlService
