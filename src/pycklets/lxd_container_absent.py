# -*- coding: utf-8 -*-


#
# module path: pycklets.lxd_container_absent.LxdContainerAbsent
#


from pyckles import AutoPycklet


class LxdContainerAbsent(AutoPycklet):
    """Makes sure an lxd container with a specific is absent on this machine.

       Args:
         name: A name to identify this container with.

    """

    FRECKLET_ID = "lxd-container-absent"

    def __init__(self, name=None):

        super(LxdContainerAbsent, self).__init__(var_names=["name"])
        self._name = name

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name


frecklet_class = LxdContainerAbsent
