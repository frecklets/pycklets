# -*- coding: utf-8 -*-


#
# module path: pycklets.matomo_standalone.MatomoStandalone
#


from pyckles import AutoPycklet


class MatomoStandalone(AutoPycklet):
    """Install [Matomo](https://matomo.org/) web analytics service.

     This also installs Nginx as a reverse proxy, and, optionally, configures https with a letsencrypt certificate.

     Make sure to specify the 'server_admin' email address
     when using this with https. There is no validation check for this yet.

     Once finished, and you visit the hostname you provided (or localhost if you didn't), you'll be presented with a
     setup page. If you didn't change the defaults (which you should, at least for 'matomo_db_password'), here are the defaults:

         Database Server: 127.0.0.1
         Login: matomo
         Password: matomo_password
         Database Name: matomo

     Leave the other settings.

       Args:
         base_path: The matomo webapp parent directory.
         hostnames: The hostname(s) of the server.
         letsencrypt_staging: Whether to use the letsencrypt staging server.
         matomo_db_dump_file: An (optional) database dump file.
         matomo_db_import: Whether to import a sql dump.
         matomo_db_name: The name of the database to use.
         matomo_db_password: The password for the database.
         matomo_db_user: The db user.
         path: The path to the vhost file.
         server_admin: The email address to use in the vhost file and with letsencrypt.
         use_https: Request a lets-encrypt certificate and serve devpi via https (needs 'server_admin' set).
         webserver_group: The group to run the webserver as (if applicable).
         webserver_user: The user to run the webserver as.

    """

    FRECKLET_ID = "matomo-standalone"

    def __init__(
        self,
        base_path="/var/www",
        hostnames=["localhost"],
        letsencrypt_staging=None,
        matomo_db_dump_file=None,
        matomo_db_import=None,
        matomo_db_name="matomo",
        matomo_db_password="matomo_password",
        matomo_db_user="matomo",
        path="/etc/nginx/sites-enabled/matomo.conf",
        server_admin=None,
        use_https=None,
        webserver_group="www-data",
        webserver_user="www-data",
    ):

        super(MatomoStandalone, self).__init__(
            var_names=[
                "base_path",
                "hostnames",
                "letsencrypt_staging",
                "matomo_db_dump_file",
                "matomo_db_import",
                "matomo_db_name",
                "matomo_db_password",
                "matomo_db_user",
                "path",
                "server_admin",
                "use_https",
                "webserver_group",
                "webserver_user",
            ]
        )
        self._base_path = base_path
        self._hostnames = hostnames
        self._letsencrypt_staging = letsencrypt_staging
        self._matomo_db_dump_file = matomo_db_dump_file
        self._matomo_db_import = matomo_db_import
        self._matomo_db_name = matomo_db_name
        self._matomo_db_password = matomo_db_password
        self._matomo_db_user = matomo_db_user
        self._path = path
        self._server_admin = server_admin
        self._use_https = use_https
        self._webserver_group = webserver_group
        self._webserver_user = webserver_user

    @property
    def base_path(self):
        return self._base_path

    @base_path.setter
    def base_path(self, base_path):
        self._base_path = base_path

    @property
    def hostnames(self):
        return self._hostnames

    @hostnames.setter
    def hostnames(self, hostnames):
        self._hostnames = hostnames

    @property
    def letsencrypt_staging(self):
        return self._letsencrypt_staging

    @letsencrypt_staging.setter
    def letsencrypt_staging(self, letsencrypt_staging):
        self._letsencrypt_staging = letsencrypt_staging

    @property
    def matomo_db_dump_file(self):
        return self._matomo_db_dump_file

    @matomo_db_dump_file.setter
    def matomo_db_dump_file(self, matomo_db_dump_file):
        self._matomo_db_dump_file = matomo_db_dump_file

    @property
    def matomo_db_import(self):
        return self._matomo_db_import

    @matomo_db_import.setter
    def matomo_db_import(self, matomo_db_import):
        self._matomo_db_import = matomo_db_import

    @property
    def matomo_db_name(self):
        return self._matomo_db_name

    @matomo_db_name.setter
    def matomo_db_name(self, matomo_db_name):
        self._matomo_db_name = matomo_db_name

    @property
    def matomo_db_password(self):
        return self._matomo_db_password

    @matomo_db_password.setter
    def matomo_db_password(self, matomo_db_password):
        self._matomo_db_password = matomo_db_password

    @property
    def matomo_db_user(self):
        return self._matomo_db_user

    @matomo_db_user.setter
    def matomo_db_user(self, matomo_db_user):
        self._matomo_db_user = matomo_db_user

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path

    @property
    def server_admin(self):
        return self._server_admin

    @server_admin.setter
    def server_admin(self, server_admin):
        self._server_admin = server_admin

    @property
    def use_https(self):
        return self._use_https

    @use_https.setter
    def use_https(self, use_https):
        self._use_https = use_https

    @property
    def webserver_group(self):
        return self._webserver_group

    @webserver_group.setter
    def webserver_group(self, webserver_group):
        self._webserver_group = webserver_group

    @property
    def webserver_user(self):
        return self._webserver_user

    @webserver_user.setter
    def webserver_user(self, webserver_user):
        self._webserver_user = webserver_user


frecklet_class = MatomoStandalone
