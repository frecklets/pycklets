# -*- coding: utf-8 -*-


#
# module path: pycklets.nomad_installed.NomadInstalled
#


from pyckles import AutoPycklet


class NomadInstalled(AutoPycklet):
    """Download the Hashicorp Nomad executable and install it locally.

       Args:
         arch: The architecture of the host system.
         dest: The (absolute) path to the parent folder of the downloaded executable file.
         group: The group of the executable.
         owner: The owner of the executable.
         platform: The platform of the host system.
         version: The version of Nomad to install.

    """

    FRECKLET_ID = "nomad-installed"

    def __init__(
        self,
        arch=None,
        dest=None,
        group=None,
        owner=None,
        platform=None,
        version="0.9.3",
    ):

        super(NomadInstalled, self).__init__(
            var_names=["arch", "dest", "group", "owner", "platform", "version"]
        )
        self._arch = arch
        self._dest = dest
        self._group = group
        self._owner = owner
        self._platform = platform
        self._version = version

    @property
    def arch(self):
        return self._arch

    @arch.setter
    def arch(self, arch):
        self._arch = arch

    @property
    def dest(self):
        return self._dest

    @dest.setter
    def dest(self, dest):
        self._dest = dest

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    @property
    def platform(self):
        return self._platform

    @platform.setter
    def platform(self, platform):
        self._platform = platform

    @property
    def version(self):
        return self._version

    @version.setter
    def version(self, version):
        self._version = version


frecklet_class = NomadInstalled
