# -*- coding: utf-8 -*-


#
# module path: pycklets.curl_installed.CurlInstalled
#


from pyckles import AutoPycklet


class CurlInstalled(AutoPycklet):
    """Install the '[curl](https://curl.haxx.se/) utility.

       Args:
         pkg_mgr: the package manager to use

    """

    FRECKLET_ID = "curl-installed"

    def __init__(self, pkg_mgr="auto"):

        super(CurlInstalled, self).__init__(var_names=["pkg_mgr"])
        self._pkg_mgr = pkg_mgr

    @property
    def pkg_mgr(self):
        return self._pkg_mgr

    @pkg_mgr.setter
    def pkg_mgr(self, pkg_mgr):
        self._pkg_mgr = pkg_mgr


frecklet_class = CurlInstalled
