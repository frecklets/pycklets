# -*- coding: utf-8 -*-


#
# module path: pycklets.python_virtualenv_execute_shell.PythonVirtualenvExecuteShell
#


from pyckles import AutoPycklet


class PythonVirtualenvExecuteShell(AutoPycklet):
    """Execute a command inside a virtualenv, the 'command' needs to be available in the virtualenv (<venv>/bin/<command>).

     You can easily use the 'execute-shell' frecklet instead of this, because for now with this you need to know the path to the virtualenv or system-wide. If you used the 'python-virtualenv' frecklet and the 'python_type' 'pyenv',
     your virtualenv will be located under <user_home>/.pyenv/versions/<venv_name>.
     In the future this will take the same vars that the 'python-virtualenv' frecklet takes, and auto-calculate the
     path to the virtualenv.

     For now, only absolute virtualenv paths are supported, '~' won't work.

     Also, this frecklet does not support commands that require user input. For this, you'd have to create a frecklet
     similar to this, which uses 'expect'. Not hard, just not available yet.

       Args:
         chdir: The working directory.
         command: The command to execute.
         environment: A dictionary of environment variables to add/set.
         ignore_error: Whether to ignore any potential errors.
         no_log: Whether to hide the log of this command (because for example the command contains sensitive information).
         user: The user to execute this command as.
         virtualenv_path: The (absolute) virtualenv (base) path.

    """

    FRECKLET_ID = "python-virtualenv-execute-shell"

    def __init__(
        self,
        chdir=None,
        command=None,
        environment=None,
        ignore_error=None,
        no_log=None,
        user=None,
        virtualenv_path=None,
    ):

        super(PythonVirtualenvExecuteShell, self).__init__(
            var_names=[
                "chdir",
                "command",
                "environment",
                "ignore_error",
                "no_log",
                "user",
                "virtualenv_path",
            ]
        )
        self._chdir = chdir
        self._command = command
        self._environment = environment
        self._ignore_error = ignore_error
        self._no_log = no_log
        self._user = user
        self._virtualenv_path = virtualenv_path

    @property
    def chdir(self):
        return self._chdir

    @chdir.setter
    def chdir(self, chdir):
        self._chdir = chdir

    @property
    def command(self):
        return self._command

    @command.setter
    def command(self, command):
        self._command = command

    @property
    def environment(self):
        return self._environment

    @environment.setter
    def environment(self, environment):
        self._environment = environment

    @property
    def ignore_error(self):
        return self._ignore_error

    @ignore_error.setter
    def ignore_error(self, ignore_error):
        self._ignore_error = ignore_error

    @property
    def no_log(self):
        return self._no_log

    @no_log.setter
    def no_log(self, no_log):
        self._no_log = no_log

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user

    @property
    def virtualenv_path(self):
        return self._virtualenv_path

    @virtualenv_path.setter
    def virtualenv_path(self, virtualenv_path):
        self._virtualenv_path = virtualenv_path


frecklet_class = PythonVirtualenvExecuteShell
