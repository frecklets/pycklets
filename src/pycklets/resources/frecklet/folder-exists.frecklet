doc:
  short_help: Ensure a folder exists.
  help: |
    Ensure a folder exists on the filesystem

    If the ``owner`` and/or ``group`` variables is/are specified, create those in case they don't exist yet.

    If the ``owner`` variable is specified, this frecklet will use elevated permissions.
  examples:
  - title: Create a folder if it doesn't exist yet, including owner and group if necessary.
    vars:
      path: /tmp/freckles
      owner: freckles
      group: wheel
  - title: Create a folder if it doesn't exist yet.
    vars:
      path: /tmp/freckles
  - title: Create a folder incl. parent folders if necessary.
    vars:
      path: /tmp/parent_1/parent_2/freckles
args:
  path:
    doc:
      short_help: The path to the folder.
    type: string
    required: true
    cli:
      param_type: argument
  owner:
    doc:
      short_help: The owner of the folder, will be created if necessary.
    type: string
    required: false
    cli:
      metavar: USER_NAME
  group:
    doc:
      short_help: The group of the folder, will be created if necessary.
    type: string
    required: false
    cli:
      metavar: GROUP_NAME
  mode:
    doc:
      short_help: The permissions of the folder.
    type: string
    required: false
    cli:
      metavar: MODE
#  become:
#    doc:
#      short_help: Whether to use root privileges to create the folder.
#    type: boolean
#    default: false
#    required: false
#    cli:
#      is_flag: true
  system_user:
    doc:
      short_help: Whether the user (and group) should be of system user/group type.
    type: boolean
    default: false
    required: false
    cli:
      show_default: true
      is_flag: true
  force_chown:
    doc:
      short_help: Whether to force changing of ownership, even if folder already exists.
    default: true
    required: false
    type: boolean
    cli:
      param_decls:
      - --force-chown/--no-force-chown

meta:
  is_interface: true
  tags:
  - file
  - filesystem
  - folder
  - mkdir
  - featured-frecklecutable

frecklets:
- group-exists:
    group: '{{:: group ::}}'
    system_group: '{{:: system_user ::}}'
    frecklet::skip: "{{:: group | true_if_empty_or('root') ::}}"
- user-exists:
    name: '{{:: owner ::}}'
    system_user: '{{:: system_user ::}}'
    frecklet::skip: "{{:: owner | true_if_empty_or('root') ::}}"
- frecklet:
    name: folder-exists.at.yml
    type: ansible-tasklist
    resources:
      ansible-tasklist:
      - folder-exists.at.yml
    properties:
      elevated: '{{:: owner | true_if_not_empty ::}}'
      idemptotent: true
      internet: false
    desc:
      short: "create directory: {{:: path ::}}'"
      long: |
        Create folder '{{:: path ::}}' if it doesn't exist yet. Fail if '{{:: path ::}}' already exists and is not a folder.

        {%:: if mode ::%}Ensure the folders mode is '{{:: mode ::}}' (recursively, incl. children){%:: else ::%}Leave the folders mode as is (or default if folder has to be created){%:: endif ::%}. {%:: if group ::%}Set group (recursively, incl. children) to be '{{:: group ::}}'{%:: else ::%}Use the executing users main group (or leave be if file already exists){%:: endif ::%} and {%:: if owner ::%}set user to be '{{:: owner ::}}' (recursively){%:: else ::%}use the executing users name as the owner (or leave be if file already exists){%:: endif ::%}.
      references:
        "'file' Ansible module": https://docs.ansible.com/ansible/latest/modules/file_module.html
  task:
    become: '{{:: owner | true_if_not_empty ::}}'
    include-type: import
  vars:
    __path__: '{{:: path ::}}'
    __owner__: '{{:: owner ::}}'
    __group__: '{{:: group ::}}'
    __mode__: '{{:: mode ::}}'
    __force_chown__: '{{:: force_chown ::}}'



