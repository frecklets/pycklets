doc:
  short_help: Ensure a file exists and has a certain content.
  help: |
    Ensure a file exists and its content is the one specified as input. Create the necessary user and parent directories if necessary.

    If a user is created, it won't have any password set, so you might have to do that in a different (ideally earlier - to have more control) step.

    If intermediate parent directories have to be created, they will inherit the owner/group information of the file to be created. If any of those
    parent directories already exist, they won't be touched at all.

    If a ``owner`` value is provided, this will use 'root' permissions to (potentially) create the parent folder, and the file.

  examples:
  - title: Create temporary file with some content
    vars:
      path: /tmp/freckles/file_with_content
      content: |
        Hello world!

        I love you!
  - title: Create file with some content, set owner and create that user if necessary
    vars:
      path: /tmp/freckles/another_file
      content: |
        Hello other world!

        I am owned by somebody!
      owner: somebody

args:
  path:
    doc:
      short_help: The path to the file.
    type: string
    required: true
    cli:
      param_type: argument
  owner:
    doc:
      short_help: The owner of the file.
    type: string
    required: false
    cli:
      metavar: USER
  group:
    doc:
      short_help: The group of the file.
    type: string
    required: false
    cli:
      metavar: GROUP
  mode:
    doc:
      short_help: The permissions of the file.
    type: string
    required: false
    cli:
      metavar: MODE
  content:
    type: string
    required: true
    doc:
      short_help: The file content.
  parent_dir_mode:
    doc:
      short_help: The permissions of the parent directory.
    type: string
    required: false
    cli:
      metavar: MODE
  system_user:
    doc:
      short_help: Whether the user and group should be of system user/group type.
    type: boolean
    default: false
    required: false
    cli:
      show_default: true
      is_flag: true

meta:
  tags:
  - file
  - filesystem
  - file-content
  - featured-frecklecutable

frecklets:
- parent-folder-exists:
    path: '{{:: path ::}}'
    owner: '{{:: owner ::}}'
    group: '{{:: group ::}}'
    mode: '{{:: parent_dir_mode ::}}'
    become: '{{:: owner | true_if_not_empty ::}}'
- task:
    become: '{{:: owner | true_if_not_empty ::}}'
  frecklet:
    name: copy
    type: ansible-module
    desc:
      short: 'write content to file: {{:: path ::}}'
      long: |
        Create or overwrite file '{{:: path ::}}' so it has the following content:

        {{:: content |indent(4, True)::}}

        {%:: if mode ::%}Ensure the file mode is '{{:: mode ::}}'{%:: else ::%}Leave file mode as is (or default if file has to be created){%:: endif ::%}. {%:: if group ::%}Set group to be '{{:: group ::}}'{%:: else ::%}Use the executing users main group (or leave be if file already exists){%:: endif ::%} and {%:: if owner ::%}set user to be '{{:: owner ::}}'{%:: else ::%}use the executing users name as the owner (or leave be if file already exists){%:: endif ::%}.
      references:
        "'copy' ansible' module": https://docs.ansible.com/ansible/latest/modules/copy_module.html
    properties:
      idempotent: true
      elevated: '{{:: owner | true_if_not_empty ::}}'
      internet: false
  vars:
    dest: '{{:: path ::}}'
    owner: '{{:: owner ::}}'
    group: '{{:: group ::}}'
    mode: '{{:: mode ::}}'
    content: "{{:: content ::}}\n"
