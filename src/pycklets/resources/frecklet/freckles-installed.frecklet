doc:
  short_help: Install 'freckles'.
  help: |
    Internally, this uses the 'freck' bash script hosted on 'https://freckles.sh' to install frecklets.
  examples:
  - title: Install 'freckles' for current user.
    vars: {}
  - title: Install the development version of 'freckles' for the current user, and run it once with a dummy job to pre-seed dependencies.
    vars:
      version: dev
      init: true
  - title: Install 'freckles' for user 'admin'. Create user if it doesn't exist, and overwrite the freckles binary if already present.
    vars:
      update: true
      user: admin

args:
  version:
    doc:
      short_help: Which version of 'freckles' to install.
    type: string
    required: false
    allowed:
    - stable
    - dev
    default: stable
  update:
    doc:
      short_help: Whether to update 'freckles' if it is already installed.
    type: boolean
    required: false
    default: false
    cli:
      param_decls:
      - --update
      - -u
  init:
    doc:
      short_help: Whether to run a dummy freckles job (to initialize dependencies).
    type: boolean
    required: false
    default: false
    cli:
      param_decls:
      - --init
      - -i
  user:
    doc:
      short_help: The user to install 'freckles' for.
    type: string
    required: false

frecklets:
- user-exists:
    frecklet::skip: '{{:: user | true_if_empty ::}}'
    name: '{{:: user ::}}'
- curl-installed
- execute-shell:
    command: 'curl https://freckles.sh | UPDATE={%:: if update ::%}true{%:: else ::%}false{%:: endif ::%} VERSION={{:: version ::}} bash'
    become_user: '{{:: user ::}}'
- execute-shell:
    frecklet::skip: '{{:: init | true_if_empty ::}}'
    command: $HOME/.local/share/freckles/bin/frecklecute debug-var ansible_env
    become_user: '{{:: user ::}}'
meta: {}
