doc:
  short_help: Create an instance on Amazon EC2, if it doesn't exist yet.

args:
  aws_access_key:
    doc:
      short_help: The AWS access key.
    type: string
    required: true
  aws_secret_key:
    doc:
      short_help: The AWS secret key.
    type: string
    secret: true
    required: true
  region:
    doc:
      short_help: The AWS region to use.
      references:
        AWS region documentation: https://docs.aws.amazon.com/general/latest/gr/rande.html#ec2_region
    type: string
    required: true
  group_name:
    doc:
      short_help: The name of the security group.
    type: string
    required: true
  description:
    doc:
      short_help: Description of the security group.
    type: string
    required: true
  vpc_id:
    doc:
      short_help: ID of the VPC to create the group in.
    type: string
    required: false
  register:
    doc:
      short_help: The name of the variable to register the result of this frecklet.
    type: string
    required: false
    cli:
      metavar: VAR_NAME

frecklets:
- frecklet:
    name: ec2_group
    type: ansible-module
    register: '{{:: register ::}}'
    properties:
      elevated: false
      idempotent: true
      internet: true
    desc:
      short: 'create EC2 security group: {{:: group_name ::}}'
    resources:
      python-package:
      - boto
      - boto3
      - botocore
  task:
    include-type: include
    delegate_to: localhost
  vars:
    aws_access_key: '{{:: aws_access_key ::}}'
    aws_secret_key: '{{:: aws_secret_key ::}}'
    region: '{{:: region ::}}'
    state: present
    name: '{{:: group_name ::}}'
    description: '{{:: description ::}}'
    vpc_id: '{{:: vpc_id ::}}'
    rules:
    - proto: tcp
      from_port: 22
      to_port: 22
      cidr_ip: 0.0.0.0/0
    - proto: tcp
      from_port: 80
      to_port: 80
      cidr_ip: 0.0.0.0/0
    - proto: tcp
      from_port: 443
      to_port: 443
      cidr_ip: 0.0.0.0/0
    rules_egress:
    - proto: all
      cidr_ip: 0.0.0.0/0


meta: {}
