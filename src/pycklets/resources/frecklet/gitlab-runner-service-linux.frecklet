doc:
  short_help: Ensures a gitlab-runner service exists on a machine

args:
  _import: gitlab-runner-installed
  user:
    doc:
      short_help: The name of the gitlab-runner service user.
    type: string
    required: false
    default: gitlab-runner
  group:
    doc:
      short_help: The name of the gitlab-runner service group.
    type: string
    required: false
    default: gitlab-runner
  install_docker:
    doc:
      short_help: Whether to also install Docker.
    type: boolean
    required: false
    default: false
    cli:
      param_decls:
      - --install-docker

frecklets:
- user-exists:
    name: '{{:: user ::}}'
    group: '{{:: group ::}}'
- gitlab-runner-installed:
    dest: /usr/local/bin
    owner: '{{:: user ::}}'
    group: '{{:: group ::}}'
    platform: linux
    arch: '{{:: arch ::}}'
- execute-shell:
    command: /usr/local/bin/gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
    become_user: root
    chdir: /usr/local/bin
- execute-shell:
    command: /usr/local/bin/gitlab-runner start
    become_user: root
    chdir: /usr/local/bin
- docker-service:
    frecklet::skip: '{{:: install_docker | negate ::}}'
    users:
    - gitlab-runner
meta: {}
