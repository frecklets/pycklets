# -*- coding: utf-8 -*-


#
# module path: pycklets.docker_frecklet_packer_template_file.DockerFreckletPackerTemplateFile
#


from pyckles import AutoPycklet


class DockerFreckletPackerTemplateFile(AutoPycklet):
    """Packer template to create a Docker image from a frecklet.

       Args:
         freckles_extra_args: Extra base args for the freckles run.
         frecklet_path: The path to the frecklet file.
         group: The group of the file.
         image_name: The name of the Docker image to create.
         mode: The permissions of the file.
         owner: The owner of the file.
         path: The path to the file.
         source_image: The name of the source Docker image.

    """

    FRECKLET_ID = "docker-frecklet-packer-template-file"

    def __init__(
        self,
        freckles_extra_args=None,
        frecklet_path=None,
        group=None,
        image_name=None,
        mode=None,
        owner=None,
        path=None,
        source_image="ubuntu",
    ):

        super(DockerFreckletPackerTemplateFile, self).__init__(
            var_names=[
                "freckles_extra_args",
                "frecklet_path",
                "group",
                "image_name",
                "mode",
                "owner",
                "path",
                "source_image",
            ]
        )
        self._freckles_extra_args = freckles_extra_args
        self._frecklet_path = frecklet_path
        self._group = group
        self._image_name = image_name
        self._mode = mode
        self._owner = owner
        self._path = path
        self._source_image = source_image

    @property
    def freckles_extra_args(self):
        return self._freckles_extra_args

    @freckles_extra_args.setter
    def freckles_extra_args(self, freckles_extra_args):
        self._freckles_extra_args = freckles_extra_args

    @property
    def frecklet_path(self):
        return self._frecklet_path

    @frecklet_path.setter
    def frecklet_path(self, frecklet_path):
        self._frecklet_path = frecklet_path

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def image_name(self):
        return self._image_name

    @image_name.setter
    def image_name(self, image_name):
        self._image_name = image_name

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, mode):
        self._mode = mode

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path

    @property
    def source_image(self):
        return self._source_image

    @source_image.setter
    def source_image(self, source_image):
        self._source_image = source_image


frecklet_class = DockerFreckletPackerTemplateFile
