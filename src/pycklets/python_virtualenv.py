# -*- coding: utf-8 -*-


#
# module path: pycklets.python_virtualenv.PythonVirtualenv
#


from pyckles import AutoPycklet


class PythonVirtualenv(AutoPycklet):
    """Installs Python either via system packages or [pyenv](https://github.com/pyenv/pyenv), then use that to create a virtualenv with the
     specified name and Python version. Also lets you specify (optional) system dependencies as well as packages to
     install in the virtualenv.

     If you choose 'system' as 'python_type', only '2' and '3' are allowed as versions. If you choose 'pyenv', you can select any of the options that can be displayed via ``pyenv install -l``.

     Also, in case you choose 'pyenv' as the 'python_type', this will add a piece of code to ``$HOME/.bashrc`` to load pyenv when the user logs in.

       Args:
         group: The group who owns/runs the virtualenv.
         pip_extra_args: Extra arguments forwarded to 'pip'.
         python_base_path: The base path to install Python into (if using 'pyenv' or 'conda').
         python_build_opts: Build options to be forwarded (if supported by 'install_type').
         python_packages: All necessary Python packages.
         python_type: How to install Python. Defaults to 'pyenv'.
         python_version: The version of python.
         system_dependencies: System packages the application depends on.
         system_user: Whether the user and group should be a system user/group.
         uid: The uid of the user to create (optional).
         update: Update packages if already installed.
         user: The user who owns/runs the virtualenv.
         venv_base_path: The path that holds the virtualenv directory.
         venv_name: The name of the virtualenv to set up.
         venv_python_exe: The (optional) path to an existing Python executable to be used for the venv.

    """

    FRECKLET_ID = "python-virtualenv"

    def __init__(
        self,
        group=None,
        pip_extra_args=None,
        python_base_path=None,
        python_build_opts=None,
        python_packages=None,
        python_type="pyenv",
        python_version="latest",
        system_dependencies=None,
        system_user=None,
        uid=None,
        update=None,
        user=None,
        venv_base_path=None,
        venv_name=None,
        venv_python_exe=None,
    ):

        super(PythonVirtualenv, self).__init__(
            var_names=[
                "group",
                "pip_extra_args",
                "python_base_path",
                "python_build_opts",
                "python_packages",
                "python_type",
                "python_version",
                "system_dependencies",
                "system_user",
                "uid",
                "update",
                "user",
                "venv_base_path",
                "venv_name",
                "venv_python_exe",
            ]
        )
        self._group = group
        self._pip_extra_args = pip_extra_args
        self._python_base_path = python_base_path
        self._python_build_opts = python_build_opts
        self._python_packages = python_packages
        self._python_type = python_type
        self._python_version = python_version
        self._system_dependencies = system_dependencies
        self._system_user = system_user
        self._uid = uid
        self._update = update
        self._user = user
        self._venv_base_path = venv_base_path
        self._venv_name = venv_name
        self._venv_python_exe = venv_python_exe

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def pip_extra_args(self):
        return self._pip_extra_args

    @pip_extra_args.setter
    def pip_extra_args(self, pip_extra_args):
        self._pip_extra_args = pip_extra_args

    @property
    def python_base_path(self):
        return self._python_base_path

    @python_base_path.setter
    def python_base_path(self, python_base_path):
        self._python_base_path = python_base_path

    @property
    def python_build_opts(self):
        return self._python_build_opts

    @python_build_opts.setter
    def python_build_opts(self, python_build_opts):
        self._python_build_opts = python_build_opts

    @property
    def python_packages(self):
        return self._python_packages

    @python_packages.setter
    def python_packages(self, python_packages):
        self._python_packages = python_packages

    @property
    def python_type(self):
        return self._python_type

    @python_type.setter
    def python_type(self, python_type):
        self._python_type = python_type

    @property
    def python_version(self):
        return self._python_version

    @python_version.setter
    def python_version(self, python_version):
        self._python_version = python_version

    @property
    def system_dependencies(self):
        return self._system_dependencies

    @system_dependencies.setter
    def system_dependencies(self, system_dependencies):
        self._system_dependencies = system_dependencies

    @property
    def system_user(self):
        return self._system_user

    @system_user.setter
    def system_user(self, system_user):
        self._system_user = system_user

    @property
    def uid(self):
        return self._uid

    @uid.setter
    def uid(self, uid):
        self._uid = uid

    @property
    def update(self):
        return self._update

    @update.setter
    def update(self, update):
        self._update = update

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user

    @property
    def venv_base_path(self):
        return self._venv_base_path

    @venv_base_path.setter
    def venv_base_path(self, venv_base_path):
        self._venv_base_path = venv_base_path

    @property
    def venv_name(self):
        return self._venv_name

    @venv_name.setter
    def venv_name(self, venv_name):
        self._venv_name = venv_name

    @property
    def venv_python_exe(self):
        return self._venv_python_exe

    @venv_python_exe.setter
    def venv_python_exe(self, venv_python_exe):
        self._venv_python_exe = venv_python_exe


frecklet_class = PythonVirtualenv
