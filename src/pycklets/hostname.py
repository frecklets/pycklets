# -*- coding: utf-8 -*-


#
# module path: pycklets.hostname.Hostname
#


from pyckles import AutoPycklet


class Hostname(AutoPycklet):
    """Set system’s hostname according to the platforms recommended practices.

     Also add the new hostnames to the line containing '127.0.0.1' in /etc/hosts.

       Args:
         hostname: The hostname.

    """

    FRECKLET_ID = "hostname"

    def __init__(self, hostname=None):

        super(Hostname, self).__init__(var_names=["hostname"])
        self._hostname = hostname

    @property
    def hostname(self):
        return self._hostname

    @hostname.setter
    def hostname(self, hostname):
        self._hostname = hostname


frecklet_class = Hostname
