# -*- coding: utf-8 -*-


#
# module path: pycklets.hcloud_ssh_key_authorized.HcloudSshKeyAuthorized
#


from pyckles import AutoPycklet


class HcloudSshKeyAuthorized(AutoPycklet):
    """Add ssh key to Hetzner cloud.

       Args:
         api_token: The hcloud API token.
         key_content: The content of the public ssh key.
         name: The alias of the ssh key on hcloud.
         password: The password to unlock the key (only used if key doesn't exist already).
         path: The path to the private ssh key.
         user: The name of the ssh key owner.

    """

    FRECKLET_ID = "hcloud-ssh-key-authorized"

    def __init__(
        self,
        api_token=None,
        key_content=None,
        name=None,
        password=None,
        path="~/.ssh/id_",
        user=None,
    ):

        super(HcloudSshKeyAuthorized, self).__init__(
            var_names=["api_token", "key_content", "name", "password", "path", "user"]
        )
        self._api_token = api_token
        self._key_content = key_content
        self._name = name
        self._password = password
        self._path = path
        self._user = user

    @property
    def api_token(self):
        return self._api_token

    @api_token.setter
    def api_token(self, api_token):
        self._api_token = api_token

    @property
    def key_content(self):
        return self._key_content

    @key_content.setter
    def key_content(self, key_content):
        self._key_content = key_content

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def password(self):
        return self._password

    @password.setter
    def password(self, password):
        self._password = password

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user


frecklet_class = HcloudSshKeyAuthorized
