# -*- coding: utf-8 -*-


#
# module path: pycklets.php_lang_installed.PhpLangInstalled
#


from pyckles import AutoPycklet


class PhpLangInstalled(AutoPycklet):
    """Installs PHP on RedHat/CentOS and Debian/Ubuntu servers.

     Currently it's not possible selecting the version of php to install. That will be fixed later.

       Args:
         extra_packages: A list of extra php packages to install.

    """

    FRECKLET_ID = "php-lang-installed"

    def __init__(self, extra_packages=None):

        super(PhpLangInstalled, self).__init__(var_names=["extra_packages"])
        self._extra_packages = extra_packages

    @property
    def extra_packages(self):
        return self._extra_packages

    @extra_packages.setter
    def extra_packages(self, extra_packages):
        self._extra_packages = extra_packages


frecklet_class = PhpLangInstalled
