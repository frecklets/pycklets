# -*- coding: utf-8 -*-


#
# module path: pycklets.ssh_key_is_absent.SshKeyIsAbsent
#


from pyckles import AutoPycklet


class SshKeyIsAbsent(AutoPycklet):
    """Ensures an ssh key is absent for a user.

       Args:
         path_to_key: The path to the ssh key.
         user: The name of the user.

    """

    FRECKLET_ID = "ssh-key-is-absent"

    def __init__(self, path_to_key=None, user=None):

        super(SshKeyIsAbsent, self).__init__(var_names=["path_to_key", "user"])
        self._path_to_key = path_to_key
        self._user = user

    @property
    def path_to_key(self):
        return self._path_to_key

    @path_to_key.setter
    def path_to_key(self, path_to_key):
        self._path_to_key = path_to_key

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user


frecklet_class = SshKeyIsAbsent
