# -*- coding: utf-8 -*-


#
# module path: pycklets.deb_file_installed.DebFileInstalled
#


from pyckles import AutoPycklet


class DebFileInstalled(AutoPycklet):
    """Install a Debian package file.

       Args:
         deb_file: (Absolute) path or url to the deb file.

    """

    FRECKLET_ID = "deb-file-installed"

    def __init__(self, deb_file=None):

        super(DebFileInstalled, self).__init__(var_names=["deb_file"])
        self._deb_file = deb_file

    @property
    def deb_file(self):
        return self._deb_file

    @deb_file.setter
    def deb_file(self, deb_file):
        self._deb_file = deb_file


frecklet_class = DebFileInstalled
