# -*- coding: utf-8 -*-


#
# module path: pycklets.ssh_key_added_to_service.SshKeyAddedToService
#


from pyckles import AutoPycklet


class SshKeyAddedToService(AutoPycklet):
    """Ensure an ssh key exists.

       Args:
         path: The path to the (private) ssh key.
         services: n/a
         user: The name of the user.

    """

    FRECKLET_ID = "ssh-key-added-to-service"

    def __init__(self, path=None, services=None, user=None):

        super(SshKeyAddedToService, self).__init__(
            var_names=["path", "services", "user"]
        )
        self._path = path
        self._services = services
        self._user = user

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path

    @property
    def services(self):
        return self._services

    @services.setter
    def services(self, services):
        self._services = services

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user


frecklet_class = SshKeyAddedToService
