# -*- coding: utf-8 -*-


#
# module path: pycklets.basic_hardening.BasicHardening
#


from pyckles import AutoPycklet


class BasicHardening(AutoPycklet):
    """This frecklet can be used to harden a freshly installed server. It installs and configures the [fail2ban](https://www.fail2ban.org) and [ufw](http://gufw.org/) packages.

       Args:
         fail2ban: Whether to install and enable fail2ban.
         ufw: Whether to install and enable the ufw firewall.
         ufw_open_tcp: A list of tcp ports to open (if ufw enabled).
         ufw_open_udp: A list of udp ports to open (if ufw enabled).

    """

    FRECKLET_ID = "basic-hardening"

    def __init__(self, fail2ban=True, ufw=True, ufw_open_tcp=None, ufw_open_udp=None):

        super(BasicHardening, self).__init__(
            var_names=["fail2ban", "ufw", "ufw_open_tcp", "ufw_open_udp"]
        )
        self._fail2ban = fail2ban
        self._ufw = ufw
        self._ufw_open_tcp = ufw_open_tcp
        self._ufw_open_udp = ufw_open_udp

    @property
    def fail2ban(self):
        return self._fail2ban

    @fail2ban.setter
    def fail2ban(self, fail2ban):
        self._fail2ban = fail2ban

    @property
    def ufw(self):
        return self._ufw

    @ufw.setter
    def ufw(self, ufw):
        self._ufw = ufw

    @property
    def ufw_open_tcp(self):
        return self._ufw_open_tcp

    @ufw_open_tcp.setter
    def ufw_open_tcp(self, ufw_open_tcp):
        self._ufw_open_tcp = ufw_open_tcp

    @property
    def ufw_open_udp(self):
        return self._ufw_open_udp

    @ufw_open_udp.setter
    def ufw_open_udp(self, ufw_open_udp):
        self._ufw_open_udp = ufw_open_udp


frecklet_class = BasicHardening
