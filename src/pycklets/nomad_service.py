# -*- coding: utf-8 -*-


#
# module path: pycklets.nomad_service.NomadService
#


from pyckles import AutoPycklet


class NomadService(AutoPycklet):
    """Download the Consul binary into '/usr/local/bin' and create a
     systemd service unit ('consul') and enable/start it if so specified.

     It is recommended to create the approriate Nomad server/client configuration files beforehand (in '/etc/nomad.d'), but this frecklet also allows for providing basic configuration options if necessary.

       Args:
         arch: The architecture of the host system.
         dest: The (absolute) path to the parent folder of the downloaded executable file.
         enabled: Whether to enable the service.
         nomad_config: The nomad configuration.
         platform: The platform of the host system.
         started: Whether to start the service.
         version: The version of Nomad to install.

    """

    FRECKLET_ID = "nomad-service"

    def __init__(
        self,
        arch=None,
        dest=None,
        enabled=None,
        nomad_config=None,
        platform=None,
        started=None,
        version="0.9.3",
    ):

        super(NomadService, self).__init__(
            var_names=[
                "arch",
                "dest",
                "enabled",
                "nomad_config",
                "platform",
                "started",
                "version",
            ]
        )
        self._arch = arch
        self._dest = dest
        self._enabled = enabled
        self._nomad_config = nomad_config
        self._platform = platform
        self._started = started
        self._version = version

    @property
    def arch(self):
        return self._arch

    @arch.setter
    def arch(self, arch):
        self._arch = arch

    @property
    def dest(self):
        return self._dest

    @dest.setter
    def dest(self, dest):
        self._dest = dest

    @property
    def enabled(self):
        return self._enabled

    @enabled.setter
    def enabled(self, enabled):
        self._enabled = enabled

    @property
    def nomad_config(self):
        return self._nomad_config

    @nomad_config.setter
    def nomad_config(self, nomad_config):
        self._nomad_config = nomad_config

    @property
    def platform(self):
        return self._platform

    @platform.setter
    def platform(self, platform):
        self._platform = platform

    @property
    def started(self):
        return self._started

    @started.setter
    def started(self, started):
        self._started = started

    @property
    def version(self):
        return self._version

    @version.setter
    def version(self, version):
        self._version = version


frecklet_class = NomadService
