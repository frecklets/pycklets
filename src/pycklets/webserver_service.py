# -*- coding: utf-8 -*-


#
# module path: pycklets.webserver_service.WebserverService
#


from pyckles import AutoPycklet


class WebserverService(AutoPycklet):
    """This frecklet helps you set up a webserver.

     Currently the 'nginx' and 'apache' webservers are supported. This does not configure the webserver, except for some
     basic stuff. It does, however, setup dependencies like letsencrypt certificates, htpasswd files, etc.

     More features are planned, like setting up php and configure it to work with the webserver, for example.

     To do webserver configuration, use one of the templig frecklets, like 'apache-vhost-exists'. More of those will be
     coming in the future.

       Args:
         basic_auth_user_file: The file to store htpasswd information.
         basic_auth_users: A dict with username as key, password as value.
         letsencrypt_domains: The domains (for requesting https certificates).
         letsencrypt_email: The email to use when requesting a letsencrypt certificate.
         letsencrypt_staging: Whether to use the letsencrypt staging server (for developing -- production only allows a few requests per day).
         letsencrypt_webroot: The document root, needed for letsencrypt certificate requests.
         listen_port: The port the webserver is listening on by default.
         listen_port_https: The port the webserver is listening on by default (for https).
         php_config: PHP config
         php_fpm_config: PHP fpm config
         php_fpm_group: The group to run the php-fpm daemon.
         php_fpm_user: The user to run the php-fpm daemon.
         php_packages: Platform-dependent php-package names.
         restart_webserver: Whether to restart the webserver afterwards.
         skip_cert_request: Whether to skip letsencrypt certificate request.
         use_basic_auth: Whether to setup basic auth.
         use_https: Whether to use https and request a letsencrypt certificate.
         use_php: Whether to install & enable PHP (fpm).
         webserver: The webserver to install, either 'nginx' (default) or 'apache'.
         webserver_apache_config: Extra, Apache-specific vars.
         webserver_group: The group to run the webserver as (if applicable).
         webserver_nginx_config: Extra, Nginx-specific vars.
         webserver_use_pagespeed: Whether to install the pagespeed module.
         webserver_user: The user to run the webserver as.

    """

    FRECKLET_ID = "webserver-service"

    def __init__(
        self,
        basic_auth_user_file="/etc/htpasswd",
        basic_auth_users=None,
        letsencrypt_domains=None,
        letsencrypt_email=None,
        letsencrypt_staging=None,
        letsencrypt_webroot=None,
        listen_port=80,
        listen_port_https=443,
        php_config=None,
        php_fpm_config=None,
        php_fpm_group=None,
        php_fpm_user=None,
        php_packages=None,
        restart_webserver=None,
        skip_cert_request=None,
        use_basic_auth=None,
        use_https=None,
        use_php=None,
        webserver="nginx",
        webserver_apache_config=None,
        webserver_group=None,
        webserver_nginx_config=None,
        webserver_use_pagespeed=None,
        webserver_user=None,
    ):

        super(WebserverService, self).__init__(
            var_names=[
                "basic_auth_user_file",
                "basic_auth_users",
                "letsencrypt_domains",
                "letsencrypt_email",
                "letsencrypt_staging",
                "letsencrypt_webroot",
                "listen_port",
                "listen_port_https",
                "php_config",
                "php_fpm_config",
                "php_fpm_group",
                "php_fpm_user",
                "php_packages",
                "restart_webserver",
                "skip_cert_request",
                "use_basic_auth",
                "use_https",
                "use_php",
                "webserver",
                "webserver_apache_config",
                "webserver_group",
                "webserver_nginx_config",
                "webserver_use_pagespeed",
                "webserver_user",
            ]
        )
        self._basic_auth_user_file = basic_auth_user_file
        self._basic_auth_users = basic_auth_users
        self._letsencrypt_domains = letsencrypt_domains
        self._letsencrypt_email = letsencrypt_email
        self._letsencrypt_staging = letsencrypt_staging
        self._letsencrypt_webroot = letsencrypt_webroot
        self._listen_port = listen_port
        self._listen_port_https = listen_port_https
        self._php_config = php_config
        self._php_fpm_config = php_fpm_config
        self._php_fpm_group = php_fpm_group
        self._php_fpm_user = php_fpm_user
        self._php_packages = php_packages
        self._restart_webserver = restart_webserver
        self._skip_cert_request = skip_cert_request
        self._use_basic_auth = use_basic_auth
        self._use_https = use_https
        self._use_php = use_php
        self._webserver = webserver
        self._webserver_apache_config = webserver_apache_config
        self._webserver_group = webserver_group
        self._webserver_nginx_config = webserver_nginx_config
        self._webserver_use_pagespeed = webserver_use_pagespeed
        self._webserver_user = webserver_user

    @property
    def basic_auth_user_file(self):
        return self._basic_auth_user_file

    @basic_auth_user_file.setter
    def basic_auth_user_file(self, basic_auth_user_file):
        self._basic_auth_user_file = basic_auth_user_file

    @property
    def basic_auth_users(self):
        return self._basic_auth_users

    @basic_auth_users.setter
    def basic_auth_users(self, basic_auth_users):
        self._basic_auth_users = basic_auth_users

    @property
    def letsencrypt_domains(self):
        return self._letsencrypt_domains

    @letsencrypt_domains.setter
    def letsencrypt_domains(self, letsencrypt_domains):
        self._letsencrypt_domains = letsencrypt_domains

    @property
    def letsencrypt_email(self):
        return self._letsencrypt_email

    @letsencrypt_email.setter
    def letsencrypt_email(self, letsencrypt_email):
        self._letsencrypt_email = letsencrypt_email

    @property
    def letsencrypt_staging(self):
        return self._letsencrypt_staging

    @letsencrypt_staging.setter
    def letsencrypt_staging(self, letsencrypt_staging):
        self._letsencrypt_staging = letsencrypt_staging

    @property
    def letsencrypt_webroot(self):
        return self._letsencrypt_webroot

    @letsencrypt_webroot.setter
    def letsencrypt_webroot(self, letsencrypt_webroot):
        self._letsencrypt_webroot = letsencrypt_webroot

    @property
    def listen_port(self):
        return self._listen_port

    @listen_port.setter
    def listen_port(self, listen_port):
        self._listen_port = listen_port

    @property
    def listen_port_https(self):
        return self._listen_port_https

    @listen_port_https.setter
    def listen_port_https(self, listen_port_https):
        self._listen_port_https = listen_port_https

    @property
    def php_config(self):
        return self._php_config

    @php_config.setter
    def php_config(self, php_config):
        self._php_config = php_config

    @property
    def php_fpm_config(self):
        return self._php_fpm_config

    @php_fpm_config.setter
    def php_fpm_config(self, php_fpm_config):
        self._php_fpm_config = php_fpm_config

    @property
    def php_fpm_group(self):
        return self._php_fpm_group

    @php_fpm_group.setter
    def php_fpm_group(self, php_fpm_group):
        self._php_fpm_group = php_fpm_group

    @property
    def php_fpm_user(self):
        return self._php_fpm_user

    @php_fpm_user.setter
    def php_fpm_user(self, php_fpm_user):
        self._php_fpm_user = php_fpm_user

    @property
    def php_packages(self):
        return self._php_packages

    @php_packages.setter
    def php_packages(self, php_packages):
        self._php_packages = php_packages

    @property
    def restart_webserver(self):
        return self._restart_webserver

    @restart_webserver.setter
    def restart_webserver(self, restart_webserver):
        self._restart_webserver = restart_webserver

    @property
    def skip_cert_request(self):
        return self._skip_cert_request

    @skip_cert_request.setter
    def skip_cert_request(self, skip_cert_request):
        self._skip_cert_request = skip_cert_request

    @property
    def use_basic_auth(self):
        return self._use_basic_auth

    @use_basic_auth.setter
    def use_basic_auth(self, use_basic_auth):
        self._use_basic_auth = use_basic_auth

    @property
    def use_https(self):
        return self._use_https

    @use_https.setter
    def use_https(self, use_https):
        self._use_https = use_https

    @property
    def use_php(self):
        return self._use_php

    @use_php.setter
    def use_php(self, use_php):
        self._use_php = use_php

    @property
    def webserver(self):
        return self._webserver

    @webserver.setter
    def webserver(self, webserver):
        self._webserver = webserver

    @property
    def webserver_apache_config(self):
        return self._webserver_apache_config

    @webserver_apache_config.setter
    def webserver_apache_config(self, webserver_apache_config):
        self._webserver_apache_config = webserver_apache_config

    @property
    def webserver_group(self):
        return self._webserver_group

    @webserver_group.setter
    def webserver_group(self, webserver_group):
        self._webserver_group = webserver_group

    @property
    def webserver_nginx_config(self):
        return self._webserver_nginx_config

    @webserver_nginx_config.setter
    def webserver_nginx_config(self, webserver_nginx_config):
        self._webserver_nginx_config = webserver_nginx_config

    @property
    def webserver_use_pagespeed(self):
        return self._webserver_use_pagespeed

    @webserver_use_pagespeed.setter
    def webserver_use_pagespeed(self, webserver_use_pagespeed):
        self._webserver_use_pagespeed = webserver_use_pagespeed

    @property
    def webserver_user(self):
        return self._webserver_user

    @webserver_user.setter
    def webserver_user(self, webserver_user):
        self._webserver_user = webserver_user


frecklet_class = WebserverService
