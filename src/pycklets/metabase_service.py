# -*- coding: utf-8 -*-


#
# module path: pycklets.metabase_service.MetabaseService
#


from pyckles import AutoPycklet


class MetabaseService(AutoPycklet):
    """This only installs the application and systemd unit, it does not configure the service in any way.
     If you want everything installed ready to go, use the 'metabase-standalone' frecklet.

       Args:
         metabase_db_file: The path to the database file, in case of h2.
         metabase_db_host: The database host.
         metabase_db_name: The name of the database.
         metabase_db_password: The database password.
         metabase_db_port: The database port.
         metabase_db_type: Database type to connect to.
         metabase_db_user: The database user.
         metabase_host: The hostname to listen on.
         metabase_port: The port for metabase to listen on.
         metabase_version: The version of metabase.
         password_complexity: Allowed password complexity (weak|normal|strong).
         password_length: Minimal password length.

    """

    FRECKLET_ID = "metabase-service"

    def __init__(
        self,
        metabase_db_file=None,
        metabase_db_host="localhost",
        metabase_db_name="metabase",
        metabase_db_password=None,
        metabase_db_port=None,
        metabase_db_type="h2",
        metabase_db_user="metabase",
        metabase_host="localhost",
        metabase_port=3000,
        metabase_version="0.31.2",
        password_complexity="normal",
        password_length=8,
    ):

        super(MetabaseService, self).__init__(
            var_names=[
                "metabase_db_file",
                "metabase_db_host",
                "metabase_db_name",
                "metabase_db_password",
                "metabase_db_port",
                "metabase_db_type",
                "metabase_db_user",
                "metabase_host",
                "metabase_port",
                "metabase_version",
                "password_complexity",
                "password_length",
            ]
        )
        self._metabase_db_file = metabase_db_file
        self._metabase_db_host = metabase_db_host
        self._metabase_db_name = metabase_db_name
        self._metabase_db_password = metabase_db_password
        self._metabase_db_port = metabase_db_port
        self._metabase_db_type = metabase_db_type
        self._metabase_db_user = metabase_db_user
        self._metabase_host = metabase_host
        self._metabase_port = metabase_port
        self._metabase_version = metabase_version
        self._password_complexity = password_complexity
        self._password_length = password_length

    @property
    def metabase_db_file(self):
        return self._metabase_db_file

    @metabase_db_file.setter
    def metabase_db_file(self, metabase_db_file):
        self._metabase_db_file = metabase_db_file

    @property
    def metabase_db_host(self):
        return self._metabase_db_host

    @metabase_db_host.setter
    def metabase_db_host(self, metabase_db_host):
        self._metabase_db_host = metabase_db_host

    @property
    def metabase_db_name(self):
        return self._metabase_db_name

    @metabase_db_name.setter
    def metabase_db_name(self, metabase_db_name):
        self._metabase_db_name = metabase_db_name

    @property
    def metabase_db_password(self):
        return self._metabase_db_password

    @metabase_db_password.setter
    def metabase_db_password(self, metabase_db_password):
        self._metabase_db_password = metabase_db_password

    @property
    def metabase_db_port(self):
        return self._metabase_db_port

    @metabase_db_port.setter
    def metabase_db_port(self, metabase_db_port):
        self._metabase_db_port = metabase_db_port

    @property
    def metabase_db_type(self):
        return self._metabase_db_type

    @metabase_db_type.setter
    def metabase_db_type(self, metabase_db_type):
        self._metabase_db_type = metabase_db_type

    @property
    def metabase_db_user(self):
        return self._metabase_db_user

    @metabase_db_user.setter
    def metabase_db_user(self, metabase_db_user):
        self._metabase_db_user = metabase_db_user

    @property
    def metabase_host(self):
        return self._metabase_host

    @metabase_host.setter
    def metabase_host(self, metabase_host):
        self._metabase_host = metabase_host

    @property
    def metabase_port(self):
        return self._metabase_port

    @metabase_port.setter
    def metabase_port(self, metabase_port):
        self._metabase_port = metabase_port

    @property
    def metabase_version(self):
        return self._metabase_version

    @metabase_version.setter
    def metabase_version(self, metabase_version):
        self._metabase_version = metabase_version

    @property
    def password_complexity(self):
        return self._password_complexity

    @password_complexity.setter
    def password_complexity(self, password_complexity):
        self._password_complexity = password_complexity

    @property
    def password_length(self):
        return self._password_length

    @password_length.setter
    def password_length(self, password_length):
        self._password_length = password_length


frecklet_class = MetabaseService
