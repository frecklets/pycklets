# -*- coding: utf-8 -*-


#
# module path: pycklets.ec2_instance_exists.Ec2InstanceExists
#


from pyckles import AutoPycklet


class Ec2InstanceExists(AutoPycklet):
    """Create an instance on Amazon EC2, if it doesn't exist yet.

     For now, the 'instance_name' variable is required, because otherwise the underlying Ansible module errors out if no tags dict (or an empty one) is provided. Need to investigate how to do this best.

       Args:
         aws_access_key: The AWS access key.
         aws_secret_key: The AWS secret key.
         image_id: An image to use for the instance.
         instance_name: The Name tag for the instance.
         instance_type: Instance type to use for the instance.
         key_name: Name of the SSH access key to assign to the instance - must exist in the region the instance is created.
         region: The AWS region to use.
         register_var: The name of the variable to register the result of this frecklet.
         security_groups: A list of security group IDs or names (strings).
         tags: A hash/dictionary of tags to add to the new instance or to add/remove from an existing one.
         wait: Wait for the instance to be created.
         wait_timeout: How long to wait (in seconds) for the instance to finish booting/terminating.

    """

    FRECKLET_ID = "ec2-instance-exists"

    def __init__(
        self,
        aws_access_key=None,
        aws_secret_key=None,
        image_id="ami-08d658f84a6d84a80",
        instance_name=None,
        instance_type="t2.micro",
        key_name=None,
        region=None,
        register_var="ec2_instance",
        security_groups=None,
        tags=None,
        wait=True,
        wait_timeout=600,
    ):

        super(Ec2InstanceExists, self).__init__(
            var_names=[
                "aws_access_key",
                "aws_secret_key",
                "image_id",
                "instance_name",
                "instance_type",
                "key_name",
                "region",
                "register_var",
                "security_groups",
                "tags",
                "wait",
                "wait_timeout",
            ]
        )
        self._aws_access_key = aws_access_key
        self._aws_secret_key = aws_secret_key
        self._image_id = image_id
        self._instance_name = instance_name
        self._instance_type = instance_type
        self._key_name = key_name
        self._region = region
        self._register_var = register_var
        self._security_groups = security_groups
        self._tags = tags
        self._wait = wait
        self._wait_timeout = wait_timeout

    @property
    def aws_access_key(self):
        return self._aws_access_key

    @aws_access_key.setter
    def aws_access_key(self, aws_access_key):
        self._aws_access_key = aws_access_key

    @property
    def aws_secret_key(self):
        return self._aws_secret_key

    @aws_secret_key.setter
    def aws_secret_key(self, aws_secret_key):
        self._aws_secret_key = aws_secret_key

    @property
    def image_id(self):
        return self._image_id

    @image_id.setter
    def image_id(self, image_id):
        self._image_id = image_id

    @property
    def instance_name(self):
        return self._instance_name

    @instance_name.setter
    def instance_name(self, instance_name):
        self._instance_name = instance_name

    @property
    def instance_type(self):
        return self._instance_type

    @instance_type.setter
    def instance_type(self, instance_type):
        self._instance_type = instance_type

    @property
    def key_name(self):
        return self._key_name

    @key_name.setter
    def key_name(self, key_name):
        self._key_name = key_name

    @property
    def region(self):
        return self._region

    @region.setter
    def region(self, region):
        self._region = region

    @property
    def register_var(self):
        return self._register_var

    @register_var.setter
    def register_var(self, register_var):
        self._register_var = register_var

    @property
    def security_groups(self):
        return self._security_groups

    @security_groups.setter
    def security_groups(self, security_groups):
        self._security_groups = security_groups

    @property
    def tags(self):
        return self._tags

    @tags.setter
    def tags(self, tags):
        self._tags = tags

    @property
    def wait(self):
        return self._wait

    @wait.setter
    def wait(self, wait):
        self._wait = wait

    @property
    def wait_timeout(self):
        return self._wait_timeout

    @wait_timeout.setter
    def wait_timeout(self, wait_timeout):
        self._wait_timeout = wait_timeout


frecklet_class = Ec2InstanceExists
