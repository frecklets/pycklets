# -*- coding: utf-8 -*-


#
# module path: pycklets.locales_generated.LocalesGenerated
#


from pyckles import AutoPycklet


class LocalesGenerated(AutoPycklet):
    """Generate a list of locales on a system.

       Args:
         locales: A list of locales.

    """

    FRECKLET_ID = "locales-generated"

    def __init__(self, locales=None):

        super(LocalesGenerated, self).__init__(var_names=["locales"])
        self._locales = locales

    @property
    def locales(self):
        return self._locales

    @locales.setter
    def locales(self, locales):
        self._locales = locales


frecklet_class = LocalesGenerated
