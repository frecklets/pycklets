# -*- coding: utf-8 -*-


#
# module path: pycklets.archive_extracted.ArchiveExtracted
#


from pyckles import AutoPycklet


class ArchiveExtracted(AutoPycklet):
    """Extracts an archive into a given location. Makes sure the parent folder of this location exists, as well as an optionally specified owner/group of the extracted archive.

     When using this, make sure the required 'unarchive' tools (like 'unzip', 'bzip2', etc) are installed already on the target system.

       Args:
         creates: If the specified absolute path (file or directory) already exists, this step will not be run.
         dest: The remote absolute path where the archive should be unpacked.
         group: The group of the folder, will be created if necessary.
         keep_newer: Do not replace existing files that are newer than files from the archive.
         mode: The permissions of the extracted archive.
         owner: The owner of the folder, will be created if necessary.
         parent_dir_mode: The permissions of the parent directory.
         remote_src: Whether the src file is remote.
         src: The path/url to the archive.

    """

    FRECKLET_ID = "archive-extracted"

    def __init__(
        self,
        creates=None,
        dest=None,
        group=None,
        keep_newer=None,
        mode=None,
        owner=None,
        parent_dir_mode=None,
        remote_src=None,
        src=None,
    ):

        super(ArchiveExtracted, self).__init__(
            var_names=[
                "creates",
                "dest",
                "group",
                "keep_newer",
                "mode",
                "owner",
                "parent_dir_mode",
                "remote_src",
                "src",
            ]
        )
        self._creates = creates
        self._dest = dest
        self._group = group
        self._keep_newer = keep_newer
        self._mode = mode
        self._owner = owner
        self._parent_dir_mode = parent_dir_mode
        self._remote_src = remote_src
        self._src = src

    @property
    def creates(self):
        return self._creates

    @creates.setter
    def creates(self, creates):
        self._creates = creates

    @property
    def dest(self):
        return self._dest

    @dest.setter
    def dest(self, dest):
        self._dest = dest

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, group):
        self._group = group

    @property
    def keep_newer(self):
        return self._keep_newer

    @keep_newer.setter
    def keep_newer(self, keep_newer):
        self._keep_newer = keep_newer

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, mode):
        self._mode = mode

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    @property
    def parent_dir_mode(self):
        return self._parent_dir_mode

    @parent_dir_mode.setter
    def parent_dir_mode(self, parent_dir_mode):
        self._parent_dir_mode = parent_dir_mode

    @property
    def remote_src(self):
        return self._remote_src

    @remote_src.setter
    def remote_src(self, remote_src):
        self._remote_src = remote_src

    @property
    def src(self):
        return self._src

    @src.setter
    def src(self, src):
        self._src = src


frecklet_class = ArchiveExtracted
