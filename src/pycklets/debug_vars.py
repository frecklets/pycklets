# -*- coding: utf-8 -*-


#
# module path: pycklets.debug_vars.DebugVars
#


from pyckles import AutoPycklet


class DebugVars(AutoPycklet):
    """Displays the content of an (internal) Ansible variable.

       Args:
         vars: the Ansible variable name(s) to debug

    """

    FRECKLET_ID = "debug-vars"

    def __init__(self, vars=None):

        super(DebugVars, self).__init__(var_names=["vars"])
        self._vars = vars

    @property
    def vars(self):
        return self._vars

    @vars.setter
    def vars(self, vars):
        self._vars = vars


frecklet_class = DebugVars
