# -*- coding: utf-8 -*-


#
# module path: pycklets.vagrant_installed.VagrantInstalled
#


from pyckles import AutoPycklet


class VagrantInstalled(AutoPycklet):
    """Installs the [Vagrant](https://vagrantup.com) package.

       Args:
         force_update: Install/update Vagrant, even if it is already installed.
         version: The version of Vagrant to install.

    """

    FRECKLET_ID = "vagrant-installed"

    def __init__(self, force_update=None, version="2.2.4"):

        super(VagrantInstalled, self).__init__(var_names=["force_update", "version"])
        self._force_update = force_update
        self._version = version

    @property
    def force_update(self):
        return self._force_update

    @force_update.setter
    def force_update(self, force_update):
        self._force_update = force_update

    @property
    def version(self):
        return self._version

    @version.setter
    def version(self, version):
        self._version = version


frecklet_class = VagrantInstalled
