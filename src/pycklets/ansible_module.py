# -*- coding: utf-8 -*-


#
# module path: pycklets.ansible_module.AnsibleModule
#


from pyckles import AutoPycklet


class AnsibleModule(AutoPycklet):
    """This is a generic task to execute any of the officially supported, [available Ansible modules](https://docs.ansible.com/ansible/latest/modules/list_of_all_modules.html).

     Currently only a few basic metadata keys are supported: ``become`` &``become_user``. The other ones will be added
     shortly.

       Args:
         become: Whether to become another user.
         become_user: The user to become.
         module_vars: The parameters for the module.
         name: The module name.

    """

    FRECKLET_ID = "ansible-module"

    def __init__(self, become=None, become_user=None, module_vars=None, name=None):

        super(AnsibleModule, self).__init__(
            var_names=["become", "become_user", "module_vars", "name"]
        )
        self._become = become
        self._become_user = become_user
        self._module_vars = module_vars
        self._name = name

    @property
    def become(self):
        return self._become

    @become.setter
    def become(self, become):
        self._become = become

    @property
    def become_user(self):
        return self._become_user

    @become_user.setter
    def become_user(self, become_user):
        self._become_user = become_user

    @property
    def module_vars(self):
        return self._module_vars

    @module_vars.setter
    def module_vars(self, module_vars):
        self._module_vars = module_vars

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name


frecklet_class = AnsibleModule
