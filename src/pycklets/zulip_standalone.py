# -*- coding: utf-8 -*-


#
# module path: pycklets.zulip_standalone.ZulipStandalone
#


from pyckles import AutoPycklet


class ZulipStandalone(AutoPycklet):
    """Install a Zulip standalone server.

     This uses the install script recommended by the Zulip project to setup a Zulip service and all dependencies.

     #TODO: configure email settings, use 'expect' instaed of patching the install script for letsencrypt

       Args:
         email: The email of the Zulip administrator.
         hostname: The domain name of the Zulip server.
         version: The version of Zulip.

    """

    FRECKLET_ID = "zulip-standalone"

    def __init__(self, email=None, hostname=None, version="2.0.3"):

        super(ZulipStandalone, self).__init__(
            var_names=["email", "hostname", "version"]
        )
        self._email = email
        self._hostname = hostname
        self._version = version

    @property
    def email(self):
        return self._email

    @email.setter
    def email(self, email):
        self._email = email

    @property
    def hostname(self):
        return self._hostname

    @hostname.setter
    def hostname(self, hostname):
        self._hostname = hostname

    @property
    def version(self):
        return self._version

    @version.setter
    def version(self, version):
        self._version = version


frecklet_class = ZulipStandalone
