# -*- coding: utf-8 -*-


#
# module path: pycklets.freckles_dev_project.FrecklesDevProject
#


from pyckles import AutoPycklet


class FrecklesDevProject(AutoPycklet):
    """Prepare freckles dev project locally.

     Uses [pyenv](https://github.com/pyenv/pyenv) to install Python and create a virtualenv named 'freckles-dev'. Clone/pull all required dependency projects into the project folder, then
     install them into the virtualenv.

     Also create a freckles context called 'freckles-dev', which points to all the development
     frecklets and resources, and sets the 'keep_run_folder' context configuration value to 'true'.
     To be used like:

         frecklecute -c freckles-dev user-exists markus

     Note: you still need to 'unlock' ('freckles context unlock') the context if you haven't done so yet, to be able to change this context.

     Also create a file called '.python-version' in the project base, so that pyenv always loads
     the 'freckles-dev' virtualenv automatically when the user visits this folder.

     Finally, create a frecklet in the '.freckles' sub-folder, called 'update-freckles-dev', which
     lets you pull all relevant git repositories with one command:

         frecklecute -c freckles-dev update-freckles-dev

     Note: the first time you do this might take a while because freckles runtime dependencies will be installed

       Args:
         project_base: The base folder for the project sources (needs to be absolute!).

    """

    FRECKLET_ID = "freckles-dev-project"

    def __init__(self, project_base=None):

        super(FrecklesDevProject, self).__init__(var_names=["project_base"])
        self._project_base = project_base

    @property
    def project_base(self):
        return self._project_base

    @project_base.setter
    def project_base(self, project_base):
        self._project_base = project_base


frecklet_class = FrecklesDevProject
