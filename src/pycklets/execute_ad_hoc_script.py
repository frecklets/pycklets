# -*- coding: utf-8 -*-


#
# module path: pycklets.execute_ad_hoc_script.ExecuteAdHocScript
#


from pyckles import AutoPycklet


class ExecuteAdHocScript(AutoPycklet):
    """Create an executable file from a template string in a temporary location, make the file
     executable, execute it, and finally delete it.

       Args:
         become: Whether to use elevated permissions to execute the script.
         become_user: The user to execute the command as.
         script_template: The content of the script.

    """

    FRECKLET_ID = "execute-ad-hoc-script"

    def __init__(self, become=None, become_user=None, script_template=None):

        super(ExecuteAdHocScript, self).__init__(
            var_names=["become", "become_user", "script_template"]
        )
        self._become = become
        self._become_user = become_user
        self._script_template = script_template

    @property
    def become(self):
        return self._become

    @become.setter
    def become(self, become):
        self._become = become

    @property
    def become_user(self):
        return self._become_user

    @become_user.setter
    def become_user(self, become_user):
        self._become_user = become_user

    @property
    def script_template(self):
        return self._script_template

    @script_template.setter
    def script_template(self, script_template):
        self._script_template = script_template


frecklet_class = ExecuteAdHocScript
