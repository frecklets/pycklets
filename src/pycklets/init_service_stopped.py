# -*- coding: utf-8 -*-


#
# module path: pycklets.init_service_stopped.InitServiceStopped
#


from pyckles import AutoPycklet


class InitServiceStopped(AutoPycklet):
    """Make sure an init service is stopped.

       Args:
         name: The name of the service.

    """

    FRECKLET_ID = "init-service-stopped"

    def __init__(self, name=None):

        super(InitServiceStopped, self).__init__(var_names=["name"])
        self._name = name

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name


frecklet_class = InitServiceStopped
