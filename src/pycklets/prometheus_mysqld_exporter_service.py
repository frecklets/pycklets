# -*- coding: utf-8 -*-


#
# module path: pycklets.prometheus_mysqld_exporter_service.PrometheusMysqldExporterService
#


from pyckles import AutoPycklet


class PrometheusMysqldExporterService(AutoPycklet):
    """This role is not fully functional yet. #TODO

       Args:
         version: The version of the mysqld exporter.
         web_listen_address: Address on which the mysqld exporter will listen.

    """

    FRECKLET_ID = "prometheus-mysqld-exporter-service"

    def __init__(self, version=None, web_listen_address=None):

        super(PrometheusMysqldExporterService, self).__init__(
            var_names=["version", "web_listen_address"]
        )
        self._version = version
        self._web_listen_address = web_listen_address

    @property
    def version(self):
        return self._version

    @version.setter
    def version(self, version):
        self._version = version

    @property
    def web_listen_address(self):
        return self._web_listen_address

    @web_listen_address.setter
    def web_listen_address(self, web_listen_address):
        self._web_listen_address = web_listen_address


frecklet_class = PrometheusMysqldExporterService
