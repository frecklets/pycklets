# -*- coding: utf-8 -*-


#
# module path: pycklets.keycloak_standalone.KeycloakStandalone
#


from pyckles import AutoPycklet


class KeycloakStandalone(AutoPycklet):
    """Install a Keycloak standalone service.

     This follows more or less the [Keycloak documentation](https://www.keycloak.org/docs/latest/getting_started/)
     on how to install Keycloak in standalone mode.

     It also installs Postgresql and the Nginx webserver, including https certificate (if so specified).

     In case no 'keycloak_db_password' is specified, freckles will generate a random one.

       Args:
         admin_email: The email for letsencrypt.
         hostname: The (external) domain name, to be used by the reverse proxy.
         keycloak_admin_password: The initial admin user password.
         keycloak_bind_ip: The ip address keycloak listens on.
         keycloak_bind_ip_management: The ip address the keycloak management interface listens on.
         keycloak_db_name: The database name.
         keycloak_db_password: The postgres database password.
         keycloak_db_user: The database user.
         letsencrypt_staging: Whether to use the letsencrypt staging server (for development).
         version: The version of keycloak.

    """

    FRECKLET_ID = "keycloak-standalone"

    def __init__(
        self,
        admin_email=None,
        hostname=None,
        keycloak_admin_password=None,
        keycloak_bind_ip="127.0.0.1",
        keycloak_bind_ip_management=None,
        keycloak_db_name="keycloak",
        keycloak_db_password=None,
        keycloak_db_user="keycloak",
        letsencrypt_staging=None,
        version="6.0.1",
    ):

        super(KeycloakStandalone, self).__init__(
            var_names=[
                "admin_email",
                "hostname",
                "keycloak_admin_password",
                "keycloak_bind_ip",
                "keycloak_bind_ip_management",
                "keycloak_db_name",
                "keycloak_db_password",
                "keycloak_db_user",
                "letsencrypt_staging",
                "version",
            ]
        )
        self._admin_email = admin_email
        self._hostname = hostname
        self._keycloak_admin_password = keycloak_admin_password
        self._keycloak_bind_ip = keycloak_bind_ip
        self._keycloak_bind_ip_management = keycloak_bind_ip_management
        self._keycloak_db_name = keycloak_db_name
        self._keycloak_db_password = keycloak_db_password
        self._keycloak_db_user = keycloak_db_user
        self._letsencrypt_staging = letsencrypt_staging
        self._version = version

    @property
    def admin_email(self):
        return self._admin_email

    @admin_email.setter
    def admin_email(self, admin_email):
        self._admin_email = admin_email

    @property
    def hostname(self):
        return self._hostname

    @hostname.setter
    def hostname(self, hostname):
        self._hostname = hostname

    @property
    def keycloak_admin_password(self):
        return self._keycloak_admin_password

    @keycloak_admin_password.setter
    def keycloak_admin_password(self, keycloak_admin_password):
        self._keycloak_admin_password = keycloak_admin_password

    @property
    def keycloak_bind_ip(self):
        return self._keycloak_bind_ip

    @keycloak_bind_ip.setter
    def keycloak_bind_ip(self, keycloak_bind_ip):
        self._keycloak_bind_ip = keycloak_bind_ip

    @property
    def keycloak_bind_ip_management(self):
        return self._keycloak_bind_ip_management

    @keycloak_bind_ip_management.setter
    def keycloak_bind_ip_management(self, keycloak_bind_ip_management):
        self._keycloak_bind_ip_management = keycloak_bind_ip_management

    @property
    def keycloak_db_name(self):
        return self._keycloak_db_name

    @keycloak_db_name.setter
    def keycloak_db_name(self, keycloak_db_name):
        self._keycloak_db_name = keycloak_db_name

    @property
    def keycloak_db_password(self):
        return self._keycloak_db_password

    @keycloak_db_password.setter
    def keycloak_db_password(self, keycloak_db_password):
        self._keycloak_db_password = keycloak_db_password

    @property
    def keycloak_db_user(self):
        return self._keycloak_db_user

    @keycloak_db_user.setter
    def keycloak_db_user(self, keycloak_db_user):
        self._keycloak_db_user = keycloak_db_user

    @property
    def letsencrypt_staging(self):
        return self._letsencrypt_staging

    @letsencrypt_staging.setter
    def letsencrypt_staging(self, letsencrypt_staging):
        self._letsencrypt_staging = letsencrypt_staging

    @property
    def version(self):
        return self._version

    @version.setter
    def version(self, version):
        self._version = version


frecklet_class = KeycloakStandalone
