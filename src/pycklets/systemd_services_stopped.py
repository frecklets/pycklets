# -*- coding: utf-8 -*-


#
# module path: pycklets.systemd_services_stopped.SystemdServicesStopped
#


from pyckles import AutoPycklet


class SystemdServicesStopped(AutoPycklet):
    """This frecklet makes sure a list of sytemd services are stopped.

     Non-existing services will be ignored.

       Args:
         services: A list of services.

    """

    FRECKLET_ID = "systemd-services-stopped"

    def __init__(self, services=None):

        super(SystemdServicesStopped, self).__init__(var_names=["services"])
        self._services = services

    @property
    def services(self):
        return self._services

    @services.setter
    def services(self, services):
        self._services = services


frecklet_class = SystemdServicesStopped
