# -*- coding: utf-8 -*-


#
# module path: pycklets.freckles_installed.FrecklesInstalled
#


from pyckles import AutoPycklet


class FrecklesInstalled(AutoPycklet):
    """Internally, this uses the 'freck' bash script hosted on 'https://freckles.sh' to install frecklets.

       Args:
         init: Whether to run a dummy freckles job (to initialize dependencies).
         update: Whether to update 'freckles' if it is already installed.
         user: The user to install 'freckles' for.
         version: Which version of 'freckles' to install.

    """

    FRECKLET_ID = "freckles-installed"

    def __init__(self, init=None, update=None, user=None, version="stable"):

        super(FrecklesInstalled, self).__init__(
            var_names=["init", "update", "user", "version"]
        )
        self._init = init
        self._update = update
        self._user = user
        self._version = version

    @property
    def init(self):
        return self._init

    @init.setter
    def init(self, init):
        self._init = init

    @property
    def update(self):
        return self._update

    @update.setter
    def update(self, update):
        self._update = update

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user

    @property
    def version(self):
        return self._version

    @version.setter
    def version(self, version):
        self._version = version


frecklet_class = FrecklesInstalled
