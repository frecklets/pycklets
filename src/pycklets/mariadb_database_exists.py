# -*- coding: utf-8 -*-


#
# module path: pycklets.mariadb_database_exists.MariadbDatabaseExists
#


from pyckles import AutoPycklet


class MariadbDatabaseExists(AutoPycklet):
    """Installs MariaDB service on a host, and ensures a database with the provided name is present.

     If a database dump file is provided, the database will be imported from it.
     Otherwise an empty table will be created.

     If a database dump file is provided, the database name can also be 'all',
     in which case all databases contained in it will be imported.

     If a username is supplied, but no ``db_user_priv``, the user 'db_name.*:ALL' privileges granted.

       Args:
         db_dump_file: An (optional) database dump file.
         db_host: The 'host' part of the MySQL username.
         db_import: Whether to use a sql dump file to create the database.
         db_name: The name of the database to use from the dump (or 'all').
         db_user: The name of the database user.
         db_user_password: The password for the database user.
         db_user_priv: The user privileges.
         mysql_group_id: An (optional) custom mysql group id
         mysql_user_id: An (optional) custom mysql user id

    """

    FRECKLET_ID = "mariadb-database-exists"

    def __init__(
        self,
        db_dump_file=None,
        db_host=None,
        db_import=None,
        db_name=None,
        db_user=None,
        db_user_password=None,
        db_user_priv=None,
        mysql_group_id=None,
        mysql_user_id=None,
    ):

        super(MariadbDatabaseExists, self).__init__(
            var_names=[
                "db_dump_file",
                "db_host",
                "db_import",
                "db_name",
                "db_user",
                "db_user_password",
                "db_user_priv",
                "mysql_group_id",
                "mysql_user_id",
            ]
        )
        self._db_dump_file = db_dump_file
        self._db_host = db_host
        self._db_import = db_import
        self._db_name = db_name
        self._db_user = db_user
        self._db_user_password = db_user_password
        self._db_user_priv = db_user_priv
        self._mysql_group_id = mysql_group_id
        self._mysql_user_id = mysql_user_id

    @property
    def db_dump_file(self):
        return self._db_dump_file

    @db_dump_file.setter
    def db_dump_file(self, db_dump_file):
        self._db_dump_file = db_dump_file

    @property
    def db_host(self):
        return self._db_host

    @db_host.setter
    def db_host(self, db_host):
        self._db_host = db_host

    @property
    def db_import(self):
        return self._db_import

    @db_import.setter
    def db_import(self, db_import):
        self._db_import = db_import

    @property
    def db_name(self):
        return self._db_name

    @db_name.setter
    def db_name(self, db_name):
        self._db_name = db_name

    @property
    def db_user(self):
        return self._db_user

    @db_user.setter
    def db_user(self, db_user):
        self._db_user = db_user

    @property
    def db_user_password(self):
        return self._db_user_password

    @db_user_password.setter
    def db_user_password(self, db_user_password):
        self._db_user_password = db_user_password

    @property
    def db_user_priv(self):
        return self._db_user_priv

    @db_user_priv.setter
    def db_user_priv(self, db_user_priv):
        self._db_user_priv = db_user_priv

    @property
    def mysql_group_id(self):
        return self._mysql_group_id

    @mysql_group_id.setter
    def mysql_group_id(self, mysql_group_id):
        self._mysql_group_id = mysql_group_id

    @property
    def mysql_user_id(self):
        return self._mysql_user_id

    @mysql_user_id.setter
    def mysql_user_id(self, mysql_user_id):
        self._mysql_user_id = mysql_user_id


frecklet_class = MariadbDatabaseExists
