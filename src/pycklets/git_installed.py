# -*- coding: utf-8 -*-


#
# module path: pycklets.git_installed.GitInstalled
#


from pyckles import AutoPycklet


class GitInstalled(AutoPycklet):
    """Ensures git is installed if not available.

     Currently this only uses the system package manager, which means root permissions are required. In the future also
     (at least) a conda install will be supported.

       Args:

    """

    FRECKLET_ID = "git-installed"

    def __init__(self,):

        super(GitInstalled, self).__init__(var_names=[])


frecklet_class = GitInstalled
