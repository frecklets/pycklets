# -*- coding: utf-8 -*-


#
# module path: pycklets.ssh_key_no_password_exists.SshKeyNoPasswordExists
#


from pyckles import AutoPycklet


class SshKeyNoPasswordExists(AutoPycklet):
    """Ensures a password-less ssh key exists for a user.

     Creates one with empty password if necessary.

     TODO: suppor an (optional) password

       Args:
         path: The path to the ssh key.
         user: The name of the user.

    """

    FRECKLET_ID = "ssh-key-no-password-exists"

    def __init__(self, path=None, user=None):

        super(SshKeyNoPasswordExists, self).__init__(var_names=["path", "user"])
        self._path = path
        self._user = user

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user


frecklet_class = SshKeyNoPasswordExists
