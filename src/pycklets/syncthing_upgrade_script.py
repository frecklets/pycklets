# -*- coding: utf-8 -*-


#
# module path: pycklets.syncthing_upgrade_script.SyncthingUpgradeScript
#


from pyckles import AutoPycklet


class SyncthingUpgradeScript(AutoPycklet):
    """Create a shell script that tries the 'syncthing -upgrade' command, and if successful, restarts all running
     syncthing service units.

       Args:
         add_cron_job: Whether to add a cron job (default) or not.

    """

    FRECKLET_ID = "syncthing-upgrade-script"

    def __init__(self, add_cron_job=True):

        super(SyncthingUpgradeScript, self).__init__(var_names=["add_cron_job"])
        self._add_cron_job = add_cron_job

    @property
    def add_cron_job(self):
        return self._add_cron_job

    @add_cron_job.setter
    def add_cron_job(self, add_cron_job):
        self._add_cron_job = add_cron_job


frecklet_class = SyncthingUpgradeScript
