# -*- coding: utf-8 -*-


#
# module path: pycklets.sleep.Sleep
#


from pyckles import AutoPycklet


class Sleep(AutoPycklet):
    """In certain situations you need to wait a bit in order for external circumstances to catch up. Ideally you'll
     have a different, more deterministic way of doing so (e.g. the 'wait-for-ssh' frecklet), but in some cases you might have to fall back on a simple 'sleep'.

       Args:
         time: The amount of seconds to wait.

    """

    FRECKLET_ID = "sleep"

    def __init__(self, time=None):

        super(Sleep, self).__init__(var_names=["time"])
        self._time = time

    @property
    def time(self):
        return self._time

    @time.setter
    def time(self, time):
        self._time = time


frecklet_class = Sleep
