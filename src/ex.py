from pyckles.contexts import debug_context as ctx
from pycklets import StaticWebsiteFromFolder, FileWithContent

host = "example.com"

stat = StaticWebsiteFromFolder()
stat.hostname = host
stat.path = "/var/www/html"
stat.webserver = "apache"
stat.use_https = True
stat.server_admin = "hello@example.com"

fc = FileWithContent()
fc.path = "/var/www/html/index.html"
fc.content = f"<h1><i>{host}</i> says 'hello', World!</h1>"

ctx.run([stat, fc], run_config=f"admin@{host}")
