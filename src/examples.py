# -*- coding: utf-8 -*-

"""Examples of how to use libraries containing Pycklets.

If you generated this library from your own frecklets, the 'RegisterVar' class might not be available. Replace that
with one of your own, for those examples to work.

The example frecklet(s) used here are selected because they are read-only, and don't alter anything on the machine
the examples run on.

"""

# for poduction use, import the 'default_context' instead, or create your own
# from pycklets import default_context as pyckles_context

# this might not work if you auto-generated this from your own frecklets which don't include the default frecklet repo
from pyckles import create_pyckles_context
from pycklets.register_var import RegisterVar
from pycklets.ansible_role import AnsibleRole
from pycklets.ansible_module import AnsibleModule


pyckles_context = create_pyckles_context(pycklets_package_names="pycklets", debug=True)

def register_var_localhost(register_name):
    """
    Call the 'register-var' ( https://freckles.io/frecklets/default/freckles/register-var ) frecklet.

    We use the provided 'default_context' that comes with every auto-generated pyckles source code. This context
    has a callback configured to print out all steps of the underlying freckles run. For production use, use the 'default_context' instead,
    or, even better, create your own.

    Args:
        register_name: the key to register the result under

    Returns:
        dict: a dict with all Ansible facts stored under the 'register_name' key

    """

    rv = RegisterVar()
    rv.register_name = register_name
    rv.var_name = "ansible_facts"

    result = pyckles_context.run(rv)
    return result


def register_var_remote_host(register_name, target):
    """
    Run the 'register-var' frecklet on a remote host.

    Pretty much the same as the 'register_var_localhost' example, only this time we run the frecklet on a remote host.
    For this to work, we need to have an ssh key (and ssh-agent, so we don't need to unlock the key with a password)
    available on the machine where this runs on, and the remote host configured so that our ssh key can login as the user
    we specify.

    Args:
        register_name: the key to register the result under
        target: the target host to run on, in the form: 'user@domain.tld'

    Returns:
        dict: a dict with all Ansible facts of the remote host, stored under the 'register_name' key

    """

    rv = RegisterVar()
    rv.register_name = register_name
    rv.var_name = "ansible_facts"

    result = pyckles_context.run(rv, run_config=target)
    return result


def register_var_custom_context(register_name):
    """
    Run the 'register-var' frecklet using a custom context.

    In this example we configure the 'plain' output callback (which prints very basic information about the tasks that
    are run), as well as a custom folder location for the run (instead of the default '~/.local/share/freckles/runs').
    We also specify the run folder should not be deleted ('keep_run_folder') (maybe for debug purposes), and we don't
    want a symlink to the run-folder (default: ~/.local/share/freckles/runs/current).

    Check the freckles configuration documentation ( https://freckles.io/doc/configuration ) for more details about
    possible settings etc.

    Args:
        register_name: the key to register the result under

    Returns:

    """

    rv = RegisterVar()
    rv.register_name = register_name
    rv.var_name = "ansible_facts"

    context_config = {
        "callback": ["plain"],
        "keep_run_folder": True,
        "run_folder": "/tmp/my_run",
        "current_run_folder": "",
    }

    context = create_pyckles_context("pycklets", config=context_config, context_type="python")

    result = context.run(rv)
    return result


def install_nodenv():
    """
    Run an arbitrary role from Ansible galaxy ( https://galaxy.ansible.com ).

    In this example we use the 'bluk.nodenv' ( https://github.com/bluk/ansible-role-nodenv ) role, but you
    can really use any role you like. For production use, I'd recommend wrapping the role in a frecklet though,
    as that allows you to specify the role variables, add documentation, etc. Check out the 'java-lang-installed'
    frecklet as an example, there are also plenty more in the default & community frecklet repositories to use
    as templates.

    In adition to using the 'ansible-role' frecklet, we also create our own context for this example, because
    we need to set the 'allow_remote' config value to 'True' (the default freckles configuration does not allow
    any remote resources).

    """

    ar = AnsibleRole()
    ar.name = "bluk.nodenv"
    # check https://github.com/bluk/ansible-role-nodenv for possible vars and other details about this role
    ar.role_vars = {"nodenv_install_path": "/tmp/nodenv"}

    context_config = {
        "callback": [{"default": {"profile": "full"}}],
        "keep_run_folder": True,
        "allow_remote": True,
    }

    context = create_pyckles_context("pycklets", config=context_config)

    context.run(ar)


def lineinfile():
    """
    Run an arbitrary Ansible module ( https://docs.ansible.com/ansible/latest/modules/list_of_all_modules.html ).

    Similar to the 'install_nodenv' example, we can run any Ansible module we want. Ideally, the module is wrapped in
    a frecklet, but it's not necessary.

    Here we use the 'lineinfile' ( https://docs.ansible.com/ansible/latest/modules/lineinfile_module.html#lineinfile-module )
    module, which allows us to make sure a certain line is present in a file.
    """

    am = AnsibleModule()
    am.name = "lineinfile"
    # check https://docs.ansible.com/ansible/latest/modules/lineinfile_module.html#lineinfile-module for possible vars and other details about this role
    am.module_vars = {
        "path": "/tmp/selinux_config",
        "regexp": "^SELINUX=",
        "line": "SELINUX=enforcing",
        "create": True,
    }

    pyckles_context.run(am)


if __name__ == "__main__":

    # result = register_var_localhost("machine_facts")
    # result = register_var_remote_host("machine_facts", "freckles@dev.cutecode.co")
    result = register_var_custom_context("machine_facts")


    for k, v in result.result["machine_facts"].items():
        print("----")
        print(k + ":\n")
        print(v)
        print()

    # other examples:
    # install_nodenv()
    # lineinfile()
